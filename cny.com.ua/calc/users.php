<?php
/** модель работы с пользователями **/
  require_once("db_connect.php");
// global $db;
function make_user($name,$chat_id){
	global $db;
	$name = mysqli_real_escape_string($db,$name);
	$chat_id = $chat_id;
	$query = "insert into `users`(name,chat_id) values('".$name."','".$chat_id."')";
	return mysqli_query($db,$query) or die("пользователя создать не удалось");
}

function is_user_set($name){
	global $db;
	$name = mysqli_real_escape_string($db,$name);
	$result = mysqli_query($db,"select * from `users` where name='".$name."' LIMIT 1");

    if(mysqli_fetch_array($result) !== NULL) return true;
    return false;
}

// задание настройки
function set_udata($name,$data = array()){
	global $db;
	$name = mysqli_real_escape_string($db,$name);
	if(!is_user_set($name)){
		make_user($name,0); // если каким-то чудом этот пользователь не зарегистрирован в базе
	}
	$data = json_encode($data,JSON_UNESCAPED_UNICODE);
	return mysqli_query($db,"update `users` SET data_json = '".$data."' WHERE name = '".$name."'"); // обновляем запись в базе
}

function add_udata($name,$chat_id,$data = array()){
	global $db;
	$name = mysqli_real_escape_string($db,$name);
	if(!is_user_set($name)){
		make_user($name,0); // если каким-то чудом этот пользователь не зарегистрирован в базе
	}
	$data = json_encode($data,JSON_UNESCAPED_UNICODE);
	return mysqli_query($db,"insert into `users`(name,chat_id,data_json) values('".$name."','".$chat_id."','".$data."')"); // обновляем запись в базе
}

function get_prev_numb_udata($name){
	global $db;
	$res = array();
	$name = mysqli_real_escape_string($db,$name);
	$result = mysqli_query($db,"select * from `users` where name='".$name."'  ORDER BY ID DESC LIMIT 2");
	while ($row = mysqli_fetch_assoc($result)) {
		 if(isset($row['data_json'])){
			$arr[] = json_decode($row['data_json'], true);
		} 
	}
	
	$prev_number=$arr['1'];
	/* $arr = mysqli_fetch_assoc($result);
    if(isset($arr['data_json'])){
		$res = json_decode($arr['data_json'], true);
	} */
	
	return $prev_number;
}

// считываение настройки
function get_udata($name){
	global $db;
	$res = array();
	$name = mysqli_real_escape_string($db,$name);
	$result = mysqli_query($db,"select * from `users` where name='".$name."'  ORDER BY ID DESC");
	$arr = mysqli_fetch_assoc($result);
    if(isset($arr['data_json'])){
		$res = json_decode($arr['data_json'], true);
	}
	
	return $res;
}

// считываение настройки
function get_users_udata($names){
	global $db;
	$res = array();
	//$name = mysqli_real_escape_string($db,$name);
	$result = mysqli_query($db,"select * from `users` where name IN ".$names." ORDER BY ID DESC ");
	$arr = mysqli_fetch_assoc($result);
    if(isset($arr['data_json'])){
		$res = json_decode($arr['data_json'], true);
		$res['from_user']=$arr['name'];
	}
	print_r($result);
	return $res;
}
//$usenames="('Svetashhhhhh','SuperAgeeva')";
print_r(get_users_udata($usenamess));
