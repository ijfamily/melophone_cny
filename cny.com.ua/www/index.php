<?php header("Cache-Control: no-store, no-cache, must-revalidate");?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<!-- TITLE OF SITE -->
	<title>Сервис мгновенных переводов в Китай | Денежные Переводы</title>

	<!-- META DATA -->
	<meta name="description" content="Денежные переводы в Китай из Украины. RMB CNY - перевод и отправка из Киева Харькова Одессы" />
	<meta name="keywords" content="CNY, RMB, денежный перевод, отправка денег в Китай" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Cache-Control" content="no-cache">
	
<meta http-equiv="pragma" content="no-cache"/>

	<!-- =========================
	FAV AND TOUCH ICONS  
	============================== -->
	<link rel="icon" href="images/favicon.ico">
	 
	<!-- =========================
	STYLESHEETS 
	============================== -->
	<!-- BOOTSTRAP -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	
	<!-- FOR EXTERNAL PLUGINS -->
	<link rel="stylesheet" href="css/swiper.min.css"> <!-- Screenshot Slider -->
	<link rel="stylesheet" href="css/themes/default/default.css"> <!-- Nivo Lightbox -->
	<link rel="stylesheet" href="css/nivo-lightbox.css"> <!-- Nivo Lightbox Theme -->

	<!-- TYPOGRAPHY -->
	<link rel="stylesheet" href="css/typography/typography-1.css">

	<!-- MAIN -->
	<link rel="stylesheet" href="css/style.css?555">

	<!-- TEMPLATE COLORS -->
	<link rel="stylesheet" href="css/colors/blue.css">


	<!-- RESPONSIVE FIXES -->
	<!-- <link rel="stylesheet" href="css/responsive.css"> -->

	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159556639-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-159556639-1');
</script> 

<script src="https://www.google.com/recaptcha/api.js?render=6Lc5j_UUAAAAAC8vx9DMznUaaK9bZMehYUAkZkN4"></script>
<script>
    grecaptcha.ready(function() {
    // do request for recaptcha token
    // response is promise with passed token
        grecaptcha.execute('6Lc5j_UUAAAAAC8vx9DMznUaaK9bZMehYUAkZkN4', {action:'validate_captcha'})
                  .then(function(token) {
            // add token value to form
            document.getElementById('g-recaptcha-response').value = token;
        });
    });
</script>
</head>
<body class="body-container">
<!-- =========================
     NAVBAR 
============================== -->
<div class="navbar navbar-nemo navbar-transparent navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nemo-navigation" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar icon-bar-top"></span>
				<span class="icon-bar icon-bar-middle"></span>
				<span class="icon-bar icon-bar-bottom"></span>
			</button>

			<div class="head-contacts">
				<a href="tel:+380975716869" class="h-phone"><img src="/icons/call.svg" width="20">&nbsp;+38&nbsp;(097)&nbsp;571-68-69</a><br>
				<a href="tg://resolve?domain=cnyua" class="h-telega"><img src="/icons/phone.svg" width="20">&nbsp;Telegram @cnyua</a>
			</div>

			<!-- YOUR LOGO -->
			<a class="navbar-brand" href="#home">
				<img src="images/logo.svg" alt="" class="hidden-xs" >
				
				<img src="images/logo-alt.svg" alt="" class="visible-xs">
			</a>
		</div>
		
		<div class="collapse navbar-collapse" id="nemo-navigation">
			<ul class="nav navbar-nav navbar-right">
			
				<li><a href="#work">Процесс оплаты</a></li>
				<li><a href="#full-form">Перевести деньги</a></li>
				<li><a href="#join-us">Вывод юаней</a></li>
				<li><a href="#contacts">Контакты</a></li>
			</ul>
		</div>
	</div>
</div>




<!-- =========================
     ELEARNING HOME
============================== -->
<section class="home section-welcome cover-bg" id="home">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">

				<div class="heading-text">Первый помощник для успешного бизнеса с Китаем!</div>
				<h1 class="sub-heading mb40">Сервис мгновенных переводов в Китай CNY.COM.UA</h1>

				<div class="home-calc-wrap">	
					<div class="home-calc-text-kurs mb20" id="home-calc-text-kurs">
						
					
					</div>
					<form class="home-calc-form" id="home-calc-form">
						
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="calc-form-wrap">
									<label class="calc-form-wrap-label"  for="home_send-summ">Отправляете</label>
									<input class="form-control calc-form-wrap-input-send" name="send_summ" id="home_send-summ" placeholder="0,00" inputmode="numeric" type="number" min="0" value="" required onchange="">
									<div class="input-send_currency">
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-send-input" name="send_currency" value="uah" id="radio-ua" checked>
											<label class="styled-currency-radio-label"  title="Украинская гривна" for="radio-ua"><img src="images/ua.png" title="Гривна">&nbsp;UAH</label>
										</div> 
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-send-input" name="send_currency" value="usd" id="radio-usd" >
											<label class="styled-currency-radio-label"  title="Доллар США" for="radio-usd"><img src="images/usd.png">&nbsp;USD</label>
										</div> 
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-send-input" name="send_currency" value="cny" id="radio-cny" >
											<label class="styled-currency-radio-label"  title="Доллар США" for="radio-cny"><img src="images/cny.png">&nbsp;CNY</label>
										</div> 
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="calc-form-wrap home-send-get-wrap">
									<label class="calc-form-wrap-label" for="home_send-summ">Получаете</label>
									<input class="form-control calc-form-wrap-input-get" name="send_get" id="home_send-get" placeholder="0,00" type="number" >
									<!-- <div class="input-get_currency"><img src="images/cny.png">&nbsp;CNY</div> -->
									<div class="input-send_currency">
										<div class="input-send_currency_radio styled-resive-radio">
											<input type="radio" class="styled-currency-input radio-resive-input" name="resive_currency" value="uah" id="resive-ua" >
											<label class="styled-currency-radio-label" title="Украинская гривня" for="resive-ua"><img src="images/ua.png" title="Гривна">&nbsp;UAH</label>
										</div> 
										<div class="input-send_currency_radio styled-resive-radio">
											<input type="radio" class="styled-currency-input radio-resive-input" name="resive_currency" value="usd" id="resive-usd" >
											<label class="styled-currency-radio-label"  title="Доллар США" for="resive-usd"><img src="images/usd.png">&nbsp;USD</label>
										</div> 
										<div class="input-send_currency_radio styled-resive-radio">
											<input type="radio" class="styled-currency-input radio-resive-input" name="resive_currency" value="cny" id="resive-cny" checked>
											<label class="styled-currency-radio-label"  title="Китайский Юань" for="resive-cny"><img src="images/cny.png">&nbsp;CNY</label>
										</div> 
									</div>
								</div>
							</div>
							<div class="col-xs-12">
							
								 
								<p class="sub-heading text-center">Делайте денежные переводы в&nbsp;Китай БЕСПЛАТНО&nbsp;&mdash; без комиссии!</p>
							</div>
						 
							<div class="col-xs-12">
								<div class="calc-form-btn">
									<a class="btn btn-lg btn-base" href="#home-calc-form_full">ВЫПОЛНИТЬ ОБМЕН</a>
								</div>
								
							</div>
						 
						</div>
					</form>
				</div>
				
				<div class="welcome-contacts" style="display: flex;
    align-items: center;
    justify-content: center;     flex-direction: row;
    flex-wrap: wrap;">
					<div class="viber" style="margin :0px 5px 5px 5px">
						<img src="images/viber.svg" style="width:40px;"> <a href="viber://add?number=380975716869" style="color:#fff">+38(097)571-68-69</a>
					</div>
					
					<div class="whatsapp" style="margin :0px 5px 5px 5px">
						<img src="images/whatsapp.svg" style="width:40px;"> <a href="https://api.whatsapp.com/send?phone=+38(097)571-68-69" style="color:#fff">+38(097)571-68-69</a>
					</div>
					
					<div class="wechat" style="margin :0px 5px 5px 5px">
						<img src="images/wechat.svg" style="width:40px;"> <a href="weixin://contacts/profile/franklin0411" style="color:#fff">franklin0411</a>
					</div>
					
				</div>

			</div>
		</div>	<!-- /.row -->
	</div>
</section>
 
<div class="submit-form-popup">
	<div class="form-inner success">
		<i class="icon svg-check"></i>
		<p>Спасибо за запрос! Наш менеджер свяжется с вами в ближайшее время.</p>
	</div>

	<div class="form-inner failed">
		<i class="icon svg-close-circle"></i>
		<p>Извините, произошла неизвестная ошибка, попробуйте отправить запрос еще раз</p>
	</div>
</div>

 
<section class="secondary-bg features elearning-features text-center padding-120-75 white-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2 p_lr45 mb50">
				<div class="feature-item">
					<img src="images/ico-exchage.svg" class="feature-item-icon">
					<h3 class="title-text">Самый выгодный курс обмена в Украине.</h3>
					 
				</div>

			</div>

			<div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2 p_lr45 mb50">
				<div class="feature-item">
					<img src="images/ico-tax.svg" class="feature-item-icon">
					<h3 class="title-text">Нет скрытых комиссий и дополнительных процентов.</h3>
					 
				</div>
			</div>

			<div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2 p_lr45 mb50">
				<div class="feature-item">
					<img src="images/ico-time.svg" class="feature-item-icon">
					<h3 class="title-text">Получите перевод юаней (CNY) мгновенно (от 3 до 30 минут).</h3>
					
				</div>
			</div>
		</div>	<!-- /.row -->
	</div>
</section>

<section class="features app-features-5 padding-120-75 white-bg" id="work">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8 col-sm-10 col-lg-offset-3 col-md-offset-2 col-sm-offset-1">
				<div class="section-heading text-center">

					<h2 class="heading-text" style="color:#0283c4">Процедура денежного перевода в Китай. </h2>
					<hr class="lines">
					<p class="sub-heading">Схема работы нашей системы переводов юаней из Украины и вывода юаней и в Украину очень проста и прозрачна. Отправьте в телеграм или на почту реквизиты получателя. Это может  быть любой счет или номер в карты в китайском банке. (ABC. Bank of China, ICBC bank или другие)</p>

				</div> <!-- /.section-heading -->
			</div>
		</div>	<!-- /.row -->
		<div class="row">
		
			<div class="col-xs-12 col-lg-6">
				<h3 class="heading-text">За наличные  </h3>
				<p>Самый простой способ отправить деньги (CNY юань) вашему поставщику за товар - это воспользоваться нашими услугами в одном из наших центров, практически в любом городе Украины. Вы приносите наличные гривны или доллары и получаете моментальный платеж по китайским  реквизитам и квитанцию. Как правило такой способ занимает меньше всего времени и здесь полностью отсутствуют комиссии, так как этот метод минует все банки посредники. 
					
				</p>
			</div>
			
			<div class="col-xs-12 col-lg-6">
				<h3 class="heading-text">Онлайн банкинг </h3>
				<p>Если вы находитесь не в областном центре или же вам нужна мелкая сумма для оплаты, то вы можете перевести деньги с Приват24 или другого украинского банка на наш счет. И также в считанные минуты получить платеж в китайском банке.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="features app-features-5 padding-75-75 white-bg" id="full-form">
	<div class="container">
		
		<div class="row">
			<div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2 mb50">
				<h2 class="heading-text padding-0-25">Сделать перевод</h2>
					 
				<div class="media media-chained">
					<div class="media-left">
						
						<i class="icon svg-lightning-regular icon-shape width-75 radius-30 icon-border-1"></i>

					</div>
					<div class="media-body">

						<h3 class="title-text">1. ЗАПОЛНИТЕ ФОРМУ ПЕРЕВОДА</h3>
						<p>В форме указывайте контакты, по которым с вами можно связаться в самое ближайшее время.</p>

					</div>
				</div>

				<div class="media media-chained">
					<div class="media-left">
						
						<i class="icon svg-call icon-shape width-75 radius-30 icon-border-1"></i>
						
					</div>
					<div class="media-body">

						<h3 class="title-text">2. ДОЖДИТЕСЬ ЗВОНКА МЕНЕДЖЕРА</h3>
						<p>В течение 15 минут, по указанным вами контактам, с вами свяжется наш менеджер. </p>

					</div>
				</div>

				<div class="media media-chained">
					<div class="media-left">
						
						<i class="icon svg-basic-geolocalize-05 icon-shape width-75 radius-30 icon-border-1"></i>

					</div>
					<div class="media-body">
						
						<h3 class="title-text">3. ПОДТВЕРДИТЕ ДАННЫЕ ИЗ ФОРМЫ И СОВЕРШИТЕ ПЕРЕВОД</h3>
						<p>Подтвердите менеджеру правильность заполненных вами данных о переводе и ваш перевод будет совершен!</p>

					</div>
				</div>
				
				

			</div>
			<div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2 mb50">
				 
				 
				<div class="send-form-wrap">
					<form action="php/sendmail.php" method="POST" class="form-widget form-dark-bg" id="home-calc-form_full">
						<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
						<input type="hidden" name="action" value="validate_captcha">
						<h3 class="title-text text-center">Форма перевода</h3>
						<div class="home-calc-text-kurs mb20" id="home-calc-text-kurs">
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="calc-form-wrap">
									<label class="calc-form-wrap-label"  for="home_send-summ">Отправляете</label>
									
									<input class="form-control calc-form-wrap-input-send" name="send_summ1" id="home_send-summ_inform" placeholder="0,00" inputmode="numeric" type="number" min="0" value="" required onchange=" ">
									
									<div class="input-send_currency">
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-send-input" name="send_currency1" value="uah" id="radio-ua1" checked>
											<label class="styled-currency-radio-label" for="radio-ua1"><img src="images/ua.png" title="Гривна">&nbsp;UAH</label>
										</div> 
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-send-input" name="send_currency1" value="usd" id="radio-usd1" >
											<label class="styled-currency-radio-label"  title="Доллар США" for="radio-usd1"><img src="images/usd.png">&nbsp;USD</label>
										</div> 
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-send-input" name="send_currency1" value="cny" id="radio-cny1" >
											<label class="styled-currency-radio-label"  title="Китайский Юань" for="radio-cny1"><img src="images/cny.png">&nbsp;CNY</label>
										</div> 
									</div>
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-6">
							
								<div class="calc-form-wrap home-send-get-wrap">
									<label class="calc-form-wrap-label" for="home_send-summ">Получаете</label>
									<input class="form-control calc-form-wrap-input-get" name="send_get1" id="home_send-get1" placeholder="0,00" type="number" >
									<!-- <div class="input-get_currency"><img src="images/cny.png">&nbsp;CNY</div> -->
									<div class="input-send_currency">
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-resive-input" name="resive_currency1" value="uah" id="resive-ua1">
											<label class="styled-currency-radio-label" for="resive-ua1"><img src="images/ua.png" title="Гривна">&nbsp;UAH</label>
										</div> 
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-resive-input" name="resive_currency1" value="usd" id="resive-usd1" >
											<label class="styled-currency-radio-label"  title="Доллар США" for="resive-usd1"><img src="images/usd.png">&nbsp;USD</label>
										</div> 
										<div class="input-send_currency_radio styled-currency-radio">
											<input type="radio" class="styled-currency-input radio-resive-input" name="resive_currency1" value="cny" id="resive-cny1"  checked>
											<label class="styled-currency-radio-label"  title="Китайский Юань" for="resive-cny1"><img src="images/cny.png">&nbsp;CNY</label>
										</div> 
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<input type="text" name="contact-name" id="contact-name" class="form-control" placeholder="Ваше ФИО">
						</div>

						<div class="form-group">
							<input type="email" name="contact-email" id="contact-email" class="form-control" placeholder="Ваш Email" required>
						</div>

						<div class="form-group">
							<input type="text" name="contact-phone"  class="form-control" placeholder="+38(___)___-__-__"  pattern=".{17}" value="+38" inputmode="numeric" id="v_fld_phone"  oninvalid="this.setCustomValidity('Не хватает цифр')"
    oninput="this.setCustomValidity('')"  required>
							<span class="fld-text"></span>
						</div>
						 
						<div class="form-group">
							<textarea rows="2" cols="3" id="contact-msg" name="contact-msg" class="form-control" placeholder="Сообщение"></textarea>
						</div>

						<button type="submit" class="btn btn-base btn-lg" id="submitFormButton"><span>Отправить</span></button>
						
						<div class="result">
							<p class="success-msg"><i class="icon svg-check icon-xs"></i> Спасибо, ожидайте звонок от нашего менджера в течение 15 минут.</p>
							<p class="error-msg"><i class="icon svg-close-circle icon-xs"></i> Ошибка, попробуйте еще раз.</p>
						</div>
						
						<div class="welcome-contacts" style="margin-top:40px; display: flex;
    align-items: center;
    justify-content: flex-start;     flex-direction: row;
    flex-wrap: wrap;">
					<div class="viber" style="margin :0px 5px 5px 5px">
						<img src="images/viber.svg" style="width:40px;"> <a href="viber://add?number=380975716869" style="color:#fff">+38(097)571-68-69</a>
					</div>
					
					<div class="whatsapp" style="margin :0px 5px 5px 5px">
						<img src="images/whatsapp.svg" style="width:40px;"> <a href="https://api.whatsapp.com/send?phone=+38(097)571-68-69" style="color:#fff">+38(097)571-68-69</a>
					</div>
					
					<div class="wechat" style="margin :0px 5px 5px 5px">
						<img src="images/wechat.svg" style="width:40px;"> <a href="weixin://contacts/profile/franklin0411" style="color:#fff">franklin0411</a>
					</div>
					
				</div>
					</form>
					
					
				</div>

			</div>
			<div class="col-xs-12">
				<p class="text-center text-lead text-bold">
					<b>Сотрудники нашей службы поддержки всегда готовы помочь вам. </b>
				</p>
				<div class="row text-center">
				
					<div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2 mb50">
						<div class="feature-item">
							<span class="icon svg-call icon-shape shape-bg width-90 radius-30"></span>
							<h3 class="title-text"><a href="tel:+380975716869">+38(097)571-68-69</a></h3>
							<p>Обратитесь к оператору на горячую линию</p>
						</div>
					</div>

					<div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2 mb50">
						<div class="feature-item">
							<span class="icon svg-icon-mail icon-shape shape-bg width-90 radius-30"></span>
							<h3 class="title-text"><a href="mailto:info@cny.com.ua">info@cny.com.ua</a></h3>
							<p>Оставьте запрос на электронную почту </p>
						</div>
					</div>

					<div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2 mb50">
							<div class="feature-item">
							<span class="icon svg-icon-like icon-shape shape-bg width-90 radius-30"></span>
							<h3 class="title-text"><a href="tg://resolve?domain=@cnyua">Telegram @cnyua</a></h3>
							<p>Обратитесь к оператору в Телеграм</p>
						</div>
					</div>
				</div>	<!-- /.row -->
				 
			</div>
		</div>	<!-- /.row -->
	</div>
</section>
<section class="descriptions course-description secondary-bg padding-120-125" id="course-description">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8 col-sm-10 col-lg-offset-3 col-md-offset-2 col-sm-offset-1">
				<div class="section-heading text-center">

					<h2 class="heading-text">Наши преимущества</h2>
					<hr class="lines">
				

				</div> <!-- /.section-heading -->
			</div>
		</div>	<!-- /.row -->

		<div class="row">
			<div class="col-md-12 swiper-container">
				<div class="swiper-wrapper">

					<div class="swiper-slide"> <!-- SLIDER ELEMENT -->
						<div class="desc-pic-header secondary-color-bg">
							<header class="desc-title">
								<img src="images/desc-pic-1.jpg" alt="" class="bg-thumb">

								<div class="title-element gradient-overlay">
									<i class="icon svg-icon-ribbon-alt icon-xs"></i>
									<h3 class="title-text">Оплачивайте счета в Китае</h3>
								</div>

							</header>

							<div class="desc-body">

								<p>Можно оплачивать счета в Китае наличным долларом  или через онлайн банкинг</p>
								 

							</div>
						</div>
					</div> <!-- /END SLIDER ELEMENT -->

					<div class="swiper-slide"> <!-- SLIDER ELEMENT -->
						<div class="desc-pic-header secondary-color-bg text-center">
							<header class="desc-title">
								<img src="images/desc-pic-1.jpg" alt="" class="bg-thumb">

								<div class="title-element gradient-overlay">
									<i class="icon svg-icon-ribbon-alt icon-xs"></i>
									<h3 class="title-text">Моментальный платеж</h3>
								</div>

							</header>

							<div class="desc-body">

								<p>Моментальный платеж в Китай: Приват-24 - CNY (юань)</p>

							</div>
						</div>
					</div> <!-- /END SLIDER ELEMENT -->

					<div class="swiper-slide"> <!-- SLIDER ELEMENT -->
						<div class="desc-pic-header secondary-color-bg text-center">
							<header class="desc-title">
								<img src="images/desc-pic-1.jpg" alt="" class="bg-thumb">

								<div class="title-element gradient-overlay">
									<i class="icon svg-icon-ribbon-alt icon-xs"></i>
									<h3 class="title-text">Самый легкий способ </h3>
								</div>

							</header>

							<div class="desc-body">

								<p>Самый легкий способ 
конвертировать гривну 
со счетов 
в китайский юань.</p>
							 

							</div>
						</div>
					</div> <!-- /END SLIDER ELEMENT -->

					<div class="swiper-slide"> <!-- SLIDER ELEMENT -->
						<div class="desc-pic-header secondary-color-bg text-center">
							<header class="desc-title">
								<img src="images/desc-pic-1.jpg" alt="" class="bg-thumb">

								<div class="title-element gradient-overlay">
									<i class="icon svg-icon-map-alt icon-xs"></i>
									<h3 class="title-text">Нам доверяют </h3>
								</div>

							</header>

							<div class="desc-body">

								<p>Нам доверяют 
								сотни партнеров 
								по всей Украине. </p> 
							</div>
						</div>
					</div> <!-- /END SLIDER ELEMENT -->

					 
				</div>

				<div class="swiper-pagination"></div>
			</div>
		</div> <!-- /.row -->
	</div>
</section>

<!-- =========================
     SCREENSHOTS 
============================== -->
<section class="screenshots screenshots-1 secondary-bg" id="screenshot">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8 col-sm-10 col-lg-offset-3 col-md-offset-2 col-sm-offset-1">
				<div class="section-heading text-center">

					<h2 class="heading-text">Отзывы</h2>
					<hr class="lines">
					<p class="sub-heading">Доверяя нам, вы преобретаете широкие озможности для переводов по всему миру и в Китай.</p>

				</div> <!-- /.section-heading -->
			</div>
		</div>	<!-- /.row -->

		<div class="swiper-container screenshot-slides" id="screenshot-1-swiper">
			
			<img src="images/screenshots/screenshot-frame.png" class="screenshot-frame" alt="">

			<div class="swiper-wrapper">
				<!-- SLIDES -->
				<div class="swiper-slide">
					<a class="lightbox"  data-lightbox-gallery="recap"  href="images/o1.jpg"><img src="images/o1.jpg" alt=""></a>
				</div>
				<div class="swiper-slide">
					<a class="lightbox"  data-lightbox-gallery="recap"  href="images/o2.jpg"><img src="images/o2.jpg" alt=""></a>
				</div>
				<div class="swiper-slide">
					<a class="lightbox"  data-lightbox-gallery="recap"  href="images/o3.jpg"><img src="images/o3.jpg" alt=""></a>
				</div>
				<div class="swiper-slide">
					<a class="lightbox"  data-lightbox-gallery="recap"  href="images/o4.jpg"><img src="images/o4.jpg" alt=""></a>
				</div>
				<div class="swiper-slide">
					<a class="lightbox"  data-lightbox-gallery="recap"  href="images/o5.jpg"><img src="images/o5.jpg" alt=""></a>
				</div>
				<div class="swiper-slide">
					<a class="lightbox"  data-lightbox-gallery="recap"  href="images/o6.jpg"><img src="images/o6.jpg" alt=""></a>
				</div>
			</div>

			<!-- If we need pagination -->
			<div class="swiper-pagination"></div>

		</div>	<!-- /.swiper-container -->
	</div>
</section>

<!-- =========================
     	COURSE INSTRUCTORS 
============================== -->
<section class="course-instructors white-bg padding-120-75" id="course-instructors">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8 col-sm-10 col-lg-offset-3 col-md-offset-2 col-sm-offset-1">
				<div class="section-heading text-center">

					<h2 class="heading-text">Наличные сделки - ищите свой город в списке:</h2>
					
					<p class="sub-heading">Киев, Одесса, Харьков, Львов, Днепр, Полтава, Черкассы, Кривой рог, Николаев, Херсон, Винница, Франковск, Хмельницкий, Черновцы, Львов, Луцк, Шостка, Кременчуг, Умань, Белая Церковь, Житомир, Ровно, Чернигов, Тернополь, Торецк, Курахово, Селидово, Повкровск, Северодонецк, Кременная, Краматорск, Бахмут, Славянск, Лиман, Дружковка,Авдеевка, Акимовка, Апостолово, Бердянск, Васильевка, Веселое, Волнянск, Днепрорудное, Каменка, Константиновка, Марганец, Мариуполь, Мелитополь, Михайловка, Никополь, Орехов, Пологи, Приморкс, Токмак, Энергодар </p>
					
					<hr class="lines">
					<p class="sub-heading">Онлайн сделка - в любом городе Украины! </p>

				</div> <!-- /.section-heading -->
				
				<div class="ukraine-map">
					<img src="images/map.png" class="img-responsive center-block">
				</div>
			</div>
		</div>	<!-- /.row -->
  
	</div>
</section>



 
<!-- =========================
     JOIN US 
============================== -->
<section class="join-us   cover-bg" id="join-us">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8 col-sm-10 col-lg-offset-3 col-md-offset-2 col-sm-offset-1 mb70">
				<div class="section-heading text-center">

					<h2 class="heading-text">Вывод юаней из Китая </h2>
					<p class="sub-heading" style="margin-bottom:0px;">в Украину в наличные доллары или на карту в гривне. </p>

				</div> <!-- /.section-heading -->
				
				<div class="panel-group panel-nemo-1 text-center" id="accordionOne" role="tablist" aria-multiselectable="true" style="margin-top:0px; padding-top:0;">
					<div class="panel"> <!-- PANEL START -->
						<div role="tab" id="headingOne_accordionOne">

							<h3 class="panel-title">
								<span role="button" data-toggle="collapse" data-parent="#accordionOne"  aria-expanded="true" aria-controls="collapseOne_accordionOne">
									<i class="icon svg-icon-lightbulb-alt"> </i>
									Если у вас есть бизнес в Китае - поможем доставить Ваш юань в Украину за 5 минут.
								</span>
							</h3>

						</div>
						 
					</div> <!-- /END PANEL END -->

					<div class="panel"> <!-- PANEL START -->
						<div role="tab" id="headingTwo_accordionOne">

							<h3 class="panel-title">
								<span role="button" data-toggle="collapse" data-parent="#accordionOne"   aria-expanded="true" aria-controls="collapseTwo_accordionOne">
									<i class="icon svg-credit-card-regular"> </i>
									Возможность отправить юани напрямую на Приват24 на ваш карточный счет
								</span>
							</h3>

						</div>
						 
					</div> <!-- /END PANEL END -->

					<div class="panel"> <!-- PANEL START -->
						<div role="tab" id="headingThree_accordionOne">

							<h3 class="panel-title">
								<span role="button" data-toggle="collapse" data-parent="#accordionOne"   aria-expanded="true" aria-controls="collapseThree_accordionOne">
									<i class="icon svg-icon-ribbon-alt"></i>
									Самый безопасный способ отправки денег из Китая - это CNY.COM.UA
								</span>
							</h3>

						</div>
						 
					</div> <!-- /END PANEL END -->
				</div>
				
				<div class="calc-form-btn">
					<a href="#home-calc-form_full" class="btn btn-lg btn-base">Заказать вывод</a>
				</div>
			</div>
		</div>	<!-- /.row --> 
	</div>
</section>



<!-- =========================
     FOOTER 
============================== -->
<footer class="footers footer-4" id="contacts" style="color: #c3c3c3;">
	<div class="container">
		<div class="row table-behaviour">
			<div class="col-lg-3 ">
				<div class="widgets widget-get-in-touch">
					<div class="inner-wids-section">
						<h3 class="title-text">Киев</h3>
						<address>
							 
							<p>ул. проспект Победы 18</p>
						</address>
					</div>

					<div class="inner-wids-section">
						<h3 class="title-text">Одесса</h3>
						<address>
							<p>ул. Базовая, 6</p> 
							
						</address>
					</div>

					<div class="inner-wids-section">
						<h3 class="title-text">Харьков</h3>
						<address>
							 
							<p>ул. Тюринская, 147</p>
						</address>
					</div>	
					
				</div>
			</div>
			
			<div class="col-lg-3 ">
				<div class="widgets widget-get-in-touch">
					<div class="inner-wids-section">
						<h3 class="title-text">Режим работы:</h3>
						<div>
							ПН: 9:00 - 18:00<br>
							ВТ: 9:00 - 18:00<br>
							СР: 9:00 - 18:00<br>
							ЧТ: 9:00 - 18:00<br>
							ПТ: 9:00 - 18:00<br>
							СБ: 10:00 - 16:00<br>
							ВС - выходной<br>
						</div> 
					</div>	
					
				</div>
			</div>

			<div class="col-lg-3 ">
				 <div class="widgets widget-get-in-touch">
					<div class="inner-wids-section">
						<h3 class="title-text">Горячая линия</h3>
						<address>
							 
							<p><a href="tel:+380975716869">+38 (097) 571-68-69</a></p>
						</address>
					</div>

					<div class="inner-wids-section">
						<h3 class="title-text">Почта</h3>
						<address>
							 
							<p><a href="mailto:info@cny.com.ua">info@cny.com.ua</a></p>
						</address>
						
						
					</div>
 
				</div>
			</div>
			
			<div class="col-lg-3">
					<div class="viber" style="margin-bottom:5px;">
						<img src="images/viber.svg" style="width:40px;"> <a href="viber://add?number=380975716869">+38(097)571-68-69</a>
					</div>
					
					<div class="whatsapp" style="margin-bottom:5px;">
						<img src="images/whatsapp.svg" style="width:40px;"> <a href="https://api.whatsapp.com/send?phone=+38(097)571-68-69">+38(097)571-68-69</a>
					</div>
					
					<div class="wechat" style="margin-bottom:5px;">
						<img src="images/wechat.svg" style="width:40px;"> <a href="weixin://contacts/profile/franklin0411">franklin0411</a>
					</div>
					
					<div class="inner-wids-section text-right">
						<p>&copy;2020 CNY.COM.UA </p>
					</div>
			</div>

		</div>	<!-- /.row -->
	</div>
</footer>

<!-- Modal -->
<div class="modal fade" id="subscribe_modal_1" tabindex="-1" role="dialog" aria-labelledby="subscribe_modal_1_label">
  <div class="modal-dialog" role="document">
    <div class="modal-content simple-success-msg">
      <div class="modal-body text-center">

      	<i class="icon svg-basic-elaboration-message-happy icon-md"></i>
		<p>Спасибо за запрос. Ожидайте звонок нашего менеджера в течение 10 минут.</p>
			<button type="button" class="btn btn-base btn-rectangle btn-sm" data-dismiss="modal"><span>ОК</span></button>

      </div>      
    </div>
  </div>
</div>


<!-- =========================
     SCRIPTS 
============================== -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/matchMedia.js"></script>
<script src="js/jquery.mixitup.js"></script>
<script src="js/swiper.jquery.min.js"></script>
<script src="js/SVGinject.js"></script>
<!-- <script src="js/smoothscroll.js"></script> -->
<script src="js/jquery.datetimepicker.full.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/imask.js"></script>
 <script src="//code.jivosite.com/widget/FZUUFQ0XgD" async></script>
<script src="js/application.js?16-04-20/11"></script>


<?
 
$link = mysqli_connect('mc379390.mysql.tools','mc379390_admcny','zOD7o5k6#(','mc379390_admcny');

if (!$link) {
    echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
    echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

if ($result1 = mysqli_query($link, "SELECT field_ua_to_usd_value FROM node__field_ua_to_usd WHERE entity_id=1")) {
	$result1=mysqli_fetch_array($result1);
  
	
	echo '<input id="node__field_ua_to_usd" type="hidden" value="'.$result1['0'].'">';
    /* free result set */
    mysqli_free_result($result1);
}

if ($result2 = mysqli_query($link, "SELECT field_cny_to_usd_value FROM node__field_cny_to_usd WHERE entity_id=1")) {
	$result2=mysqli_fetch_array($result2);
  
	
	echo '<input id="node__field_cny_to_usd" type="hidden" value="'.$result2['0'].'">';
    /* free result set */
    mysqli_free_result($result2);
}

if ($result3 = mysqli_query($link, "SELECT `changed` FROM `node_field_revision`  WHERE `nid`=1 ORDER BY `changed` DESC")) {
	
	$result3=mysqli_fetch_array($result3);
	
	//print_r($result3['0']);
	
	$cny_date=date("d-m-Y H:i:s",$result3['0']);     
	
	echo '<input id="node__field_cny_date" type="hidden" value="'.$cny_date.'">';
    /* free result set */
    mysqli_free_result($result3);
}
?>
</body>
</html>