<?php  
if( !defined( 'ABSPATH')){ exit(); }

/* news */
add_filter('news_widget_block','my_news_widget_block');
function my_news_widget_block(){
	$widget = '
	<div class="widget widget_news_div">
		<div class="widget_title">
			<div class="widget_title_ins">
				[title]
			</div>	
		</div>
			<div class="clear"></div>
		[news]
	    
		<div class="widget_news_more_div">
			<a href="[url]" class="widget_news_more">'. __('All news','pntheme') .' ([countpost])</a>
				<div class="clear"></div>
		</div>		
	</div>
	';
	return $widget;
}

add_filter('news_widget_one','my_news_widget_one', 99, 5);
function my_news_widget_one($widget, $item, $count, $r, $date_format){
	$widget = '
	<div class="widget_news_line">
		<div class="widget_news_date">
			<div class="wmon">'. get_month_title(get_the_time( 'm', $item->ID)) .'</div>
			<div class="wdate">'. get_the_time( 'd', $item->ID) .'</div>
		</div>
		<div class="widget_news_content">
			<div class="widget_news_text">
				'. wp_trim_words(pn_strip_input($item->post_content), 10) .'
			</div>
			<a href="'. get_permalink($item->ID) .'" class="widget_news_more">'. __('Read more','pntheme') .'</a>
		</div>
			<div class="clear"></div>
	</div>	
	';
	return $widget;
}
/* end news */

/* reviews */
add_filter('reviews_widget_block','my_reviews_widget_block');
function my_reviews_widget_block(){
	$widget = '
	<div class="widget widget_reviews_div">
		<div class="widget_title">
			<div class="widget_title_ins">
				[title]
			</div>
		</div>
		
		<div class="clear"></div>
		
		[reviews]
	
		<div class="widget_reviews_more_div">
			<a href="[url]" class="widget_reviews_more">'. __('All reviews','pntheme') .' ([countpost])</a>
			<div class="clear"></div>
		</div>
	</div>
	';
	
	return $widget;
}

add_filter('reviews_widget_one','my_reviews_widget_one',99,5);
function my_reviews_widget_one($widget, $item, $count, $r, $time_format){
	$site = $item->user_site;
	$site1 = $site2 = '';
	if($site){
		$site1 = '<a href="'. $site .'" rel="nofollow" target="_blank">';
		$site2 = '</a>';
	}	
		
	$cl = '';
	if($r%2==0){ $cl = 'odd'; }
		
	$widget = '
	<div class="widget_reviews_line '. $cl .'">
		<div class="widget_reviews_author">'. $site1 . pn_strip_input($item->user_name) . $site2 .',</div>
		<div class="widget_reviews_date">'. get_mytime($item->review_date, $time_format) .'</div>
			<div class="clear"></div>
			
		<div class="widget_reviews_content">
			'. wp_trim_words(pn_strip_input($item->review_text), 10) .'
		</div>
	</div>	
	';
	
	return $widget;
}
/* end reviews */

/* status work */
add_filter('exb_statusworkwidget','my_exb_statusworkwidget',99,7);
function my_exb_statusworkwidget($temp, $worked, $title, $time, $hidedate, $text1, $text2){
	if($worked == 'online'){
		$text = $text1;
	} else {
		$text = $text2;
	}

	$cont ='
	<div class="statuswork_widget '. $worked .'"><div class="statuswork_widgetvn">
		<div class="statuswork_widget_text">'. $text .'</div>';
		if($hidedate == 0){
			$cont .='<div class="statuswork_widget_time">'. date('d.m.Y H:i:s',$time) .'</div>';
		}
		$cont .='
	</div></div>
	';
	return $cont;
}
/* end statuswork */

/* register widget */
add_filter('exb_register_widget','my_exb_register_widget');
function my_exb_register_widget($arg){

$arg = '
	[return_link]
    [result]
		<div class="wline">
			<input type="text" name="login" placeholder="'. __('Login', 'pntheme') .'" value="" />
		</div>
		<div class="wline">
			<input type="text" name="email" placeholder="'. __('E-mail', 'pntheme') .'" value="" />
		</div>				
		<div class="wline">
			<input type="password" name="pass" placeholder="'. __('Password', 'pntheme') .'" value="" />
		</div>	
		<div class="wline">
			<input type="password" name="pass2" placeholder="'. __('Password repeatedly', 'pntheme') .'" value="" />
		</div>	
				
		[captcha]
				
		<div class="wline">
            [rule]
        </div>		
				
	<div class="wlinesubmit">
		[submit]
	</div>                    				
		<div class="clear"></div>
';

    return $arg;
}
/* end register widget */

/* login widget */
add_filter('exb_login_widget','my_exb_login_widget');
function my_exb_login_widget($arg){

	$arg = '
		[return_link]
		[result]
		
		<div class="wline">
			[loginput_placeholder]
		</div>
		<div class="wline">
			[passinput_placeholder]
		</div>	
					
		[captcha]
					
		<div class="wlineleft">
			<p><a href="[reglink]">'. __('Register','pntheme') .'</a></p>
		</div>
		<div class="wlineright">
			<p><a href="[lostlink]">'. __('Forgot password?','pntheme') .'</a></p>
		</div>                    				
			<div class="clear"></div>
		<div class="wlinesubmit">
			[submit]
		</div>  		
	';

    return $arg;
}

add_filter('exb_puser_widget','my_exb_puser_widget');
function my_exb_puser_widget($arg){
global $exchangebox, $investbox;

	$arg = '
	<div class="uswidin">
		<div class="uswidinleft">'. __('Your discount','pntheme') .'</div>  
		<div class="uswidinright">[user_skidka]%</div>
			<div class="clear"></div>
	</div>

	<ul>
		<li class="'. get_userpage_pn('account') .'"><a href="[account]">'. __('Personal account','pntheme') .'</a></li>
		<li class="'. get_userpage_pn('usersobmen') .'"><a href="[usersobmen]">'. __('Your operations','pntheme') .'</a></li>

		<li class="'. get_userpage_pn('partnerstats') .'"><a href="[partnerstats]">'. __('Affiliate account','pntheme') .'</a></li>
		<li class="'. get_userpage_pn('payouts') .'"><a href="[payouts]">'. __('Withdrawal','pntheme') .'</a></li>
		<li class="'. get_userpage_pn('banners') .'"><a href="[banners]">'. __('Promotional materials','pntheme') .'</a></li>
		<li class="'. get_userpage_pn('partnersFAQ') .'"><a href="[partnersFAQ]">'. __('Affiliate FAQ','pntheme') .'</a></li>';
		
		if(is_object($investbox)){
			$arg .= '
			<li><a href="'. $investbox->get_page('toinvest') .'">'. __('To invest','pntheme') .'</a></li>
			';
		}
		if($exchangebox->get_option('txtxml','xml')){
			$arg .= '
			<li><a href="[xml]">'. __('XML-file rates','pntheme') .'</a></li>
			';
		}		
		if($exchangebox->get_option('txtxml','txt')){
			$arg .= '
			<li><a href="[txt]">'. __('TXT-file rates','pntheme') .'</a></li>
			';			
		}
		
		$arg .= '
	</ul>

	<div class="wexit">
		<a href="[logout]">'. __('Exit','pntheme') .'</a>
	</div>
	';
	
    return $arg;
}

add_filter('exb_user_widget','my_exb_user_widget');
function my_exb_user_widget($arg){
global $exchangebox, $investbox;

	$arg = '
	<div class="uswidin">
		<div class="uswidinleft">'. __('Your discount','pntheme') .'</div>  
		<div class="uswidinright">[user_skidka]%</div>
			<div class="clear"></div>
	</div>

		<ul>
			<li class="'. get_userpage_pn('account') .'"><a href="[account]">'. __('Personal account','pntheme') .'</a></li>
			<li class="'. get_userpage_pn('usersobmen') .'"><a href="[usersobmen]">'. __('Your operations','pntheme') .'</a></li>';
			
			if(is_object($investbox)){
				$arg .= '
				<li><a href="'. $investbox->get_page('toinvest') .'">'. __('To invest','pntheme') .'</a></li>
				';
			}			
			if($exchangebox->get_option('txtxml','xml')){
				$arg .= '
				<li><a href="[xml]">'. __('XML-file rates','pntheme') .'</a></li>
				';
			}		
			if($exchangebox->get_option('txtxml','txt')){
				$arg .= '
				<li><a href="[txt]">'. __('TXT-file rates','pntheme') .'</a></li>
				';			
			}			
			
			$arg .= '
		</ul>
		
	<div class="wexit">
		<a href="[logout]">'. __('Exit','pntheme') .'</a>
	</div>
	';
	
    return $arg;
}
/* end login widget */

/* last exchange */
add_filter('get_lchange_line','my_get_lchange_line');
function my_get_lchange_line($widget){
	
	$widget = '
	<div class="lobmentable">
		<div class="lobmline">
			<div class="lobmlineico">
				<div class="lobmenlinewico" style="background: url([logo1]) no-repeat center center"></div>
					<div class="clear"></div>
			</div>
			<div class="lobmlinebac">
				[sum1]&nbsp;[vtype1]
			</div>
				<div class="clear"></div>
		</div>
							
		<div class="lobmlinepr"></div>
							
		<div class="lobmline">
			<div class="lobmlineico">
				<div class="lobmenlinewico" style="background: url([logo2]) no-repeat center center"></div>
					<div class="clear"></div>
			</div>
			<div class="lobmlinebac">
				[sum2]&nbsp;[vtype2]
			</div>
				<div class="clear"></div>
		</div>	
			<div class="clear"></div>		
		<div class="lobmendate">[date]</div>
	</div>	
	';
	
	return $widget;
}
/* end last exchange */

/* crypto */
add_filter('exb_curs_widget','my_exb_curs_widget');
function my_exb_curs_widget($temp){
	$temp = '
	<div class="bexbcurs_widget">
		<div class="bexbcurs_widgetvn">
			<div class="bexbcurs_widgettitle">
				<div class="bexbcurs_widgettitlevn">
					[title]
				</div>
			</div>				
				
			[table]
				
			<div class="bexbcurs_update">'. __('Update rate','pntheme') .': [date]</div>	
				
		</div>
	</div>
	';
	return $temp;
}
/* end crypto */

/* home */
add_filter('exb_before_xchange_form','my_exb_before_xchange');
function my_exb_before_xchange(){
    return '
		<div class="homeobmentable">
	';
}
add_filter('exb_after_xchange_form','my_exb_after_xchange_form');
function my_exb_after_xchange_form(){
    return '
			<div class="clear"></div>
		</div>
	';
}
add_filter('exb_before_xchange_left','my_exb_before_xchange_left');
function my_exb_before_xchange_left(){
    return '<div class="widgethometitle"><div class="widt">'. __('Giving','pntheme') .'</div></div>';
}
add_filter('exb_before_xchange_right','my_exb_before_xchange_right');
function my_exb_before_xchange_right(){
    return '
	<div class="widgethometitle2">
		<div class="widt1">'. __('Receive','pntheme') .'</div>
		<div class="widt2">'. __('Reserve','pntheme') .'</div>
			<div class="clear"></div>
	</div>
	';
}
/* end home */

add_filter('exb_before_account_form','my_exb_before_contact_form');
add_filter('exb_before_banners_page','my_exb_before_contact_form');
add_filter('exb_before_payouts_page','my_exb_before_contact_form');
add_filter('exb_before_partnerstats_page','my_exb_before_contact_form');
add_filter('exb_before_sitemap_page','my_exb_before_contact_form');
add_filter('exb_before_userobmen_page','my_exb_before_contact_form');
add_filter('exb_before_tarifs_page','my_exb_before_contact_form');
add_filter('exb_before_otzivs_page','my_exb_before_contact_form');
add_filter('before_toinvest_page','my_exb_before_contact_form');
add_filter('before_indeposit_page','my_exb_before_contact_form');
function my_exb_before_contact_form($arg){
global $post;
    $arg = '
	<div class="thecontent">		
		<h1 class="titlepage">'. pn_strip_input(ctv_ml($post->post_title)) .'</h1>	
	';
	return $arg;
}

add_filter('exb_before_xchangestep1_form','my_exb_before_contact_form1');
add_filter('exb_before_xchangestep2_form','my_exb_before_contact_form1');
add_filter('exb_before_xchangestep3_form','my_exb_before_contact_form1');
function my_exb_before_contact_form1($arg){
global $post;
    $arg = '
	<div class="thecontent">		
	';
	return $arg;
}

add_filter('exb_after_account_form','my_exb_after_contact_form');
add_filter('exb_after_banners_page','my_exb_after_contact_form');
add_filter('exb_after_payouts_page','my_exb_after_contact_form');
add_filter('exb_after_partnerstats_page','my_exb_after_contact_form');
add_filter('exb_after_sitemap_page','my_exb_after_contact_form');
add_filter('exb_after_userobmen_page','my_exb_after_contact_form');
add_filter('exb_after_tarifs_page','my_exb_after_contact_form');
add_filter('exb_after_otzivs_page','my_exb_after_contact_form');
add_filter('exb_after_xchangestep1_form','my_exb_after_contact_form');
add_filter('exb_after_xchangestep2_form','my_exb_after_contact_form');
add_filter('exb_after_xchangestep3_form','my_exb_after_contact_form');
add_filter('after_toinvest_page','my_exb_after_contact_form');
add_filter('after_indeposit_page','my_exb_after_contact_form');
function my_exb_after_contact_form($arg){
    $arg = '</div>';
	return $arg;
}