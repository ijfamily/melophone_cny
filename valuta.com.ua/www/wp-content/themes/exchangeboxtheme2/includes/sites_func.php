<?php
if( !defined( 'ABSPATH')){ exit(); }

remove_action('wp_head','start_post_rel_link',10,0);
remove_action('wp_head','index_rel_link');
remove_action('wp_head','adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head','wp_shortlink_wp_head', 10, 0 );
remove_action('wp_head','feed_links_extra', 3);
remove_action('wp_head','feed_links', 2);

remove_action('wp_head','print_emoji_detection_script',7);
remove_action('wp_print_styles','print_emoji_styles',10);

function new_excerpt_length($length) {
	return 45;
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

add_filter('comment_text', 'not_transform_quotes',100);
add_filter('the_title', 'not_transform_quotes',100);
add_filter('the_content', 'not_transform_quotes',100);
add_filter('the_excerpt', 'not_transform_quotes', 100);
function not_transform_quotes($content){
    return str_replace(array('&#171;','&#187;'),'"',$content);
}

if (function_exists('register_nav_menu')) {
	register_nav_menu('the_top_menu', __('Top menu for Guests','pntheme'));
	register_nav_menu('the_top_menu_user', __('Top menu for Users','pntheme'));
	register_nav_menu('the_bottom_menu', __('Bottom menu','pntheme'));
}
function no_menu(){}

function no_menu_standart(){
	wp_nav_menu(array(
		'sort_column' => 'menu_order',
		'container' => 'div',
		'container_class' => 'menu',
		'menu_class' => 'tmenu js_menu',
		'menu_id' => '',
		'depth' => '3',
		'fallback_cb' => 'no_menu',
		'theme_location' => 'the_top_menu'
	));	
}

register_sidebar(array(
    'name'=> __('Sidebar'),
	'id' => 'unique-sidebar-id',
	'before_title' => '<div class="widget_title"><div class="widget_titlevn">',
	'after_title' => '</div></div>',
	'before_widget' => '<div class="widget"><div class="widget_ins">',
	'after_widget' => '<div class="clear"></div></div></div>',
));

add_action('wp_enqueue_scripts', 'my_themeinit', 0);
function my_themeinit(){
global $or_template_directory, $exchangebox;

	$vers = $exchangebox->plugin_version;
	if($exchangebox->is_debug_mode()){
		$vers = current_time('timestamp');
	}
		
	wp_deregister_style('open-sans');
	wp_enqueue_style('open-sans', is_ssl_url("http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700&subset=latin,cyrillic-ext,cyrillic"), false, $vers);
	wp_enqueue_style('theme style', $or_template_directory . "/style.css", false, $vers);
	wp_enqueue_script('jquery site js', $or_template_directory.'/js/all.js', false, $vers);	

}