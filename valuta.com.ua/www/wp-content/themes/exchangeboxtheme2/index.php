<?php  
if( !defined( 'ABSPATH')){ exit(); }

get_header(); 
?>

<div class="thecontent">
		
	<h1 class="titlepage"><?php _e('News','pntheme'); ?></h1>
			
	<?php 
	if (have_posts()) : ?>
		<div class="manynews">
        <?php while (have_posts()) : the_post(); ?>			
            <div class="posts" itemscope itemtype="http://schema.org/Article">
				<h2 class="poststitle"><a href="<?php the_permalink();?>" itemprop="url" rel="bookmark" title="<?php the_title_attribute(); ?>"><span itemprop="name"><?php the_title(); ?></span></a></h2>
	
				<div class="postsexcerpt" itemprop="articleBody">
					<?php the_excerpt(); ?>
				</div>	
	
				<div class="metabox">
				
					<div class="onepostdate" itemprop="datePublished" content="<?php the_time('Y-m-d'); ?>">
						<?php the_time(get_option('date_format')); ?>
					</div>		
					
					<div class="metaboxleft">
						<?php _e('Category','pntheme'); ?>: <?php the_terms( $post->ID, 'category','<span itemprop="articleSection">',', ','</span>'); ?>
					</div>
					
					<?php the_tags( '<div class="metaboxleft"><span>'. __('Tags','pntheme') .':</span> ', ', ', '</div>' ); ?>
						
					<a href="<?php the_permalink();?>" itemprop="url" class="mores"><?php _e('Read more','pntheme'); ?></a>
						<div class="clear"></div>
				</div>
			</div>				
	<?php endwhile; ?>
		</div>	
		
	<?php the_pagenavi(); ?>	

    <?php else : ?>

		<div class="posts">
			<div class="text">
				<?php _e('Unfortunately, the section is empty','pntheme'); ?>							
			</div>
		</div>					
					
    <?php endif; ?>	
		
</div>

<?php get_footer(); ?>