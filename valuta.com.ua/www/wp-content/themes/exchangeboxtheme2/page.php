<?php 
if( !defined( 'ABSPATH')){ exit(); } 
get_header(); 
?>

	<div class="thecontent">
		
	<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>		
		<h1 class="titlepage"><?php the_title();?></h1>
		
		<div class="text">
		
		    <?php the_content(); ?>
					
				<div class="clear"></div>
		</div>
	<?php endwhile; ?>								
    <?php endif; ?>		
	
    </div>		
	
<?php get_footer();?>