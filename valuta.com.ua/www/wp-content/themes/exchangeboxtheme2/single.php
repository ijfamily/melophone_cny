<?php  
if( !defined( 'ABSPATH')){ exit(); } 
get_header(); 

global $or_site_url;
$sof = get_option('show_on_front'); 
if($sof == 'page'){
    $news_url = get_permalink(get_option('page_for_posts'));
} else {
    $news_url = $or_site_url;
}
?>


    <div class="thecontent" itemscope itemtype="http://schema.org/Article">
		
	<?php 
	if (have_posts()) : ?>
        <?php while (have_posts()) : the_post();  		
		?>		
			<h1 class="titlepage" itemprop="name"><?php the_title(); ?></h1>
		
			<div class="text" itemprop="articleBody">
		   
                <?php the_content(); ?>		   
		   
		        <div class="clear"></div> 
		    </div>	
	
			<div class="metabox">
				<div class="onepostdate" itemprop="datePublished" content="<?php the_time('Y-m-d'); ?>">
					<?php the_time(get_option('date_format')); ?>
				</div>
				
				<div class="metaboxleft">
					<?php _e('Category','pntheme'); ?>: <?php the_terms( $post->ID, 'category','<span itemprop="articleSection">',', ','</span>'); ?>
				</div>
				
				<?php the_tags( '<div class="metaboxleft"><span>'. __('Tags','pntheme') .':</span> ', ', ', '</div>' ); ?>
				
				<a href="<?php echo $news_url;?>" class="mores"><?php _e('to the news list','pntheme'); ?></a>
					<div class="clear"></div>
			</div>
		
	<?php endwhile; ?>		
    <?php endif; ?>	
	
	</div>
<?php get_footer();?>