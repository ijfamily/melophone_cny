<?php  
if( !defined( 'ABSPATH')){ exit(); }
 
get_header(); 
?>

	<div class="thecontent">
		<h1 class="titlepage"><?php _e('Error 404','pntheme'); ?></h1>
		<div class="text">
		
			<h3><?php _e('What does it mean?','pntheme'); ?></h3>

			<ul>
				<li><?php _e('Page has been renamed','pntheme'); ?></li>
				<li><?php _e('Page has been deleted','pntheme'); ?></li>
				<li><?php _e('Pages never existed','pntheme'); ?></li>						
			</ul>
							
			<p><?php printf(__('Go to <a href="%s">main</a>, please.','pntheme'), get_site_url_or()); ?>.</p>
		            
			<div class="clear"></div>
		</div>
	</div>
	
<?php get_footer();?>