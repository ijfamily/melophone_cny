<?php  
if( !defined( 'ABSPATH')){ exit(); } 

add_action('admin_menu', 'pn_adminpage_theme_partners');
function pn_adminpage_theme_partners(){
global $exchangebox;
	
	add_submenu_page("pn_themeconfig", __('Partners','pntheme'), __('Partners','pntheme'), 'administrator', "pn_theme_partners", array($exchangebox, 'admin_temp'));
}

add_action('pn_adminpage_title_pn_theme_partners', 'pn_adminpage_title_pn_theme_partners');
function pn_adminpage_title_pn_theme_partners($page){
	_e('Partners','pntheme');
}

add_filter('pn_theme_partners_option', 'def_pn_theme_partners_option', 1);
function def_pn_theme_partners_option($options){
global $wpdb, $exchangebox;
	
	$change = get_option('themechange');
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Partners','pntheme'),
		'submit' => __('Save','pntheme'),
		'colspan' => 2,
	);
	$options['ptitle'] = array(
		'view' => 'inputbig',
		'title' => __('Title', 'pntheme'),
		'default' => is_isset($change,'ptitle'),
		'name' => 'ptitle',
		'work' => 'input',
	);

	return $options;	
} 

add_action('pn_adminpage_content_pn_theme_partners','def_pn_adminpage_content_pn_theme_partners');
function def_pn_adminpage_content_pn_theme_partners(){
	
	$form = new PremiumForm();
	$params_form = array(
		'filter' => 'pn_theme_partners_option',
		'method' => 'post',
	);
	$form->init_form($params_form);
	
} 

add_action('premium_action_pn_theme_partners','def_premium_action_pn_theme_partners');
function def_premium_action_pn_theme_partners(){
global $wpdb;	

	only_post();

	pn_only_caps(array('administrator'));
	
	$themechange = get_option('themechange');
	$themechange['ptitle'] = pn_strip_input(is_param_post('ptitle'));
	update_option('themechange', $themechange);
	
	$back_url = is_param_post('_wp_http_referer');
	$back_url .= '&reply=true';
			
	wp_safe_redirect($back_url);
	exit;	
}