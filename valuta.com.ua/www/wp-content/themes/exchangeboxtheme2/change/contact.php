<?php  
if( !defined( 'ABSPATH')){ exit(); }

add_action('admin_menu', 'pn_adminpage_theme_contact');
function pn_adminpage_theme_contact(){
global $exchangebox;
	
	add_submenu_page("pn_themeconfig", __('Contact Information','pntheme'), __('Contact Information','pntheme'), 'administrator', "pn_theme_contact", array($exchangebox, 'admin_temp'));
}

add_action('pn_adminpage_title_pn_theme_contact', 'pn_adminpage_title_pn_theme_contact');
function pn_adminpage_title_pn_theme_contact($page){
	_e('Contact Information','pntheme');
} 
	
add_filter('pn_theme_contact_option', 'def_pn_theme_contact_option', 1);
function def_pn_theme_contact_option($options){
global $wpdb, $exchangebox;	
	
	$change = get_option('themechange');
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Contact Information','pntheme'),
		'submit' => __('Save','pntheme'),
		'colspan' => 2,
	);
	$options['tel'] = array(
		'view' => 'inputbig',
		'title' => __('Phone', 'pntheme'),
		'default' => is_isset($change,'tel'),
		'name' => 'tel',
		'work' => 'input',
	);
	$options['icq'] = array(
		'view' => 'inputbig',
		'title' => __('ICQ', 'pntheme'),
		'default' => is_isset($change,'icq'),
		'name' => 'icq',
		'work' => 'input',
	);
	$options['skype'] = array(
		'view' => 'inputbig',
		'title' => __('Skype', 'pntheme'),
		'default' => is_isset($change,'skype'),
		'name' => 'skype',
		'work' => 'input',
	);
	$options['mail'] = array(
		'view' => 'inputbig',
		'title' => __('E-mail', 'pntheme'),
		'default' => is_isset($change,'mail'),
		'name' => 'mail',
		'work' => 'input',
	);
	$options['telegram'] = array(
		'view' => 'inputbig',
		'title' => __('Telegram', 'pntheme'),
		'default' => is_isset($change,'telegram'),
		'name' => 'telegram',
		'work' => 'input',
	);
	$options['viber'] = array(
		'view' => 'inputbig',
		'title' => __('Viber', 'pntheme'),
		'default' => is_isset($change,'viber'),
		'name' => 'viber',
		'work' => 'input',
	);
	$options['whatsup'] = array(
		'view' => 'inputbig',
		'title' => __('WhatsApp', 'pntheme'),
		'default' => is_isset($change,'whatsup'),
		'name' => 'whatsup',
		'work' => 'input',
	);
	$options['jabber'] = array(
		'view' => 'inputbig',
		'title' => __('Jabber', 'pntheme'),
		'default' => is_isset($change,'jabber'),
		'name' => 'jabber',
		'work' => 'input',
	);			
	$options['regrab'] = array(
		'view' => 'textarea',
		'title' => __('Work time','pntheme'),
		'default' => is_isset($change,'regrab'),
		'name' => 'regrab',
		'width' => '',
		'height' => '100px',
		'work' => 'text',
	);	
	return $options;	
} 

add_action('pn_adminpage_content_pn_theme_contact','def_pn_adminpage_content_pn_theme_contact');
function def_pn_adminpage_content_pn_theme_contact(){
	
	$form = new PremiumForm();
	$params_form = array(
		'filter' => 'pn_theme_contact_option',
		'method' => 'post',
	);
	$form->init_form($params_form);
	
}	

add_action('premium_action_pn_theme_contact','def_premium_action_pn_theme_contact');
function def_premium_action_pn_theme_contact(){
global $wpdb;	

	only_post();

	pn_only_caps(array('administrator'));

	$themechange = get_option('themechange');
	
    $themechange['skype'] = pn_strip_input(is_param_post('skype'));
	$themechange['icq'] = pn_strip_input(is_param_post('icq'));
	$themechange['mail'] = pn_strip_input(is_param_post('mail'));
	$themechange['tel'] = pn_strip_input(is_param_post('tel'));
	$themechange['telegram'] = pn_strip_input(is_param_post('telegram'));
	$themechange['viber'] = pn_strip_input(is_param_post('viber'));
	$themechange['whatsup'] = pn_strip_input(is_param_post('whatsup'));
	$themechange['jabber'] = pn_strip_input(is_param_post('jabber'));
	$themechange['regrab'] = pn_strip_input(is_param_post('regrab'));
	
	update_option('themechange', $themechange);
	
	$back_url = is_param_post('_wp_http_referer');
	$back_url .= '&reply=true';
			
	wp_safe_redirect($back_url);
	exit;	
}