<?php  
if( !defined( 'ABSPATH')){ exit(); }

add_action('admin_menu', 'pn_adminpage_theme_footer');
function pn_adminpage_theme_footer(){
global $exchangebox;	
	
	add_submenu_page("pn_themeconfig", __('Footer','pntheme'), __('Footer','pntheme'), 'administrator', "pn_theme_footer", array($exchangebox, 'admin_temp'));
}

add_action('pn_adminpage_title_pn_theme_footer', 'pn_adminpage_title_pn_theme_footer');
function pn_adminpage_title_pn_theme_footer($page){
	_e('Footer','pntheme');
} 
	
add_filter('pn_theme_footer_option', 'def_pn_theme_footer_option', 1);
function def_pn_theme_footer_option($options){
global $wpdb, $exchangebox;		
	
	$change = get_option('themechange');
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Footer','pntheme'),
		'submit' => __('Save','pntheme'),
		'colspan' => 2,
	);
	$options['footer'] = array(
		'view' => 'textarea',
		'title' => __('Copyright','pntheme'),
		'default' => is_isset($change,'footer'),
		'name' => 'footer',
		'width' => '',
		'height' => '100px',
		'work' => 'text',
	);
	
	return $options;	
} 

add_action('pn_adminpage_content_pn_theme_footer','def_pn_adminpage_content_pn_theme_footer');
function def_pn_adminpage_content_pn_theme_footer(){
	
	$form = new PremiumForm();
	$params_form = array(
		'filter' => 'pn_theme_footer_option',
		'method' => 'post',
	);
	$form->init_form($params_form);
	
}		

add_action('premium_action_pn_theme_footer','def_premium_action_pn_theme_footer');
function def_premium_action_pn_theme_footer(){
global $wpdb;	

	only_post();

	pn_only_caps(array('administrator'));

	$themechange = get_option('themechange');
	
	$themechange['footer'] = pn_strip_text(is_param_post('footer'));
	
	update_option('themechange', $themechange);
	
	$back_url = is_param_post('_wp_http_referer');
	$back_url .= '&reply=true';
			
	wp_safe_redirect($back_url);
	exit;	
}