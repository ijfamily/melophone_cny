<?php  
if( !defined( 'ABSPATH')){ exit(); }

add_action('admin_menu', 'pn_adminpage_theme_header');
function pn_adminpage_theme_header(){
global $exchangebox;
	
	add_submenu_page("pn_themeconfig", __('Color scheme','pntheme'), __('Color scheme','pntheme'), 'administrator', "pn_theme_checktheme", array($exchangebox, 'admin_temp'));
}

add_action('pn_adminpage_title_pn_theme_checktheme', 'pn_adminpage_title_pn_theme_checktheme');
function pn_adminpage_title_pn_theme_checktheme($page){
	_e('Color scheme','pntheme');
}
	
add_filter('pn_theme_checktheme_option', 'def_pn_theme_checktheme_option', 1);
function def_pn_theme_checktheme_option($options){
global $wpdb, $exchangebox;		
	
	$change = get_option('themechange');
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Color scheme','pntheme'),
		'submit' => __('Save','pntheme'),
		'colspan' => 2,
	);
	$options['checktheme'] = array(
		'view' => 'select',
		'title' => __('Color scheme','pntheme'),
		'options' => array('blue' => __('Blue','pntheme'), 'green' => __('Green','pntheme'), 'gray' => __('Gray','pntheme'), 'red' => __('Red','pntheme')),
		'default' => is_isset($change,'checktheme'),
		'name' => 'checktheme',
		'work' => 'input',
	);	
	return $options;	
} 

add_action('pn_adminpage_content_pn_theme_checktheme','def_pn_adminpage_content_pn_theme_checktheme');
function def_pn_adminpage_content_pn_theme_checktheme(){
	
	$form = new PremiumForm();
	$params_form = array(
		'filter' => 'pn_theme_checktheme_option',
		'method' => 'post',
	);
	$form->init_form($params_form);
	
}	

add_action('premium_action_pn_theme_checktheme','def_premium_action_pn_theme_checktheme');
function def_premium_action_pn_theme_checktheme(){
global $wpdb;	

	only_post();

	pn_only_caps(array('administrator'));

	$themechange = get_option('themechange');
	
    $themechange['checktheme'] = pn_strip_input(is_param_post('checktheme'));
	
	update_option('themechange', $themechange);	
	
	$back_url = is_param_post('_wp_http_referer');
	$back_url .= '&reply=true';
			
	wp_safe_redirect($back_url);
	exit;	
}