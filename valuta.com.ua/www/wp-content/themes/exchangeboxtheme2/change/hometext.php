<?php  
if( !defined( 'ABSPATH')){ exit(); }

add_action('admin_menu', 'pn_adminpage_theme_hometext');
function pn_adminpage_theme_hometext(){
global $exchangebox;	
	
	add_submenu_page("pn_themeconfig", __('The text of the main','pntheme'), __('The text of the main','pntheme'), 'administrator', "pn_theme_hometext", array($exchangebox, 'admin_temp'));
}

add_action('pn_adminpage_title_pn_theme_hometext', 'pn_adminpage_title_pn_theme_hometext');
function pn_adminpage_title_pn_theme_hometext($page){
	_e('The text of the main','pntheme');
} 

add_filter('pn_theme_hometext_option', 'def_pn_theme_hometext_option', 1);
function def_pn_theme_hometext_option($options){
global $wpdb, $exchangebox;	
	
	$change = get_option('themechange');
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('The text of the main','pntheme'),
		'submit' => __('Save','pntheme'),
		'colspan' => 2,
	);
	$options['hometexttop'] = array(
		'view' => 'editor',
		'title' => __('Top text', 'pntheme'),
		'default' => is_isset($change,'hometexttop'),
		'name' => 'hometexttop',
		'work' => 'text',
		'rows' => 10,
		'media' => false,
	);
	$options['line1'] = array(
		'view' => 'line',
		'colspan' => 2,
	);		
	$options['hometitle'] = array(
		'view' => 'inputbig',
		'title' => __('Title', 'pntheme'),
		'default' => is_isset($change,'hometitle'),
		'name' => 'hometitle',
		'work' => 'input',
	);	
	$options['hometext'] = array(
		'view' => 'editor',
		'title' => __('Text', 'pntheme'),
		'default' => is_isset($change,'hometext'),
		'name' => 'hometext',
		'work' => 'text',
		'rows' => 10,
		'media' => false,
	);	
	
	return $options;	
} 

add_action('pn_adminpage_content_pn_theme_hometext','def_pn_adminpage_content_pn_theme_hometext');
function def_pn_adminpage_content_pn_theme_hometext(){

	$form = new PremiumForm();
	$params_form = array(
		'filter' => 'pn_theme_hometext_option',
		'method' => 'post',
	);
	$form->init_form($params_form);
	
}	

add_action('premium_action_pn_theme_hometext','def_premium_action_pn_theme_hometext');
function def_premium_action_pn_theme_hometext(){
global $wpdb;

	only_post();

	pn_only_caps(array('administrator'));	

	$themechange = get_option('themechange');
	
    $themechange['hometitle'] = pn_strip_input(is_param_post('hometitle'));
	$themechange['hometext'] = pn_strip_text(is_param_post('hometext'));
	$themechange['hometexttop'] = pn_strip_text(is_param_post('hometexttop'));
	
	update_option('themechange', $themechange);
	
	$back_url = is_param_post('_wp_http_referer');
	$back_url .= '&reply=true';
	
	wp_safe_redirect($back_url);
	exit;
}