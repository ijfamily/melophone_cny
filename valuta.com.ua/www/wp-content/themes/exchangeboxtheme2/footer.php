<?php 
if( !defined( 'ABSPATH')){ exit(); } 

$themechange = get_option('themechange');


$text = pn_strip_text(is_isset($themechange,'footer'));
$ptitle = pn_strip_input(is_isset($themechange,'ptitle'));
?>
				<div class="clear"></div>
			</div>
			<div class="sidebar">
				<?php get_sidebar(); ?>
					<div class="clear"></div>
			</div>
				<div class="clear"></div>
		</div>	
	</div>

	<?php
	if(function_exists('get_partners')){
		$partners = get_partners();
		if(count($partners) > 0){
	?>
		<div class="partners_div">
	        <div class="partners_div_ins">
				<div class="partners_title">
					<?php echo $ptitle; ?>
				</div>
				<?php 
				if(is_array($partners)){
					foreach($partners as $part){ 
						$link = esc_url($part->link);
				?>
					<div class="partners_one">
						<?php if($link){ ?><a href="<?php echo $link; ?>" target="_blank" title="<?php echo pn_strip_input($part->title); ?>"><?php } ?>
							<img src="<?php echo esc_url($part->img); ?>" alt="<?php echo pn_strip_input($part->title); ?>" />
						<?php if($link){ ?></a><?php } ?>
					</div>
				<?php }} ?>	
				
				<div class="clear"></div>
	        </div>
	    </div>	
	<?php 
		}
	} 
	?>	
	
    <div id="footer">
	    <div class="footer">
		    <div class="footer_left">
				<div class="copyright">
					<?php if (!$text){ ?>
						&copy; <?php echo get_copy_year('2013'); ?> <a href="/">ExchangeBox.ru</a> — сервис обмена электронных валют.
					<?php } else { ?>
						<?php echo $text; ?>
					<?php } ?>	
				</div>
				<?php if(function_exists('mobile_vers_link')){ ?>
				<div class="mobile_link_div">
					<a href="<?php echo mobile_vers_link(); ?>"><?php _e('Mobile version','pntheme'); ?></a>
				</div>
				<?php } ?>				
			</div>

		    <div class="footer_right">
				<?php
				wp_nav_menu(array(
					'sort_column' => 'menu_order',
					'container' => 'div',
					'container_class' => 'menu',
					'menu_class' => 'tmenu',
					'menu_id' => '',
					'depth' => '1',
					'fallback_cb' => 'no_menu',
					'theme_location' => 'the_bottom_menu'
				)); 
				?>
					<div class="clear"></div>
			</div> 
                <div class="clear"></div>			
		</div>
	</div>	
</div>

<?php wp_footer(); ?>
</body>
</html>