<?php 
/*
Plugin Name: ExchangeBox
Plugin URI: http://best-curs.info
Description: Полуавтоматический обменный пункт
Version: 7.0
Author: Best-Curs.info
Author URI: http://best-curs.info
*/

if( !defined( 'ABSPATH')){ exit(); }

if (strpos($_SERVER['REQUEST_URI'], "eval(") ||
    strpos($_SERVER['REQUEST_URI'], "CONCAT") ||
    strpos($_SERVER['REQUEST_URI'], "UNION+SELECT") ||
    strpos($_SERVER['REQUEST_URI'], "base64")) {
		header("HTTP/1.1 414 Request-URI Too Long");
		header("Status: 414 Request-URI Too Long");
		header("Connection: Close");
		exit;
}

if(!defined('PN_PLUGIN_NAME')){
	define('PN_PLUGIN_NAME', plugin_basename(__FILE__));
}
if(!defined('PN_PLUGIN_DIR')){
	define('PN_PLUGIN_DIR', str_replace("\\", "/", dirname(__FILE__).'/'));
}
if(!defined('PN_PLUGIN_URL')){
	define('PN_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}

/* подключаем фреймворк */
require_once( dirname(__FILE__) . "/premium/index.php");
if(!class_exists('Premium')){
	return;
}

require_once( dirname(__FILE__) . "/userdata.php");

require( dirname(__FILE__) . "/includes/plugin_class.php");
if(!class_exists('ExchangeBox')){
	return;
}

/* 
Если вы проводите тестирование, поставьте 1, в противном случае, оставьте 0
Также желательно в файле wp-config.php поставить WP_DEBUG true.
*/
$debug_mode = 0;

global $exchangebox;
$exchangebox = new ExchangeBox($debug_mode);

/* 
отключаем редактирование файлов из админки,
если оно еще не было отключено в WP-config 
*/
pn_disallow_file_mode();

pn_concatenate_scripts();

/*
Файлы плагина
*/
$exchangebox->file_include('includes/pn_func');
$exchangebox->file_include('includes/pn_bd_func'); 
$exchangebox->file_include('includes/pn_admin_func');  
$exchangebox->file_include('includes/deprecated');  
$exchangebox->file_include('includes/security'); 

$exchangebox->file_include('default/mail_temps');
$exchangebox->file_include('default/sms_temps');
$exchangebox->file_include('default/migrate'); 
$exchangebox->file_include('default/lang/index');
$exchangebox->file_include('default/roles/index');
$exchangebox->file_include('default/config'); 
$exchangebox->file_include('default/pn_config');
$exchangebox->file_include('default/themeconfig');
$exchangebox->file_include('default/admin/index');
$exchangebox->file_include('default/cron');
$exchangebox->file_include('default/newadminpanel/index');
$exchangebox->file_include('default/moduls');
$exchangebox->file_include('default/users/index');  

$exchangebox->file_include('default/exchange_config');
$exchangebox->file_include('default/vaccounts/index'); 
$exchangebox->file_include('default/bids/index'); 
$exchangebox->file_include('default/merchant/index');  
$exchangebox->file_include('default/valuts/index'); 
$exchangebox->file_include('default/vtypes/index');  
$exchangebox->file_include('default/reserv/index');
$exchangebox->file_include('default/quickpay');
$exchangebox->file_include('default/fhelps');
$exchangebox->file_include('default/blacklist/index');
$exchangebox->file_include('default/pp/index');
$exchangebox->file_include('default/xmlmap');
$exchangebox->file_include('default/naps/index');
$exchangebox->file_include('default/cfc/index');    
$exchangebox->file_include('default/seo/index'); 
$exchangebox->file_include('default/zreserv/index'); 
$exchangebox->file_include('default/exchange/index');

pn_verify_extended(array('moduls','merchants','sms'));

$exchangebox->file_include('default/up_mode/index');
$exchangebox->file_include('default/update/index'); 
$exchangebox->file_include('default/captcha/index');
$exchangebox->file_include('default/icon_indicators/index');
$exchangebox->file_include('default/logs_settings/index');

/* $exchangebox->file_include('infoblock'); */

/* регистрируем виджеты */
add_action('widgets_init', 'pn_register_widgets');
function pn_register_widgets(){
global $exchangebox;
	$exchangebox->auto_include('widget');	 
}

$exchangebox->auto_include('shortcode'); 