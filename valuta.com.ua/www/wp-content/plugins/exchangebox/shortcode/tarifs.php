<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('siteplace_js','siteplace_js_tarifs');
function siteplace_js_tarifs(){	
?>	
/* tarifs */
jQuery(function($){
	$('.javahref').on('click', function(){
	    var the_link = $(this).attr('name');
	    window.location = the_link;
	});
});		
/* end tarifs */
<?php	
} 

function tarifs_shortcode($atts, $content) {
global $wpdb, $exchangebox;
        
	$temp = '';
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
		
	$techreg = get_technical_mode(); 
	if($techreg == 0 or $techreg == 2 or $techreg == 3){ 
		$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide')); 
		if($gostnaphide != 1 or $gostnaphide == 1 and $user_id){		
		
			$v = get_valuts_data();

			$where = get_naps_where('tar');
			$napobmens = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC");
			$naps = array();
			foreach($napobmens as $napob){
				$naps[$napob->valsid1] = $napob;
			}

			$napobmens2 = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder2 ASC");
			$napsto = array();
			foreach($napobmens2 as $napob){
				$napsto[$napob->valsid1][] = $napob;
			}		

			$temp .= apply_filters('exb_before_tarifs_page','');	
		
			$temp .='<div class="tarifblock">
			<div class="tarifblockvn">
			';	
		
			foreach($naps as $napob){
				$valid = $napob->valsid1;
				$temp .='
				<div class="tarifbl"><div class="tarifblvn">	
					<div class="tariftitle">
						<div class="tariftitlevn">
							'. pn_strip_input($v[$valid]->vname .' '. $v[$valid]->vtype) .'
						</div><div class="clear"></div>
					</div><div class="clear"></div>
				';
				
				$temp .='
				<table class="tariftable">
					<tr>
						<th colspan="3" class="first tathotd">'. __('Giving','pn') .'</th>
						<th colspan="2" class="tathpoluch">'. __('Receive','pn') .'</th>
						<th class="last tathrezerv">'. __('Reserve','pn') .'</th>
					</tr>';	
					
					$tarifs = is_isset($napsto,$valid);
					if(is_array($tarifs)){
						foreach($tarifs as $tar){
							$valsid1 = $tar->valsid1;
							$valsid2 = $tar->valsid2;
							
							$vd1 = $v[$valsid1];
							$vd2 = $v[$valsid2];
							
							$temp .='
						    <tr class="javahref" name="'. get_exchange_link($vd1->xname, $vd2->xname) .'">
							    
								<td class="tacursotd"><div>'. is_out_sum(is_sum($tar->curs1, 12), 3, 'course') .'&nbsp;'. pn_strip_input($vd1->vtype) .'</div></td>
							    <td class="tacursonetitle"><div>
                                <div class="obmenline">
				                    <div class="obmenlineico" style="background: url('. is_ssl_url($vd1->vlogo) .') no-repeat center center"></div>
					                <div class="obmenlinetext">
					                    '. pn_strip_input($vd1->vname .' '. $vd1->vtype) .'
					                </div>
				                        <div class="clear"></div>
				                </div>							
							    </div></td>
							    <td class="tavnstr"><div></div></td>
							    <td class="tacursotd"><div>'. is_out_sum(is_sum($tar->curs2, 12), 3, 'course') .'&nbsp;'. pn_strip_input($vd2->vtype) .'</div></td>
							    <td><div class="javahreftd">
                                <div class="obmenline">
				                    <div class="obmenlineico" style="background: url('. is_ssl_url($vd2->vlogo) .') no-repeat center center"></div>
					                <div class="obmenlinetext">
					                    '. pn_strip_input($vd2->vname .' '. $vd2->vtype) .'
					                </div>
				                        <div class="clear"></div>
				                </div>							
							    </div></td>
							    <td class="tarezervs"><div>'. is_out_sum(get_valut_reserv($vd2), 2, 'reserv') .'</div></td>';
						    
							$temp .='</tr>';
						}
					}
					
				$temp .='</table>';
				$temp .='</div></div>';
			}		
	
			$temp .='</div></div>';
	
			$temp .= apply_filters('exb_after_tarifs_page','');	
	
		} else {
			$temp = '<div class="resultfalse">'. __('directions are available only to authorized users','pn') .'</div>';
		}	 
	} else {
		$techregtext = get_technical_mode_text();
		$temp = '<div class="resultfalse">'. $techregtext .'</div>';
	}	
	
	return $temp;
}
add_shortcode('exb_tarifs', 'tarifs_shortcode');

add_filter('get_naps_where', 'get_naps_where_tarifs', 10, 2);
function get_naps_where_tarifs($where, $place){
	if($place == 'tar'){
		$ui = wp_get_current_user();
		$user_id = intval($ui->ID);
		if(!$user_id){ $where .= "AND hidegost IN('0','2') "; }
	}
	
	return $where;
}