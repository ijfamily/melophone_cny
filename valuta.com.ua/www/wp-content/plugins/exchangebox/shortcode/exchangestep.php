<?php
if( !defined( 'ABSPATH')){ exit(); } 

add_action('template_redirect','bids_initialization');
function bids_initialization(){
global $wpdb, $bids_id, $bids_data;

	$bids_id = 0;
	$bids_data = array();

	$hashed = is_bid_hash(get_query_var('hashed'));
	if($hashed){
		$data = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed'");
		if(isset($data->id)){
			$bids_data = $data;
			$bids_id = $data->id;
		}
	} 
	
	$bids_data = (object)$bids_data;
}

add_action('wp_before_admin_bar_render', 'wp_before_admin_bar_render_exchangestep');
function wp_before_admin_bar_render_exchangestep() {
global $wp_admin_bar, $bids_data;
	
    if(current_user_can('administrator')){
		if(!is_admin()){
			if(isset($bids_data->id)){
				$wp_admin_bar->add_menu( array(
					'id'     => 'show_bids',
					'href' => admin_url('admin.php?page=pn_bids&bidid='.$bids_data->id),
					'title'  => __('Go to bid','pn'),	
				));	
				$wp_admin_bar->add_menu( array(
					'id'     => 'edit_naps',
					'parent' => 'show_bids',
					'href' => admin_url('admin.php?page=pn_add_naps&item_id='.$bids_data->naprid),
					'title'  => __('Edit direction exchange','pn'),	
				));				
				
				$valut1 = pn_strip_input($bids_data->valut1).' '.pn_strip_input($bids_data->valut1type);
				$valut2 = pn_strip_input($bids_data->valut2).' '.pn_strip_input($bids_data->valut2type);
				
				$wp_admin_bar->add_menu( array(
					'id'     => 'edit_valut1',
					'parent' => 'show_bids',
					'href' => admin_url('admin.php?page=pn_add_valuts&item_id='.$bids_data->valut1i),
					'title'  => sprintf(__('Edit "%s"','pn'), $valut1),	
				));
				$wp_admin_bar->add_menu( array(
					'id'     => 'edit_valut2',
					'parent' => 'show_bids',
					'href' => admin_url('admin.php?page=pn_add_valuts&item_id='.$bids_data->valut2i),
					'title'  => sprintf(__('Edit "%s"','pn'), $valut2),	
				));			
			}
		}
	}
}

add_action('siteplace_js','siteplace_js_exchange_checkrule');
function siteplace_js_exchange_checkrule(){
?>
jQuery(function($){
	
	$('#check_rule_step').on('click', function(){
		if($('#createzajatos').prop('checked')){
			$('#createzaja').prop('disabled',true);
		} else {
			$('#createzaja').prop('disabled',false);
		}		
	});	
	
	$(document).on('click', '.iampay', function(){
		if (!confirm("<?php echo esc_attr(__('Are you sure you pay a claim?','pn')); ?>")) {
			return false;
		}
	});		
			
});		
<?php 
} 

add_action('siteplace_js','siteplace_js_exchange_timer');
function siteplace_js_exchange_timer(){
?>
jQuery(function($){
	
	if($('#check_payment_div').length > 0){
		var nowdata = 0;
		var redir = 0;
		function check_payment_now(){	
			nowdata = parseInt(nowdata) + 1;
			if(redir == 0){
				if(nowdata > 30){
					var durl = $('#check_payment_div').attr('data-url');
					window.location.href = durl;
					redir = 1;
				}	
			}
		}
		setInterval(check_payment_now,1000);
	}
	
});		
<?php 
}

function exchangestep_page_shortcode($atts, $content){
global $wpdb, $bids_data;
	
	$temp = '<div class="resultfalse">'. __('Error! Application does not exist','pn') .'</div>';

	if(isset($bids_data->id)){
		$temp = apply_filters('exb_before_xchangestep2_form','');
			
		$temp .= '
		<div class="xchangestep2div">
			<div class="xchangestep2divvn">
				<h1 class="xchangestep2title js_exchangestep_title">'. get_exchangestep_title() .'</h1>
			</div>
				<div class="clear"></div>
		</div>		
			<div class="clear"></div>
		';
			
		$temp .= apply_filters('exchangestep_'. is_status_name($bids_data->status), '', $bids_data); 
			
		$temp .= apply_filters('exb_after_xchangestep2_form','');	
	}	
	
	return $temp;
}
add_shortcode('exchangestep', 'exchangestep_page_shortcode');

add_filter('exchangestep_auto','get_exchangestep_auto',1,2);
function get_exchangestep_auto($temp, $item){
global $wpdb, $exchangebox;
	
    $temp = '';
	if(isset($item->id)){
		
		$id = intval($item->id);
		$naps_id = intval($item->naprid);
		
		$valut1 = intval($item->valut1i);
		$valut2 = intval($item->valut2i);
		
		$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1'");
		$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut2'");
	
		$naps = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."napobmens WHERE id='$naps_id' AND status='1'");	
		
		if(!isset($naps->id) or !isset($vd1->id) or !isset($vd2->id)){
			return '<div class="resultfalse">'. __('Exchange direction is disabled','pn') .'</div>';
		}		
		
		$txt1 = pn_strip_input($vd1->txt1);
		if(!$txt1){ $txt1 = __('From the account','pn'); }
		
		$txt2 = pn_strip_input($vd2->txt2);
		if(!$txt2){ $txt2 = __('At the expense','pn'); }
		
		$dmetas = @unserialize($item->dmetas);	
		
		$status = is_status_name($item->status);
		
		$temp .= '
		<div class="xchangestep2body">
			<div class="xchangestep2bodyvn">			
		';
			$temp .= '
			<div class="stepblock">
				<div class="steptitle">'. __('Giving','pn') .' <span class="steptitlemin">'. __('In view of the commission payment system','pn') .' <b>'. pn_strip_input($vd1->vname) .'</b></span></div>
				<div class="stepblleft">
				
					<p><span class="steptitsumm">'. __('Sum','pn') .':</span> <b>'. pn_strip_input($item->summz1) .'</b> <span class="steptitvtype">'. pn_strip_input($vd1->vname .' '. $vd1->vtype) .'</span></p>
					';
					
					if($item->schet1){
						$temp .='<p><span class="steptitsumm">'. $txt1 .':</span> <b>'. get_secret_value($item->schet1, $exchangebox->get_option('exchange','an1_hidden')) .'</b></p>';
					}

					if(isset($dmetas[1]) and is_array($dmetas[1])){
						foreach($dmetas[1] as $value){	
							$title = pn_strip_input(is_isset($value,'title'));
							$data = pn_strip_input(is_isset($value,'data'));
							$hidden = intval(is_isset($value,'hidden'));
							if(trim($data)){
								$temp .= '<p><span class="steptitsumm">'. $title .':</span> '. get_secret_value($data, $hidden) .'</p>';
							}
						}
					}										
					
					$temp .= '
				</div>
				<div class="stepblright">
					<div class="stepicotext">'. pn_strip_input($vd1->vname .' '. $vd1->vtype) .'</div>
					<div class="stepico" style="background: url('. is_ssl_url($vd1->vlogo) .') no-repeat center center;"></div>
						<div class="clear"></div>
				</div>
					<div class="clear"></div>
			</div>
			';
			
			$temp .= '
			<div class="stepblock">
				<div class="steptitle">'. __('Receive','pn') .' <span class="steptitlemin">'. __('In view of the commission payment system','pn') .' <b>'. pn_strip_input($vd2->vname) .'</b></span></div>
				<div class="stepblleft">
					
					<p><span class="steptitsumm">'. __('Sum','pn') .':</span> <b>'. pn_strip_input($item->summz2) .'</b> <span class="steptitvtype">'. pn_strip_input($vd2->vname .' '. $vd2->vtype) .'</span></p>
					';
					
					if($item->schet2){
						$temp.='<p><span class="steptitsumm">'. $txt2 .':</span> <b>'. get_secret_value($item->schet2, $exchangebox->get_option('exchange','an2_hidden')) .'</b></p>';
					}

					if(isset($dmetas[2]) and is_array($dmetas[2])){
						foreach($dmetas[2] as $value){
										
							$title = pn_strip_input(is_isset($value,'title'));
							$data = pn_strip_input(is_isset($value,'data'));
							$hidden = intval(is_isset($value,'hidden'));
							if(trim($data)){
								$temp .= '<p><span class="steptitsumm">'. $title .':</span> '. get_secret_value($data, $hidden) .'</p>';
							}
						}
					}					
					
					$temp .= '
				</div>
				<div class="stepblright">
					<div class="stepicotext">'. pn_strip_input($vd2->vname .' '. $vd2->vtype) .'</div>
					<div class="stepico" style="background: url('. is_ssl_url($vd2->vlogo) .') no-repeat center center;"></div>				
				
					<div class="clear"></div>
				</div>				
					<div class="clear"></div>
			</div>
			';

			$temp .= '
			<div class="stepblock lichdann">
				<div class="steptitle">'. apply_filters('exchnage_personaldata_title',__('Personal data','pn')) .'</div>
				
				<div class="lichdannvn">
				';
				
				$last_name = pn_strip_input($item->fname);
				$first_name = pn_strip_input($item->iname);
				$second_name = pn_strip_input($item->oname);
				$hvid = $exchangebox->get_option('exchange','an3_hidden'); 
				
				if($last_name){ 
					$temp .='
					<div class="stepline"><span class="step2name">'. __('Last name','pn') .':</span> '. get_secret_value($last_name,$hvid) .'</div>
					';
				}
				if($first_name){ 
					$temp .='
					<div class="stepline"><span class="step2name">'. __('First name','pn') .':</span> '. get_secret_value($first_name,$hvid) .'</div>
					';
				}			
				if($second_name){ 
					$temp .='
					<div class="stepline"><span class="step2name">'. __('Second name','pn') .':</span> '. get_secret_value($second_name,$hvid) .'</div>
					';
				}
				
				$temp .='
					<div class="stepline"><span class="step2name">'. __('E-mail','pn') .':</span> '. get_secret_value($item->email,$hvid) .'</div>
				';
				
				if($item->tel){
					$temp  .='
					<div class="stepline"><span class="step2name">'. __('Phone','pn') .':</span> '. get_secret_value($item->tel,$hvid) .'</div>
					';
				}
				if($item->skype){
					$temp  .='
					<div class="stepline"><span class="step2name">'. __('Skype','pn') .':</span> '. get_secret_value($item->skype,$hvid) .'</div>
					';
				}
				if($item->pnomer){
					$temp  .='
					<div class="stepline"><span class="step2name">'. __('Passport','pn') .':</span> '. get_secret_value($item->pnomer,$hvid) .'</div>
					';
				}				

				$temp .= apply_filters('exb_obmen_site_ld','',$item);
					
				$temp .='	
				</div>
			</div>
			';		

			$temp .= '
			<div class="stepcheckbox">
		        <div class="checkbox" id="check_rule_step"><input type="checkbox" id="createzajatos" name="" value="1" /> '. sprintf(__('With the <a href="%s">rules</a> of service read and agree','pn'), $exchangebox->get_page('tos') ) .'</div>
		    </div>';
			
			$temp .= '
			<div class="biginfo">
			    <div class="biginfovn">
				    <div class="binfotitle">'. __('Attention!','pn') .'</div>
				    '. __('The service after clicking "Create request" means that you accept all the basic requirements for the users of the service.','pn') .'
			    </div>
			</div>';			
			
			$temp .= '
		    <div class="stepwarning">'. __('Carefully check the data before making payment requests!','pn') .'</div>
			';
			
			$temp .= '
            <form action="'. get_ajax_link('createbids') .'" class="ajax_post_form" method="post">
				<input type="hidden" name="hash" value="'. is_bid_hash($item->hashed) .'" />
				<div class="step2submit">
		            <input type="submit" disabled="disabled" name="" formtarget="_top" id="createzaja" value="'. __('Create request','pn') .'" />
		        </div>

				<div class="ajax_post_bids_res" style="padding: 10px 0px;">
					<div class="resultgo"></div>
				</div>				
            </form>			
			';				
		
		$temp .= '
			</div>
		</div>';	
	}
	
	return $temp;
}

add_filter('exchangestep_new','get_exchangestep_new',1,2);
function get_exchangestep_new($temp, $item){
global $wpdb, $exchangebox;
	
	$temp = '';
	if(isset($item->id)){
		
		$id = intval($item->id);
		$naps_id = intval($item->naprid);
		
		$valut1 = intval($item->valut1i);
		$valut2 = intval($item->valut2i);
		
		$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1'");
		$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut2'");
	
		$naps = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."napobmens WHERE id='$naps_id' AND status='1'");	
		
		if(!isset($naps->id) or !isset($vd1->id) or !isset($vd2->id)){
			return '<div class="resultfalse">'. __('Exchange direction is disabled','pn') .'</div>';
		}		
	
		$status = is_status_name($item->status);
		
		$xzt = apply_filters('get_merchant_id', '', $vd1->xzt, $item);		
		$xzt = is_extension_name($xzt);		
	
		$gmerchant = 0;
		$gmerchant = apply_filters('exchangebox_merchant_gmerchant', $gmerchant, $xzt);	
	
		$temp .= '
		<div class="xchangestep3body block_status_new">
			<div class="xchangestep3bodyvn">
		';
	
			$text = trim(get_naps_txtmeta($naps_id, 'timeline_txt'));
			if(trim($text)){
			    $temp .= '
			    <div class="biginfo">
					<div class="biginfovn">
				        <div class="binfotitle">'. __('Attention!','pn') .'</div>
						'. apply_filters('the_content',$text) .'
					</div>
				</div>';
			}			
	
			$status_text = pn_strip_input($exchangebox->get_option('naps_status', 'status_' . $status));
			$instruction = trim(get_naps_txtmeta($naps_id, 'status_new'));
			$instruction = apply_filters('instruction_merchant', $instruction, $xzt, $item);	
	
			$def_status = array(
				'new' => array(
					'text' => __('accepted, waiting for pay','pn'),
				),
			);	
			if(!$status_text and isset($def_status[$status]['text'])){
				$status_text = $def_status[$status]['text'];
			}
		
			if($instruction){
				$temp .= '
				<div class="merchantinst">
					<div class="merchantinstvn">
						<h3>'. __('How to pay','pn') .':</h3>
						'. apply_filters('the_content',$instruction) .'
							<div class="clear"></div>
					</div>
				</div>';
			}	
			
			$sumpay = apply_filters('summ_to_pay', $item->summ1, $xzt, $item);
	
			$date_format = get_option('date_format');
			$time_format = get_option('time_format');		
			$status_date = get_mytime($item->tdate, "{$date_format}, {$time_format}");	
	
			$temp .= '
				<div class="plategblock">
					<span class="plblockvid">'. __('Amount of payment','pn') .':</span> <span class="plblocksumm">'. pn_strip_input($sumpay) .'</span> <span class="plblocktype">'. pn_strip_input($item->valut1type) .'</span>
				</div>
			    <div class="zawarntext">
			        <p><span class="red">'. __('Please be careful!','pn') .'</span> '. __('All fields must be filled out in strict accordance with the instructions, otherwise the payment can not pass.','pn') .'</p>
			    </div>
			    <div class="statusza">
			        <div class="zadate">'. __('Time of registration','pn') .': '. $status_date .'</div>
				    <p><span class="newstatbo">'. __('Bid status','pn') .':</span> '. $status_text .'</p>
			    </div>						
			';	
	
			if(is_true_userhash($item)){
	
				$temp .= '
				<div class="oplblockstep">
					<div class="oplblockstepleft">
						<form action="'. get_ajax_link('deletebids') .'" method="post">	
							<input type="hidden" name="hash" value="'. is_bid_hash($item->hashed) .'" />
							<input type="submit" name="delete" formtarget="_top" class="rsubmitdel" value="'. __('Cancel the bid','pn') .'" />	
						</form>
					</div>
					<div class="oplblockstepright">';
						
						$paybutton = '
						<form action="'. get_ajax_link('payedbids') .'" method="post">
							<input type="hidden" name="hash" value="'. is_bid_hash($item->hashed) .'" />
							<input type="submit" name="payed" formtarget="_top" class="iampay" value="'. __('I paid','pn') .'" />
						</form>';
						
						$paybutton = apply_filters('exchangebox_merchant_paybutton_'. $xzt, $paybutton, $naps, $item);
						$temp .= apply_filters('exchangebox_merchant_paybutton', $paybutton, $xzt, $naps, $item);
					
					$temp .= '
					</div>
						<div class="clear"></div>	
				</div>					
				';	
			
			} else {
				
				$temp .= '<div class="block_change_browse" style="padding: 10px 0 0 0; color: #ff0000; text-align: center;">'. __('Error! you can not control the application, as another browser','pn') .'</div>';
				
			}
	
			$temp .= apply_filters('exchangebox_merchant_xchanger_after', '', $xzt, $naps, $item);
	
			if($gmerchant == 1){
				if(isset($_GET['auto_check'])){
					$temp .= '
					<div class="block_check_payment" id="check_payment_div" data-url="'. get_bids_url($item->hashed) .'?auto_check=true"></div>
					<div style="padding: 10px 0 0 0;">
						<div class="zayastatus" style="margin-bottom: 10px;">
							<p>'. __('Every 30 seconds there is a check payment.','pn') .'</p>
						</div>			
						
						<form action="'. get_bids_url($item->hashed) .'" method="get">
							<input type="submit" name="" formtarget="_top" style="display: block; margin: 0 auto;" value="'. __('Disable check payment','pn') .'" />
						</form>
					</div>					
					';
				} else {
					$temp .='
					<div style="padding: 10px 0 0 0;">
						<div class="zayastatus" style="margin-bottom: 10px;">
							<p>'. __('Attention! Click "Check for Payment", if you have paid the application.','pn') .'</p>
							<p>'. __('Every 30 seconds there is a check payment.','pn') .'</p>
						</div>			
						
						<form action="'. get_bids_url($item->hashed) .'" method="get">
							<input type="hidden" name="auto_check" value="true" />
							<input type="submit" name="" formtarget="_top" style="display: block; margin: 0 auto;" value="'. __('Check your payment','pn') .'" />
						</form>
					</div>					
					';
				}
			}
	
		$temp .= '
			</div>
		</div>';	
	}
	
	return $temp;
}

add_filter('exchangestep_error','get_exchangestep_other_satus',1,2);
add_filter('exchangestep_delete','get_exchangestep_other_satus',1,2);
add_filter('exchangestep_payed','get_exchangestep_other_satus',1,2);
add_filter('exchangestep_realpay','get_exchangestep_other_satus',1,2);
add_filter('exchangestep_success','get_exchangestep_other_satus',1,2);
add_filter('exchangestep_returned','get_exchangestep_other_satus',1,2);
add_filter('exchangestep_verify','get_exchangestep_other_satus',1,2);
function get_exchangestep_other_satus($temp, $item){
global $wpdb, $exchangebox;
	
	$temp = '';
	
	if(isset($item->id)){
	
		$id = intval($item->id);
		$naps_id = intval($item->naprid);
		
		$valut1 = intval($item->valut1i);
		$valut2 = intval($item->valut2i);
		
		$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1'");
		$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut2'");
	
		$naps = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."napobmens WHERE id='$naps_id' AND status='1'");	
		
		if(!isset($naps->id) or !isset($vd1->id) or !isset($vd2->id)){
			return '<div class="resultfalse">'. __('Exchange direction is disabled','pn') .'</div>';
		}		
	
		$status = is_status_name($item->status);
		
		$autocheck = intval($exchangebox->get_option('naps_timer', 'status_'.$status));
		$status_text = pn_strip_input($exchangebox->get_option('naps_status', 'status_'.$status));
		$instruction = trim(get_naps_txtmeta($naps_id, 'status_'.$status));

		$tml = 0;
		if($status == 'realpay' or $status == 'payed'){
			$tml = 1;
		} 							
		
		$def_status = array(	
			'payed' => array(
				'text' => __('Received confirmation of payment from the client','pn'),
			),
			'verify' => array(
				'text' => __('Bid paid','pn'),
			),
			'realpay' => array(
				'text' => __('Bid paid','pn'),
			),			
			'delete' => array(
				'text' => __('Bid deleted','pn'),
			),
			'returned' => array(
				'text' => __('money will be returned','pn'),
			),				
			'error' => array(
				'text' => __('Error','pn'),
			),
			'success' => array(
				'text' => __('Bid completed','pn'),
			),	
		);		
		
		if(!$status_text and isset($def_status[$status]['text'])){
			$status_text = $def_status[$status]['text'];
		}		
		
		$comment_user = trim($item->ucomments);
		if($comment_user){
			if($instruction){
				$instruction .= '<br />';
			}
			$instruction .= $comment_user;
		}
		
		$account1 = $account2 = '';
		
		$temp .= '
		<div class="xchangestep3body block_status_'. $status .'">
			<div class="xchangestep3bodyvn">
		';		
		
			$timeline = trim(get_naps_txtmeta($naps_id, 'timeline_txt'));
			if($timeline and $tml){
			    $temp .= '
			        <div class="biginfo">
					<div class="biginfovn">
				        <div class="binfotitle">'. __('Attention!','pn') .'</div>
						'. apply_filters('the_content',$timeline) .'
					</div>
				</div>';
			}

			if($instruction){
				$temp .= '
				<div class="merchantinst">
					<div class="merchantinstvn">
						'. apply_filters('the_content',$instruction) .'
							<div class="clear"></div>
					</div>
				</div>';
			}		
		
			if($item->schet1){
				$txt = pn_strip_input(is_isset($vd1,'txt1'));
				if(!$txt){ $txt = __('From the account','pn'); }
									
				$account1 = ', '. $txt .': <span class="statuszaschet">'. get_secret_value($item->schet1, $exchangebox->get_option('exchange','an1_hidden')) .'</span>';	
			}					
				
			if($item->schet2){
				$txt = pn_strip_input(is_isset($vd2,'txt2'));
				if(!$txt){ $txt = __('At the expense','pn'); }
									
				$account2 = ', '. $txt .': <span class="statuszaschet">'. get_secret_value($item->schet2, $exchangebox->get_option('exchange','an2_hidden')) .'</span>';	
			}	

			$date_format = get_option('date_format');
			$time_format = get_option('time_format');		
			$status_date = get_mytime($item->tdate, "{$date_format}, {$time_format}");				
			
		    $temp .= '
			<div class="statuszadel topped"> 
			    <p><span class="statuszabolds">'. __('Giving','pn') .':</span> <span class="statuszasumm">'. pn_strip_input($item->summz1) .'</span> '. pn_strip_input($item->valut1) .' '. pn_strip_input($item->valut1type) .' '. $account1 .'</p>
				<p><span class="statuszabolds">'. __('Receive','pn') .':</span> <span class="statuszasumm">'. pn_strip_input($item->summz2) .'</span> '. pn_strip_input($item->valut2) .' '. pn_strip_input($item->valut2type) .' '. $account2 .'</p>
			</div>			
			
			<div class="statuszadel">
			    <div class="zadate">'. __('Time of registration','pn') .': '. $status_date .'</div>
				<p><span class="statuszabolds">'. __('Bid status','pn') .':</span> <span class="statuszay">'. $status_text .'</span></p>
			</div>				
			';			
		
		$temp .= '
			</div>
		</div>
		';
		
		if($autocheck == 1){
			$temp .= '
            <script type="text/javascript"> 
				function fresh() {
					window.location.href="'. get_bids_url($item->hashed) .'";
				}
				setInterval("fresh()",30000);
            </script>			
			';
		}
	}
	
	return $temp;
}		