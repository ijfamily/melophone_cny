<?php
if( !defined( 'ABSPATH')){ exit(); } 

add_action('template_redirect','naps_initialization',0);
function naps_initialization($title){
global $wpdb, $naps_id, $naps_data;

	$naps_id = 0;
	$naps_data = array();

	$site_value1 = is_site_value(get_query_var('valut1'));
	$site_value2 = is_site_value(get_query_var('valut2'));
	if(is_exchange_page() and $site_value1 and $site_value2){
		
		$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE vactive='1' AND xname='$site_value1'");
		$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE vactive='1' AND xname='$site_value2'");
		
		if(isset($vd1->id) and isset($vd2->id)){
			$valut_id1 = intval($vd1->id);
			$valut_id2 = intval($vd2->id);
			$where = get_naps_where('exchange');
			$naps = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."napobmens WHERE valsid1='$valut_id1' AND valsid2='$valut_id2' AND $where");
			if(isset($naps->id)){
				
				$naps_id = intval($naps->id);
				$naps_data['item1'] = pn_strip_input($vd1->vname .' '. $vd1->vtype);
				$naps_data['item2'] = pn_strip_input($vd2->vname .' '. $vd2->vtype);
				$naps_data['valut1'] = $vd1->id;
				$naps_data['valut2'] = $vd2->id;
				$naps_data['vd1'] = $vd1;
				$naps_data['vd2'] = $vd2;
				$naps_data['direction'] = $naps;
				
			}
		}
		
	} 
	
	$naps_data = (object)$naps_data;
	
	return $title;
}

add_action('wp_before_admin_bar_render', 'wp_before_admin_bar_render_naps');
function wp_before_admin_bar_render_naps(){
global $wp_admin_bar, $naps_id, $naps_data;
	
    if(current_user_can('administrator')){
		if(!is_admin()){
			if(is_exchange_page()){
				$wp_admin_bar->add_menu( array(
					'id'     => 'edit_naps',
					'href' => admin_url('admin.php?page=pn_add_naps&item_id='.$naps_id),
					'title'  => __('Edit direction exchange','pn'),	
				));	
				$wp_admin_bar->add_menu( array(
					'id'     => 'edit_valut1',
					'parent' => 'edit_naps',
					'href' => admin_url('admin.php?page=pn_add_valuts&item_id='.$naps_data->valut1),
					'title'  => sprintf(__('Edit "%s"','pn'), $naps_data->item1),	
				));
				$wp_admin_bar->add_menu( array(
					'id'     => 'edit_valut2',
					'parent' => 'edit_naps',
					'href' => admin_url('admin.php?page=pn_add_valuts&item_id='.$naps_data->valut2),
					'title'  => sprintf(__('Edit "%s"','pn'), $naps_data->item2),	
				));				
			}
		}
	}

}

/* добавляем JS */ 
add_action('siteplace_js','siteplace_js_exchange_step1');
function siteplace_js_exchange_step1(){
?>	
jQuery(function($){

    $(document).on('click', '.rselected', function(){
	    $('.rselected').removeClass('act');
		$(this).addClass('act');		
        return false;
    });
	
    $(document).on('click',function(event) {
        if ($(event.target).closest(".rselected").length) return;
        $(".rselected").removeClass('act');
        event.stopPropagation();
    });	
	
function big_ajax_zapros(ids,idtab,idtab2){
	var dataString = 'ids='+ids+'&idtab='+idtab+'&idtab2='+idtab2;
    $.ajax({
        type: "POST",
        url: "<?php echo get_ajax_link('exchange_step1'); ?>",
        data: dataString,
	    dataType: 'json',
		error: function(res, res2, res3){
			<?php do_action('pn_js_error_response', 'ajax'); ?>
		},					
        success: function(res){
		    if(res['status'] == 'success'){
			    $('title').html(res['title']);
				$('#ajaxtitlepager, .js_exchangestep_title').html(res['title2']);
				$('#naprtext1').html(res['html1']);
				$('#naprtext2').html(res['html2']);
				$('#ajaxobmen1').html(res['ajaxobmen1']);
				$('#ajaxobmen2').html(res['ajaxobmen2']);
				$('#persdata_block').html(res['persdata']);
				$('.js_naps_id').attr('value', res['idobmen']);
				var thelink = res['thelink'];
				if(thelink){
					window.history.replaceState(null, null, thelink);
				}
			} else {
			    alert(res['text']);
			}
        }
    }); 	
}		
	
    $(document).on('click', '.rselectlinemenu .rselectline', function(){
	    $(this).parents('.rselectlinemenu').find('.rselectline').removeClass('cur');
	    $(this).addClass('cur');
        $(this).parents('.rselected').removeClass('act');
		var num = $.trim($(this).attr('name'));
		var idtab = $(this).parents('.rselected').attr('name');
        $(this).parents('.rselected').find('input').val(num);
		var thethtm = $(this).html();
		$(this).parents('.rselected').find('.rselecttitlevn').html(thethtm);

		if(idtab==1){
		    var num1 = num;
			var num2 = $('.rselected:last').find('input').val();
		} else {
		    var num2 = num;
			var num1 = $('.rselected:first').find('input').val();		
		}
		
		big_ajax_zapros(idtab, num1, num2);
		
        return false;
    });	
	
});
<?php	
}
/* end добавляем JS */

add_action('myaction_site_exchange_step1', 'def_myaction_ajax_exchange_step1');
function def_myaction_ajax_exchange_step1(){
global $wpdb, $exchangebox, $naps_id, $naps_data;	
	
	$log = array();
	$log['status'] = '';
	$log['html1'] = $log['html2'] = $log['ajaxobmen1'] = $log['ajaxobmen2'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = __('Error','pn');		
	
	$exchangebox->up_mode();
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	$techreg = get_technical_mode();
	if($techreg == 0 or $techreg == 2 or $techreg == 3){ /* тех. режим */
		$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide')); 
		if($gostnaphide != 1 or $gostnaphide == 1 and $user_id){ /* видимость для гостей */	
		
			$id = intval(is_param_post('ids'));
			$v1 = intval(is_param_post('idtab'));
			$v2 = intval(is_param_post('idtab2'));		
		
			if($v1 and $v2){
				$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$v1'");
				$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$v2'");				
				if(isset($vd1->id) and isset($vd2->id)){
					$valut1 = $vd1->id;
					$valut2 = $vd2->id;					

					$v = array(); /* массив валют с данными */
					$valutsn = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts");
					foreach($valutsn as $valut){
						$v[$valut->id] = $valut;
					}					
					
					$where = get_naps_where('exchange');
					$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE valsid1='$v1' AND valsid2='$v2' AND $where");
					if(!isset($naps->id)){
						if($id == 1){
							$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where AND valsid1='$v1' ORDER BY rorder ASC");
						} else {
							$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where AND valsid2='$v2' ORDER BY rorder ASC");
						}
					}
					if(isset($naps->id)){
						
						$vd1 = $v[$naps->valsid1];
						$vd2 = $v[$naps->valsid2];
						
						$naps_data = array();
						$naps_data['item1'] = pn_strip_input($vd1->vname .' '. $vd1->vtype);
						$naps_data['item2'] = pn_strip_input($vd2->vname .' '. $vd2->vtype);
						$naps_data['valut1'] = $vd1->id;
						$naps_data['valut2'] = $vd2->id;
						$naps_data['vd1'] = $vd1;
						$naps_data['vd2'] = $vd2;
						$naps_data['direction'] = $naps;
						$naps_data = (object)$naps_data;						
						
						$log['status'] = 'success';
						$log['thelink'] = get_exchange_link($vd1->xname, $vd2->xname);
						$log['idobmen'] = $naps->id;
						
						$name = get_option('blogname');
						$item_title1 = pn_strip_input($vd1->vname .' '. $vd1->vtype);
						$item_title2 = pn_strip_input($vd2->vname .' '. $vd2->vtype);
						$title = sprintf(__('Exchange %1$s to %2$s','pn'),$item_title1,$item_title2);

						$log['title'] = $name . '- '. $title;
						$log['title2'] = $title;

						$log['html1'] = get_the_naprtext1($naps->id,'<div class="biginfo"><div class="biginfovn"><div class="binfotitle">'. __('Attention!','pn') .'</div>','</div></div>');
						$log['html2'] = get_the_naprtext2($naps->id, $title);
						$log['ajaxobmen1'] = get_the_napravlennost($vd1, $vd2, $naps, '1');
						$log['ajaxobmen2'] = get_the_napravlennost($vd1, $vd2, $naps, '2');
						$log['persdata'] = get_persdata_block($naps);
						
					} else {
						$log['status'] = 'error';
						$log['status_code'] = 1;
						$log['status_text'] = __('Error! The direction does not exist','pn');											
					}
				} else {	
					$log['status'] = 'error';
					$log['status_code'] = 1;
					$log['status_text'] = __('Error! The direction does not exist','pn');			
				}
			} else {
				$log['status'] = 'error';
				$log['status_code'] = 1;
				$log['status_text'] = __('Error! system error','pn');				
			}
		} else {
			$log['status'] = 'error';
			$log['status_code'] = 1;
			$log['status_text'] = __('Error! directions are available only to authorized users','pn');			
		}
	} else {
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = get_technical_mode_text();		
	}
	
	echo json_encode($log);
	exit;
}	

function get_exchange_page($atts){
global $wpdb, $exchangebox, $naps_id, $naps_data;

	$n_atts = array();
	if(is_array($atts)){
		foreach($atts as $k => $v){
			$n_atts[$k] = str_replace(array('&quot;','&#039;'),'',$v);
		}
	}	
		
	$temp = '';
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	$techreg = get_technical_mode();
	
	if($techreg){
		$techregtext = get_technical_mode_text();
		if($techregtext){
			$temp .= '<div class="resultfalse">'. $techregtext .'</div>';
		}
	}	
	
	if($techreg == 0 or $techreg == 2 or $techreg == 3){  /* тех. режим */
		$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide')); 
		if($gostnaphide != 1 or $gostnaphide == 1 and $user_id){ /* видимость для гостей */	
	
			$sv1 = is_site_value(is_isset($n_atts,'id1'));
			if(!$sv1){ $sv1 = is_site_value(get_query_var('valut1')); }
			$sv2 = is_site_value(is_isset($n_atts,'id2'));
			if(!$sv2){ $sv2 = is_site_value(get_query_var('valut2')); }
			
			if($sv1 and $sv2){
				$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE xname='$sv1'");
				$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE xname='$sv2'");				
				if(isset($vd1->id) and isset($vd2->id)){
					$valut1 = $vd1->id;
					$valut2 = $vd2->id;

					$where = get_naps_where('exchange');
					$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE valsid1='$valut1' AND valsid2='$valut2' AND $where");
					if(isset($naps->id)){
						
						$naps_data = array();
						$naps_data['item1'] = pn_strip_input($vd1->vname .' '. $vd1->vtype);
						$naps_data['item2'] = pn_strip_input($vd2->vname .' '. $vd2->vtype);
						$naps_data['valut1'] = $vd1->id;
						$naps_data['valut2'] = $vd2->id;
						$naps_data['vd1'] = $vd1;
						$naps_data['vd2'] = $vd2;
						$naps_data['direction'] = $naps;
						$naps_data = (object)$naps_data;						
						
						$temp .= get_exchange_html($vd1,$vd2,$naps);
					} else {
						$temp = '<div class="resultfalse">'. __('Error! Direction is available only to registered users','pn') .'</div>';
					}
				} else {	
					$temp = '<div class="resultfalse">'. __('Error! The direction does not exist','pn') .'</div>';
				}
			} else {	
				$temp = '<div class="resultfalse">'. __('Error! system error','pn') .'</div>';
			}
		} else {
			$temp = '<div class="resultfalse">'. __('Error! directions are available only to authorized users','pn') .'</div>';
		}
	} 
	
	return $temp;
}

function exchange_page_shortcode($atts, $content) {
	
	$temp = '
	<form method="post" class="ajax_post_bids" action="'. get_ajax_link('bidsform') .'">
		<div class="exch_ajax_wrap">
			<div class="exch_ajax_wrap_abs"></div>
			<div id="exch_html">'. get_exchange_page($atts) .'</div>
		</div>
	</form>
	';
	
	return $temp;
}
add_shortcode('xchange', 'exchange_page_shortcode');

function get_exchange_html($vd1,$vd2,$naps){
global $wpdb, $exchangebox;
	
	$temp = '';	
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);		
	
	$naps_id = $naps->id;	
	
	$item_title1 = pn_strip_input($vd1->vname .' '. $vd1->vtype);
	$item_title2 = pn_strip_input($vd2->vname .' '. $vd2->vtype);
	
	$title = sprintf(__('Exchange %1$s to %2$s','pn'),$item_title1,$item_title2);
	
	$temp .= apply_filters('exb_before_xchangestep1_form','');
	
	$temp .='
	<input type="hidden" name="naps_id" class="js_naps_id" value="'. $naps->id .'" />
	<input type="hidden" name="place" value="2" />
	';
	
	$temp .='
	<div class="xchangestep1div">
		<div class="xchangestep1divvn">
			<h1 class="xchangestep1title js_exchangestep_title">'. $title .'</h1>
		</div>
	</div>
		<div class="clear"></div>
		
	<div class="xchangestep1body">
		<div class="xchangestep1bodyvn">';
	
			$temp .= '<div id="naprtext1">';		
			$temp .= get_the_naprtext1($naps->id,'<div class="biginfo"><div class="biginfovn"><div class="binfotitle">'. __('Attention!','pn') .'</div>','</div></div>');
			$temp .= '</div>';	
	
			$temp .= '
			<div class="blockchange topped">
				<div class="obmentitle"><div class="obmentitlevn">'. __('Giving','pn') .' &rarr;</div></div><div class="clear"></div>';
				  
				$temp .= ' 
				<div id="ajaxobmen1" class="theblocks">';
					
				$temp .= get_the_napravlennost($vd1,$vd2,$naps,'1','shortcode');
					
				$temp .= '
				</div>
				';
					
			$temp .= '	
			</div>	
			<div class="blockchange bottomed">
				<div class="obmentitle"><div class="obmentitlevn">'. __('Receive','pn') .' &larr;</div></div><div class="clear"></div>';	
				
				$temp .= ' 
				<div id="ajaxobmen2" class="theblocks">';
					
				$temp .= get_the_napravlennost($vd1,$vd2,$naps,'2','shortcode');
					
				$temp .= '
				</div>
				';				
				
			$temp .= '	
			</div>
				<div class="clear"></div>
			';		
	
			$temp .= '	
			<div class="obmentablediv">
				<div class="obmentabledivvn" id="persdata_block">
					'. get_persdata_block($naps) .'
				</div>
			</div>';	
	
			$temp .= '
			<div class="xchangestep1submit">	
				<input type="submit" formtarget="_top" name="" value="'. __('Continue','pn') .'" />
					<div class="clear"></div>
			</div>
			';
			
	$temp .= '
		</div>
	</div>';
	
	$temp .= apply_filters('exb_after_xchangestep1_form','');
						
	$temp .= '<div id="naprtext2">';
	$temp .= get_the_naprtext2($naps->id,$title, $naps);
	$temp .= '</div>';	
	
	return $temp;
}