<?php
if( !defined( 'ABSPATH')){ exit(); }

global $exchangebox;
if($exchangebox->get_option('partners','status') != 1){ return; }

function exb_partnerstats_shortcode($atts, $content) {
global $wpdb, $exchangebox;
	
	$temp = '';
	
	$url = get_site_url_or() .'/';
	$temp .= apply_filters('exb_before_partnerstats_page','');	
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	if($user_id){

		$date_reg = get_mytime($ui->user_registered, get_option('date_format'));
		$plinks = get_partner_plinks($user_id);
		$referals = $wpdb->get_var("SELECT COUNT(ID) FROM ".$wpdb->prefix."users WHERE ref_id = '$user_id'");
		$count_obmen = get_user_count_refobmen($user_id);
		$obmen_sum = get_user_sum_refobmen($user_id);
		if($plinks > 0 and $count_obmen > 0){
			$cti = is_sum(($count_obmen/$plinks)*100,2);
		} elseif($count_obmen > 0){
			$cti = $count_obmen*100;
		} else {
			$cti = 0;
		}		
		
		$balans = get_partner_money($user_id);
		$minpay = is_sum($exchangebox->get_option('partners','minpay'));
		$balans2 = get_partner_money_now($user_id);
		$dbalans = 0;
		if($balans2 >= $minpay){
			$dbalans = $balans2;
		} 
 		$z_all = get_partner_earn_all($user_id); 
		$pay1 = $wpdb->get_var("SELECT SUM(bsumm) FROM ".$wpdb->prefix."payoutuser WHERE userid='$user_id' AND bstatus='1'");
		$pay2 = $wpdb->get_var("SELECT SUM(bsumm) FROM ".$wpdb->prefix."payoutuser WHERE userid='$user_id' AND bstatus='0'");
		$pay1 = is_sum($pay1);
		$pay2 = is_sum($pay2);
		
		$stand_refid = stand_refid();
		$cur_type = cur_type();
	
		$temp .= '
		<div class="stattablediv">
			<div class="stattabledivvn">
				<table class="stattable">
					<tr>
						<th>'. __('Identification Number','pn') .'</th>
						<td>'. $user_id .'</td>
					</tr>
					<tr>
						<th>'. __('Registration date','pn') .'</th>
						<td>'. $date_reg .'</td>
					</tr>
					<tr>
						<th>'. __('E-mail Communication','pn') .'</th>
						<td>'. is_email($ui->user_email) .'</td>
					</tr>
					<tr>
						<th>'. __('Your commission','pn') .'</th>
						<td>'. is_out_sum(get_user_pers_refobmen($user_id), 12, 'all') .'%</td>
					</tr>						
				</table>
			</div>
		</div>
		';
		 
/*
		<tr>
			<td class="stusleft">'. __('Amount of exchanges','pn') .'</td>
			<td class="stusright">'. $obmen_sum .' '. cur_type() .'</td>
		</tr>
*/					
		 
		$temp .= '
		<div class="statuserdiv">
			<div class="statuserdivvn">
				<table class="statusertable">
				    <tr>
					    <th class="first">'. __('Statistics','pn') .'</th>
					    <th class="last"></th>
				    </tr>
					<tr>
						<td class="stusleft">'. __('Visitors','pn') .'</td>
						<td class="stusright">'. $plinks .'</td>
					</tr>
					<tr>
						<td class="stusleft">'. __('Referrals','pn') .'</td>
						<td class="stusright">'. $referals .'</td>
					</tr>
					<tr>
						<td class="stusleft">'. __('Exchanges','pn') .'</td>
						<td class="stusright">'. $count_obmen .'</td>
					</tr>						
					<tr>
						<td class="stusleft">'. __('CTR','pn') .'</td>
						<td class="stusright">'. $cti .' %</td>
					</tr>
					<tr>
						<td class="stusleft">'. __('Earned for all time','pn') .'</td>
						<td class="stusright">'. is_out_sum($z_all, 12, 'all') .' '. $cur_type .'</td>
					</tr>
					<tr>
						<td class="stusleft">'. __('Pending payments','pn') .'</td>
						<td class="stusright">'. is_out_sum($pay2, 12, 'all') .' '. $cur_type .'</td>
					</tr>
					<tr>
						<td class="stusleft">'. __('Payout','pn') .'</td>
						<td class="stusright">'. is_out_sum($pay1, 12, 'all') .' '. $cur_type .'</td>
					</tr>
					<tr>
						<td class="stusleft">'. __('Current balance','pn') .'</td>
					    <td class="stusright">'. is_out_sum($balans2, 12, 'all') .' '. $cur_type .'</td>
					</tr>
					<tr>
						<td class="stusleft">'. __('Available for paying','pn') .'</td>
						<td class="stusright">'. is_out_sum($dbalans, 12, 'all') .' '. $cur_type .'</td>
					</tr>						
				</table>
			</div>
		</div>			
		';
			
		$temp .= '
		<div class="promouserdiv">
			<div class="promouserdivvn">	
				<h3>'. __('Promotional materials','pn') .'</h3>
						
				<p>'. __('Advertising text links that you place on your website, blog, forums, question and answer services, social networks, bookmarking services will lead users to the site, and you will receive a guaranteed reward for the user navigates.','pn') .'</p>

				<p>'. __('Below are the main options of promotional materials to your affiliate link. You can use any text or use our links. You just need to copy the selected code on your website and start earning money.','pn') .'</p>
						
				<h4>'. __('Affiliate Link','pn') .':</h4>
					
				<textarea class="ptextareaus" onclick="this.select()">'. $url.'?'. $stand_refid .'='.$user_id.'</textarea>
					
				<h4>'. __('Affiliate link in the HTML-code (for placement on websites and blogs)','pn') .':</h4>
					
				<textarea class="ptextareaus" onclick="this.select()"><a target="_blank" href="'.$url.'?'. $stand_refid .'='.$user_id.'">'. __('Currency exchange','pn') .'</a></textarea>
					
				<h4>'. __('Hidden affiliate link in the HTML-code (for placement on websites and blogs)','pn') .':</h4>
			
				<textarea class="ptextareaus" onclick="this.select()"><a target="_blank" href="'.$url.'" onclick="this.href='.$url.'?'. $stand_refid .'='.$user_id.'">'. __('Currency exchange','pn') .'</a></textarea>
			
				<h4>'. __('Affiliate link to BBCode (for posting on the forums)','pn') .':</h4>
			
				<textarea class="ptextareaus" onclick="this.select()">[url="'.$url.'?'. $stand_refid .'='.$user_id.'"]'. __('Currency exchange','pn') .'[/url]</textarea>
			</div>
		</div>	   
		';			
	
	} else {
		$temp .= '<div class="resultfalse">'. __('Error! Page is disable','pn') .'</div>';
	}
	
	$temp .= apply_filters('exb_after_partnerstats_page','');

	return $temp;
}
add_shortcode('exb_partnerstats', 'exb_partnerstats_shortcode');