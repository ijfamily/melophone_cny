<?php
if( !defined( 'ABSPATH')){ exit(); }

function exb_usersobmen_shortcode($atts, $content) {
global $wpdb;

	$temp = '';

	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);

	if($user_id){

		$user_discount = get_user_discount($user_id);
		$user_exchange = get_user_count_exchanges($user_id);
		$user_exchange_sum = get_user_sum_exchanges($user_id);

		$temp .= apply_filters('exb_before_userobmen_page','');

		$temp .= '
		<div class="usstatdiv">
			<div class="usstatdivvn">
				<table class="usstattable">				
					<tr>
						<th>'. __('Personal discount','pn') .'</th>
						<td>'. is_out_sum($user_discount, 12, 'all') .'%</td>
					</tr>
					<tr>
						<th>'. __('Success exchanges','pn') .'</th>
						<td>'. is_out_sum($user_exchange, 12, 'all') .'</td>
					</tr>
					<tr>
						<th>'. __('Amount of exchanges','pn') .'</th>
						<td>'. is_out_sum($user_exchange_sum, 12, 'all') .' '. cur_type() .'</td>
					</tr>						
				</table>
			</div>
		</div>		
		';

		$temp .= '
		<div class="usobmendiv">
			<div class="usobmendivvn">
				<table class="usobmentable">
					<tr>
						<th class="uothall uothid first">'. __('ID','pn') .'</th>
						<th class="uothall uothdate">'. __('Date','pn') .'</th>
						<th class="uothall uothcurs">'. __('Course','pn') .'</th>
						<th class="uothall uothotd">'. __('Giving','pn') .'</th>
						<th class="uothall uothpoluch">'. __('Receive','pn') .'</th>
						<th class="uothall uothstatus last">'. __('Status','pn') .'</th>
					</tr>';
					$limit = apply_filters('limit_list_userxch', 15); 
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE user_id='$user_id' AND status != 'auto'"); 
					$pagenavi = get_pagenavi_calc($limit,get_query_var('paged'),$count);
					$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."bids WHERE user_id='$user_id' AND status != 'auto' ORDER BY id DESC LIMIT ". $pagenavi['offset'] .",".$pagenavi['limit']);						
					if(count($items) > 0){	
						
						$date_format = get_option('date_format');
						$time_format = get_option('time_format');						
						
						foreach($items as $item){
							$st = is_status_name($item->status);
							$status = get_bid_status($st);

							$temp .='
							<tr>
								<td class="uothid">'. $item->id .'</td>
								<td class="uotddate">'. get_mytime($item->tdate, "{$date_format}, {$time_format}") .'</td>
								<td class="uotdcurs">'. is_out_sum(is_sum($item->curs1), 12, 'course') .' '. pn_strip_input($item->valut1) .' '. is_site_value($item->valut1type) .' <br /> '. is_out_sum(is_sum($item->curs2), 12, 'course') .' '. pn_strip_input($item->valut2) .' '. is_site_value($item->valut2type) .'</td>
								<td class="uotdotd">'. is_out_sum(is_sum($item->summz1), 12, 'all') .'</td>
								<td class="uotdpoluch">'. is_out_sum(is_sum($item->summz2), 12, 'all') .'</td>
								<td class="uotdstatus"><a href="'. get_bids_url($item->hashed) .'" class="uostat bt_'. $st .'">'. $status .'</a></td>
							</tr>
							';
					}
				} else {
					$temp .='<tr><td colspan="6" class="noitems">'. __('No items','pn') .'</td></tr>';
				}							
		$temp .='</table>
            </div>				
		</div>
		';
		
		$temp .= get_pagenavi($pagenavi);		
	
		$after = apply_filters('exb_after_userobmen_page','');
		$temp .= $after;	
	
	} else {
		$temp .= '<div class="resultfalse">'. __('Error! Page is available only for authorized users','pn') .'</div>';
	}

	return $temp;
}
add_shortcode('exb_usersobmen', 'exb_usersobmen_shortcode');