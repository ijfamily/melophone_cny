<?php
if( !defined( 'ABSPATH')){ exit(); }

global $exchangebox;
if($exchangebox->get_option('partners','status') != 1){ return; }

/* добавляем JS */
add_action('siteplace_js','siteplace_js_promotional');
function siteplace_js_promotional(){
?>	
/* partners */
jQuery(function($){
    $(".promo_menu li a").on('click',function () {
        if(!$(this).hasClass('act')){
		    $(".pbcontainer, .promo_menu li").removeClass('act');
		    $(".pbcontainer").filter(this.hash).addClass('act');
		    $(this).parents('li').addClass('act');
        }
        return false;
    });
	
    $(".bannerboxlink a").on('click',function() {
        var text = $(this).text();
		var st = $(this).attr('show-title');
		var ht = $(this).attr('hide-title');
		
        if(text == st){
            $(this).html(ht);
        } else {
            $(this).html(st);
        }
        $(this).parents(".bannerboxone").find("textarea").toggle();
	    $(this).toggleClass('act');

        return false;
    });
});	
/* end partners */	
<?php	
}	
/* end добавляем JS */

function exb_banners_shortcode($atts, $content) {
global $wpdb;

	$temp = '';
	$temp .= apply_filters('exb_before_banners_page','');
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	if($user_id){

		$banner_pages = array('banners' => __('Banners','pn'), 'text' => __('Text materials','pn'));
		$banner_pages = apply_filters('banner_pages',$banner_pages);
	
		$promo = pn_strip_input(is_param_get('promo'));
		if(!$promo){ $promo = key($banner_pages); }
		
		$temp .= '
		<div class="promopage">
			<div class="promopage_ins promopagevn">
			
				<div class="promo_topmenu" id="promotopmenu">
					<ul>';
						
						if(is_array($banner_pages)){
							foreach($banner_pages as $key => $title){
								$cl = '';
								if($key == $promo){
									$cl = 'current';
								}
								
								$temp .= '
								<li class="'. $cl .'"><a href="?promo='. $key .'">'. $title .'</a></li>
								';
							}
						}
						$temp .= '
						<div class="clear"></div>
					</ul>
				</div>
			    <div class="clear"></div>
		';
		
			$url = get_site_url_or() .'/';
			$banners = get_option('banners');
		
			$nbanners = apply_filters('pp_banners',array());
			$nbanners = (array)$nbanners;
	
		    if($promo == 'text'){ /* текст */
			
				foreach($nbanners as $key => $title){
					if(isset($banners[$key]) and is_array($banners[$key])){
						$text = $banners[$key];
				
						$temp .= '
						<div class="promotext_warning promotextwarning">
							'. __('Note: Here are descriptions of exemplary embodiments of the service, which has published many dozens of sites x. It is strongly recommended when using any of the above text, rewriting the content in your own words.','pn') .'
						</div>';
				
						foreach($text as $txt){
							$txt = str_replace('[url]',$url,$txt);
							$txt = str_replace('[partner_link]',$url.'?'. stand_refid() .'='.$user_id,$txt);
							$txt = trim(stripslashes($txt));
							if($txt){
								$temp .= '
								<div class="one_promotxt onepromotxt">
									'. $txt .'
								</div>
								<div class="one_promotxt_code onepromotxttext">
									<textarea class="partner_textarea" onclick="this.select()">'. $txt .'</textarea>
								</div>		
								';
							}
						}

					}
					break;
				}
			
			} else { /* banner */
							
			
				$temp .= '
				<div class="promo_menu" id="promomenu">
					<ul>';
						$r=0;
						if(isset($nbanners['text'])){ unset($nbanners['text']); }
						foreach($nbanners as $myb => $value){ 
							if(isset($banners[$myb]) and is_array($banners[$myb])){ $r++;
								$cl = '';
									if($r==1){ $cl = 'act'; }
									
									$value = str_replace(array(__('Banners','pn'),'(',')'),'',$value);
									
									$temp .= '
										<li class="'. $cl .'"><a href="#ftab'. $r .'">'. $value .'</a></li>
									';
							}
							if($r%9==0){ $temp .= '<div class="clear"></div>'; }
						}
						$temp .= '
					</ul>
				</div>';

				$temp .= '<div class="bannerbox">';
                
			    $r=0;
				
				foreach($nbanners as $myb => $value){ 
					if(isset($banners[$myb]) and is_array($banners[$myb])){  $r++;
					
						if($r == 1){ $clay='act'; } else { $clay=''; }
						$temp .= '<div id="ftab'. $r .'" class="pbcontainer '. $clay .'">';
					    
						foreach($banners[$myb] as $txt){
							$txt = str_replace('[url]',$url,$txt);
							$txt = str_replace('[partner_link]',$url.'?'. stand_refid() .'='.$user_id,$txt);
							$txt = trim(stripslashes($txt));
							if($txt){    
							
								$temp .= '
								<div class="prevbanner">'.$txt.'</div>
								<div class="bannerboxone">
									<div class="bannerboxlink">
										<a href="#" show-title="'. __('Show code','pn') .'" hide-title="'. __('Hide code','pn') .'">'. __('Show code','pn') .'</a>
									</div>
									<div class="bannerboxtextarea">
										<textarea class="partner_textarea" onclick="this.select()">'.$txt.'</textarea>
									</div>
								</div>		
								';
							}
						}		

						$temp .= '</div>';
					
					}
				}
				
				$temp .= '</div>';	
			
			}	
	
		$temp .= '
			</div>
		</div>
		';	
	
	} else {
		$temp .= '<div class="resultfalse">'. __('Error! Page is disable','pn') .'</div>';
	}
	
	$after = apply_filters('exb_after_banners_page','');
	$temp .= $after;

	return $temp;
}
add_shortcode('exb_banners', 'exb_banners_shortcode');