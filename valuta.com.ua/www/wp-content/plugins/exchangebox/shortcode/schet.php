<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_quicktags_pn_add_naps','adminpage_quicktags_page_vaccounts');
add_action('pn_adminpage_quicktags_pn_naps_temp','adminpage_quicktags_page_vaccounts');
function adminpage_quicktags_page_vaccounts(){
?>
edButtons[edButtons.length] = 
new edButton('premium_vaccounts', '<?php _e('Currency account','pn'); ?>','[exb_schet valuts="" unique="0" hide="0" fix="1"]');

edButtons[edButtons.length] = 
new edButton('premium_bidid', '<?php _e('Bid id','pn'); ?>','[bid_id]');

edButtons[edButtons.length] = 
new edButton('premium_bid_sum', '<?php _e('Bid amount','pn'); ?>','[bid_sum]');
<?php	
}

function exb_bid_sum() {
global $wpdb, $bids_data;
	if(isset($bids_data->id)){
		return is_sum($bids_data->summ1);
	}
}
add_shortcode('bid_sum', 'exb_bid_sum');

function exb_bid_id() {
global $wpdb, $bids_data;
	if(isset($bids_data->id)){
		return $bids_data->id;
	}
}
add_shortcode('bid_id', 'exb_bid_id');

function exb_schet_shortcode($atts, $content) {
global $wpdb, $bids_data;

    $valut_id = intval(is_isset($atts,'valuts'));
	if(!$valut_id){ $valut_id = is_isset($bids_data,'valut2i'); }
	$unique = intval(is_isset($atts,'unique'));
	$hide = intval(is_isset($atts,'hide'));
	$fix = intval(is_isset($atts,'fix'));

	$naschet = '';
	if(isset($bids_data->id) and $fix == 1){
		$naschet = pn_strip_input($bids_data->naschet);
	}

	$theval = '';
	if($valut_id and !$naschet){
		$val = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."vschets WHERE visib='0' AND valid='$valut_id' ORDER BY RAND() LIMIT 1");
	    if(isset($val->id)){
			$val_id = $val->id;
			$naschet = $theval = pn_strip_input(get_vaccs_txtmeta($val_id, 'accountnum'));
			
			$array = array();
			$array['prosm'] = $val->prosm+1;
			if($unique == 1){
				$array['visib'] = 1;
			}
			$wpdb->update($wpdb->prefix."vschets", $array, array('id'=>$val->id));
		}
	}
	
	if(isset($bids_data->id) and $theval){
		update_bids_naschet($bids_data->id, $theval);
	}
	
	if($hide == 0){
		return $naschet;
	} else {
		return '';
	}
}
add_shortcode('exb_schet', 'exb_schet_shortcode');