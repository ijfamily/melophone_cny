<?php
if( !defined( 'ABSPATH')){ exit(); } 

global $exchangebox;
if($exchangebox->get_option('partners','status') != 1){ return; }

/* добавляем JS */
add_action('siteplace_js','siteplace_js_payouts');
function siteplace_js_payouts(){
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	if($user_id){	
?>	
/* payouts */
jQuery(function($){
	
    $('.delpay_link').on('click',function(){
		var id = $(this).attr('name');
		var thet = $(this);
		thet.addClass('act');

		var param ='id=' + id;
        $.ajax({
			type: "POST",
			url: "<?php echo get_ajax_link('delete_payoutlink');?>",
			dataType: 'json',
			data: param,
			error: function(res, res2, res3){
				<?php do_action('pn_js_error_response', 'ajax'); ?>
			},		
			success: function(res)
			{
				if(res['status'] == 'success'){
					window.location.href = res['url'];
				} 
				if(res['status'] == 'error'){
					<?php do_action('pn_js_alert_response'); ?>
				}
				
				thet.removeClass('act');
			}
        });
	
        return false;
    });
	
});	
/* end payouts */	
<?php	
	}
} 
/* end добавляем JS */

function exb_payouts_shortcode($atts, $content){
global $wpdb, $exchangebox;

	$temp = '';
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	if($user_id){

		$temp .= apply_filters('exb_before_payouts_page','');

        $minpay = is_sum($exchangebox->get_option('partners','minpay'), 2);
        $balans = get_partner_money_now($user_id);
	    if($balans >= $minpay){
            $dbalans = $balans;
			$dis = '';
		} else {
			$dbalans = 0;
			$dis = 'disabled="disabled"';
		}		
		
		$cur_type = cur_type();
		
		$ptext = pn_strip_text($exchangebox->get_option('partners','payouttext'));
		if(!$ptext){ $ptext = sprintf(__('Minimum withdrawal amount is <span class="red">%1$s %2$s</span>. All payments to be done right after admin verifies your account. Actually it takes less than 24 hours after submitting withdrawal request.','pn'), '[minpay]', '[currency]'); }
			
		$ptext = str_replace('[minpay]', $minpay, $ptext);
		$ptext = str_replace('[currency]', $cur_type,$ptext);		
		
        $temp .= '
		<div class="payments_minitext">
		    <div class="payments_minitextvn">   
		        '. $ptext .'
		    </div>
		</div><div class="clear"></div>';
        
        $temp .='
		<form method="post" class="ajax_post_form" action="'. get_ajax_link('payoutform') .'">
		<div class="resultgo"></div>
		
        <div class="exbpayform">
            <div class="exbpayformvn">
			    
                <table class="exbpaytable">		
                <tr>
                    <td class="pttitle"><div class="ptlabel">'. __('Purse','pn') .':</div></td>
                    <td class="ptwebmoney">
					    <div class="ptvaluts">
                            <select name="valuts" class="rselect">
								<option value="0">--'. __('No item','pn') .'--</option>
							';
							    
							$valuts = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts WHERE vtype='$cur_type' AND vactive='1' AND pvivod='1' ORDER BY vname ASC");	
							foreach($valuts as $val){ 
							    $rezerv = is_sum($val->valut_reserv); 
								if($rezerv > $dbalans){
									$temp .='
										<option value="'. $val->id .'">'. pn_strip_input($val->vname) .'</option>
									';
								}
                            }							
								
							$temp .='
							</select>
						</div>					
					    <div class="ptschet">
                            <input type="text" name="schet" class="exbinput exbpayinput" value="" />
						</div>
                    </td>
					<td class="ptsubmittd">
					    <div class="ptsubmitdiv">
						    <input type="submit" '. $dis .' formtarget="_top" class="exbsubmit exbpaysubmit" value="'. __('Payment order','pn') .' '. $dbalans .' '. $cur_type .'" />
						</div>
					</td>
                </tr>
		        </table>
			</div>
		</div>
		</form>';
		
		$temp .= '
		<div class="paytable_title"><div class="paytable_titlevn">'. __('Withdrawal requests','pn') .':</div></div><div class="clear"></div>
        ';

        $temp .= '
        <div class="paydiv">
            <div class="paydivvn">	
			
                <table class="paytablearchive">

		            <tr>
			            <th class="pthall pthdate first">'. __('Date','pn') .'</th>
						<th class="pthall pthpurse">'. __('Purse','pn') .'</th>
			            <th class="pthall pthsumm">'. __('Sum','pn') .'</th>
			            <th class="pthall pthstatus">'. __('Status','pn') .'</th>
						<th class="pthall last"></th>
		            </tr>';
					
					$perpage = apply_filters('limit_list_payouts', 15);
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."payoutuser WHERE userid = '$user_id'");
					$pagenavi = get_pagenavi_calc($perpage,get_query_var('paged'),$count);
					if($count > 0){
						$pays = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."payoutuser WHERE userid = '$user_id' ORDER BY id DESC LIMIT ".$pagenavi['offset'].", ".$pagenavi['limit']);
                    
						$date_format = get_option('date_format');
						$time_format = get_option('time_format');					
					
                        foreach ($pays as $pay) { 
						    
                            $status = intval($pay->bstatus);
                            $link ='';
                            if($status == 0){
                                $link = '<a href="#" name="'. $pay->id .'" class="delpay_link" title="'. __('Cancel payment','pn') .'">'. __('Cancel payment','pn') .'</a>';
                            }

							if($status == 0){
								$st = '<div class="paystatus pst1">'. __('Application of waiting','pn') .'</div>';
							} elseif($status == 1){
								$st = '<div class="paystatus pst2">'. __('Application is made','pn') .'</div>';
							} elseif($status == 2){
								$st = '<div class="paystatus pst3">'. __('Application is rejected','pn') .'</div>';
							} elseif($status == 3){
								$st = '<div class="paystatus pst4">'. __('Application canceled by the user','pn') .'</div>';
							}
							
							$valuts = pn_strip_input($pay->valuts);
							if(!$valuts){ $valuts = 'Webmoney'; }
				
                            $temp .= '
                            <tr>
                                <td class="ptdate">'. get_mytime($pay->bdate, "{$date_format}, {$time_format}") .'</td>
                                <td class="ptrekvizit"><span class="ptvaluts">'. $valuts .'</span><br />'. pn_strip_input($pay->schet) .'</td>
								<td class="ptammount">'. is_out_sum(is_sum($pay->bsumm), 12, 'all') .' '. $cur_type .'</td>
                                <td class="ptstatus">'. $st .'</td>  
                                <td class="ptdellink">'. $link .'</td>  
                            </tr>';
	                    }				
				    } else {
                        $temp .= '
                            <tr>
	                            <td colspan="4" class="noitems">'. __('No items','pn') .'</td>
                            </tr>
                        ';
                    }					
                $temp .= '
				</table>
			
            </div>				
		</div>
		';	
		$temp .= get_pagenavi($pagenavi);	
	
		$temp .= apply_filters('exb_after_payouts_page','');		
	
	} else {
		$temp .= '<div class="resultfalse">'. __('Error! Page is available only for authorized users','pn') .'</div>';
	}

	return $temp;
}
add_shortcode('exb_payouts', 'exb_payouts_shortcode');

add_action('myaction_site_delete_payoutlink', 'def_myaction_ajax_delete_payoutlink');
function def_myaction_ajax_delete_payoutlink(){
global $wpdb, $exchangebox;	
	
    $log = array();
	$log['response'] = '';
	$log['status'] = '';
	$log['status_text'] = '';
	$log['status_code'] = 0;	
	
	$exchangebox->up_mode();
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	if(!$user_id){
		$log['status'] = 'error'; 
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! You must login','pn');
		echo json_encode($log);
		exit;		
	}
		
	$id = intval(is_param_post('id'));	
	if($id > 0){
		$item = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."payoutuser WHERE userid = '$user_id' AND bstatus = '0' AND id = '$id'");
		if(isset($item->id)){
			
			do_action('pn_payoutuser_not_before', $id, $item);
		
			$arr = array();
			$arr['bstatus'] = 3;		
			$wpdb->update($wpdb->prefix.'payoutuser', $arr, array('id'=>$item->id));
			
			do_action('pn_payoutuser_not', $id, $item);
			do_action('pn_payoutuser_not_after', $id, $item);			
		}
	}	
	
	$log['status'] = 'success';
	$log['url'] = apply_filters('payouts_redirect', $exchangebox->get_page('payouts')); 
	echo json_encode($log);
	exit;
}

add_action('myaction_site_payoutform', 'def_myaction_ajax_payoutform');
function def_myaction_ajax_payoutform(){
global $wpdb, $exchangebox;	
	
	only_post();
	
    $log = array();	
	$log['response'] = '';
	$log['status'] = '';
	$log['status_text'] = '';
	$log['status_code'] = 0;	
	
	$exchangebox->up_mode();
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	if(!$user_id){
		$log['status'] = 'error'; 
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! You must login','pn');
		echo json_encode($log);
		exit;		
	}
		
	$minpay = is_sum($exchangebox->get_option('partners','minpay'), 2);
	$balans = get_partner_money_now($user_id);
	if($balans >= $minpay and $balans > 0){  
		$cur_type = cur_type();		
		$valut_id = intval(is_param_post('valuts'));	
		$item = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE vtype='$cur_type' AND vactive='1' AND pvivod='1' AND id='$valut_id'");	
		if(isset($item->id)){
			$reserv = is_sum($item->valut_reserv); 
			if($reserv >= $balans){
				
				$account = pn_maxf_mb(pn_strip_input(is_param_post('schet')),250);
				if ($account) {
				
					$arr = array();
					$arr['bdate'] = current_time('mysql');
					$arr['userid'] = $user_id;
					$arr['user_login'] = is_user($ui->user_login);
					$arr['bsumm'] = $balans;
					$arr['valuts'] = pn_strip_input($item->vname);
					$arr['valsid'] = $item->id;
					$arr['schet'] = $account;
					$arr['bstatus'] = 0;			
					$wpdb->insert($wpdb->prefix.'payoutuser', $arr);				
					$insert_id = $wpdb->insert_id;

					$arr['id'] = $insert_id;
					$payoutuser_item = (object)$arr;
					
					do_action('pn_payoutuser_wait_before', $insert_id, $payoutuser_item);
					do_action('pn_payoutuser_wait', $insert_id, $payoutuser_item);
					do_action('pn_payoutuser_wait_after', $insert_id, $payoutuser_item);
					
					$notify_tags = array();
					$notify_tags['[sitename]'] = pn_strip_input(get_bloginfo('sitename'));
					$notify_tags['[user]'] = is_user($ui->user_login);
					$notify_tags['[sum]'] = $arr['bsumm'] .' '. pn_strip_input($item->vname);	

					$user_send_data = array();
					$result_mail = apply_filters('premium_send_message', 0, 'payout', $notify_tags, $user_send_data);					  					 
						
					$log['status'] = 'success';
					$log['status_text'] = __('Payout is successfully requested','pn');
					$log['url'] = apply_filters('payouts_redirect', $exchangebox->get_page('payouts')); 

				} else {
					$log['status'] = 'error';
					$log['status_code'] = 1;
					$log['status_text'] = __('Error! Invalid account number','pn');
				}							
			} else {
				$log['status'] = 'error'; 
				$log['status_code'] = 1;
				$log['status_text'] = __('Error! Selected currency can not be ordered payment','pn');
			}
		} else {
			$log['status'] = 'error'; 
			$log['status_code'] = 1;
			$log['status_text'] = __('Error! Selected currency can not be ordered payment','pn');
		}	
	} else {
		$log['status'] = 'error'; 
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! No enough money for order','pn');
	}		
	
	echo json_encode($log);
	exit;
}