<?php
/*
title: Adgroup
description: мерчант Adgroup
version: 1.2
*/

if(!class_exists('merchant_adgroup')){
	class merchant_adgroup extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'CLIENT_ID', 'CLIENT_SECRET', 
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'.$this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_filter('get_merchant_admin_options_'.$this->name,array($this, 'get_merchant_admin_options'),1,2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_action('myaction_merchant_'. $this->name .'_cron', array($this,'myaction_merchant_cron'));
		}					
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data, 'CLIENT_ID') 
				and is_deffin($this->m_data, 'CLIENT_SECRET') 
			){
				$text = '';
			}
			
			return $text;
		}			
		
		function get_merchant_admin_options($options, $data){
			
			$text = '
			<strong>Cron:</strong> <a href="'. get_merchant_link($this->name.'_cron') .'" target="_blank">'. get_merchant_link($this->name.'_cron') .'</a>			
			';

			$options[] = array(
				'view' => 'line',
				'colspan' => 2,
			);						
			
			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);

			if(isset($options['note'])){
				unset($options['note']);
			}				
			
			return $options;
		}		
		
		function merchant_formstep_autocheck($autocheck, $m_id){
			
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			
			return $autocheck;
		}	
		
		function merchant_paybutton($temp, $naps, $item){
		global $wpdb;
		
			$temp = '';
			
			$qiwi_link = trim(get_bids_meta($item->id, 'qiwi_link'));
			if(!$qiwi_link){
				$pay_sum = pn_strip_input(round($item->summ1,2));
				$currency = mb_strtoupper($item->valut1type);
			
				try {
					$class = new ADGROUP_API(is_deffin($this->m_data,'CLIENT_ID'), is_deffin($this->m_data,'CLIENT_SECRET'));
					$res = $class->create_link($pay_sum, $currency);
					if(isset($res['comment'], $res['link'])){
						$qiwi_link = $res['link'];
						update_bids_meta($item->id, 'qiwi_link', $qiwi_link);
						
						$qiwi_id = $res['comment'];
						update_bids_meta($item->id, 'qiwi_id', $qiwi_id);
					}
				}
				catch (Exception $e)
				{
					if(current_user_can('administrator')){
						die($e);
					}
				}					
			}

			if($qiwi_link){
				$temp = '
				<form action="'. $qiwi_link .'" method="post">
					<input type="submit" formtarget="_top" value="'. __('Proceed to checkout','pn') .'" />
				</form>';
			}				
			
			return $temp;
		}
		
		function myaction_merchant_cron(){
			global $wpdb;
	
			try {
				$class = new ADGROUP_API(is_deffin($this->m_data,'CLIENT_ID'), is_deffin($this->m_data,'CLIENT_SECRET'));
				$orders = $class->get_history(100);
				if(is_array($orders)){
					$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids WHERE status = 'new'");
					foreach($items as $item){
						foreach($orders as $res){
							$item_id = $item->id;
							$trans_id = $res['trans_id'];
							$to_account = pn_maxf_mb(pn_strip_input($res['dest_address']),500);
							$trans_in = get_bids_meta($item_id, 'qiwi_id');
							if($trans_in and $trans_in == $trans_id){
								$data = get_data_merchant_for_id($item_id, $item);
								
								$currency = $res['sum_currency'];
								
								$in_sum = $res['sum_amount'];
								$in_sum = is_sum($in_sum,2);
								$err = $data['err'];
								$status = $data['status'];
								$m_id = $data['m_id']; 
								
								$bid_currency = $data['vtype'];
								
								$pay_purse = is_isset($res, 'source_address');
									
								$bid_sum = is_sum($data['summ'],2);	
								if($err == 0){
									if($bid_currency == $currency){
										if($in_sum >= $bid_sum){
											the_merchant_bid_payed($item_id, $in_sum, $pay_purse, '', $res['id'], 'user');
										}
									}		 		 
								}
							}
						}
					}
				}				
			}
			catch (Exception $e)
			{
				die($e);
			}				
		}
	}
}

new merchant_adgroup(__FILE__, 'Adgroup');