<?php
/*
title: [ru_RU:]EdinarCoin[:ru_RU][en_US:]EdinarCoin[:en_US]
description: [ru_RU:]мерчант EdinarCoin[:ru_RU][en_US:]EdinarCoin merchant[:en_US]
version: 1.2
*/

if(!class_exists('merchant_edinarcoin')){
	class merchant_edinarcoin extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'TOKEN', 'ACCOUNT',
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'.$this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_filter('get_merchant_admin_options_'.$this->name,array($this, 'get_merchant_admin_options'),1,2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_filter('summ_to_pay',array($this,'summ_to_pay'),10,3);
			add_action('myaction_merchant_'. $this->name .'_adadress', array($this,'myaction_merchant_adadress'));
			add_action('myaction_merchant_'. $this->name .'_cron', array($this,'myaction_merchant_cron'));
			
			add_filter('list_user_notify',array($this,'user_mailtemp'));
			add_filter('list_admin_notify',array($this,'admin_mailtemp'));
			add_filter('list_notify_tags_generate_address1_edinar',array($this,'mailtemp_tags_generate_address'));
			add_filter('list_notify_tags_generate_address2_edinar',array($this,'mailtemp_tags_generate_address'));
		}
		
		function user_mailtemp($places_admin){
			$places_admin['generate_address1_edinar'] = sprintf(__('Address generation for %s','pn'), 'Edinar');
			return $places_admin;
		}

		function admin_mailtemp($places_admin){
			$places_admin['generate_address2_edinar'] = sprintf(__('Address generation for %s','pn'), 'Edinar');
			return $places_admin;
		}

		function mailtemp_tags_generate_address($tags){
			
			$tags['bid_id'] = __('ID Order','pn');
			$tags['address'] = __('Address','pn');
			$tags['sum'] = __('Amount','pn');
			
			return $tags;
		}			
		
		function summ_to_pay($sum, $m_id, $item){ 
			
			if($m_id and $m_id == $this->name){
				return $item->summ1;
			}	
			
			return $sum;
		}		
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data, 'TOKEN') 
				and is_deffin($this->m_data, 'ACCOUNT') 
			){
				$text = '';
			}
			
			return $text;
		}	
		
		function get_merchant_admin_options($options, $data){
			
			if(isset($options['note'])){
				unset($options['note']);
			}	

			$text = '
			<strong>CRON URL:</strong> <a href="'. get_merchant_link($this->name.'_cron') .'" target="_blank">'. get_merchant_link($this->name.'_cron') .'</a><br />
			';

			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);			
			
			return $options;
		}		
		
		function merchant_formstep_autocheck($autocheck, $m_id){
			
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			
			return $autocheck;
		}	
		
		function merchant_paybutton($temp, $naps, $item){

			$temp = '
			<form action="'. get_merchant_link($this->name.'_adadress') .'" target="_blank" method="get">
				<input type="hidden" name="hash" value="'. is_bid_hash($item->hashed) .'" />
				<input type="submit" formtarget="_top" value="'. __('Continue','pn') .'" />
			</form>											
			';	
			
			return $temp;
		}
		
		function myaction_merchant_adadress(){
 		global $wpdb;

			$hashed = is_bid_hash(is_param_get('hash'));
			if($hashed){
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed'");
				if(isset($item->id)){
					$item_id = $item->id;
					$status = $item->status;
					$valut1i = intval($item->valut1i);
					$valut1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1i'");
					$xzt = $valut1->xzt;
					$vtype = $valut1->vtype;
					$enable_currency = array('EDC');
					if(in_array($vtype, $enable_currency)){	
						if($status=='new' and is_enable_merchant($xzt) and $xzt == $this->name){
							$sum = pn_strip_input($item->summ1);
							
							$naschet = pn_strip_input($item->naschet);
							if(!$naschet){
								try{
									$token = is_deffin($this->m_data,'TOKEN');
									$account = is_deffin($this->m_data,'ACCOUNT');
									$class = new Edinar($token);
									$result_url = get_merchant_link($this->name.'_cron').'?hook_id=' . $item_id;
									$naschet = $class->add_adress($account, $result_url);
									if($naschet){
										update_bids_naschet($item_id, $naschet);
									}
									
									$notify_tags = array();
									$notify_tags['[sitename]'] = pn_strip_input(get_bloginfo('sitename'));
									$notify_tags['[bid_id]'] = $item_id;
									$notify_tags['[address]'] = $naschet;
									$notify_tags['[sum]'] = $sum;
									$notify_tags = apply_filters('notify_tags_generate_address_edinar', $notify_tags);		

									$user_send_data = array();
									$result_mail = apply_filters('premium_send_message', 0, 'generate_address2_edinar', $notify_tags, $user_send_data); 
					
									$user_send_data = array(
										'user_email' => $item->email,
										'user_phone' => '',
									);	
									$result_mail = apply_filters('premium_send_message', 0, 'generate_address1_edinar', $notify_tags, $user_send_data);									
									
								}
								catch (Exception $e)
								{
									echo $e;
								}				
							}

							if($naschet){
							?>
							<div style="border: 1px solid #8eaed5; padding: 10px 15px; font: 13px Arial; width: 400px; border-radius: 3px; margin: 0 auto; text-align: center;">
								<p><?php printf(__('In order to pay an ID <b>%1$s</b> order send amount <b>%2$s</b> %4$s on address <b>%3$s</b>','pn'), $item_id, $sum, $naschet, $vtype); ?></p>		
								<?php echo sprintf(__('The bid will be considered paid when we receive confirmations. <a href="%1$s">Return</a> to bid page and click "Check payment".','pn'), get_bids_url($hashed)); ?></p>
							</div>
							<?php
							} else {
								pn_display_mess(__('Error','pn'));	
							}							
						} else {
							wp_redirect(get_bids_url($hashed));
							exit;				
						}				
					} else {
						pn_display_mess(__('Error','pn'));			
					} 				
				} else {
					wp_redirect(get_bids_url($hashed));
					exit;
				} 				
			} else {
				pn_display_mess(__('Application of a non-existent','pn'));
			} 			
		} 
		
		function edinarcoin_check_orders($order_id=0, $return=0){
		global $wpdb;

			$m_id = $this->name;
			$order_id = intval($order_id);
			$return_url = '';

			$where = '';
			if($order_id){
				$where = " AND id = '$order_id'";
			}
					
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids WHERE status IN ('new') AND valut1type IN('EDC') AND naschet != '' $where");
			foreach($items as $item){
				$item_id = $item->id;
				if(is_pn_crypt($item->naschet_h, $item->naschet)){
					$address = $item->naschet;
					$valut1i = intval($item->valut1i);
					$valut1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1i'");
					$xzt = $valut1->xzt;
					if(is_enable_merchant($m_id) and $xzt == $m_id){
						$sum = $item->summ1;
							
						if($return){
							$return_url = get_bids_url($item->hashed);
						}
							
						try {
							$token = is_deffin($this->m_data,'TOKEN');
							$account = is_deffin($this->m_data,'ACCOUNT');
							$class = new Edinar($token);						
							$datas = $class->get_history_address($address);
							foreach($datas as $trans_id => $data){
								$amount = is_sum($data['amount']);	
								if($amount >= $sum){
									$trans_id = pn_strip_input($trans_id);
									the_merchant_bid_payed($item_id, $amount, '', '', $trans_id, 'user');
									break;
								}
							}
						}
						catch (Exception $e)
						{
							if(current_user_can('administrator')){
								echo $e;
							}
						}					
					}			
				}
			} 
					
			if($return_url){
				wp_redirect($return_url);
				exit;
			}				
		}		
		
		function myaction_merchant_cron(){
	
			$order_id = intval(is_param_get('hook_id'));
			$this->edinarcoin_check_orders($order_id, 0);
			
		}
		
	}
}

new merchant_edinarcoin(__FILE__, 'EdinarCoin');