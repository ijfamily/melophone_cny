<?php
/*
title: AskBTC
description: мерчант AskBTC
version: 1.2
*/

if(!class_exists('merchant_askbtc')){
	class merchant_askbtc extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'CONFIRM_COUNT', 'API_KEY', 'API_SECRET', 'SECRET', 'SECRET2',
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'.$this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_filter('get_merchant_admin_options_'.$this->name,array($this, 'get_merchant_admin_options'),1,2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_filter('summ_to_pay',array($this,'summ_to_pay'),10,3);
			add_action('myaction_merchant_'. $this->name .'_adadress', array($this,'myaction_merchant_adadress'));
			add_action('myaction_merchant_'. $this->name .'_status', array($this,'myaction_merchant_status'));
			add_filter('list_user_notify',array($this,'user_mailtemp'));
			add_filter('list_admin_notify',array($this,'admin_mailtemp'));
			add_filter('list_notify_tags_generate_address1_askbtc',array($this,'mailtemp_tags_generate_address'));
			add_filter('list_notify_tags_generate_address2_askbtc',array($this,'mailtemp_tags_generate_address'));
		}
		
		function user_mailtemp($places_admin){
			$places_admin['generate_address1_askbtc'] = sprintf(__('Generation of addresses for %s','pn'), 'AskBTC');
			return $places_admin;
		}

		function admin_mailtemp($places_admin){
			$places_admin['generate_address2_askbtc'] = sprintf(__('Generation of addresses for %s','pn'), 'AskBTC');
			return $places_admin;
		}

		function mailtemp_tags_generate_address($tags){
			
			$tags['bid_id'] = __('ID Order','pn');
			$tags['address'] = __('Address','pn');
			$tags['sum'] = __('Amount','pn');
			$tags['count'] = __('Confirmations','pn');			
			
			return $tags;
		}			
		
		function summ_to_pay($sum, $m_id, $item){ 
			if($m_id and $m_id == $this->name){
				return $item->summz1;
			}	
			return $sum;
		}		
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data,'CONFIRM_COUNT') 
				and is_deffin($this->m_data,'API_KEY') 
				and is_deffin($this->m_data,'API_SECRET') 
				and is_deffin($this->m_data,'SECRET') 
				and is_deffin($this->m_data,'SECRET2')  
			){
				$text = '';
			}
			return $text;
		}	
		
		function get_merchant_admin_options($options, $data){
			if(isset($options['note'])){
				unset($options['note']);
			}			
			$text = '
			<strong>BACK URL:</strong> <a href="'. get_merchant_link($this->name.'_status') . '?secret=' . urlencode(is_deffin($this->m_data,'SECRET')) .'&secret2='. urlencode(is_deffin($this->m_data,'SECRET2')) .'" target="_blank">'. get_merchant_link($this->name.'_status') .'?secret='. urlencode(is_deffin($this->m_data,'SECRET')) .'&secret2='. urlencode(is_deffin($this->m_data,'SECRET2')) .'</a>
			';

			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);						
			return $options;
		}		
		
		function merchant_formstep_autocheck($autocheck, $m_id){
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			return $autocheck;
		}	
		
		function merchant_paybutton($temp, $naps, $item){
			$temp = '
			<form action="'. get_merchant_link($this->name.'_adadress') .'" target="_blank" method="post">
				<input type="hidden" name="hash" value="'. is_bid_hash($item->hashed) .'" />
				<input type="submit" formtarget="_top" value="'. __('Continue','pn') .'" />
			</form>											
			';	
			return $temp;
		}
		
  		function myaction_merchant_adadress(){
			global $wpdb;

			$hashed = is_bid_hash(is_param_post('hash'));	
			if($hashed){
				$data = get_merch_data($this->name);
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed'");
				if(isset($item->id)){
					$item_id = $item->id;
					$sum = pn_strip_input($item->summz1);
					$my_api_secret = is_deffin($this->m_data,'API_SECRET');
					$my_api_key = is_deffin($this->m_data,'API_KEY');					
					$status = pn_strip_input($item->status);
					$valut1i = pn_strip_input($item->valut1i);
					$valut1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1i'");
					$xzt = pn_strip_input($valut1->xzt);
					if($status=='new' and is_enable_merchant($xzt) and $xzt and $xzt==$this->name){
						$naschet = pn_strip_input($item->naschet);
						if(!$naschet){
							$currency = $item->valut1type;
							$currency_m = strtolower($currency);
							try {
								$class = new AskBtc($my_api_key, $my_api_secret);
								$naschet = $class->generate_adress($currency_m);
							} catch (Exception $e) { 
								die($e);
							}
							if($naschet){
								$naschet = pn_strip_input($naschet);
								update_bids_naschet($item_id, $naschet);
					
								$notify_tags = array();
								$notify_tags['[sitename]'] = pn_strip_input(get_bloginfo('sitename'));
								$notify_tags['[bid_id]'] = $item_id;
								$notify_tags['[address]'] = $naschet;
								$notify_tags['[sum]'] = $sum;
								$notify_tags['[count]'] = intval(is_deffin($this->m_data,'CONFIRM_COUNT'));
								$notify_tags = apply_filters('notify_tags_generate_address_askbtc', $notify_tags);		

								$user_send_data = array();
								$result_mail = apply_filters('premium_send_message', 0, 'generate_address2_askbtc', $notify_tags, $user_send_data); 
					
								$user_send_data = array(
									'user_email' => $item->email,
									'user_phone' => '',
								);	
								$result_mail = apply_filters('premium_send_message', 0, 'generate_address1_askbtc', $notify_tags, $user_send_data);										
					
							}
						}						
						
						if($naschet){	
							?>				
							<div style="border: 1px solid #8eaed5; padding: 10px 15px; font: 13px Arial; width: 400px; border-radius: 3px; margin: 0 auto;">
								<p><?php printf(__('In order to pay an ID <b>%1$s</b> order send amount <b>%2$s</b> %4$s on address <b>%3$s</b>','pn'),$item_id, $sum, $naschet, $currency); ?></p>
								<?php echo sprintf(__('The bid will be considered paid when we receive <b>%1$s</b> confirmations. <a href="%2$s">Return</a> to bid page and click "Check payment".','pn'), is_deffin($this->m_data,'CONFIRM_COUNT'), get_bids_url($hashed)); ?></p>
							</div>				
							<?php
						} else { 
							pn_display_mess(__('Error','pn'));
						} 
					} else {
						wp_redirect(get_bids_url($hashed));
						exit;
					}
				} else {
					pn_display_mess(__('Application of a non-existent','pn'));
				}
			} else {
				pn_display_mess(__('Application of a non-existent','pn'));
			}
		} 
		
		function myaction_merchant_status(){
		global $wpdb;
		
			do_action('merchant_logs', $this->name);
	
			$address = pn_strip_input(is_param_req('address')); 
			$txid = pn_strip_input(is_param_req('txid'));
			$secret = is_param_req('secret'); 
			$secret2 = is_param_req('secret2'); 
			$currency = strtoupper(is_param_req('currency'));
			$in_summ = is_sum(is_param_req('volume'));
			$confirmations = intval(is_param_req('confirmations'));

			if(urldecode($secret) != is_deffin($this->m_data,'SECRET')){
				die('wrong secret!');
			}

			if(urldecode($secret2) != is_deffin($this->m_data,'SECRET2')){
				die('wrong secret!');
			}
  
			$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE naschet='$address'");
			$id = intval(is_isset($item, 'id'));
			$data = get_data_merchant_for_id($id, $item);
			
			$err = $data['err'];
			$status = $data['status'];
			$m_id = $data['m_id'];
			$vtype = $data['vtype'];	
				 
			if($err == 0){
				if($m_id and $m_id == $this->name){
					if($vtype == $currency){
						$summ1 = $data['summ'];
						$summ2 = $in_summ;
						if($summ2 >= $summ1){		
						
							update_bids_meta($id, 'confirm_count', $confirmations);
						
							if( $confirmations >= intval(is_deffin($this->m_data,'CONFIRM_COUNT')) ) {
								if($status == 'new'){ 
									the_merchant_bid_payed($id, $summ2, '', '', $txid, 'user');			
									die( 'ok' );
								}
							} 
									
						} else {
							die('Payment amount is less than the provisions');
						}
					} else {
						die('Wrong type of currency');
					}
				} else {
					die('Merchant is off in this direction');
				}
			} else {
				die( 'Bid does not exist or the wrong ID' );
			}
			
		}
		
	}
}

new merchant_askbtc(__FILE__, 'AskBTC');