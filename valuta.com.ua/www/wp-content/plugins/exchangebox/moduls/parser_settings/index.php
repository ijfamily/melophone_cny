<?php
if( !defined( 'ABSPATH')){ exit(); }
	
/*
title: [en_US:]Rates parser (old version)[:en_US][ru_RU:]Парсер курсов (старая версия)[:ru_RU]
description: [en_US:]Rates parser (old version)[:en_US][ru_RU:]Парсер курсов (старая версия)[:ru_RU]
version: 1.1
category: [en_US:]Exchange directions[:en_US][ru_RU:]Направления обменов[:ru_RU]
cat: directions
*/	
	
$path = get_extension_file(__FILE__);
$name = get_extension_name($path);		
	
add_action('admin_menu', 'pn_adminpage_parser');
function pn_adminpage_parser(){
global $exchangebox;
	
	add_menu_page(__('Parsers','pn'), __('Parsers','pn'), 'administrator', 'pn_parser', array($exchangebox, 'admin_temp'), $exchangebox->get_icon_link('parser'));  
	add_submenu_page("pn_parser", __('Parsers','pn'), __('Parsers','pn'), 'administrator', "pn_parser", array($exchangebox, 'admin_temp'));
	
}

add_filter('get_pn_parser','get_pn_parser_def');
function get_pn_parser_def($parsers){
	
	$parsers = array(
		'1' => array(
			'title' => 'USD - RUB',
			'birg' => 'CBR.RU',
			'curs' => 1,
		),
		'2' => array(
			'title' => 'RUB - USD',
			'birg' => 'CBR.RU',
			'curs' => 1000,
		),
		'3' => array(
			'title' => 'EUR - RUB',
			'birg' => 'CBR.RU',
			'curs' => 1,
		),
		'4' => array(
			'title' => 'RUB - EUR',
			'birg' => 'CBR.RU',
			'curs' => 1000,
		),
		'5' => array(
			'title' => 'UAH - RUB',
			'birg' => 'CBR.RU',
			'curs' => 100,
		),		
		'6' => array(
			'title' => 'RUB - UAH',
			'birg' => 'CBR.RU',
			'curs' => 100,
		),
		'7' => array(
			'title' => 'KZT - RUB',
			'birg' => 'CBR.RU',
			'curs' => 100,
		),
		'8' => array(
			'title' => 'AMD - RUB',
			'birg' => 'CBR.RU',
			'curs' => 100,
		),
		'9' => array(
			'title' => 'RUB - AMD',
			'birg' => 'CBR.RU',
			'curs' => 1000,
		),
		'10' => array(
			'title' => 'BYN - RUB',
			'birg' => 'CBR.RU',
			'curs' => 1,
		),
		'11' => array(
			'title' => 'CNY - RUB',
			'birg' => 'CBR.RU',
			'curs' => 1,
		),
		'12' => array(
			'title' => 'RUB - CNY',
			'birg' => 'CBR.RU',
			'curs' => 1,
		),		
		/* *** */
		'51' => array(
			'title' => 'EUR - USD',
			'birg' => 'ECB.EU',
			'curs' => 1,
		),
		'52' => array(
			'title' => 'USD - EUR',
			'birg' => 'ECB.EU',
			'curs' => 1,
		),
		/* *** */
		
		'101' => array(
			'title' => 'USD - UAH',
			'birg' => 'NBU',
			'data' => array('buy','sale'),
			'curs' => 100,
		),
		'102' => array(
			'title' => 'UAH - USD',
			'birg' => 'NBU',
			'curs' => 1000,
		),
		'103' => array(
			'title' => 'EUR - UAH',
			'birg' => 'NBU',
			'data' => array('buy','sale'),
			'curs' => 100,
		),
		'104' => array(
			'title' => 'UAH - EUR',
			'birg' => 'NBU',
			'curs' => 1000,
		),	
		/* *** */
		
		'105' => array(
			'title' => 'USD - UAH',
			'birg' => 'PRIVATBANK.UA',
			'data' => array('buy','sale'),
			'curs' => 100,
		),
		'106' => array(
			'title' => 'UAH - USD',
			'birg' => 'PRIVATBANK.UA',
			'curs' => 1000,
		),
		'107' => array(
			'title' => 'EUR - UAH',
			'birg' => 'PRIVATBANK.UA',
			'data' => array('buy','sale'),
			'curs' => 100,
		),
		'108' => array(
			'title' => 'UAH - EUR',
			'birg' => 'PRIVATBANK.UA',
			'curs' => 1000,
		),	
		
		/* *** */
		'151' => array(
			'title' => 'USD - KZT',
			'birg' => 'NATIONALBANK.KZ',
			'curs' => 1,
		),
		'152' => array(
			'title' => 'KZT - USD',
			'birg' => 'NATIONALBANK.KZ',
			'curs' => 1000,
		),		
		'153' => array(
			'title' => 'EUR - KZT',
			'birg' => 'NATIONALBANK.KZ',
			'curs' => 1,
		),
		'154' => array(
			'title' => 'KZT - EUR',
			'birg' => 'NATIONALBANK.KZ',
			'curs' => 1000,
		),		
		'155' => array(
			'title' => 'RUB - KZT',
			'birg' => 'NATIONALBANK.KZ',
			'curs' => 1,
		),
		'156' => array(
			'title' => 'KZT - RUB',
			'birg' => 'NATIONALBANK.KZ',
			'curs' => 100,
		),		
		/* *** */
		'201' => array(
			'title' => 'USD - BYN',
			'birg' => 'NBRB.BY',
			'curs' => 1,
		),
		'202' => array(
			'title' => 'BYN - USD',
			'birg' => 'NBRB.BY',
			'curs' => 10,
		),
		'203' => array(
			'title' => 'EUR - BYN',
			'birg' => 'NBRB.BY',
			'curs' => 1,
		),
		'204' => array(
			'title' => 'BYN - EUR',
			'birg' => 'NBRB.BY',
			'curs' => 10,
		),
		'205' => array(
			'title' => 'RUB - BYN',
			'birg' => 'NBRB.BY',
			'curs' => 100,
		),	
		/* *** */
		
	);

	return $parsers;
}

function get_list_parsers($type='all', $format=''){
	if(!$format){ $format = '[para] [[birg]] [[work]]'; }
	$en_types = array('all','work','notwork');
	if(!in_array($type,$en_types)){ $type = 'all'; } 
	$parsers = apply_filters('get_pn_parser', array());
	$work_parser = get_option('work_parser');
	if(!is_array($work_parser)){ $work_parser = array(); }
	$list_parsers = array();
	if(is_array($parsers)){
		foreach($parsers as $key => $data){
			$work = intval(is_isset($work_parser,$key));
			$title = is_isset($data, 'title');
			$birg = is_isset($data, 'birg');
			$enable_title = __('inactive parser','pn'); 
			if($work){ $enable_title = __('active parser','pn'); }
			$parser_title = '';
			$parser_title = str_replace('[para]',$title,$format);
			$parser_title = str_replace('[birg]',$birg,$parser_title);
			$parser_title = str_replace('[work]',$enable_title,$parser_title);
			if($type == 'all' or $work and $type=='work' or !$work and $type=='notwork'){
				$list_parsers[$birg][] = array(
					'title' => $parser_title,
					'para' => $title,
					'id' => $key,
					'birg' => $birg,
					'work' => $enable_title,
					'enable' => $work,
					'options' => is_isset($data, 'data'),
				);
			}
		}
	}
	$new_list_parsers = array();
	foreach($list_parsers as $k => $array){
		foreach($array as $v){
			$new_list_parsers[$v['id']] = $v;
		}
	}
	
	return $new_list_parsers;
} 

add_action('myaction_request_curscron','my_request_curscron'); 
function my_request_curscron(){
global $exchangebox;	
	
	$exchangebox->up_mode('get');
	
	if(function_exists('parser_upload_data') and check_hash_cron()){
		parser_upload_data();
		_e('Done','pn');
	} else {
		_e('Cron function does not exist','pn');
	}	
}

add_action('pn_adminpage_content_pn_cron','curscron_pn_adminpage_content_pn_cron',9);
add_action('pn_adminpage_content_pn_parser','curscron_pn_adminpage_content_pn_cron',9);
function curscron_pn_adminpage_content_pn_cron(){
?>
	<div class="premium_default_window">
		<?php _e('Cron URL for updating rates of CB and cryptocurrencies','pn'); ?><br /> 
		<a href="<?php echo get_site_url_or(); ?>/request-curscron.html<?php echo get_hash_cron('?'); ?>" target="_blank"><?php echo get_site_url_or(); ?>/request-curscron.html<?php echo get_hash_cron('?'); ?></a>
	</div>	
<?php
} 

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'cron');
$exchangebox->include_patch(__FILE__, 'parser');
$exchangebox->include_patch(__FILE__, 'filters');
$exchangebox->auto_include($path.'/widget');