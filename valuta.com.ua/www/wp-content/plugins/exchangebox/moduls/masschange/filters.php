<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_content_pn_naps', 'masschange_pn_adminpage_content_pn_directions');
function masschange_pn_adminpage_content_pn_directions(){
?>	
<style>
.column-yourcb{ width: 230px!important; }
</style>
<script type="text/javascript">
jQuery(function($){
	$('.directions_masschange').change(function(){
		var id = $(this).attr('id').replace('directions_masschange_','');
		var vale = $(this).val();
		if(vale > 0){
			$('#the_directions_masschange_'+id).show();
			$('#directions_parser_'+id).val('0');
			$('#the_directions_parser_'+id).hide();	
		} else {
			$('#the_directions_masschange_'+id).hide();
		}
	});		
});
</script>
<?php
}

add_action('pn_naps_save', 'masschange_pn_directions_save');
function masschange_pn_directions_save(){
global $wpdb;	
	
	if(isset($_POST['masschange']) and is_array($_POST['masschange'])){
		foreach($_POST['masschange'] as $id => $masschange){
			$id = intval($id);
			$masschange = intval($masschange);
					
						
			$array = array();
			$array['masschange'] = $masschange;				
			$wpdb->update($wpdb->prefix . 'napobmens', $array, array('id'=>$id));
						
			if($masschange > 0){
				update_naps_to_masschange($masschange);
			}			
		}
	}		
}

add_filter('naps_manage_ap_columns', 'masschange_directions_manage_ap_columns');
function masschange_directions_manage_ap_columns($columns){
	
	$new_columns = array();
	foreach($columns as $k => $v){
		$new_columns[$k] = $v;
		
		if($k == 'course2'){
			$new_columns['yourcb'] = __('Snap to individual Central Bank rate','pn');
		}
	}
	
	return $new_columns;
}

add_filter('naps_manage_ap_col', 'masschange_directions_manage_ap_col', 10, 3);
function masschange_directions_manage_ap_col($show, $column_name, $item){
global $wpdb;
	
	if($column_name == 'yourcb'){

		$masschanges = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."masschange ORDER BY title ASC");

		$html = '
		<div style="width: 230px;">
			<select name="masschange['. $item->id .']" autocomplete="off" id="directions_masschange_'. $item->id .'" class="directions_masschange" style="width: 230px; display: block; margin: 0 0 10px;"> 
		';
			$enable = 0;
				$html .= '<option value="0" '. selected($item->masschange,0,false) .'>-- '. __('No item','pn') .' --</option>';

				foreach($masschanges as $massch){
					if($item->masschange == $massch->id){
						$enable = 1;
					}
						
					$html .= '<option value="'. $massch->id .'" '. selected($item->masschange,$massch->id,false) .'>'. pn_strip_input($massch->title) .'</option>';
				}	
		$html .= '
			</select>		
		</div>
		';
		return $html;	
	
	}
	
	return $show;
}

add_action('exb_napobmens_add_rate', 'masschange_tab_direction_tab3', 2);
function masschange_tab_direction_tab3($data){
global $wpdb;

	$masschanges = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."masschange ORDER BY title ASC");
	?>
	<tr>
		<th><?php _e('Snap to individual Central Bank rate','pn'); ?></th>
		<td colspan="2">
			<div class="premium_wrap_standart">
				<select name="masschange" id="the_masschange_select" autocomplete="off"> 
					<option value="0" <?php selected(is_isset($data, 'masschange'),0,true); ?>>-- <?php _e('is not installed','pn'); ?> --</option>
						<?php foreach($masschanges as $mass){ ?>
							<option value="<?php echo $mass->id; ?>" <?php selected(is_isset($data, 'masschange'),$mass->id,true); ?>><?php echo pn_strip_input($mass->title); ?></option>
						<?php } ?>
				</select>
			</div>
		</td>
		<td></td>
	</tr>

<script type="text/javascript">
jQuery(function(){
	$('#the_masschange_select').change(function(){
		$('#the_parser_select').val('0');
	});
});
</script>
<?php	
}  

add_filter('pn_naps_addform_post', 'masschange_pn_direction_addform_post');
function masschange_pn_direction_addform_post($array){
	
	$array['masschange'] = $masschange = intval(is_param_post('masschange'));
	
	return $array;
}

add_action('pn_naps_edit', 'masschange_pn_direction_edit',1,2);
add_action('pn_naps_add', 'masschange_pn_direction_edit',1,2);
function masschange_pn_direction_edit($data_id, $array){
	if($data_id){
		$masschange = intval(is_param_post('masschange'));
		if($masschange > 0 and function_exists('update_naps_to_masschange')){
			update_naps_to_masschange($masschange);
		}
	}	
}