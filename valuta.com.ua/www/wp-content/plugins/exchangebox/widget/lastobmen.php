<?php
class exblastobmen_Widget extends WP_Widget {
	
	public function __construct($id_base = false, $widget_options = array(), $control_options = array()){
		parent::__construct('exblastobmen_info', __('Recent exchange','pn'), $widget_options = array(), $control_options = array());
	}
	
	public function widget($args, $instance){
		extract($args);

		$title = pn_strip_input(is_isset($instance,'title'));
		if(!$title){ $title = __('Recent exchanges','pn'); }
		
		$count = intval(is_isset($instance,'count')); if($count < 1){ $count = 1; }
	
		$table = '';	
		$last_bids = get_last_bids('success', $count);	
		foreach($last_bids as $last_bid){
				
			$array = array(
				'[date]' => $last_bid['createdate'],
				'[title]' => $title,
				'[logo1]' => $last_bid['logo_give'],
				'[sum1]' => is_out_sum($last_bid['sum_give'], 12, 'all'),
				'[vtype1]' => $last_bid['vtype_give'],
				'[logo2]' => $last_bid['logo_get'],
				'[sum2]' => is_out_sum($last_bid['sum_get'], 12, 'all'),
				'[vtype2]' => $last_bid['vtype_get'],
				'[valut1]' => $last_bid['currency_give'],
				'[valut2]' => $last_bid['currency_get'],
			);	
			$array = apply_filters('get_lchange_line_data', $array);
					
			$temp = apply_filters('get_lchange_line', '');	
			$temp = get_replace_arrays($array, $temp);
			$table .= $temp;
		}			
			
		$widget = '
		<div class="lobmen_widget">
			<div class="lobmen_widgetvn">
			<div class="lobmenwidget_title"><div class="lobmenwidget_titlevn">[title]</div></div>
			
				[exchanges]

			</div>
		</div>';	

		$array = array(
			'[title]' => $title,
			'[exchanges]' => $table,
		);		
		
		$widget = apply_filters('lchange_widget_block', $widget);
		$widget = get_replace_arrays($array, $widget);				
			
		echo $widget;					
	}
	
	public function form($instance){ 
	?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title','pn'); ?>: </label><br />
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php $this->get_field_id('title'); ?>" class="widefat" value="<?php echo is_isset($instance,'title'); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Count of exchanges', 'pn'); ?>: </label><br />
			<input type="text" name="<?php echo $this->get_field_name('count'); ?>" id="<?php $this->get_field_id('count'); ?>" class="widefat" value="<?php echo is_isset($instance,'count'); ?>">
		</p>	
	<?php
	}	
	
}

register_widget('exblastobmen_Widget');