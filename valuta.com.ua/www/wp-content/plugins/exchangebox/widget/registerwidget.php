<?php
class exbregisterwidget_Widget extends WP_Widget {
	
	public function __construct($id_base = false, $widget_options = array(), $control_options = array()){
		parent::__construct('exbregisterwidget', __('Sign up','pn'), $control_options = array());
	}
	
	public function widget($args, $instance){
		extract($args);

		global $wpdb, $exchangebox;	

		$ui = wp_get_current_user();
		$user_id = intval($ui->ID);
		
		$title = pn_strip_input(is_isset($instance,'title'));
		if(!$title){ $title  = __('Sign up','pn'); }
		
		if(!$user_id){	
	
			$temp = '
			<div class="register_widget">
				<div class="register_widgetvn register_widget_ins">
					<div class="registerwidget_title register_widget_title">
						<div class="registerwidget_titlevn register_widget_title_ins">'. $title .'</div>
					</div>
					<form method="post" class="ajax_post_form" action="'. get_ajax_link('registerform') .'">';	
	
						$array = array(
							'[result]' => '<div class="resultgo"></div>',
							'[loginput]' => '<input type="text" name="login" value="" />',
							'[emailinput]' => '<input type="text" name="email" value="" />',
							'[passinput]'=> '<input type="password" name="pass" value="" />',
							'[passinput2]'=> '<input type="password" name="pass2" value="" />',
							'[captcha]' => apply_filters('widget_register_form_line',''),
							'[toslink]' => $exchangebox->get_page('tos'),
							'[loginlink]' => $exchangebox->get_page('login'),
							'[lostlink]' => $exchangebox->get_page('lostpass'),
							'[return_link]' => '<input type="hidden" name="return_url" value="'. esc_url(pn_strip_input(is_param_get('return_url'))) .'" />',
							'[rule]' => '<div class="checkbox"><input type="checkbox" name="rcheck" value="1" /> '. sprintf(__('With the <a href="%s">rules</a> of service read and agree','pn'), $exchangebox->get_page('tos') ) .'</div>',
							'[submit]' => '<input type="submit" formtarget="_top" name="" value="'. __('Sign up', 'pn') .'" />',
						);		
		
						$widget = apply_filters('exb_register_widget', '');
						$widget = get_replace_arrays($array, $widget);		

					$temp .= $widget;
					$temp .= '
					</form>
				</div>
			</div>
			';	
			echo $temp;
		
		}
    }

	public function form($instance){
	?>
	<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title','pn'); ?>: </label><br />
		<input type="text" name="<?php echo $this->get_field_name('title'); ?>" class="widefat" id="<?php $this->get_field_id('title'); ?>" value="<?php echo is_isset($instance,'title'); ?>">
	</p>
	<?php
	}
	
}

register_widget('exbregisterwidget_Widget');