<?php 
class exbloginwidget_Widget extends WP_Widget {
	
	public function __construct($id_base = false, $widget_options = array(), $control_options = array()){
		parent::__construct('exbloginwidget', __('Authorization/Menu','pn'), array( 'description' => __('Depending on the type of user login screen is displayed, or the user links','pn') ), $control_options = array());
	}
	
	public function widget($args, $instance){
		extract($args);

		global $wpdb, $exchangebox, $or_site_url;	
		
		$ui = wp_get_current_user();
		$user_id = intval($ui->ID);
		
		if($user_id){
			$title = pn_strip_input(is_isset($instance,'titlem')); 
			if(!$title){ $title = __('Menu','pn'); }
		} else {
			$title = pn_strip_input(is_isset($instance,'title'));
			if(!$title){ $title = __('Authorization','pn'); }			
		}

		$temp = '';
		
	    if($user_id){ 
		
			$temp = '
			<div class="user_widget">
				<div class="user_widgetvn user_widget_ins">
				<div class="userwidget_title user_widget_title">
					<div class="userwidget_titlevn user_widget_title_ins">'. $title .'</div>
				</div>';

					$hash = '';
					if($exchangebox->get_option('txtxml','hash') == 1){
						$hash = get_hash_cron('?');
					}				
				
					$array = array(
						'[user_skidka]' => get_user_discount($user_id),
						'[account]' => $exchangebox->get_page('account'),
						'[usersobmen]'=> $exchangebox->get_page('usersobmen'),
						'[logout]' => get_ajax_link('logout', 'get'),
						'[xml]' => $or_site_url .'/request-exportxml.xml' . $hash,
						'[txt]' => $or_site_url .'/request-exporttxt.txt' . $hash,		
					);
					$array = apply_filters('login_data_array', $array, $ui);
					 
					$parray = array(
						'[partnerstats]' => $exchangebox->get_page('partnerstats'),
						'[payouts]' => $exchangebox->get_page('payouts'),
						'[banners]'=> $exchangebox->get_page('banners'),
						'[partnersFAQ]' => $exchangebox->get_page('partnersFAQ'),
					);		
			
					if($exchangebox->get_option('partners','status') == 1){
						$widget = apply_filters('exb_puser_widget', '');
						$widget = get_replace_arrays($array, $widget, 1);
						$widget = get_replace_arrays($parray, $widget);
					} else {
						$widget = apply_filters('exb_user_widget', '');
						$widget = get_replace_arrays($array, $widget);		
					}		

					$temp .= $widget;
					
			$temp .= '
				</div>
			</div>
			';	
		
		} else { 
		
			$regilink = $exchangebox->get_page('register');
			$lostpasslink = $exchangebox->get_page('lostpass');
		
			$temp = '
			<div class="login_widget">
				<div class="login_widgetvn login_widget_ins">
				<div class="loginwidget_title login_widget_title">
					<div class="loginwidget_titlevn login_widget_title_ins">'. $title .'</div>
				</div>
				<form method="post" class="ajax_post_form" action="'. get_ajax_link('loginform') .'">';
		
					$array = array(
						'[return_link]' => '<input type="hidden" name="return_url" value="'. esc_url(pn_strip_input(is_param_get('return_url'))) .'" />',
						'[result]' => '<div class="resultgo"></div>',
						'[loginput_placeholder]' => '<input type="text" name="logmail" placeholder="'. __('Username', 'pn') .'" value="" />',
						'[passinput_placeholder]'=> '<input type="password" name="pass" placeholder="'. __('Password', 'pn') .'" value="" />',
						'[loginput]' => '<input type="text" name="logmail" value="" />',
						'[passinput]'=> '<input type="password" name="pass" value="" />',
						'[captcha]' => apply_filters('widget_login_form_line',''),
						'[reglink]' => $regilink,
						'[lostlink]' => $lostpasslink,
						'[submit]' => '<input type="submit" formtarget="_top" name="" value="'. __('Log in', 'pn') .'" />',
					);		
		
					$widget = apply_filters('exb_login_widget', '');
					$widget = get_replace_arrays($array, $widget);		

					$temp .= $widget;
				$temp .= '
				</form>
				</div>
			</div>
			';		
		
		} 
		
		echo $temp;
    }

	public function form($instance){
	?>
	<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title authorization','pn'); ?>: </label><br />
		<input type="text" name="<?php echo $this->get_field_name('title'); ?>" class="widefat" id="<?php $this->get_field_id('title'); ?>" value="<?php echo is_isset($instance,'title'); ?>">
	</p>
	<p>
		<label for="<?php echo $this->get_field_id('titlem'); ?>"><?php _e('Title menu','pn'); ?>: </label><br />
		<input type="text" name="<?php echo $this->get_field_name('titlem'); ?>" class="widefat" id="<?php $this->get_field_id('titlem'); ?>" value="<?php echo is_isset($instance,'titlem'); ?>">
	</p>
	<?php
	}
	
}

register_widget('exbloginwidget_Widget');