<?php
class exbrezerv_Widget extends WP_Widget { 
	
	public function __construct($id_base = false, $widget_options = array(), $control_options = array()){
		parent::__construct('exbrezerv_info', __('Currency reserves','pn'), $widget_options = array(), $control_options = array());
	}
	
	public function widget($args, $instance){
		extract($args);

		global $wpdb;
		$title = pn_strip_input(is_isset($instance,'title'));
		if(!$title){ $title = __('Currency reserves','pn'); } 
	
		$output = is_isset($instance,'output');
		if(!is_array($output)){ $output = array(); }
		
		$datas = list_view_valuts($output);	
	
		$reserv = '';
		$r=0;
		
		foreach($datas as $item){ $r++;
					
			if($r%2 == 0){
				$oddeven = 'even';
			} else {
				$oddeven = 'odd';
			}			
			
			$treserv = apply_filters('reserv_widget_one', '', $item, $r);
			if(!trim($treserv)){
				$treserv = '
					<tr class="'. $oddeven .'">
						<td class="rewvalut">
							<div class="obmenlinew">
								<div class="obmenlinewico" style="background: url('. $item['logo'] .') no-repeat center center"></div>
								<div class="obmenlinewtext">
								'. $item['title'] .'
								</div>
								<div class="clear"></div>
							</div>			
						</td>
						<td class="rewrezerv">'. is_out_sum($item['reserv'], 12, 'reserv') .'</td>
					</tr>					
				';
			}
			
			$reserv .= $treserv;
		} 		
		
		$array = array(
			'[table]' => $reserv,
			'[title]' => $title,
		);		
		
		$widget = '
		<div class="rezerv_widget">
			<div class="rezerv_widgetvn">
				<div class="rezervwidget_title">
					<div class="rezervwidget_titlevn">
						[title]
					</div>
				</div>
				<table class="rewtable">	
					[table]
				</table>	
			</div>
		</div>
		';		
		
		$widget = apply_filters('reserv_widget_block', $widget);
		$widget = get_replace_arrays($array, $widget);
		echo $widget;	
	}
	
	public function form($instance){ 
	?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>: </label><br />
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php $this->get_field_id('title'); ?>" class="widefat" value="<?php echo is_isset($instance,'title'); ?>">
		</p>
		
		<div style="padding: 0 0 2px 0;"><label><?php _e('Show currency reserve','pn'); ?>:</label></div>
		<div style="border: 1px solid #dedede; padding: 10px; margin: 0 0 10px 0;">
			<div style="max-height: 200px; overflow-y: scroll;" class="cf_div">
				<div><label style="font-weight: 500;"><input class="check_all" type="checkbox" name="0" value="0"> <?php _e('Check/Uncheck all','pn'); ?></label></div>
				<?php
				global $wpdb;
				$output = is_isset($instance,'output');
				if(!is_array($output)){ $output = array(); }
				$valuts = list_view_valuts();
				foreach($valuts as $item){
				?>
					<div><label><input type="checkbox" name="<?php echo $this->get_field_name('output'); ?>[]" <?php if(in_array($item['id'], $output)){ ?>checked="checked"<?php } ?> value="<?php echo $item['id']; ?>"> <?php echo $item['title']; ?></label></div>
				<?php } ?>
			</div>
		</div>
		
		<script type="text/javascript">
		jQuery(function($){
			$('.check_all').on('change', function(){
				var par = $(this).parents('.cf_div');
				if($(this).prop('checked')){
					par.find('input').prop('checked',true);
				} else {
					par.find('input').prop('checked',false);
				}
			});
		});
		</script>		
	<?php
	} 	
	
}

register_widget('exbrezerv_Widget');