<?php
class statuswork_Widget extends WP_Widget {
	
	public function __construct($id_base = false, $widget_options = array(), $control_options = array()){
		parent::__construct('get_statuswork', __('Service work status','pn'), $widget_options = array(), $control_options = array());
	}
	
	public function widget($args, $instance){
		extract($args);

		$title = __('Service work status','pn');
		$worked = pn_strip_input(is_isset($instance,'worked'));
		$wtime = intval(is_isset($instance,'wtime'));
		$hidedate = intval(is_isset($instance,'hidedate'));
		$text1 = pn_strip_input(is_isset($instance,'text1'));
		if(!$text1){ $text1 = __('Operator','pn') .' <span class="opstatus">'. __('online','pn') .'</span>'; }
	
		$text2 = pn_strip_input(is_isset($instance,'text2'));
		if(!$text2){ $text2 = __('Operator','pn') .' <span class="opstatus">'. __('offline','pn') .'</span>'; }	
	
		$now_time = current_time('timestamp');
	
		if($wtime == 1){
			
			$today = date('d.m.Y',$now_time);
			$yestarday = date('d.m.Y', ($now_time - (24*60*60)));
			$tomorrow = date('d.m.Y', ($now_time + (24*60*60)));
			
			$h1 = zeroise(intval(is_isset($instance,'wt1')),2);
			$m1 = zeroise(intval(is_isset($instance,'wt2')),2);
			$h2 = zeroise(intval(is_isset($instance,'wt3')),2);
			$m2 = zeroise(intval(is_isset($instance,'wt4')),2);
		
			$worked = 'offline';
		
			if($h1 > $h2 or $h1 == $h2 and $m1 > $m2){ /* если график работы в течении двух дней */
				
				$time1 = strtotime($yestarday .' '. $h1.':'.$m1);
				$time2 = strtotime($today .' '. $h2.':'.$m2);
				$time3 = strtotime($today .' '. $h1.':'.$m1);
				$time4 = strtotime($tomorrow .' '. $h2.':'.$m2);

				if($now_time >= $time1 and $now_time < $time2 or $now_time >= $time3 and $now_time < $time4){
					$worked = 'online';
				}				
				
			}  else { /* если график работы в течении дня */
	
				$time1 =  strtotime($today.' '. $h1.':'.$m1);
				$time2 =  strtotime($today.' '. $h2.':'.$m2);
				if($now_time >= $time1 and $now_time < $time2){
					$worked = 'online';
				}	
	
			}					
		}
	
		$temp = '
		<div class="statuswork_widget '. $worked .'">
			<div class="statuswork_widget_title"><div class="statuswork_widget_titlevn">'. $title .'</div></div>';
    
			if($hidedate == 0){
				$temp .='<div class="statuswork_widget_date">'. date('d.m.Y H:i:s',$now_time) .'</div>';
			}
	
			$temp .='
			<div class="statusworkblock">
				<div class="statusworkblockvn '. $worked .'">';
					if($worked == 'online'){ 
						$temp .= $text1;
					} else {
						$temp .= $text2;
					} 
			$temp .='
				</div>
			</div>
		</div>
		';

		$temp = apply_filters('exb_statusworkwidget',$temp, $worked, $title, $now_time, $hidedate, $text1, $text2);

		echo $temp;

	}

	public function form($instance){ 
	?>
		<p>
		<label for="<?php echo $this->get_field_id('text1'); ?>"><?php _e('Text','pn'); ?> "<?php _e('Operator online','pn'); ?>": </label><br />
		<input type="text" name="<?php echo $this->get_field_name('text1'); ?>" id="<?php $this->get_field_id('text1'); ?>" class="widefat" value="<?php echo is_isset($instance,'text1'); ?>">
		</p>
		<p>
		<label for="<?php echo $this->get_field_id('text2'); ?>"><?php _e('Text','pn'); ?> "<?php _e('Operator offline','pn'); ?>": </label><br />
		<input type="text" name="<?php echo $this->get_field_name('text2'); ?>" id="<?php $this->get_field_id('text2'); ?>" class="widefat" value="<?php echo is_isset($instance,'text2'); ?>">
		</p>
		<p>
		<label for="<?php echo $this->get_field_id('hidedate'); ?>"><?php _e('Hide date?','pn'); ?>: </label><br />
		<select name="<?php echo $this->get_field_name('hidedate'); ?>" id="<?php $this->get_field_id('hidedate'); ?>">
			<option value="0"><?php _e('No','pn'); ?></option>
			<option value="1" <?php selected(is_isset($instance,'hidedate'),1); ?>><?php _e('Yes','pn'); ?></option>
		</select>
		</p>
		<p>
		<label for="<?php echo $this->get_field_id('worked'); ?>"><?php _e('Operator status','pn'); ?>: </label><br />
		<select name="<?php echo $this->get_field_name('worked'); ?>" id="<?php $this->get_field_id('worked'); ?>">
			<option value="online"><?php _e('ONLINE','pn'); ?></option>
			<option value="offline" <?php selected(is_isset($instance,'worked'),'offline'); ?>><?php _e('OFFLINE','pn'); ?></option>
		</select>
		</p>
		<p>
		<label for="<?php echo $this->get_field_id('wtime'); ?>"><?php _e('Observe mode?','pn'); ?>: </label><br />
		<select name="<?php echo $this->get_field_name('wtime'); ?>" id="<?php $this->get_field_id('wtime'); ?>">
			<option value="0"><?php _e('No','pn'); ?></option>
			<option value="1" <?php selected(is_isset($instance,'wtime'), 1); ?>><?php _e('Yes','pn'); ?></option>
		</select>
		</p>
		<p><label><?php _e('Work time','pn'); ?>: </label><br />
			<select name="<?php echo $this->get_field_name('wt1'); ?>">
				<option value="00">00</option>
				<?php $r=0; while($r++<23){ if($r < 10){ $r = '0'.$r; } ?>
					<option value="<?php echo $r; ?>" <?php selected(is_isset($instance,'wt1'), $r); ?>><?php echo $r; ?></option>
				<?php } ?>
			</select> :
			<select name="<?php echo $this->get_field_name('wt2'); ?>">
				<option value="00">00</option>
				<?php $r=0; while($r++<59){ if($r < 10){ $r = '0'.$r; } ?>
					<option value="<?php echo $r; ?>" <?php selected(is_isset($instance,'wt2'), $r); ?>><?php echo $r; ?></option>
				<?php } ?>
			</select>	
			<strong style="padding: 0 5px;">-</strong>
			<select name="<?php echo $this->get_field_name('wt3'); ?>">
				<option value="00">00</option>
				<?php $r=0; while($r++<23){ if($r < 10){ $r = '0'.$r; } ?>
					<option value="<?php echo $r; ?>" <?php selected(is_isset($instance,'wt3'), $r); ?>><?php echo $r; ?></option>
				<?php } ?>
			</select> :
			<select name="<?php echo $this->get_field_name('wt4'); ?>">
				<option value="00">00</option>
				<?php $r=0; while($r++<59){ if($r < 10){ $r = '0'.$r; } ?>
					<option value="<?php echo $r; ?>" <?php selected(is_isset($instance,'wt4'), $r); ?>><?php echo $r; ?></option>
				<?php } ?>
			</select>	
		</p>
	<?php
	}
	
}

register_widget('statuswork_Widget');