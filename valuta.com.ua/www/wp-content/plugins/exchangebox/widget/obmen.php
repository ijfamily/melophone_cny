<?php
class exbobmen_Widget extends WP_Widget {
	
	public function __construct($id_base = false, $widget_options = array(), $control_options = array()){
		parent::__construct('exbobmen_info', __('Exchange widget','pn'), $widget_options = array(), $control_options = array());
	}
	
	public function widget($args, $instance){
		extract($args); 

		global $obmenwidgettrue;
		if(!$obmenwidgettrue){
			$obmenwidgettrue = true;
			
			if(function_exists('the_exchange_widget')){
				the_exchange_widget();
			}
			
		}
	}
	
}

register_widget('exbobmen_Widget');