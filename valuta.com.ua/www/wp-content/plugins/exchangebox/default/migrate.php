<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_migrate', 'pn_adminpage_title_pn_migrate');
function pn_adminpage_title_pn_migrate($page){
	_e('Migration','pn');
} 

/* настройки */
add_action('pn_adminpage_content_pn_migrate','def_pn_adminpage_content_pn_migrate');
function def_pn_adminpage_content_pn_migrate(){
	
	$form = new PremiumForm();
?>
<div class="premium_body">
	<table class="premium_standart_table">
		<?php
		$form->h3(sprintf(__('Migration (if version is lesser than %s)','pn'),'6.0'), '');
		
		$r=0;
		while($r++<31){
		?>		
		<tr>
			<td>		
				<input name="submit" type="submit" class="button pn_prbar" data-count-url="<?php pn_the_link_post('migrate_step_count','post'); ?>&step=1_<?php echo $r; ?>" data-title="<?php printf(__('Step %s','pn'),$r); ?>" value="<?php printf(__('Step %s','pn'),$r); ?>" />	
				<input name="submit" type="submit" class="button pn_prbar" data-count-url="<?php pn_the_link_post('migrate_step_count','post'); ?>&step=1_<?php echo $r; ?>&tech=1" data-title="<?php printf(__('Step %s','pn'),$r); ?>" value="<?php printf(__('Technical step %s','pn'),$r); ?>" />		
			</td>
		</tr>
		<?php 
		} 
		?>
	</table>
</div>

<div class="premium_body">
	<table class="premium_standart_table">
		<?php
		$form->h3(sprintf(__('Migration (if version is lesser than %s)','pn'),'6.1'), '');
		
		$r=0;
		while($r++<5){
		?>		
		<tr>
			<td>		
				<input name="submit" type="submit" class="button pn_prbar" data-count-url="<?php pn_the_link_post('migrate_step_count','post'); ?>&step=2_<?php echo $r; ?>" data-title="<?php printf(__('Step %s','pn'),$r); ?>" value="<?php printf(__('Step %s','pn'),$r); ?>" />	
				<input name="submit" type="submit" class="button pn_prbar" data-count-url="<?php pn_the_link_post('migrate_step_count','post'); ?>&step=2_<?php echo $r; ?>&tech=1" data-title="<?php printf(__('Step %s','pn'),$r); ?>" value="<?php printf(__('Technical step %s','pn'),$r); ?>" />		
			</td>
		</tr>
		<?php 
		} 
		?>
	</table>
</div>

<div class="premium_body">
	<table class="premium_standart_table">
		<?php
		$form->h3(sprintf(__('Migration (if version is lesser than %s)','pn'),'7.0'), '');
		
		$r=0;
		while($r++<10){
		?>		
		<tr>
			<td>		
				<input name="submit" type="submit" class="button pn_prbar" data-count-url="<?php pn_the_link_post('migrate_step_count','post'); ?>&step=3_<?php echo $r; ?>" data-title="<?php printf(__('Step %s','pn'),$r); ?>" value="<?php printf(__('Step %s','pn'),$r); ?>" />	
				<input name="submit" type="submit" class="button pn_prbar" data-count-url="<?php pn_the_link_post('migrate_step_count','post'); ?>&step=3_<?php echo $r; ?>&tech=1" data-title="<?php printf(__('Step %s','pn'),$r); ?>" value="<?php printf(__('Technical step %s','pn'),$r); ?>" />		
			</td>
		</tr>
		<?php 
		} 
		?>
	</table>
</div>
	
<div class="premium_shadow js_techwindow"></div>
<div class="prbar_wrap js_techwindow">
	<div class="prbar_wrap_ins">
		<div class="prbar_close"></div>
		<div class="prbar_title"></div>
		<div class="prbar_content">
		
			<div class="prbar_num">
				<?php printf(__('Found: %1s %2s %3s requests','pn'), '<input type="text" name="" class="prbar_num_count" value="','0','" />'); ?>
			</div>
			<div class="prbar_control">
				<div class="prbar_input">
					<?php _e('Perform','pn'); ?>: <input type="text" name="" class="prbar_count" value="100" />
				</div>
				<div class="prbar_submit"><?php _e('Run','pn'); ?></div>
					<div class="premium_clear"></div>
			</div>
			
			<div class="prbar_ind"><div class="prbar_ind_abs"></div><div class="prbar_ind_text">0%</div></div>
			<div class="prbar_log_wrap">
				<div class="prbar_log"></div>			
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(function($){
	$(document).PrBar({ 
		trigger: '.pn_prbar',
		start_title: '<?php _e('determining the number of requests','pn'); ?>...',
		end_title: '<?php _e('number of requests defined','pn'); ?>',
		line_text: '<?php _e('%now% of %max% steps completed','pn'); ?>',
		line_success: '<?php _e('step %now% is successful','pn'); ?>',
		end_progress: '<?php _e('action is completed','pn'); ?>',
		success: function(res){
			res.prop('disabled', true);
		}
	});
});
</script>
<?php
}

add_action('premium_action_migrate_step_count','def_premium_action_migrate_step_count');
function def_premium_action_migrate_step_count(){
global $wpdb;	

	only_post();

	$log = array();
	$log['status'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = '';
	$log['count'] = 0;
	$log['link'] = '';
	
	$step = is_param_get('step');
	$tech = intval(is_param_get('tech'));
	if(current_user_can('administrator')){
		$count = 0;
		
		if(!$tech){
			
			if($step == '1_1'){
				$count = 1;				
			}	

			if($step == '1_2'){	
				$tables = array(
					'_partners','_user_discounts','_archive_data','_plinks','_partner_pers','_payoutuser','_blacklist','_vtypes','_valuts_meta','_valuts',
					'_transactionrezerv','_custom_fields_valut','_masschange','_vschets','_naps_meta','_napobmens',
					'_zapros_rezerv','_bids_meta','_bids','_abitcoin',
				);
				$count = count($tables);
			}			
			
			if($step == '1_3'){
				$count = 1;				
			}

			if($step == '1_4'){
				$count = 1;				
			}			

			if($step == '1_5'){
				$count = 1;				
			}

			if($step == '1_6'){
				$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."exbblacklist");
				if($query == 1){
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."exbblacklist");
				}
			}

			if($step == '1_7'){
				$count = 1;				
			}	

			if($step == '1_8'){
				$count = $wpdb->get_var("SELECT COUNT(umeta_id) FROM ". $wpdb->prefix ."usermeta WHERE meta_key='partproc'");				
			}

			if($step == '1_9'){
				$count = $wpdb->query("SELECT id FROM ".$wpdb->prefix."transactionuser");			
			}

			if($step == '1_10'){
				$count = $wpdb->get_var("SELECT COUNT(umeta_id) FROM ". $wpdb->prefix ."usermeta WHERE meta_key='ip_br'");		
			}

			if($step == '1_11'){
				$count = $wpdb->get_var("SELECT COUNT(umeta_id) FROM ". $wpdb->prefix ."usermeta WHERE meta_key='userfio'");			
			}

			if($step == '1_12'){
				$count = $wpdb->get_var("SELECT COUNT(umeta_id) FROM ". $wpdb->prefix ."usermeta WHERE meta_key='userskidka'");		
			}

			if($step == '1_13'){
				$count = $wpdb->get_var("SELECT COUNT(umeta_id) FROM ". $wpdb->prefix ."usermeta WHERE meta_key='referrer'");		
			}

			if($step == '1_14'){
				$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."perehod");
				if($query == 1){				
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."perehod");
				}	
			}			

			if($step == '1_15'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."payoutuser WHERE user_login=''");		
			}

			if($step == '1_16'){
				$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."exbpskidka");
				if($query == 1){
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."exbpskidka");
				}
			}

			if($step == '1_17'){
				$count = 1;		
			}

			if($step == '1_18'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."valuts WHERE valut_reserv='0'");		
			}

			if($step == '1_19'){
				$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."valutsmeta");
				if($query == 1){
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."valutsmeta");	
				}	
			}

			if($step == '1_20'){
				$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."valutsmeta");
				if($query == 1){
					$count = 1;	
				}	
			}			
			
			if($step == '1_21'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."napobmens WHERE parser='1'");				
			}
			
			if($step == '1_22'){
				$count = 1;		
			}
			
			if($step == '1_23'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."napobmens");				
			}			
			
			if($step == '1_24'){
				$count = 1;				
			}

			if($step == '1_25'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."bids WHERE exsum='0' AND status != 'auto'");				
			}

			if($step == '1_26'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."bids");				
			}

			if($step == '1_27'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."bids WHERE status != 'auto' AND naschet != ''");				
			}

			if($step == '1_28'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."vschets");				
			}

			if($step == '1_29'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."bids");				
			}

			if($step == '1_30'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."valuts");				
			}

			if($step == '1_31'){
				$count = 1;				
			}

			if($step == '2_1'){
				$count = 1;				
			}	

			if($step == '2_2'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."custom_fields_valut");				
			}

			if($step == '2_3'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."napobmens");				
			}	

			if($step == '2_4'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."naps_meta");				
			}

			if($step == '2_5'){	 /*****************/
				$count = $wpdb->get_var("SELECT COUNT(umeta_id) FROM ". $wpdb->prefix ."usermeta");
			}

			if($step == '3_1'){	 /*****************/
				$count = 1;	
			}

			if($step == '3_2'){	 /*****************/
				$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."change");
				if($query == 1){				
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."change");
				}
			}

			if($step == '3_3'){	 /*****************/
				$count = 1;	
			}	

			if($step == '3_4'){
				$count = $wpdb->get_var("SELECT COUNT(ID) FROM ". $wpdb->prefix ."users");		
			}

			if($step == '3_5'){
				$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."warning_mess");
				if($query == 1){				
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."warning_mess");
				}
			}

			if($step == '3_6'){
				$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."exbotziv");
				if($query == 1){				
					$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."exbotziv");
				}
			}

			if($step == '3_7'){
				$count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."napobmens");				
			}

			if($step == '3_8'){	 /*****************/
				$count = 1;	
			}

			if($step == '3_9'){	 /*****************/
				$count = 1;	
			}

			if($step == '3_10'){	 /*****************/
				$count = 1;	
			}			
			
		}
		
		$log['status'] = 'success';
		$log['count'] = $count;
		$log['link'] = pn_link_post('migrate_step_request','post').'&step='.$step;
		$log['status_text'] = __('Ok!','pn');

	} else {
		$log['status'] = 'error';
		$log['status_code'] = 1; 
		$log['status_text'] = __('Error! insufficient privileges!','pn');
	}
	
	echo json_encode($log);
	exit;	
}

add_action('premium_action_migrate_step_request','def_premium_action_migrate_step_request');
function def_premium_action_migrate_step_request(){
global $wpdb, $exchangebox;	

	only_post();

	$log = array();
	$log['status'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = '';
	$log['count'] = 0;
	$log['link'] = '';
	
	$step = is_param_get('step');
	$idspage = intval(is_param_post('idspage'));
	$limit = intval(is_param_post('limit')); if($limit < 1){ $limit = 1; }
	$offset = ($idspage - 1) * $limit;
	if(current_user_can('administrator')){
		
		if($step == '1_1'){	 /*****************/

			/* old change */
			$pages = get_option('exb_pages');
			if($pages){
				update_option('the_pages', $pages); 
				delete_option('exb_pages');		
			}	
			$exb_mails = get_option('exb_mails');
			if($exb_mails){
				delete_option('exb_mails'); 
			}
			$banners = get_option('exb_banners');
			if($banners){
				update_option('banners', $banners);
				delete_option('exb_banners'); 
			}	
			$exb_change = get_option('exb_change');
			if($exb_change){ 
				if(isset($exb_change['adminpage'])){
					$exchangebox->update_option('admin_panel_url', '', trim($exb_change['adminpage']));
				}
				if(isset($exb_change['otziv']['count'])){
					$exchangebox->update_option('reviews', 'count', $exb_change['otziv']['count']);
				}	
				if(isset($exb_change['otziv']['gostprava'])){
					$exchangebox->update_option('reviews', 'method', $exb_change['otziv']['gostprava']);
				}		
				if(isset($exb_change['otziv']['site'])){
					$website = 0;
					if($exb_change['otziv']['site'] == 'true'){
						$website = 1;
					}
					$exchangebox->update_option('reviews', 'website', $website);
				}		
				if(isset($exb_change['captcha']['otziv'])){
					$captcha = 0;
					if($exb_change['captcha']['otziv'] == 'true'){
						$captcha = 1;
					}
					$exchangebox->update_option('captcha','reviewsform',$captcha);
				}
				if(isset($exb_change['captcha']['contact'])){
					$captcha = 0;
					if($exb_change['captcha']['contact'] == 'true'){
						$captcha = 1;
					}
					$exchangebox->update_option('captcha','contactform',$captcha);
				}
				if(isset($exb_change['captcha']['login'])){
					$captcha = 0;
					if($exb_change['captcha']['login'] == 'true'){
						$captcha = 1;
					}
					$exchangebox->update_option('captcha','loginform',$captcha);
				}
				if(isset($exb_change['captcha']['register'])){
					$captcha = 0;
					if($exb_change['captcha']['register'] == 'true'){
						$captcha = 1;
					}
					$exchangebox->update_option('captcha','registerform',$captcha);
				}
				if(isset($exb_change['captcha']['lostpass'])){
					$captcha = 0;
					if($exb_change['captcha']['lostpass'] == 'true'){
						$captcha = 1;
					}
					$exchangebox->update_option('captcha','lostpassform',$captcha);
				}		
				if(isset($exb_change['change']['pptrue'])){
					$item = 0;
					if($exb_change['change']['pptrue'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('partners','status',$item);
				}
				if(isset($exb_change['partners']['minvipl'])){
					$item = is_sum($exb_change['partners']['minvipl']);
					$exchangebox->update_option('partners','minpay',$item);
				}		
				if(isset($exb_change['partners']['partm'])){
					$item = 0;
					if($exb_change['partners']['partm'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('partners','calc',$item);
				}		
				if(isset($exb_change['partners']['pvrez'])){
					$item = 0;
					if($exb_change['partners']['pvrez'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('partners','reserv',$item);
				}					
				if(isset($exb_change['courez'])){
					$exchangebox->update_option('exchange', 'courez', $exb_change['courez']);
				}		
				if(isset($exb_change['change']['cursfile'])){
					$exchangebox->update_option('change', 'cursfile', intval($exb_change['change']['cursfile']));
					$exchangebox->update_option('change', 'cursfilelen', 5);
				}	
				if(isset($exb_change['change']['pfname'])){
					$item = 0;
					if($exb_change['change']['pfname'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('exchange','pfname',$item);
				}
				if(isset($exb_change['change']['piname'])){
					$item = 0;
					if($exb_change['change']['piname'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('exchange','piname',$item);
				}
				if(isset($exb_change['change']['poname'])){
					$item = 0;
					if($exb_change['change']['poname'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('exchange','poname',$item);
				}
				if(isset($exb_change['change']['ptel'])){
					$item = 0;
					if($exb_change['change']['ptel'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('exchange','ptel',$item);
				}
				if(isset($exb_change['change']['pskype'])){
					$item = 0;
					if($exb_change['change']['pskype'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('exchange','pskype',$item);
				}
				if(isset($exb_change['change']['techreg'])){
					$item = 0;
					if($exb_change['change']['techreg'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('exchange','techreg',$item);
				}
				if(isset($exb_change['change']['techregtext'])){
					$exchangebox->update_option('exchange', 'techregtext', $exb_change['change']['techregtext']);
				}		
				$helps = array('fio_f','fio_i','fio_o','email','tel','skype');
				foreach($helps as $help){
					if(isset($exb_change['help'][$help])){
						$exchangebox->update_option('help', $help, $exb_change['help'][$help]);
					}
				}
				$txts = array('txt1','txt2','txt3','txt4','txt5','txt6');
				foreach($txts as $help){
					if(isset($exb_change['wablons'][$help])){
						$exchangebox->update_option('wablons', $help, $exb_change['wablons'][$help]);
					}
				}		
				$cursed = array(
					'RUB' => 'usdrub',
					'EUR' => 'usdeur',
					'UAH' => 'usduah',
					'AMD' => 'usdamd',
					'KZT' => 'usdkzt',
					'GLD' => 'usdgld',
					'BYN' => 'usdbyn',
					'BTC' => 'usdbtc',
					'TRY' => 'usdtry',
				);
				foreach($cursed as $key => $curse){
					if(isset($exb_change['vncurs'][$curse])){
						$item = is_sum($exb_change['vncurs'][$curse]);
						$wpdb->update($wpdb->prefix ."vtypes", array('vncurs' => $item), array('xname'=> $key));
					}
				}
				if(isset($exb_change['change']['delbid'])){
					$item = 0;
					if($exb_change['change']['delbid'] == 'true'){
						$item = 1;
					}
					$exchangebox->update_option('exchange','autodelete',$item);
				}
				if(isset($exb_change['change']['day'])){
					$exchangebox->update_option('exchange', 'ad_h', $exb_change['change']['day']);
				}
				if(isset($exb_change['change']['minuts'])){
					$exchangebox->update_option('exchange', 'ad_m', $exb_change['change']['minuts']);
				}		

				delete_option('exb_change'); 
			}	
			/* end old change */					
		
			$mailtemp = get_option('mailtemp'); 
			if(!is_array($mailtemp)){ $mailtemp = array(); }
			
			if(isset($mailtemp['worked_bids1'])){
				$mailtemp['payed_bids1'] = $mailtemp['realpay_bids1'] = $mailtemp['worked_bids1'];
				unset($mailtemp['worked_bids1']);
			}
			if(isset($mailtemp['worked_bids2'])){
				$mailtemp['payed_bids2'] = $mailtemp['realpay_bids2'] = $mailtemp['worked_bids2'];
				unset($mailtemp['worked_bids2']);
			}			

			update_option('mailtemp', $mailtemp);

			$check_new_user = get_option('check_new_user');
			if(!is_array($check_new_user)){
				$check_new_user = array('0','1','2','3','4');
				update_option('check_new_user', $check_new_user);
			}		
		}

		if($step == '1_2'){	 /*****************/			
		
			$tables = array(
				'_partners', '_user_discounts','_archive_data','_plinks','_partner_pers','_payoutuser','_blacklist','_vtypes','_valuts_meta','_valuts',
				'_transactionrezerv','_custom_fields_valut','_masschange','_vschets','_naps_meta','_napobmens',
				'_zapros_rezerv','_bids_meta','_bids',
			);	
			$array = array_slice($tables, $offset, $limit);
			foreach($array as $tb){
				$tb = ltrim($tb,'_');
				$table = $wpdb->prefix . $tb;
				$query = $wpdb->query("CHECK TABLE {$table}");
				if($query == 1){
					$wpdb->query("ALTER TABLE {$table} ENGINE=InnoDB");
				}
			}

		}		
		
		if($step == '1_3'){	 /*****************/
			$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."term_meta");
			if($query == 1){		
				$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."term_meta");
				foreach($items as $item){
					$id = $item->id;
					$arr = array(
						'term_id' => $item->item_id,
						'meta_key' => $item->meta_key,
						'meta_value' => $item->meta_value,
					);
					$wpdb->insert($wpdb->prefix.'termmeta', $arr);
					$wpdb->query("DELETE FROM ". $wpdb->prefix ."term_meta WHERE id = '$id'");
				}
			}				
		}			
		
		if($step == '1_4'){	 /*****************/
			$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."exbskidka");
			if($query == 1){
				
				$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."exbskidka");
				foreach($items as $item){
					
					$arr = array();
					$arr['id'] = $item->id;
					$arr['sumec'] = is_sum($item->sumec);
					$arr['discount'] = is_sum($item->skidka);
					$wpdb->insert($wpdb->prefix ."user_discounts", $arr);
				}				

			}		
		}
		
		if($step == '1_5'){	 /*****************/
		
			$arr = array('vname','vlogo','xname','vtype','txt1','txt2');
			foreach($arr as $arrs){
				$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE '$arrs'");
				if ($query) { 
					$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts CHANGE `$arrs` `$arrs` longtext NOT NULL");
				}			
			}
			
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts WHERE xml_value=''");
			foreach($items as $item){
				$value = is_xml_value($item->xname);
				$id = intval($item->id);
				
				$wpdb->update($wpdb->prefix ."valuts", array('xml_value'=>$value), array('id'=>$id));
			}			
			
		}

		if($step == '1_6'){	 /*****************/
		
			$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."exbblacklist");
			if($query == 1){
				
				$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."exbblacklist LIMIT {$offset},{$limit}");
				foreach($items as $item){
					$arr = array();
					$arr['id'] = $item->id;
					$arr['meta_key'] = pn_strip_input($item->meta_key);
					$arr['meta_value'] = pn_strip_input($item->meta_value);
					$wpdb->insert($wpdb->prefix ."blacklist", $arr);
				}				

			}

		}

		if($step == '1_7'){	 /*****************/
		
			$wpdb->update($wpdb->prefix ."usermeta", array('meta_key'=>'user_bann','meta_value'=> 1), array('meta_key'=>'banned','meta_value'=>'2'));
			$wpdb->update($wpdb->prefix ."usermeta", array('meta_key'=>'user_phone'), array('meta_key'=>'usertel'));
			$wpdb->update($wpdb->prefix ."usermeta", array('meta_key'=>'user_skype'), array('meta_key'=>'userskype'));
			
		}

		if($step == '1_8'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."usermeta WHERE meta_key='partproc' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$value = is_sum($item->meta_value);
				$umeta_id = intval($item->umeta_id);
				$user_id = intval($item->user_id);
				
				$wpdb->update($wpdb->prefix ."users", array('partner_pers'=>$value), array('ID'=>$user_id));
				$wpdb->query("DELETE FROM ".$wpdb->prefix."usermeta WHERE umeta_id = '$umeta_id'");
			}
			
		}

		if($step == '1_9'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."transactionuser LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$wpdb->query("DELETE FROM ".$wpdb->prefix."transactionuser WHERE id = '{$item->id}'");
				$wpdb->update($wpdb->prefix ."bids", array('ref_id' => $item->userid, 'pcalc' => 1, 'ref_sum' => $item->bsumm), array('id'=> $item->bidid));
			}			
			
		}

		if($step == '1_10'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."usermeta WHERE meta_key='ip_br' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$value = unserialize($item->meta_value);
				$umeta_id = intval($item->umeta_id);
				$user_id = intval($item->user_id);
				if(is_array($value)){
					$user_browser = pn_maxf(pn_strip_input(is_isset($value,'browser')),500);
					$user_ip = pn_strip_input(preg_replace( '/[^0-9a-fA-F:., ]/', '',is_isset($value,'ip') ));
					update_user_meta( $user_id, 'user_browser', $user_browser) or add_user_meta( $user_id, 'user_browser', $user_browser, true );
					update_user_meta( $user_id, 'user_ip', $user_ip) or add_user_meta( $user_id, 'user_ip', $user_ip, true );
				}
				$wpdb->query("DELETE FROM ".$wpdb->prefix."usermeta WHERE umeta_id = '$umeta_id'");
			}
			
		}

		if($step == '1_11'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."usermeta WHERE meta_key='userfio' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$value = unserialize($item->meta_value);
				$umeta_id = intval($item->umeta_id);
				$user_id = intval($item->user_id);
				if(is_array($value)){
					$first_name = pn_maxf_mb(pn_strip_input(is_isset($value,'i')),250);
					$last_name = pn_maxf_mb(pn_strip_input(is_isset($value,'f')),250);
					$second_name = pn_maxf_mb(pn_strip_input(is_isset($value,'o')),250);
					update_user_meta( $user_id, 'first_name', $first_name) or add_user_meta( $user_id, 'first_name', $first_name, true );
					update_user_meta( $user_id, 'last_name', $last_name) or add_user_meta( $user_id, 'last_name', $last_name, true );
					update_user_meta( $user_id, 'second_name', $second_name) or add_user_meta( $user_id, 'second_name', $second_name, true );
				}
				$wpdb->query("DELETE FROM ".$wpdb->prefix."usermeta WHERE umeta_id = '$umeta_id'");
			}
			
		}

		if($step == '1_12'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."usermeta WHERE meta_key='userskidka' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$value = is_sum($item->meta_value);
				$umeta_id = intval($item->umeta_id);
				$user_id = intval($item->user_id);
				
				$wpdb->update($wpdb->prefix ."users", array('user_discount'=>$value), array('ID'=>$user_id));
				$wpdb->query("DELETE FROM ".$wpdb->prefix."usermeta WHERE umeta_id = '$umeta_id'");
			}
			
		}

		if($step == '1_13'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."usermeta WHERE meta_key='referrer' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$value = intval($item->meta_value);
				$umeta_id = intval($item->umeta_id);
				$user_id = intval($item->user_id);
				
				$wpdb->update($wpdb->prefix ."users", array('ref_id'=>$value), array('ID'=>$user_id));
				$wpdb->query("DELETE FROM ".$wpdb->prefix."usermeta WHERE umeta_id = '$umeta_id'");
			}
			
		}

		if($step == '1_14'){	 /*****************/
		
			$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."perehod");
			if($query == 1){
				$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."perehod LIMIT {$offset},{$limit}");
				foreach($datas as $data){
					$id = $data->id;
	
					$array = array();
					$array['user_id'] = is_isset($data,'userid');
					$array['user_login'] = is_isset($data,'user_login');
					$array['pdate'] = is_isset($data,'tdate');
					$array['pbrowser'] = is_isset($data,'tbrowser');
					$array['pip'] = is_isset($data,'tip');
					$array['prefer'] = is_isset($data,'trefer');
					
					$cc_count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."plinks WHERE id='$id'");
					if($cc_count == 0){
						$array['id'] = $id;
						$wpdb->insert($wpdb->prefix ."plinks", $array);	
					} else {
						$wpdb->update($wpdb->prefix ."plinks", $array, array('id'=> $id));	
					}					

				}
			}			
			
		}		

		if($step == '1_15'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."payoutuser WHERE user_login='' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$id = intval($item->id);
				$arr = array();
				$ui = get_userdata($item->userid);
				if(isset($ui->user_login)){
					$arr['user_login'] = $ui->user_login;
					$wpdb->update($wpdb->prefix ."payoutuser", $arr, array('id'=>$id));
				}
			}		
			
		}
		
		if($step == '1_16'){	 /*****************/
		
			$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."exbpskidka");
			if($query == 1){
				$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."exbpskidka LIMIT {$offset},{$limit}");
				foreach($items as $item){
					$arr = array();
					$arr['id'] = $item->id;
					$arr['sumec'] = is_sum($item->sumec); 
					$arr['pers'] = is_sum($item->skidka);
					$wpdb->insert($wpdb->prefix ."partner_pers", $arr);
				}				
			}			
			
		}

		if($step == '1_17'){	 /*****************/
				
			$wpdb->update($wpdb->prefix ."bids", array('status'=>'payed'), array('status'=>'worked'));
			
		}

		if($step == '1_18'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts WHERE valut_reserv='0' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				update_valut_reserv($item->id);
			}
			
		}

		if($step == '1_19'){	 /*****************/
		
			$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."valutsmeta");
			if($query == 1){
				
				$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valutsmeta LIMIT {$offset},{$limit}");
				foreach($items as $item){
					
					$arr = array();
					$arr['id'] = $item->id;
					$arr['tech_name'] = pn_strip_input($item->xname);
					$arr['cf_name'] = pn_strip_input($item->xname);
					$arr['vid'] = intval($item->vid);
					$arr['valut_id'] = intval($item->valid);
					$arr['cf_req'] = $item->vmerr;
					$arr['place_id'] = $item->howe;
					$arr['minzn'] = intval($item->numleng);
					$arr['maxzn'] = 100;
					$arr['firstzn'] = '';		
					$arr['helps'] = pn_strip_input($item->helps);
					$arr['datas'] = pn_strip_input($item->datas);
					$arr['status'] = 1;
					$arr['cf_hidden'] = 0;
					$arr['cf_order'] = intval($item->cf_order);
					$wpdb->insert($wpdb->prefix ."custom_fields_valut", $arr);
				}						
				
			}
			
		}

		if($step == '1_20'){	 /*****************/
			
		}

		if($step == '1_21'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."napobmens WHERE parser='1' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$id = intval($item->id);
				$arr = array();
				
				$commis1 = is_isset($item,'commis1');
				$prorsum1 = is_isset($item, 'prorsum1');
				$prsum1 = $item->prsum1;
				$prproc1 = $item->prproc1;
				if($prorsum1 == 0){
					$arr['prproc1'] = $commis1;
					$arr['prsum1'] = 0;
				}
				if($prorsum1 == 1){
					$arr['prsum1'] = $prsum1;
					$arr['prproc1'] = 0;
				}	
				
				$commis2 = is_isset($item,'commis2');
				$prorsum2 = is_isset($item, 'prorsum2');
				$prsum2 = $item->prsum2;
				$prproc2 = $item->prproc2;
				if($prorsum2 == 0){
					$arr['prproc2'] = $commis2;
					$arr['prsum2'] = 0;
				}
				if($prorsum2 == 1){
					$arr['prsum2'] = $prsum2;
					$arr['prproc2'] = 0;
				}	
				
				$parser = $item->parser;
				$corecurs1 = is_isset($item,'corecurs1');
				$corecurs2 = is_isset($item,'corecurs2');
				$d=0;
				if($corecurs1 == 'RUB' and $corecurs2 == 'EUR'){
					$d=1;
					$arr['parser'] = 4;
				} elseif($corecurs1 == 'RUB' and $corecurs2 == 'USD'){
					$d=1;
					$arr['parser'] = 2;
				} elseif($corecurs1 == 'RUB' and $corecurs2 == 'UAH'){	
					$d=1;
					$arr['parser'] = 6;
				} elseif($corecurs1 == 'EUR' and $corecurs2 == 'RUB'){	
					$d=1;
					$arr['parser'] = 3;
				} elseif($corecurs1 == 'EUR' and $corecurs2 == 'USD'){	
					$d=1;
					$arr['parser'] = 51;
				} elseif($corecurs1 == 'EUR' and $corecurs2 == 'UAH'){	
					$d=1;
					$arr['parser'] = 103;
				} elseif($corecurs1 == 'USD' and $corecurs2 == 'RUB'){	
					$d=1;
					$arr['parser'] = 1;
				} elseif($corecurs1 == 'USD' and $corecurs2 == 'EUR'){	
					$d=1;
					$arr['parser'] = 52;
				} elseif($corecurs1 == 'USD' and $corecurs2 == 'UAH'){	
					$d=1;
					$arr['parser'] = 101;
				} elseif($corecurs1 == 'UAH' and $corecurs2 == 'RUB'){	
					$d=1;
					$arr['parser'] = 5;
				} elseif($corecurs1 == 'UAH' and $corecurs2 == 'EUR'){	
					$d=1;
					$arr['parser'] = 104;
				} elseif($corecurs1 == 'UAH' and $corecurs2 == 'USD'){		
					$d=1;
					$arr['parser'] = 102;
				}
				
				if(count($arr) > 0){
					$wpdb->update($wpdb->prefix ."napobmens", $arr, array('id'=>$id));
				}
			}		
			
		}

		if($step == '1_22'){	 /*****************/
		
			$txt4 = pn_strip_text($exchangebox->get_option('wablons','txt4'));
			$wpdb->update($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'timeline_txt'), array('meta_key'=>'wablons', 'meta_key2'=>'txt1'));
			$wpdb->update($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'description_txt'), array('meta_key'=>'wablons', 'meta_key2'=>'txt2'));
			$wpdb->update($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'status_new'), array('meta_key'=>'wablons', 'meta_key2'=>'txt3'));
			$wpdb->update($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'status_payed'), array('meta_key'=>'wablons', 'meta_key2'=>'txt4'));
			$wpdb->update($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'status_success'), array('meta_key'=>'wablons', 'meta_key2'=>'txt5'));
			$wpdb->update($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'status_new_mobile'), array('meta_key'=>'wablons', 'meta_key2'=>'txt6'));
			if($txt4){
				$wpdb->insert($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'status_realpay', 'meta_value' => $txt4));
				$wpdb->insert($wpdb->prefix ."change", array('meta_key' => 'naps_temp', 'meta_key2'=> 'status_verify', 'meta_value' => $txt4));
			}
			$cursfile = intval($exchangebox->get_option('change','cursfile'));
			if(!$cursfile){
				$exchangebox->update_option('txtxml','txt', 1);
				$exchangebox->update_option('txtxml','xml', 1);
				$exchangebox->delete_option('change','cursfile');
			}
			$cursfilelen = intval($exchangebox->get_option('change','cursfilelen'));
			if($cursfilelen){
				$exchangebox->update_option('txtxml','numtxt', $cursfilelen);
				$exchangebox->update_option('txtxml','numxml', $cursfilelen);
				$exchangebox->delete_option('change','cursfilelen');
			}			
			
		}	

		if($step == '1_23'){	 /*****************/
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."napobmens LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$value = pn_strip_text(is_isset($item,'text1'));
				if($value){
					$uppl = update_naps_txtmeta($item->id, 'timeline_txt', $value);
				}
				$value = pn_strip_text(is_isset($item,'text2'));
				if($value){
					$uppl = update_naps_txtmeta($item->id, 'description_txt', $value);
				}
				$value = pn_strip_text(is_isset($item,'text3'));
				if($value){
					$uppl = update_naps_txtmeta($item->id, 'status_new', $value);
				}
				$value = pn_strip_text(is_isset($item,'text4'));
				if($value){
					$uppl1 = update_naps_txtmeta($item->id, 'status_payed', $value);
					$uppl2 = update_naps_txtmeta($item->id, 'status_realpay', $value);
					$uppl3 = update_naps_txtmeta($item->id, 'status_verify', $value);
				}	
				$value = pn_strip_text(is_isset($item,'text5'));
				if($value){
					$uppl = update_naps_txtmeta($item->id, 'status_success', $value);
				}
				$value = pn_strip_text(is_isset($item,'text6'));
				if($value){
					$uppl = update_naps_txtmeta($item->id, 'status_new_mobile', $value);
				}				
			}		
		}

		if($step == '1_24'){	 /*****************/			
			
			$arr = array(
				array(
					'tbl' => 'users',
					'row' => 'user_discount',
				),
				array(
					'tbl' => 'vtypes',
					'row' => 'vncurs',					
				),
				array(
					'tbl' => 'valuts',
					'row' => 'valut_reserv',					
				),
				array(
					'tbl' => 'user_discounts',
					'row' => 'sumec',					
				),
				array(
					'tbl' => 'user_discounts',
					'row' => 'discount',					
				),		
				array(
					'tbl' => 'masschange',
					'row' => 'curs1',					
				),
				array(
					'tbl' => 'masschange',
					'row' => 'curs2',					
				),				
				array(
					'tbl' => 'users',
					'row' => 'partner_pers',					
				),
				array(
					'tbl' => 'partner_pers',
					'row' => 'sumec',					
				),
				array(
					'tbl' => 'partner_pers',
					'row' => 'pers',
				),					
			);	
			
			foreach($arr as $data){
				$table = $wpdb->prefix. $data['tbl'];
				$query = $wpdb->query("CHECK TABLE {$table}");
				if($query == 1){
					$row = $data['row'];
					$que = $wpdb->query("SHOW COLUMNS FROM {$table} LIKE '{$row}'");
					if ($que) {
						$wpdb->query("ALTER TABLE {$table} CHANGE `{$row}` `{$row}` varchar(50) NOT NULL default '0'");
					}	
				}
			}			

		}	

		if($step == '1_25'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids WHERE exsum='0' AND status != 'auto' LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$sm = convert_sum($item->summ1, $item->valut1type, cur_type());
				$sum = 0 + $sm;
				$wpdb->update($wpdb->prefix ."bids", array('exsum' => $sum), array('id'=> $item->id));
			}			
			
		}

		if($step == '1_26'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$dmetas = @unserialize($item->dmetas);
				if(is_array($dmetas)){
					if(!isset($dmetas[1]['title'])){
						$newmetas = array();
						if(isset($dmetas[$item->valut1i]) and is_array($dmetas[$item->valut1i])){
							$newmetas[1] = array();
							foreach($dmetas[$item->valut1i] as $dm){
								$ndm = explode('</span>',$dm);
								$newmetas[1][] = array(
									'title' => pn_strip_input(is_isset($ndm,0)),
									'data' => pn_strip_input(is_isset($ndm,1)),
									'hidden' => 0,								
								);
							}							
						}
						if(isset($dmetas[$item->valut2i]) and is_array($dmetas[$item->valut2i])){
							$newmetas[2] = array();
							foreach($dmetas[$item->valut2i] as $dm){
								$ndm = explode('</span>',$dm);
								$newmetas[2][] = array(
									'title' => pn_strip_input(is_isset($ndm,0)),
									'data' => pn_strip_input(is_isset($ndm,1)),
									'hidden' => 0,								
								);
							}							
						}						
						if(count($newmetas) > 0){
							$metas = @serialize($newmetas);
							$wpdb->update($wpdb->prefix ."bids", array('dmetas' => $metas), array('id'=> $item->id));
						}
					}
				}
			}			
			
		}

		if($step == '1_27'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids WHERE status != 'auto' AND naschet != '' LIMIT {$offset},{$limit}");
			foreach($items as $data){
				$id = $data->id;
				$naschet = trim($data->naschet);
				if($naschet){
					$naschet_h = pn_crypt_data($naschet);
					$wpdb->query("UPDATE ".$wpdb->prefix."bids SET naschet_h = '$naschet_h' WHERE id = '$id'");
				}
			}			
			
		}

		if($step == '1_28'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."vschets LIMIT {$offset},{$limit}");
			foreach($items as $data){
				$id = $data->id;
				$accountnum = pn_strip_input($data->title);
				$result = update_vaccs_txtmeta($id, 'accountnum', $accountnum);
				if($result != 1){
					$log['status'] = 'error';
					$log['status_code'] = 1; 
					$log['status_text'] = sprintf(__('Error! folder <b>%s</b> is missing or can not be written!','pn'),'/wp-content/uploads/vaccsmeta/');					
					echo json_encode($log);
					exit;
				}	
			}			
			
		}

		if($step == '1_29'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$id = $item->id;
				$arr = array();
				$schet1 = trim($item->schet1);
				if($schet1){
					$arr['schet1_hash'] = pn_crypt_data($schet1);
				}
				$schet2 = trim($item->schet2);
				if($schet2){
					$arr['schet2_hash'] = pn_crypt_data($schet2);
				}	
				if(count($arr) > 0){
					$wpdb->update($wpdb->prefix ."bids", $arr, array('id'=> $item->id));
				}
			}			
			
		}

		if($step == '1_30'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$id = $item->id;
				$arr = array();
				$arr['xzt'] = prepare_old_merch($item->xzt);
				$wpdb->update($wpdb->prefix ."valuts", $arr, array('id'=> $item->id));
			}			
			
		}

		if($step == '1_31'){	 /*****************/
		
			$merchants = get_option('merchants');
			if(is_array($merchants)){
				$new_data = array();
				foreach($merchants as $k => $v){
					$new_data[prepare_old_merch($k)] = $v;
				}
				update_option('merchants', $new_data);
			}
			
			$merch_data = get_option('merch_data');
			if(is_array($merch_data)){
				$new_data = array();
				foreach($merch_data as $k => $v){
					$new_data[prepare_old_merch($k)] = $v;
				}
				update_option('merch_data', $new_data);	
			}
			
		}		
		
		if($step == '2_1'){	 /*****************/
		
			$sn = trim($exchangebox->get_option('second_name'));
			if(!is_numeric($sn)){
				$sn = 1;
			}	
			$ch_mail = trim($exchangebox->get_option('change_email'));
			if(!is_numeric($ch_mail)){
				$ch_mail = 1;
			}
			
			$fields = array(
				'login' => 1,
				'last_name' => 1,
				'first_name' => 1,
				'second_name' => $sn,
				'user_phone' => 1,
				'user_skype' => 1,
				'website' => 1,
				'user_passport' => 1,
			);	
			$exchangebox->update_option('user_fields','',$fields);
			
			$fields = array(
				'user_email' => $ch_mail,
				'last_name' => 1,
				'first_name' => 1,
				'second_name' => 1,
				'user_phone' => 1,
				'user_skype' => 1,
				'website' => 1,
				'user_passport' => 1,
			);			
			$exchangebox->update_option('user_fields_change','',$fields);
			
			$exchangebox->delete_option('second_name');
			$exchangebox->delete_option('change_email');
			
			$last_name = trim($exchangebox->get_option('exchange', 'pfname'));
			if($last_name){ $exchangebox->update_option('persdata','last_name', $last_name); }
			
			$first_name = trim($exchangebox->get_option('exchange', 'piname'));
			if($first_name){ $exchangebox->update_option('persdata','first_name', $first_name); }			
			
			$second_name = trim($exchangebox->get_option('exchange', 'poname'));
			if($second_name){ $exchangebox->update_option('persdata','second_name', $second_name); }
			
			$user_phone = trim($exchangebox->get_option('exchange', 'ptel'));
			if($user_phone){ $exchangebox->update_option('persdata','user_phone', $user_phone); }

			$user_skype = trim($exchangebox->get_option('exchange', 'pskype'));
			if($user_skype){ $exchangebox->update_option('persdata','user_skype', $user_skype); }

			$i = trim($exchangebox->get_option('help', 'fio_f'));
			if($i){ $exchangebox->update_option('help','last_name', $i); }

			$i = trim($exchangebox->get_option('help', 'fio_i'));
			if($i){ $exchangebox->update_option('help','first_name', $i); }

			$i = trim($exchangebox->get_option('help', 'fio_o'));
			if($i){ $exchangebox->update_option('help','second_name', $i); }

			$i = trim($exchangebox->get_option('help', 'email'));
			if($i){ $exchangebox->update_option('help','user_email', $i); }

			$i = trim($exchangebox->get_option('help', 'tel'));
			if($i){ $exchangebox->update_option('help','user_phone', $i); }

			$i = trim($exchangebox->get_option('help', 'skype'));
			if($i){ $exchangebox->update_option('help','user_skype', $i); }	

			$exchangebox->delete_option('exchange', 'pfname');
			$exchangebox->delete_option('exchange', 'piname');
			$exchangebox->delete_option('exchange', 'poname');
			$exchangebox->delete_option('exchange', 'ptel');
			$exchangebox->delete_option('exchange', 'pskype');
			
			$exchangebox->delete_option('help', 'fio_f');
			$exchangebox->delete_option('help', 'fio_i');
			$exchangebox->delete_option('help', 'fio_o');
			$exchangebox->delete_option('help', 'email');
			$exchangebox->delete_option('help', 'tel');
			$exchangebox->delete_option('help', 'skype');
			
		}	

		if($step == '2_2'){	 /*****************/
		
			$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."custom_fields_valut LIMIT {$offset},{$limit}");
			foreach($datas as $data){
				$tech_name = pn_strip_input($data->tech_name);
				if(!$tech_name){					
					$arr = array(
						'tech_name' => pn_strip_input($data->cf_name),
					);
					$wpdb->update($wpdb->prefix.'custom_fields_valut', $arr, array('id'=>$data->id));						
				}
			}
			
		}	

		if($step == '2_3'){	 /*****************/
			$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."napobmens LIMIT {$offset},{$limit}");
			foreach($datas as $data){
				$id = $data->id;

				$arr = array();
				
				$parprorsum1 = intval(is_isset($data, 'parprorsum1'));
				$parcommis1 = trim(is_isset($data, 'parcommis1'));
				if($parprorsum1 == 1){
					$parcommis1 .= ' %';	
				}				
				if($parcommis1){
					$arr['parcommis1'] = $parcommis1;
				}
				
				$parprorsum2 = intval(is_isset($data, 'parprorsum2'));
				$parcommis2 = trim(is_isset($data, 'parcommis2'));
				if($parprorsum2 == 1){
					$parcommis2 .= ' %';	
				}
				if($parcommis2){
					$arr['parcommis2'] = $parcommis2;
				}	
				
				if(count($arr) > 0){
					$wpdb->update($wpdb->prefix.'napobmens', $arr, array('id'=>$data->id));
				}
			} 
		}	

		if($step == '2_4'){	 /*****************/
			$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."naps_meta LIMIT {$offset},{$limit}");
			foreach($datas as $data){
				$id = $data->id;
				$meta_key = $data->meta_key;
				$meta_key = str_replace('seotitle','seo_title', $meta_key);
				$meta_key = str_replace('seokey','seo_key', $meta_key);
				$meta_key = str_replace('seodescr','seo_descr', $meta_key);
				
				$arr = array();
				$arr['meta_key'] = $meta_key;
				$wpdb->update($wpdb->prefix.'naps_meta', $arr, array('id'=>$data->id));
			} 
		}


		if($step == '2_5'){	 /*****************/
		
			$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."usermeta LIMIT {$offset},{$limit}");
			foreach($items as $item){
				$umeta_id = intval($item->umeta_id);
				$meta_key = str_replace('userpasport','user_passport', $item->meta_key);
				$wpdb->update($wpdb->prefix ."usermeta", array('meta_key'=> $meta_key), array('umeta_id'=>$umeta_id));
			}
			
		}

		if($step == '3_1'){	 /*****************/
			$wpdb->update($wpdb->prefix.'usermeta', array('meta_key' => ''), array('meta_key' => 'locale'));
			
			$mailtemp = get_option('mailtemp');
			if($mailtemp){
				$pn_notify = array();
				$pn_notify['email'] = $mailtemp;
				update_option('pn_notify', $pn_notify);
				delete_option('mailtemp');
			}	
		}

		if($step == '3_2'){	 /*****************/
			$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."change");
			if($query == 1){
				$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."change LIMIT {$offset},{$limit}");
				foreach($datas as $data){
					$id = $data->id;
					$meta_key = $data->meta_key;
					$meta_key2 = $data->meta_key2;
					$meta_value = $data->meta_value;
					
					if($meta_key == 'up_mode'){
						$exchangebox->update_option('up_mode', '', 1);
					} else {	
						$exchangebox->update_option($meta_key, $meta_key2, $meta_value);
					}
				}
			}
		}

		if($step == '3_3'){	 /*****************/
			$pn_cron = get_option('pn_cron');
			$pn_cron = (array)$pn_cron;
			
			$old_cron = intval($exchangebox->get_option('cron',''));
			$place1 = 'site';
			$place2 = 'file';
			if($old_cron == 1){
				$place1 = 'file';
				$place2 = 'site';
			} 
			
			if(!isset($pn_cron['site'])){
				
				$array = array(
					'10min' => array(),
					'1day' => array('del_autologs'),
					'1hour' => array(),
				);
				
				foreach($array as $k => $func_names){
					foreach($func_names as $func_name){
						$pn_cron[$place1][$func_name]['work_time'] = $k;
						$pn_cron[$place2][$func_name]['work_time'] = 'none';
					}
				}
				
				$times = pn_cron_times();
				$time_now = current_time('timestamp');
				foreach($times as $time_key => $time_data){
					if($time_key != 'none'){
						$pn_cron['update_times']['site'][$time_key] = $time_now;
						$pn_cron['update_times']['file'][$time_key] = $time_now;
					}
				}
				
				update_option('pn_cron', $pn_cron);	
				
				$exchangebox->delete_option('cron','');
			}
		}

		if($step == '3_4'){	 /*****************/
			$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."users LIMIT {$offset},{$limit}");
			foreach($datas as $data){
				$id = $data->ID;
				
				$array = array();
				$array['user_browser'] = get_user_meta($id, 'user_browser', true);
				$array['user_ip'] = get_user_meta($id, 'user_ip', true);
				$array['user_bann'] = get_user_meta($id, 'user_bann', true);
				$array['admin_comment'] = get_user_meta($id, 'admin_comment', true);
				$array['last_adminpanel'] = get_user_meta($id, 'admin_time_last', true);
				$wpdb->update($wpdb->prefix ."users", $array, array('ID'=> $id));	

				delete_user_meta($id, 'user_browser');
				delete_user_meta($id, 'user_ip');
				delete_user_meta($id, 'user_bann');
				delete_user_meta($id, 'admin_comment');
				delete_user_meta($id, 'admin_time_last');
			} 
		}

		if($step == '3_5'){	 /*****************/
			$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."warning_mess");
			if($query == 1){
				$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."warning_mess LIMIT {$offset},{$limit}");
				foreach($datas as $data){
					$id = $data->id;

					$array = array();
					$array['datestart'] = $data->datestart;
					$array['dateend'] = $data->dateend;
					$array['url'] = $data->url;
					$array['text'] = $data->text;
					$array['status'] = $data->status;
					$array['theclass'] = is_isset($data,'theclass');
					
					$cc_count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."notice_head WHERE id='$id'");
					if($cc_count == 0){
						$array['id'] = $id;
						$wpdb->insert($wpdb->prefix ."notice_head", $array);	
					} else {
						$wpdb->update($wpdb->prefix ."notice_head", $array, array('id'=> $id));	
					}					

				}
			}
		}

		if($step == '3_6'){	 /*****************/
			$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."exbotziv");
			if($query == 1){
				$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."exbotziv LIMIT {$offset},{$limit}");
				foreach($datas as $data){
					$id = $data->id;
	
					$array = array();
					$array['user_id'] = is_isset($data,'user_id');
					$array['user_name'] = is_isset($data,'user_name');
					$array['user_email'] = is_isset($data,'user_email');
					$array['user_site'] = is_isset($data,'user_site');
					$array['review_date'] = is_isset($data,'otdate');
					$array['review_hash'] = is_isset($data,'otzivhash');
					$array['review_text'] = is_isset($data,'commenttext');
					$array['review_status'] = is_isset($data,'otstatus');
					
					$cc_count = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."reviews WHERE id='$id'");
					if($cc_count == 0){
						$array['id'] = $id;
						$wpdb->insert($wpdb->prefix ."reviews", $array);	
					} else {
						$wpdb->update($wpdb->prefix ."reviews", $array, array('id'=> $id));	
					}					

				}
			}
		}

		if($step == '3_7'){	 /*****************/
			$datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."napobmens LIMIT {$offset},{$limit}");
			foreach($datas as $data){
				$uf = array();
				if($exchangebox->get_option('persdata','last_name') == 1){
					$uf['last_name'] = 1;
				}
				if($exchangebox->get_option('persdata','first_name') == 1){
					$uf['first_name'] = 1;
				}
				if($exchangebox->get_option('persdata','second_name') == 1){
					$uf['second_name'] = 1;
				}
				if($exchangebox->get_option('persdata','user_phone') == 1){
					$uf['user_phone'] = 1;
				}
				if($exchangebox->get_option('persdata','user_passport') == 1){
					$uf['user_passport'] = 1;
				}
				if($exchangebox->get_option('persdata','user_skype') == 1){
					$uf['user_skype'] = 1;
				}	
				$uf = serialize($uf);
				
				$arr = array();
				$arr['uf'] = $uf;
				$wpdb->update($wpdb->prefix.'napobmens', $arr, array('id'=>$data->id));
			} 
		}		

		if($step == '3_8'){	 /*****************/
			$tables = array(
				'exbcaptcha', 'vremenno', 'term_meta', 'login_check', 'admin_captcha', 'admin_captcha_plus', 'warning_mess', 'exbskidka', 'exbotziv',
				'standart_captcha', 'standart_captcha_plus', 'exbpskidka', 'exbblacklist', 'valutsmeta'
			);
			foreach($tables as $tbl){
				$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix . $tbl);
				if($query == 1){
					$wpdb->query("DROP TABLE ". $wpdb->prefix . $tbl);
				}	
			}
		}	

		if($step == '3_9'){	 /*****************/
		
			$maxsymb = intval($exchangebox->get_option('exchange','maxsymb_course'));
			if($maxsymb < 1){ $maxsymb = 2; }
			
			$exchangebox->update_option('txtxml','decimal', $maxsymb);
			$exchangebox->update_option('admin','wm0', 1);
			$exchangebox->update_option('admin','wm1', 1);
			$exchangebox->update_option('admin','wm2', 1);
			
		}

		if($step == '3_10'){	 /*****************/
			$query = $wpdb->query("CHECK TABLE ". $wpdb->prefix ."parser_pairs");
			if($query == 1){	
				$result = get_curl_parser('https://premiumexchanger.com/migrate/step35.xml', array(), 'migration');
				if(!$result['err']){
					$out = $result['output'];
					if(is_string($out)){
						if(strstr($out, '<?xml')){
							$res = @simplexml_load_string($out);
							if(is_object($res)){
								foreach($res->item as $item){
									$arr = (array)$item;
									if(isset($arr['id'])){
										unset($arr['id']);
									}
									
									$wpdb->insert($wpdb->prefix . 'parser_pairs', $arr);
								}
							}
						}
					}
				}
			}
		}		
		
		$log['status'] = 'success';	
		$log['status_text'] = __('Ok!','pn');		
		
	} else {
		$log['status'] = 'error';
		$log['status_code'] = 1; 
		$log['status_text'] = __('Error! insufficient privileges!','pn');
	}
	
	echo json_encode($log);
	exit;	
}