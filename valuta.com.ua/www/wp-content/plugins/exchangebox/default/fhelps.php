<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_fhelps', 'adminpage_title_pn_fhelps');
function adminpage_title_pn_fhelps($page){
	_e('Field helps','pn');
} 

/* настройки */
add_action('pn_adminpage_content_pn_fhelps','def_adminpage_content_pn_fhelps');
function def_adminpage_content_pn_fhelps(){
global $wpdb, $exchangebox;

	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Field helps','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);
	$options['last_name'] = array(
		'view' => 'textarea',
		'title' => __('When filling last name','pn'),
		'default' => $exchangebox->get_option('help','last_name'),
		'name' => 'last_name',
		'width' => '',
		'height' => '100px',
	);	
	$options['first_name'] = array(
		'view' => 'textarea',
		'title' => __('When filling first name','pn'),
		'default' => $exchangebox->get_option('help','first_name'),
		'name' => 'first_name',
		'width' => '',
		'height' => '100px',
	);
	$options['second_name'] = array(
		'view' => 'textarea',
		'title' => __('When filling second name','pn'),
		'default' => $exchangebox->get_option('help','second_name'),
		'name' => 'second_name',
		'width' => '',
		'height' => '100px',
	);
	$options['user_email'] = array(
		'view' => 'textarea',
		'title' => __('When filling e-mail','pn'),
		'default' => $exchangebox->get_option('help','user_email'),
		'name' => 'user_email',
		'width' => '',
		'height' => '100px',
	);
	$options['user_phone'] = array(
		'view' => 'textarea',
		'title' => __('When filling phone','pn'),
		'default' => $exchangebox->get_option('help','user_phone'),
		'name' => 'user_phone',
		'width' => '',
		'height' => '100px',
	);
	$options['user_skype'] = array(
		'view' => 'textarea',
		'title' => __('When filling skype','pn'),
		'default' => $exchangebox->get_option('help','user_skype'),
		'name' => 'user_skype',
		'width' => '',
		'height' => '100px',
	);
	$options['user_passport'] = array(
		'view' => 'textarea',
		'title' => __('When filling passport','pn'),
		'default' => $exchangebox->get_option('help','user_passport'),
		'name' => 'user_passport',
		'width' => '',
		'height' => '100px',
	);	
	$form = new PremiumForm();
	$params_form = array(
		'filter' => 'pn_fhelps_option',
		'method' => 'post',
		'data' => '',
		'form_link' => '',
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);					
} 


add_action('premium_action_pn_fhelps','def_premium_action_pn_fhelps');
function def_premium_action_pn_fhelps(){
global $wpdb, $exchangebox;		

	only_post();
	pn_only_caps(array('administrator'));
			
	$form = new PremiumForm();		
			
	$helps = array('first_name','last_name','second_name','user_email','user_phone','user_skype','user_passport');
	foreach($helps as $help){
		$exchangebox->update_option('help',$help,pn_strip_text(is_param_post($help)));
	}
	
	do_action('exb_change_helps_post');
	
	$back_url = is_param_post('_wp_http_referer');
	$back_url .= '&reply=true';
			
	$form->answer_form($back_url);
} 