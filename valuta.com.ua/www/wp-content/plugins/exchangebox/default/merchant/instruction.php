<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_data_merchants', 'pn_admin_title_pn_data_merchants');
function pn_admin_title_pn_data_merchants(){
	_e('Merchants settings','pn');
}

add_action('pn_adminpage_content_pn_data_merchants','def_pn_admin_content_pn_data_merchants');
function def_pn_admin_content_pn_data_merchants(){
global $wpdb;
		
	$form = new PremiumForm();	
		
	$m_id = is_extension_name(is_param_get('m_id'));
	
	$list_merchants = apply_filters('list_merchants',array());
	$list_merchants_t = array();
	foreach($list_merchants as $data){
		$list_merchants_t[] = is_isset($data,'id');
	}
	
	$merch_data = get_option('merch_data');
	if(!is_array($merch_data)){ $merch_data = array(); }
	
	$selects = array();
	$selects[] = array(
		'link' => admin_url("admin.php?page=pn_data_merchants"),
		'title' => '--' . __('Make a choice','pn') . '--',
		'background' => '',
		'default' => '',
	);
	if(is_array($list_merchants)){  
		foreach($list_merchants as $data){
			$id = is_isset($data,'id');
			$title = is_isset($data,'title');
			$selects[] = array(
				'link' => admin_url("admin.php?page=pn_data_merchants&m_id=".$id),
				'title' => $title,
				'background' => '',
				'default' => $id,
			);
		}
	}	
	$form->select_box($m_id, $selects, __('Setting up','pn'));	
	
	if(in_array($m_id,$list_merchants_t)){
		$data = '';
		if(isset($merch_data[$m_id])){
			$data = $merch_data[$m_id]; 
		}	
		
		do_action('before_merchant_admin', $m_id, $data);

		$options = array();
		$options['hidden_block'] = array(
			'view' => 'hidden_input',
			'name' => 'm_id',
			'default' => $m_id,
		);	
		$options['top_title'] = array(
			'view' => 'h3',
			'title' => '',
			'submit' => __('Save','pn'),
			'colspan' => 2,
		);
		$options['instruction'] = array(
			'view' => 'textarea',
			'title' => __('Instruction','pn'),
			'default' => is_isset($data, 'text'),
			'name' => 'text',
			'width' => '',
			'height' => '200px',
			'work' => 'text',
		);	
		$tags = array(
			'id' => __('Bid ID','pn'),
			'sum1' => __('Sum Give','pn'),
			'valut1' => __('Currency name Give','pn'),
			'sum2' => __('Sum Get','pn'),
			'valut2' => __('Currency name Get','pn'),
			'account_give' => __('Account Give','pn'),
			'account_get' => __('Account Get','pn'),
			'fio' => __('User name','pn'),
			'last_name' => __('Last name','pn'),
			'first_name' => __('First name','pn'),
			'second_name' => __('Second name','pn'),
			'ip' => __('User IP','pn'),
			'skype' => __('User skype','pn'),
			'phone' => __('User phone','pn'),
			'email' => __('User email','pn'),
			'passport' => __('Passport','pn'),
		);
		$tags = apply_filters('merchant_admin_tabs', $tags, $m_id);
		$options['note'] = array(
			'view' => 'textareatags',
			'title' => __('Note for payment','pn'),
			'default' => is_isset($data, 'note'),
			'tags' => $tags,
			'width' => '',
			'height' => '200px',
			'prefix1' => '[',
			'prefix2' => ']',
			'name' => 'note',
			'work' => 'text',
		);
		
		$options = apply_filters('get_merchant_admin_options', $options, $m_id, $data);
		$params_form = array(
			'filter' => 'get_merchant_admin_options_'.$m_id,
			'method' => 'post',
			'data' => $data,
			'button_title' => __('Save','pn'),
		);
		$form->init_form($params_form, $options);				

	} 
}  

add_action('premium_action_pn_data_merchants','def_premium_action_pn_data_merchants');
function def_premium_action_pn_data_merchants(){
global $wpdb;	

	only_post();
	pn_only_caps(array('administrator'));

	$form = new PremiumForm();
	
	$m_id = is_extension_name(is_param_post('m_id'));
	
	$options = array();
	$options['instruction'] = array(
		'name' => 'text',
		'work' => 'text',
	);	
	$options['note'] = array(
		'name' => 'note',
		'work' => 'text',
	);		
	$options = apply_filters('get_merchant_admin_options', $options, $m_id, '');
	$data = $form->strip_options('get_merchant_admin_options_'.$m_id, 'post', $options);			
	
	$merch_data = get_option('merch_data');
	if(!is_array($merch_data)){ $merch_data = array(); }
						
	foreach($data as $key => $val){
		$merch_data[$m_id][$key] = $val;
	}			

	update_option('merch_data', $merch_data);

	do_action('merchant_admin_post', $data);
				
	$url = admin_url('admin.php?page=pn_data_merchants&m_id='. $m_id .'&reply=true');
	$form->answer_form($url);
}