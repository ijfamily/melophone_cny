<?php
if( !defined( 'ABSPATH')){ exit(); }
 
global $exchangebox;
$exchangebox->include_patch(__FILE__, 'merch_func');
$exchangebox->include_patch(__FILE__, 'smsgate_func');
$exchangebox->include_patch(__FILE__, 'merchants');
$exchangebox->include_patch(__FILE__, 'smsgate');
$exchangebox->include_patch(__FILE__, 'instruction');
$exchangebox->include_patch(__FILE__, 'datasmsgate');