<?php
if( !defined( 'ABSPATH')){ exit(); }

if(!function_exists('admin_menu_admin')){
	
	global $exchangebox;
	$exchangebox->include_patch(__FILE__, 'settings');
	$exchangebox->include_patch(__FILE__, 'filters');

}

add_filter('admin_footer_text', 'pn_admin_footer_text', 1);
function pn_admin_footer_text($text){
	$text .= '<div>&copy; '. get_copy_date('2013') .' <strong>ExchangeBox</strong>.</div>';
	return $text;
}

if(!function_exists('def_login_headerurl')){
	add_filter('login_headerurl', 'def_login_headerurl');
	function def_login_headerurl($login_header_url){
		$login_header_url = 'https://premiumexchanger.com/';
		return $login_header_url;
	}
}

if(!function_exists('def_login_headertitle')){
	add_filter('login_headertitle', 'def_login_headertitle');
	function def_login_headertitle($login_header_title){
		$login_header_title = 'PremiumExchanger';
		return $login_header_title;
	}
}

if(!function_exists('def_login_head')){
	add_action('login_head','def_login_head');
	function def_login_head(){
		global $exchangebox;		
		?>
		<style>
		.login h1 a {
		height: 108px;
		width: 108px;
		background: url(<?php echo $exchangebox->plugin_url; ?>images/admin-logo.png) no-repeat center center;	
		}
		</style>
	<?php
	}
}