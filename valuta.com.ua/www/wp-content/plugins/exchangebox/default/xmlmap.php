<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_xmlmap', 'adminpage_title_pn_xmlmap');
function adminpage_title_pn_xmlmap($page){
	_e('Settings XML sitemap','pn');
} 

add_action('pn_adminpage_content_pn_xmlmap','def_adminpage_content_pn_xmlmap');
function def_adminpage_content_pn_xmlmap(){
global $wpdb, $exchangebox;
?>
	<div class="premium_default_window">
		<?php _e('XML sitemap','pn'); ?>:<br /> 
		<a href="<?php echo get_site_url_or(); ?>/request-sitemap.xml" target="_blank"><?php echo get_site_url_or(); ?>/request-sitemap.xml</a>
	</div>	
<?php
	$form = new PremiumForm();
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('HTML XML sitemap','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);
	$options['news'] = array(
		'view' => 'select',
		'title' => __('Show news?','pn'),
		'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('xmlmap','news'),
		'name' => 'news',
	);	
	$options['line1'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['exchanges'] = array(
		'view' => 'select',
		'title' => __('Show exchange directions?','pn'),
		'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('xmlmap','exchanges'),
		'name' => 'exchanges',
	);	
	$options['line2'] = array(
		'view' => 'line',
		'colspan' => 2,
	);							
	$options['pages'] = array(
		'view' => 'select',
		'title' => __('Show pages?','pn'),
		'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('xmlmap','pages'),
		'name' => 'pages',
	);						
	$options['exclude_page'] = array(
		'view' => 'user_func',
		'name' => 'exclude_page',
		'func_data' => array(),
		'func' => 'pn_xmlmap_option1',
	);
	
	$params_form = array(
		'filter' => 'pn_xmlmap_option',
		'method' => 'post',
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);	
} 

function pn_xmlmap_option1($data){
global $exchangebox;	
	$args = array(
		'post_type' => 'page',
		'posts_per_page' => '-1'
	);
	$pages = get_posts($args);
	
	$exclude_pages = $exchangebox->get_option('xmlmap','exclude_page');
	if(!is_array($exclude_pages)){ $exclude_pages = array(); }

	?>
	<tr>
		<th><?php _e('Exclude pages','pn'); ?></th>
		<td>
			<div class="premium_wrap_standart">
				<?php foreach($pages as $item){ ?>
					<div><label><input type="checkbox" name="exclude_page[]" <?php if(in_array($item->ID, $exclude_pages)){ ?>checked="checked"<?php } ?> value="<?php echo $item->ID; ?>" /> <a href="<?php echo get_permalink($item->ID); ?>" target="_blank"><?php echo ctv_ml($item->post_title); ?></a></label></div>
				<?php } ?>
			</div>
		</td>		
	</tr>					
	<?php	
}

add_action('premium_action_pn_xmlmap','def_premium_action_pn_xmlmap');
function def_premium_action_pn_xmlmap(){
global $wpdb, $exchangebox;	

	only_post();
	pn_only_caps(array('administrator'));
	
	$form = new PremiumForm();
	
	$new_exclude_page = array();
	$exclude_page = is_param_post('exclude_page');
	if(is_array($exclude_page)){
		foreach($exclude_page as $val){
			$new_exclude_page[] = intval($val);
		}
	}

	$exchangebox->update_option('xmlmap','exclude_page',$new_exclude_page);

	$options = array('exchanges','pages','news');	
					
	foreach($options as $key){
		$exchangebox->update_option('xmlmap',$key, intval(is_param_post($key)));
	}				

	do_action('pn_xmlmap_option_post');
	
	$url = admin_url('admin.php?page=pn_xmlmap&reply=true');
	$form->answer_form($url);
} 

add_action('myaction_request_sitemap','def_myaction_request_sitemap');
function def_myaction_request_sitemap(){
global $wpdb, $exchangebox;

header("Content-Type: text/xml");
$site_url = get_site_url_or();
?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'. $exchangebox->plugin_url .'/sm_style/sitemap.xsl"?>'; ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">	
    <url>
		<loc><?php echo $site_url; ?></loc>
		<changefreq>daily</changefreq>
		<priority>1.0</priority>
	</url>	
<?php
if($exchangebox->get_option('up_mode') != 1){
if($exchangebox->get_option('xmlmap','pages') == 1){
	$ex = get_option('the_pages');
	$exclude_pages = $exchangebox->get_option('xmlmap','exclude_page');
	if(!is_array($exclude_pages)){ $exclude_pages = array(); }
	if(isset($ex['home'])){
		$exclude_pages[] = $ex['home'];
	}
	$exclude = join(',',$exclude_pages);
	
	$args = array(
		'post_type' => 'page',
		'posts_per_page' => '-1',
		'exclude' => $exclude
	);			
	$mposts = get_posts($args);	
	foreach($mposts as $mpos){
?>
	<url>
		<loc><?php echo get_permalink($mpos->ID); ?></loc>
		<changefreq>daily</changefreq>
		<priority>0.6</priority>
	</url>
<?php		
	}	

}

if($exchangebox->get_option('xmlmap','exchanges') == 1){
	$v = get_valuts_data();
	$where = get_naps_where('smxml');
	$obmens = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC");
	foreach($obmens as $obm){
	?>
		<url>
			<loc><?php echo get_exchange_link($v[$obm->valsid1]->xname,$v[$obm->valsid2]->xname); ?></loc>
			<changefreq>daily</changefreq>
			<priority>0.6</priority>
		</url>
	<?php
	}
}

if($exchangebox->get_option('xmlmap','news') == 1){
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => '-1',
	);			
	$mposts = get_posts($args);	
	foreach($mposts as $mpos){
?>
	<url>
		<loc><?php echo get_permalink($mpos->ID); ?></loc>
		<changefreq>daily</changefreq>
		<priority>0.6</priority>
	</url>
<?php		
	}	

}
}
?>
</urlset>
<?php
}