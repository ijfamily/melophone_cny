<?php
if( !defined( 'ABSPATH')){ exit(); }

if(!function_exists('pn_users_admin_menu')){
	add_action('admin_menu', 'pn_users_admin_menu');
	function pn_users_admin_menu(){
		global $exchangebox;	
		
		if(current_user_can('administrator')){
			$hook = add_submenu_page('users.php', __('Authorization log','pn'), __('Authorization log','pn'), 'read', 'pn_alogs', array($exchangebox, 'admin_temp'));  
			add_action( "load-$hook", 'pn_trev_hook' );
		}
	}
}

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'users');
$exchangebox->include_patch(__FILE__, 'enableip');
$exchangebox->include_patch(__FILE__, 'twofactorauth');
$exchangebox->include_patch(__FILE__, 'list_auth');
$exchangebox->include_patch(__FILE__, 'uf_settings');
$exchangebox->include_patch(__FILE__, 'pn_users');