<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_valuts_delete','cfc_pn_valuts_delete');
function cfc_pn_valuts_delete($id){
global $wpdb;

	$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."custom_fields_valut WHERE valut_id = '$id'");
	foreach($items as $item){
		$item_id = $item->id;
		do_action('pn_cfc_delete_before', $item_id, $item);
		$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."custom_fields_valut WHERE id = '$item_id'");
		do_action('pn_cfc_delete', $item_id, $item);
		if($result){
			do_action('pn_cfc_delete_after', $item_id, $item);
		}
	}
}

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'add');
$exchangebox->include_patch(__FILE__, 'list');
$exchangebox->include_patch(__FILE__, 'sort');