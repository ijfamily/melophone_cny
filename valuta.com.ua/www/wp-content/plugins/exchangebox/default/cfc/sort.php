<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_sort_cfc', 'adminpage_title_pn_sort_cfc');
function adminpage_title_pn_sort_cfc(){
	_e('Sort custom fields','pn');
}

add_action('pn_adminpage_content_pn_sort_cfc','def_adminpage_content_pn_sort_cfc');
function def_adminpage_content_pn_sort_cfc(){
global $wpdb;

	$selects = array();
	
	$form = new PremiumForm();
	
	$selects[0] = array(
		'link' => admin_url("admin.php?page=pn_sort_cfc"),
		'title' => '--'. __('Make a choice','pn') .'--',
		'default' => 0,
	);
	$places_t =array();
	$place = is_param_get('place');
	$datas = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts ORDER BY site_order ASC");
	foreach($datas as $item){
		$places_t[] = $item->id;
		$selects[] = array(
			'link' => admin_url("admin.php?page=pn_sort_cfc&place=".$item->id),
			'title' => pn_strip_input($item->vname .' '. $item->vtype),
			'default' => $item->id,
		);		
	}
	
	$form->select_box($place, $selects, __('Make a choice','pn'));

	if(in_array($place, $places_t)){
		$place = intval($place);
		$datas = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."custom_fields_valut WHERE valut_id='$place' ORDER BY cf_order ASC");	
		
		$sort_list = array();
		foreach($datas as $item){
			$sort_list[0][] = array(
				'title' => pn_strip_input($item->tech_name),
				'id' => $item->id,
				'number' => $item->id,
			);		
		}
		
		$form->sort_one_screen($sort_list);	
	?>	
		<script type="text/javascript">
		$(document).ready(function(){ 
													   
			$(".thesort ul").sortable({ 
				opacity: 0.6, 
				cursor: 'move',
				revert: true,
				update: function() {
					$('#premium_ajax').show();
					
					var order = $(this).sortable("serialize"); 
					$.post("<?php pn_the_link_post('', 'post'); ?>", order, function(theResponse){
						$('#premium_ajax').hide();
					}); 															 
				}	 				
			});

		});	
		</script>		
	<?php
	}
}

add_action('premium_action_pn_sort_cfc','def_premium_action_pn_sort_cfc');
function def_premium_action_pn_sort_cfc(){
global $wpdb;	
	if(current_user_can('administrator')){
		only_post();
		$number = is_param_post('number');
		$y = 0;
		if(is_array($number)){
			foreach($number as $theid) { $y++;
				$theid = intval($theid);
				$wpdb->query("UPDATE ".$wpdb->prefix."custom_fields_valut SET cf_order='$y' WHERE id = '$theid'");	
			}	
		}
	}
}