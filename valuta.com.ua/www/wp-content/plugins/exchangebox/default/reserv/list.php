<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_reserv', 'adminpage_title_pn_reserv');
function adminpage_title_pn_reserv(){
	_e('Adjustment reserve','pn');
}

add_action('pn_adminpage_content_pn_reserv','def_adminpage_content_pn_reserv');
function def_adminpage_content_pn_reserv(){

	if(class_exists('trev_reserv_List_Table')){
		$Table = new trev_reserv_List_Table();
		$Table->prepare_items();
		
		$search = array();
		
		global $wpdb;
		
		$valuts = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts ORDER BY site_order ASC");	
	  	$valuts_arr = array();
		$valuts_arr[0] = '--'. __('All currency','pn') .'--';
		foreach($valuts as $item){
			$valuts_arr[$item->id] = pn_strip_input($item->vname .' '. $item->vtype);
		}	

		$search[] = array(
			'view' => 'select',
			'title' => __('Currency','pn'),
			'default' => intval(is_param_get('valut_id')),
			'options' => $valuts_arr,
			'name' => 'valut_id',
		);			
		pn_admin_searchbox($search, 'reply');		
		
		$options = array();
		$options['mod'] = array(
			'name' => 'mod',
			'options' => array(
				'1' => __('expenditure','pn'),
				'2' => __('income','pn'),
			),
			'title' => '',
		);		
		pn_admin_submenu($options, 'reply');		
?>

	<form method="post" action="<?php pn_the_link_post(); ?>">
		<?php $Table->display() ?>
	</form>
<?php 
	} else {
		echo 'Class not found';
	}
}

add_action('premium_action_pn_reserv','def_premium_action_pn_reserv');
function def_premium_action_pn_reserv(){
global $wpdb;

	only_post();
	pn_only_caps(array('administrator'));
	
	$reply = '';
	$action = get_admin_action();
		
	if(isset($_POST['save'])){
		
		do_action('pn_reserv_save');
		$reply = '&reply=true';
		
	} else {
		
		if(isset($_POST['id']) and is_array($_POST['id'])){				
				
			if($action == 'delete'){
						
				foreach($_POST['id'] as $id){
					$id = intval($id);
							
					$item = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."transactionrezerv WHERE id='$id'");
					if(isset($item->id)){		
						do_action('pn_reserv_delete_before', $id, $item);
						$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."transactionrezerv WHERE id = '$id'");
						do_action('pn_reserv_delete', $id, $item);
						if($result){
							do_action('pn_reserv_delete_after', $id, $item);
						}						
					}
				}
						
			}
			
			do_action('pn_currency_reserv_action', $action, $_POST['id']);
			$reply = '&reply=true';
		} 
	
	}
			
	$url = is_param_post('_wp_http_referer') . $reply;
	$paged = intval(is_param_post('paged'));
	if($paged > 1){ $url .= '&paged='.$paged; }	
	wp_redirect($url);
	exit;			
} 

class trev_reserv_List_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;
                
        parent::__construct( array(
            'singular'  => 'id',      
			'ajax' => false,  
        ) );
        
    }
	
    function column_default($item, $column_name){
        
		if($column_name == 'sum'){
			return get_sum_color($item->vsumm);
		} elseif($column_name == 'create'){
			return get_mytime($item->vdate,'d.m.Y H:i');			
		} 
		
		return apply_filters('reserv_manage_ap_col', '', $column_name,$item);
    }	
	
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'], 
            $item->id                
        );
    }	

    function column_title($item){

        $actions = array(
            'edit'      => '<a href="'. admin_url('admin.php?page=pn_add_reserv&item_id='. $item->id) .'">'. __('Edit','pn') .'</a>',
        );
 		$primary = apply_filters('reserv_manage_ap_primary', get_vtitle($item->valsid), $item);
		$actions = apply_filters('reserv_manage_ap_actions', $actions, $item);	       
        return sprintf('%1$s %2$s',
            $primary,
            $this->row_actions($actions)
        );        
		
    }	
	
    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
			'title'     => __('Currency title','pn'),
			'sum' => __('Sum','pn'),
			'create' => __('Date of creation','pn'),
        );
		$columns = apply_filters('reserv_manage_ap_columns', $columns);
        return $columns;
    }	
	

    function get_bulk_actions() {
        $actions = array(
            'delete'    => __('Delete','pn'),
        );
        return $actions;
    }
	
    function get_sortable_columns() {
        $sortable_columns = array( 
            'create'     => array('create',false),
			'sum'     => array('sum',false),
        );
        return $sortable_columns;
    }	
    
    function prepare_items() {
        global $wpdb; 
		
        $per_page = $this->get_items_per_page('trev_reserv_per_page', 20);
        $current_page = $this->get_pagenum();
        
        $this->_column_headers = $this->get_column_info();

		$offset = ($current_page-1)*$per_page;
		$oby = is_param_get('orderby');
		if($oby == 'sum'){
		    $orderby = '(vsumm -0.0)';
		} elseif($oby == 'create'){
			$orderby = 'vdate';		
		} else {
		    $orderby = 'id';
	    }
		$order = is_param_get('order');
		if($order != 'asc'){ $order = 'desc'; }	
		
		$where = '';
		
        $mod = intval(is_param_get('mod'));
        if($mod == 1){ 
            $where .= " AND vsumm > 0"; 
		} elseif($mod == 2){
			$where .= " AND vsumm <= 0";
		}		
		
        $valut_id = intval(is_param_get('valut_id'));
        if($valut_id > 0){ 
            $where .= " AND valsid='$valut_id'"; 
		}		
		
		$where = pn_admin_search_where($where);
		
		$total_items = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."transactionrezerv WHERE id > 0 $where");
		$data = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."transactionrezerv WHERE id > 0 $where ORDER BY $orderby $order LIMIT $offset , $per_page");  		

        $current_page = $this->get_pagenum();
        $this->items = $data;
		
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  
            'per_page'    => $per_page,                     
            'total_pages' => ceil($total_items/$per_page)  
        ));
    }
	
 	function extra_tablenav( $which ) {	
	?>
		<div class="alignleft actions">
			<input type="submit" name="save" class="button" value="<?php _e('Save','pn'); ?>">
            <a href="<?php echo admin_url('admin.php?page=pn_add_reserv');?>" class="button"><?php _e('Add new','pn'); ?></a>
		</div>		
	<?php 
	} 	  
	
}

add_action('premium_screen_pn_reserv','my_myscreen_pn_reserv');
function my_myscreen_pn_reserv(){
    $args = array(
        'label' => __('Display','pn'),
        'default' => 20,
        'option' => 'trev_reserv_per_page'
    );
    add_screen_option('per_page', $args );
	if(class_exists('trev_reserv_List_Table')){
		new trev_reserv_List_Table;
	}
} 