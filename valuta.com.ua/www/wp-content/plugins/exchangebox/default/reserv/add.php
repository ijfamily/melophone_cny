<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_add_reserv', 'adminpage_title_pn_add_reserv');
function adminpage_title_pn_add_reserv(){
	$id = intval(is_param_get('item_id'));
	if($id){
		_e('Edit reserv transaction','pn');
	} else {
		_e('Add reserv transaction','pn');
	}
}

add_action('pn_adminpage_content_pn_add_reserv','def_adminpage_content_pn_add_reserv');
function def_adminpage_content_pn_add_reserv(){
global $wpdb;

	$form = new PremiumForm();

	$id = intval(is_param_get('item_id'));
	$data_id = 0;
	$data = '';
	
	if($id){
		$data = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."transactionrezerv WHERE id='$id'");
		if(isset($data->id)){
			$data_id = $data->id;
		}	
	}

	if($data_id){
		$title = __('Edit reserve transaction','pn');
	} else {
		$title = __('Add reserve transaction','pn');
	}
	
	$valuts = array();
	$valuts[0] = __('No item','pn');
	$valuts_datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts ORDER BY site_order ASC");
	foreach($valuts_datas as $valut){
		$valuts[$valut->id] = pn_strip_input($valut->vname .' '. $valut->vtype);
	}
	
	$back_menu = array();
	$back_menu['back'] = array(
		'link' => admin_url('admin.php?page=pn_reserv'),
		'title' => __('Back to list','pn')
	);
	if($data_id){
		$back_menu['add'] = array(
			'link' => admin_url('admin.php?page=pn_add_reserv'),
			'title' => __('Add new','pn')
		);	
	}
	$form->back_menu($back_menu, $data);	
	
	$options = array();
	$options['hidden_block'] = array(
		'view' => 'hidden_input',
		'name' => 'data_id',
		'default' => $data_id,
	);	
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => $title,
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['vsumm'] = array(
		'view' => 'inputbig',
		'title' => __('Sum','pn'),
		'default' => is_isset($data, 'vsumm'),
		'name' => 'vsumm',
	);
	$options['valsid'] = array(
		'view' => 'select',
		'title' => __('Currency title','pn'),
		'options' => $valuts,
		'default' => is_isset($data, 'valsid'),
		'name' => 'valsid',
	);
	$params_form = array(
		'filter' => 'pn_reserv_addform',
		'method' => 'post',
		'data' => $data,
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);			

} 

add_action('premium_action_pn_add_reserv','def_premium_action_pn_add_reserv');
function def_premium_action_pn_add_reserv(){
global $wpdb;	

	only_post();
	pn_only_caps(array('administrator'));

	$form = new PremiumForm();
	
	$data_id = intval(is_param_post('data_id'));
	$last_data = '';
	if($data_id > 0){
		$last_data = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "transactionrezerv WHERE id='$data_id'");
		if(!isset($last_data->id)){
			$data_id = 0;
		}
	}	
	
	$array = array();
			
	$array['vsumm'] = is_sum(is_param_post('vsumm'));

	$array['valsid'] = 0;
			
	$valut_id = intval(is_param_post('valsid'));
	if($valut_id){
		$valut_data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut_id'");
		$array['valsid'] = $valut_data->id;				
	} else {
		$form->error_form(__('Error! You did not choose currency','pn'));
	} 

	$array = apply_filters('pn_reserv_addform_post',$array, $last_data);
			
	if($data_id){
				
		do_action('pn_reserv_edit_before', $data_id, $array, $last_data);		
		
		$result = $wpdb->update($wpdb->prefix . 'transactionrezerv', $array, array('id'=>$data_id));
		do_action('pn_reserv_edit', $data_id, $array, $last_data);
		if($result){
			$update = 1;
				
			if(isset($last_data->valsid)){
				update_valut_reserv($last_data->valsid);
				
				if($last_data->valsid == $array['valsid']){
					$update = 0;
				}
			}
			
			if($update == 1){
				update_valut_reserv($array['valsid']);
			}
					
			do_action('pn_reserv_edit_after', $data_id, $array, $last_data);
		}		
	} else {
		
		$array['vdate'] = current_time('mysql');
		$wpdb->insert($wpdb->prefix . 'transactionrezerv', $array);
		$data_id = $wpdb->insert_id;	
				
		update_valut_reserv($array['valsid']);
				
		do_action('pn_reserv_add', $data_id, $array);
				
	}

	$url = admin_url('admin.php?page=pn_add_reserv&item_id='. $data_id .'&reply=true');
	$form->answer_form($url);
}	