<?php
if( !defined( 'ABSPATH')){ exit(); }

/* фильтры */
add_action('change_bidstatus_all', 'reserv_change_bidstatus', 1000, 3);
function reserv_change_bidstatus($action, $obmen_id, $obmen){
	update_valut_reserv($obmen->valut1i);
	update_valut_reserv($obmen->valut2i);
}

add_action('pn_payoutuser_wait_after','reserv_pn_payoutuser_wait_after',1,2);
add_action('pn_payoutuser_success_after','reserv_pn_payoutuser_wait_after',1,2);
add_action('pn_payoutuser_not_after','reserv_pn_payoutuser_wait_after',1,2);
add_action('pn_payoutuser_delete_after','reserv_pn_payoutuser_wait_after',1,2);
function reserv_pn_payoutuser_wait_after($id, $item){
	update_valut_reserv($item->valsid);
}

add_action('pn_valuts_edit','reserv_pn_valuts_edit',1,2);
function reserv_pn_valuts_edit($data_id, $array){
	update_valut_reserv($data_id);
}

add_action('pn_valuts_delete','reserv_pn_valuts_delete');
function reserv_pn_valuts_delete($id){
global $wpdb;
	
	$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."transactionrezerv WHERE valsid = '$id'");
	foreach($items as $item){
		$item_id = $item->id;
		do_action('pn_reserv_delete_before', $item_id, $item);
		$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."transactionrezerv WHERE id = '$item_id'");
		do_action('pn_reserv_delete', $item_id, $item);
		if($result){
			do_action('pn_reserv_delete_after', $item_id, $item);
		}
	}	
}

add_action('pn_reserv_delete','reserv_pn_reserv_delete', 10, 2);
function reserv_pn_reserv_delete($id, $item){
global $wpdb;

	update_valut_reserv($item->valsid);
}
/* end фильтры */

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'add');
$exchangebox->include_patch(__FILE__, 'list');