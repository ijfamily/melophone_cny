<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_add_vaccounts', 'adminpage_title_pn_add_vaccounts');
function adminpage_title_pn_add_vaccounts(){
	$id = intval(is_param_get('item_id'));
	if($id){
		_e('Edit currency account','pn');
	} else {
		_e('Add currency account','pn');
	}
}

add_action('pn_adminpage_content_pn_add_vaccounts','def_adminpage_content_pn_add_vaccounts');
function def_adminpage_content_pn_add_vaccounts(){
global $wpdb;

	$form = new PremiumForm();

	$id = intval(is_param_get('item_id'));
	$data_id = 0;
	$data = '';
	
	if($id){
		$data = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vschets WHERE id='$id'");
		if(isset($data->id)){
			$data_id = $data->id;
		}	
	}

	if($data_id){
		$title = __('Edit currency account','pn');
	} else {
		$title = __('Add currency account','pn');
	}
	
	$valuts = array();
	$valuts[0] = __('No item','pn');
	$items = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts ORDER BY vname ASC");		  
	foreach($items as $item){
		$valuts[$item->id] = pn_strip_input($item->vname .' '. $item->vtype);
	}	
	
	$back_menu = array();
	$back_menu['back'] = array(
		'link' => admin_url('admin.php?page=pn_vaccounts'),
		'title' => __('Back to list','pn')
	);
	if($data_id){
		$back_menu['add'] = array(
			'link' => admin_url('admin.php?page=pn_add_vaccounts'),
			'title' => __('Add new','pn')
		);	
	}
	$form->back_menu($back_menu, $data);

	$options = array();
	$options['hidden_block'] = array(
		'view' => 'hidden_input',
		'name' => 'data_id',
		'default' => $data_id,
	);	
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => $title,
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['text_comment'] = array(
		'view' => 'inputbig',
		'title' => __('Comment','pn'),
		'default' => is_isset($data, 'text_comment'),
		'name' => 'text_comment',
	);	
	$options['valid'] = array(
		'view' => 'select',
		'title' => __('Currency title','pn'),
		'options' => $valuts,
		'default' => is_isset($data, 'valid'),
		'name' => 'valid',
	);
	$options['title'] = array(
		'view' => 'inputbig',
		'title' => __('Curency account','pn'),
		'default' => is_isset($data, 'title'),
		'name' => 'title',
	);	
	$options['prosm'] = array(
		'view' => 'input',
		'title' => __('Hits count','pn'),
		'default' => is_isset($data, 'prosm'),
		'name' => 'prosm',
	);	
	$params_form = array(
		'filter' => 'pn_vaccounts_addform',
		'method' => 'post',
		'data' => $data,
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);	
}

add_action('premium_action_pn_add_vaccounts','def_premium_action_pn_add_vaccounts');
function def_premium_action_pn_add_vaccounts(){
global $wpdb;

	only_post();
	pn_only_caps(array('administrator'));
	
	$form = new PremiumForm();
	
	$data_id = intval(is_param_post('data_id')); 
			
	$array = array();
	$array['text_comment'] = pn_strip_input(is_param_post('text_comment'));
			
	$array['title'] = $accountnum = pn_strip_input(is_param_post('title'));
	if(!$array['title']){ $form->error_form(__('You have not entered a currency title','pn')); }
			
	$array['valid'] = intval(is_param_post('valid'));
	$array['prosm'] = intval(is_param_post('prosm'));
			
	$array = apply_filters('pn_vaccounts_addform_post',$array);
			
	if($data_id){
		do_action('pn_vaccounts_edit_before', $data_id, $array);
		$wpdb->update($wpdb->prefix.'vschets', $array, array('id'=>$data_id));
		do_action('pn_vaccounts_edit', $data_id, $array);
	} else {
		$wpdb->insert($wpdb->prefix.'vschets', $array);
		$data_id = $wpdb->insert_id;	
		do_action('pn_vaccounts_add', $data_id, $array);
	}
			
	if($data_id){
		$otv = update_vaccs_txtmeta($data_id, 'accountnum', $accountnum);
		if($otv != 1){
			$form->error_form(sprintf(__('Error! folder <b>%s</b> is missing or can not be written!','pn'),'/wp-content/uploads/vaccsmeta/'));
		}				
	}			

	$url = admin_url('admin.php?page=pn_add_vaccounts&item_id='. $data_id .'&reply=true');
	$form->answer_form($url);
}	