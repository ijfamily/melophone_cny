<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_add_vaccounts_many', 'adminpage_title_pn_add_vaccounts_many');
function adminpage_title_pn_add_vaccounts_many(){
	_e('Add list','pn');
}

add_action('pn_adminpage_content_pn_add_vaccounts_many','def_adminpage_content_pn_add_vaccounts_many');
function def_adminpage_content_pn_add_vaccounts_many(){
global $wpdb;

	$form = new PremiumForm();

	$title = __('Add list','pn');
	
	$valuts = array();
	$vtypes = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts ORDER BY vname ASC");		  
	foreach($vtypes as $vtype){
		$valuts[$vtype->id] = pn_strip_input($vtype->vname .' '. $vtype->vtype);
	}	
	
	$back_menu = array();
	$back_menu['back'] = array(
		'link' => admin_url('admin.php?page=pn_vaccounts'),
		'title' => __('Back to list','pn')
	);
	$form->back_menu($back_menu, '');	
	
	$options = array();	
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => $title,
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['valid'] = array(
		'view' => 'select',
		'title' => __('Currency title','pn'),
		'options' => $valuts,
		'default' => 0,
		'name' => 'valid',
	);	
	$options['items'] = array(
		'view' => 'textarea',
		'title' => __('Curency accounts (on a new line)','pn'),
		'default' => '',
		'name' => 'items',
		'width' => '',
		'height' => '300px',
	);
	$params_form = array(
		'filter' => 'pn_add_vaccounts_many_addform',
		'method' => 'post',
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);	
}

add_action('premium_action_pn_add_vaccounts_many','def_premium_action_pn_add_vaccounts_many');
function def_premium_action_pn_add_vaccounts_many(){
global $wpdb;

	only_post();
	pn_only_caps(array('administrator'));
	
	$form = new PremiumForm();

	$items = explode("\n",is_param_post('items'));
	if(is_array($items)){
				
		$valut_id = intval(is_param_post('valid'));
				
		foreach($items as $item){
			$accountnum = pn_strip_input($item);
			if($accountnum){
						
				$wpdb->insert($wpdb->prefix.'vschets', array('valid'=>$valut_id,'title'=>$accountnum));
				$data_id = $wpdb->insert_id;
				if($data_id){
					$otv = update_vaccs_txtmeta($data_id, 'accountnum', $accountnum);
					if($otv != 1){
						$form->error_form(sprintf(__('Error! folder <b>%s</b> is missing or can not be written!','pn'),'/wp-content/uploads/vaccsmeta/'));
					}							
				}							
						
			}
		}
	}

	$url = admin_url('admin.php?page=pn_vaccounts&reply=true');
	$form->answer_form($url);
}	