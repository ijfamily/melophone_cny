<?php
if( !defined( 'ABSPATH')){ exit(); }

global $exchangebox;	
$exchangebox->include_patch(__FILE__, 'add');
$exchangebox->include_patch(__FILE__, 'add_many');
$exchangebox->include_patch(__FILE__, 'list');