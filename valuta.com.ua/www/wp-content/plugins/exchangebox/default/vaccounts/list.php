<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_vaccounts', 'adminpage_title_pn_vaccounts');
function adminpage_title_pn_vaccounts(){
	_e('Currency accounts','pn');
}

add_action('pn_adminpage_content_pn_vaccounts','def_adminpage_content_pn_vaccounts');
function def_adminpage_content_pn_vaccounts(){

	if(class_exists('trev_vaccounts_List_Table')){
		$Table = new trev_vaccounts_List_Table();
		$Table->prepare_items();
		
		$form = new PremiumForm();
		
		$search = array();
		$search[] = array(
			'view' => 'input',
			'title' => __('Curency account','pn'),
			'default' => pn_strip_input(is_param_get('item')),
			'name' => 'item',
		);	
		$valuts_arr = array();
		$valuts_arr[0] = '--'.__('All currency','pn').'--';
		global $wpdb;
		$valuts = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts ORDER BY site_order ASC");	
	  	foreach($valuts as $item){
			$valuts_arr[$item->id] = pn_strip_input($item->vname .' '. $item->vtype);
		}	
		$search[] = array(
			'view' => 'select',
			'title' => __('Currency','pn'),
			'default' => intval(is_param_get('valut_id')),
			'name' => 'valut_id',
			'options' => $valuts_arr,
		);				
		pn_admin_searchbox($search, 'reply');
		
		$options = array();
		$options['mod'] = array(
			'name' => 'mod',
			'options' => array(
				'1' => __('active accounts','pn'),
				'2' => __('inactive accounts','pn'),
			),
			'title' => '',
		);		
		pn_admin_submenu($options, 'reply');

		$form->help(__('On shortcodes','pn'), 
		__('unique = "0" - show once randomly','pn') . '<br />' .
		__('unique = "1" - show always randomly','pn') . '<br />' .
		__('fix = "0" - do not fix account number within one order','pn') . '<br />' .
		__('fix = "1" - fix account number within one order','pn') . '<br />' .
		__('hide = "0" - visible account number','pn') . '<br />' .
		__('hide = "1" - invisible (hide) account number','pn')
		); 		
?>
	<form method="post" action="<?php pn_the_link_post(); ?>">
		<?php $Table->display() ?>
	</form>
<?php 
	} else {
		echo 'Class not found';
	}
}


add_action('premium_action_pn_vaccounts','def_premium_action_pn_vaccounts');
function def_premium_action_pn_vaccounts(){
global $wpdb;

	only_post();
	pn_only_caps(array('administrator'));

	$reply = '';
	$action = get_admin_action();	
	
	if(isset($_POST['save'])){
						
		do_action('pn_vaccounts_save');
		$reply = '&reply=true';

	} else {	
	
		if(isset($_POST['id']) and is_array($_POST['id'])){				
					
			if($action=='active'){
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vschets WHERE id='$id' AND visib != '0'");
					if(isset($item->id)){		
						do_action('pn_vaccounts_active_before', $id, $item);
						$result = $wpdb->update($wpdb->prefix.'vschets', array('visib'=>'0'), array('id'=>$id));
						do_action('pn_vaccounts_active', $id, $item);
						if($result){
							do_action('pn_vaccounts_active_after', $id, $item);
						}					
					}			
				}
			}	

			if($action=='notactive'){
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vschets WHERE id='$id' AND visib != '1'");
					if(isset($item->id)){
						do_action('pn_vaccounts_notactive_before', $id, $item);		
						$result = $wpdb->update($wpdb->prefix.'vschets', array('visib'=>'1'), array('id'=>$id));
						do_action('pn_vaccounts_notactive', $id, $item);
						if($result){
							do_action('pn_vaccounts_notactive_after', $id, $item);
						}					
					}			
				}
			}	

			if($action=='clearpr'){
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vschets WHERE id='$id' AND prosm != '0'");
					if(isset($item->id)){
						do_action('pn_vaccounts_clearpr_before', $id, $item);
						$result = $wpdb->update($wpdb->prefix.'vschets', array('prosm'=>'0'), array('id'=>$id));
						do_action('pn_vaccounts_clearpr', $id, $item);
						if($result){
							do_action('pn_vaccounts_clearpr_after', $id, $item);
						}					
					}			
				}
			}	

			if($action=='delete'){
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vschets WHERE id='$id'");
					if(isset($item->id)){
						do_action('pn_vaccounts_delete_before', $id, $item);
						$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."vschets WHERE id = '$id'");
						do_action('pn_vaccounts_delete', $id, $item);
						if($result){
							delete_vaccs_txtmeta($id);
							do_action('pn_vaccounts_delete_after', $id, $item);
						}					
					}			
				}
			}
			
			do_action('pn_vaccounts_action', $action, $_POST['id']);
			$reply = '&reply=true';
						
		} 
	}
			
	$url = is_param_post('_wp_http_referer') . $reply;
	$paged = intval(is_param_post('paged'));
	if($paged > 1){ $url .= '&paged='.$paged; }	
	wp_redirect($url);
	exit;			
}  

class trev_vaccounts_List_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;
                
        parent::__construct( array(
            'singular'  => 'id',      
			'ajax' => false,  
        ) );
        
    }
	
    function column_default($item, $column_name){
        
		if($column_name == 'idsnew'){
			$code = '[exb_schet valuts="'. $item->valid .'" unique="0" hide="0" fix="1"]';
			return "<input type='text' style='width: 100%;' name='' onclick='this.select()' value='". $code ."' />";
		} elseif($column_name == 'valuts'){
		    return get_vtitle($item->valid);
		} elseif($column_name == 'title'){
			$accountnum_or = pn_strip_input(get_vaccs_txtmeta($item->id, 'accountnum'));
			$accountnum = $item->title;
			
			if($accountnum != $accountnum_or and $accountnum_or){
				return '<span class="bred_dash">'. $accountnum .'</span> <span class="bgreen">'. $accountnum_or .'</span>';
			} else {
				return $accountnum;
			}
		} elseif($column_name == 'cv'){
			return pn_strip_input($item->prosm);
		} elseif($column_name == 'comment'){
			return pn_strip_text($item->text_comment);			
		} elseif($column_name == 'status'){
	        $st = $item->visib;
		    if($st == 1){
		        return '<span class="bred">'. __('not active account','pn') .'</span>';
		    } else { 
		        return '<span class="bgreen">'. __('active account','pn') .'</span>';
		    }		
		} 		
		return apply_filters('vaccounts_manage_ap_col', '', $column_name,$item);
		
    }	
	
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'], 
            $item->id                
        );
    }	

    function column_cid($item){

        $actions = array(
            'edit'      => '<a href="'. admin_url('admin.php?page=pn_add_vaccounts&item_id='. $item->id) .'">'. __('Edit','pn') .'</a>',
        );
        
        return sprintf('%1$s %2$s',
            $item->valid,
            $this->row_actions($actions)
        );
		
    }	
	
    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
			'cid'    => __('Shortcode-ID','pn'),
			'idsnew'    => __('Shortcode','pn'),
			'valuts'    => __('Currency title','pn'),
		    'title'    => __('Curency account','pn'),
			'status'    => __('Status','pn'),	
            'cv'    => __('Hits','pn'),	
			'comment'    => __('Comment','pn'),
        );
		$columns = apply_filters('vaccounts_manage_ap_columns', $columns);
        return $columns;
    }	
	

    function get_bulk_actions() {
        $actions = array(
			'active'    => __('Activate','pn'),
			'notactive'    => __('Deactivate','pn'),
			'clearpr'    => __('Reset the counter','pn'),
            'delete'    => __('Delete','pn'),
        );
        return $actions;
    }
    
    function get_sortable_columns() {
        $sortable_columns = array( 
            'cid'     => array('cid',false),
			'cv'     => array('cv',false),
        );
        return $sortable_columns;
    }	
	
    function prepare_items() {
        global $wpdb; 
		
        $per_page = $this->get_items_per_page('trev_vaccounts_per_page', 20);
        $current_page = $this->get_pagenum();
        
        $this->_column_headers = $this->get_column_info();

		$offset = ($current_page-1)*$per_page;
		
		$oby = is_param_get('orderby');
		if($oby == 'cid'){
		    $orderby = 'valid';		
		} elseif($oby == 'cv'){	
			$orderby = "(prosm -0.0)";			
		} else {
		    $orderby = 'id';
		}
		$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';
		if($order != 'asc'){ $order = 'desc'; }		
		
		$where = '';
		
        $mod = intval(is_param_get('mod'));
        if($mod == 1){ 
            $where .= " AND visib='0'"; 
		} elseif($mod == 2){
			$where .= " AND visib='1'";
		}		
		
        $valut_id = intval(is_param_get('valut_id'));
        if($valut_id > 0){ 
            $where .= " AND valid='$valut_id'"; 
		}

        $accountnum = pn_sfilter(pn_strip_input(is_param_get('item')));
        if($accountnum){ 
            $where .= " AND title LIKE '%$accountnum%'"; 
		}		
		$where = pn_admin_search_where($where);
		$total_items = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."vschets WHERE id > 0 $where");
		$data = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."vschets WHERE id > 0 $where ORDER BY $orderby $order LIMIT $offset , $per_page");  		

        $current_page = $this->get_pagenum();
        $this->items = $data;
		
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  
            'per_page'    => $per_page,                     
            'total_pages' => ceil($total_items/$per_page)  
        ));
    }
	
	function extra_tablenav( $which ) {  	
    ?>	
		<div class="alignleft actions">
			<input type="submit" name="save" class="button" value="<?php _e('Save','pn'); ?>">
            <a href="<?php echo admin_url('admin.php?page=pn_add_vaccounts');?>" class="button"><?php _e('Add new','pn'); ?></a>
			<a href="<?php echo admin_url('admin.php?page=pn_add_vaccounts_many');?>" class="button"><?php _e('Add list','pn'); ?></a>
		</div>		
	<?php 
	}	  
	
}

add_action('premium_screen_pn_vaccounts','my_myscreen_pn_vaccounts');
function my_myscreen_pn_vaccounts() {
    $args = array(
        'label' => __('Display','pn'),
        'default' => 20,
        'option' => 'trev_vaccounts_per_page'
    );
    add_screen_option('per_page', $args );
	if(class_exists('trev_vaccounts_List_Table')){
		new trev_vaccounts_List_Table;
	}
}