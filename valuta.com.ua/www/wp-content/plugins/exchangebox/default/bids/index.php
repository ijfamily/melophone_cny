<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('wp_before_admin_bar_render', 'wp_before_admin_bar_render_bids');
function wp_before_admin_bar_render_bids(){
global $wp_admin_bar, $wpdb, $exchangebox;
	
    if(current_user_can('administrator')){
	
		$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."bids");
		if($query == 1){	
			$z = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."bids WHERE status IN('payed','realpay','verify')");
			if($z > 0){
				$wp_admin_bar->add_menu( array(
					'id'     => 'new_paybids',
					'href' => admin_url('admin.php?page=pn_bids&mod=2'),
					'title'  => '<div style="height: 32px; width: 22px; background: url('. $exchangebox->plugin_url .'images/newchange.png) no-repeat center center"></div>',
					'meta' => array( 'title' => __('There are pending applications','pn').' ('. $z .')' )		
				));	
			}
		}
	
	}
	
}

global $exchangebox;	
$exchangebox->include_patch(__FILE__, 'list');