<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_bids', 'adminpage_title_pn_bids');
function adminpage_title_pn_bids(){
	_e('Applications','pn');
} 

add_action('pn_adminpage_content_pn_bids','pn_def_adminpage_content_pn_bids');
function pn_def_adminpage_content_pn_bids(){
global $wpdb;

	if(class_exists('trev_bids_List_Table')){
		$Table = new trev_bids_List_Table();
		$Table->prepare_items();
		
		$search = array();
		$search[] = array(
			'view' => 'select',
			'title' => __('Bids pay status','pn'),
			'default' => intval(is_param_get('payed')),
			'options' => array('0'=>'--'.__('All','pn').'--', '1'=>__('Untreated applications','pn'),'2'=>__('Treated applications','pn')),
			'name' => 'payed',
		);	
			$status = array(
				'0' => '--'.__('All','pn').'--',
				'1' => __('new application','pn'),
				'2' => __('marked bid by user as paid','pn'),
				'3' => __('paid bid','pn'),
				'4' => __('delete application','pn'),
				'5' => __('money will be returned','pn'),
				'6' => __('paid from another purse','pn'),
				'7' => __('error application','pn'),
				'8' => __('success application','pn'),
			);		
		$search[] = array(
			'view' => 'select',
			'title' => __('Bids status','pn'),
			'default' => intval(is_param_get('mod')),
			'options' => $status,
			'name' => 'mod',
		);	
		$search[] = array(
			'view' => 'line',
		);		
		$naps = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."napobmens WHERE status='1' ORDER BY rorder ASC");
		$directions = array();
		$directions[0] = '--'.__('All direction','pn').'--';
		foreach($naps as $item){
			$directions[$item->id] = get_vtitle($item->valsid1) ." = ". get_vtitle($item->valsid2);
		}	
		$search[] = array(
			'view' => 'select',
			'title' => __('Direction','pn'),
			'default' => intval(is_param_get('naps_id')),
			'options' => $directions,
			'name' => 'naps_id',
		);	

		$valuts = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts ORDER BY site_order ASC");
		$currency = array();
		$currency[0] = '--'.__('All currency','pn').'--';
		foreach($valuts as $item){
			$currency[$item->id] = pn_strip_input($item->vname) ." ". pn_strip_input($item->vtype);
		}
		$search[] = array(
			'view' => 'select',
			'title' => __('Currency give','pn'),
			'default' => intval(is_param_get('val1')),
			'options' => $currency,
			'name' => 'val1',
		);
		$search[] = array(
			'view' => 'select',
			'title' => __('Currency get','pn'),
			'default' => intval(is_param_get('val2')),
			'options' => $currency,
			'name' => 'val2',
		);	
		$search[] = array(
			'view' => 'line',
		);
		$search[] = array(
			'view' => 'input',
			'title' => __('Bid id','pn'),
			'default' => pn_strip_input(is_param_get('bidid')),
			'name' => 'bidid',
		);	
		$search[] = array(
			'view' => 'input',
			'title' => __('IP','pn'),
			'default' => pn_strip_input(is_param_get('theip')),
			'name' => 'theip',
		);
		$search[] = array(
			'view' => 'date',
			'title' => __('Date','pn'),
			'default' => pn_strip_input(is_param_get('zdate')),
			'name' => 'zdate',
		);	
		$search[] = array(
			'view' => 'line',
		);
		$search[] = array(
			'view' => 'input',
			'title' => __('User ID','pn'),
			'default' => pn_strip_input(is_param_get('suser')),
			'name' => 'suser',
		);
		$search[] = array(
			'view' => 'input',
			'title' => __('E-mail','pn'),
			'default' => pn_strip_input(is_param_get('semail')),
			'name' => 'semail',
		);
		$search[] = array(
			'view' => 'input',
			'title' => __('Skype','pn'),
			'default' => pn_strip_input(is_param_get('skype')),
			'name' => 'skype',
		);	
		$search[] = array(
			'view' => 'line',
		);	
		$search[] = array(
			'view' => 'input',
			'title' => __('Account give','pn'),
			'default' => pn_strip_input(is_param_get('schet1')),
			'name' => 'schet1',
		);	
		$search[] = array(
			'view' => 'input',
			'title' => __('Account get','pn'),
			'default' => pn_strip_input(is_param_get('schet2')),
			'name' => 'schet2',
		);			
		pn_admin_searchbox($search, 'reply');
?>
	<form method="post" action="<?php pn_the_link_post(); ?>">
		<div class="theobmentable">
			<?php $Table->display() ?>
		</div>	
	</form>
	
<script type="text/javascript">	
jQuery(function($){
	
    $('.theobmentable thead input[type=checkbox]').on('change',function(){
		if($(this).prop('checked')){
			$('.theobmentable tbody td').css({'background':'#d6ecf2'});
		} else {
			$('.theobmentable tbody td').css({'background':'#fff'});
		}
	});
	
    $('.theobmentable tbody input[type=checkbox]').on('change',function(){
		if($(this).prop('checked')){
			$(this).parents('tr').find('td').css({'background':'#d6ecf2'});
		} else {
			$(this).parents('tr').find('td').css({'background':'#fff'});
		}
		return false;
	});	
	
/* comment */
	$(document).on('click', '.question_div', function(){
		
		<?php
		$content = '<form action="'. pn_link_post('bid_user_comment') .'" class="ajaxed_comment_form" method="post"><p><textarea id="comment_the_text" name="comment"></textarea></p><p><input type="submit" name="submit" class="button-primary" value="'. __('Save','pn') .'" /></p><input type="hidden" name="id" id="comment_the_id" value="" /><input type="hidden" name="vid" id="comment_the_wid" value="0" /></form>';
		?>		
		
		$(document).JsWindow('show', {
			id: 'window_to_comment',
			div_class: 'update_window',
			title: '<div id="comment_the_title"></div>',
			content: '<?php echo $content; ?>',
			shadow: 0,
			after: init_comment_ajax_form
		});		
		
		var vid = parseInt($(this).attr('data-vid'));
		var id = parseInt($(this).attr('data-id'));
		
		$('#comment_the_id').val(id);
		$('#comment_the_wid').val(vid);
		$('.apply_loader').show();
		$('#techwindow_window_to_comment input[type=submit]').attr('disabled',true);
		
		if(vid == 0){
			$('#comment_the_title').html('<?php _e('Comment to user','pn'); ?>');
		} else {
			$('#comment_the_title').html('<?php _e('Comment to admin','pn'); ?>');
		}
		
		var param = 'id=' + id +'&vid='+ vid;
		$.ajax({
			type: "POST",
			url: "<?php pn_the_link_post('bid_comment_get');?>",
			dataType: 'json',
			data: param,
			error: function(res, res2, res3){
				<?php do_action('pn_js_error_response', 'ajax'); ?>
			},			
			success: function(res)
			{		
				$('.apply_loader').hide();
				$('#techwindow_window_to_comment input[type=submit]').attr('disabled',false);
				if(res['status'] == 'error'){
					<?php do_action('pn_js_alert_response'); ?>
				} else if(res['status'] == 'success'){
					$('#comment_the_text').val(res['comment']);						
				}					
			}
		});	
		
	});
	
	function init_comment_ajax_form(){
		
		$('.ajaxed_comment_form').ajaxForm({
			dataType:  'json',
			beforeSubmit: function(a,f,o) {
				$('#techwindow_window_to_comment input[type=submit]').attr('disabled',true);
			},
			error: function(res, res2, res3) {
				<?php do_action('pn_js_error_response', 'form'); ?>
			},			
			success: function(res) {
				$('#techwindow_window_to_comment input[type=submit]').attr('disabled',false);
				if(res['status'] == 'error'){ 
					<?php do_action('pn_js_alert_response'); ?>
				} else if(res['status'] == 'success'){
					
					var vid = parseInt($('#comment_the_wid').val());
					var id = $('#comment_the_id').val();
					
					if(vid == 0){
						var comf = $('#ucomment-'+id);
					} else {
						var comf = $('#acomment-'+id);
					}					
					
					if(res['comment'] == 'true'){
						comf.addClass('active');
					} else {
						comf.removeClass('active');
					}
					
				}
			}
		});
	}		
/* end comment */	
	
});
</script>	
<?php 
	} else {
		echo 'Class not found';
	}
}

/* comments */
add_action('premium_action_bid_comment_get', 'pn_premium_action_bid_comment_get');
function pn_premium_action_bid_comment_get(){
global $wpdb;

	only_post();

	$log = array();
	$log['response'] = '';
	$log['status'] = '';
	$log['status_code'] = 0;
	$log['status_text'] = '';
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	if(current_user_can('administrator')){
	
		$id = intval(is_param_post('id'));
		$vid = intval(is_param_post('vid'));

		$item = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."bids WHERE id='$id'");
		if($vid == 0){
			$comment = pn_strip_text($item->ucomments);
		} else {
			$comment = pn_strip_text($item->acomments);
		}
		
		$log['comment'] = $comment;
		$log['status'] = 'success';
	} else {
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Authorisation Error','pn');
	}	
	
	echo json_encode($log);
	exit;
}

add_action('premium_action_bid_user_comment', 'pn_premium_action_bid_user_comment');
function pn_premium_action_bid_user_comment(){
global $wpdb;

	only_post();

	$log = array();
	$log['response'] = '';
	$log['status'] = '';
	$log['status_code'] = 0;
	$log['status_text'] = '';

	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	if(current_user_can('administrator')){
	
		$id = intval(is_param_post('id'));
		$vid = intval(is_param_post('vid'));
		$text = pn_strip_text(is_param_post('comment'));
		if($vid == 0){
			$wpdb->update($wpdb->prefix.'bids', array('ucomments'=>$text), array('id'=>$id));
		} else {
			$wpdb->update($wpdb->prefix.'bids', array('acomments'=>$text), array('id'=>$id));
		}
		if($text){
			$log['comment'] = 'true';
		} else {
			$log['comment'] = 'false';
		}
		$log['status'] = 'success';
	} else {
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Authorisation Error','pn');
	}	
	
	echo json_encode($log);	
	exit;
}
/* end comments */

add_action('premium_action_pn_bids','def_premium_action_pn_bids');
function def_premium_action_pn_bids(){
global $wpdb;	

	only_post();
	pn_only_caps(array('administrator'));
	
	$reply = '';
	$action = get_admin_action();
						
	if(isset($_POST['id']) and is_array($_POST['id'])){				
				
		$my_dir = wp_upload_dir();
		$dir = $my_dir['basedir'].'/bids/';
		$time = current_time('mysql');				
			
		/* удаляем полностью */
		if($action == 'realdelete'){
			foreach($_POST['id'] as $id){
				$id = intval($id);
								
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$id'");
				if(isset($item->id)){
									
					$wpdb->query("DELETE FROM ".$wpdb->prefix."bids WHERE id = '$id'");
					$wpdb->query("DELETE FROM ".$wpdb->prefix."bids_meta WHERE item_id = '$id'");						
						
					do_action('change_bidstatus_all', 'realdelete', $item->id, $item, 'admin');
					do_action('change_bidstatus_realdelete', $item->id, $item, 'admin'); 					
									
					$file = $dir . $id .'.txt';
					if(is_file($file)){
						@unlink($file);
					}
									
				}			
			}	 
		}
		/* end удаляем полностью */
	
		/* ЧС */
		if($action == 'blacklist'){
			foreach($_POST['id'] as $id){
				$id = intval($id);
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$id'");
				if(isset($item->id)){
									
					$account1 = pn_strip_input($item->schet1);
					if($account1){
						$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."blacklist WHERE meta_value='$account1' AND meta_key='0'");
						if($cc == 0){
							$wpdb->insert($wpdb->prefix.'blacklist', array('meta_value'=>$account1,'meta_key'=>0));
						}
					}

					$account2 = pn_strip_input($item->schet2);
					if($account2){
						$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."blacklist WHERE meta_value='$account2' AND meta_key='0'");
						if($cc == 0){
							$wpdb->insert($wpdb->prefix.'blacklist', array('meta_value'=>$account2,'meta_key'=>0));
						}	
					}						
									
					$user_email = is_email($item->email);
					if($user_email){
						$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."blacklist WHERE meta_value='$user_email' AND meta_key='1'");
						if($cc == 0){
							$wpdb->insert($wpdb->prefix.'blacklist', array('meta_value'=>$user_email,'meta_key'=>1));
						}
					}						
									
					$user_phone = str_replace('+','',pn_strip_input($item->tel));
					if($user_phone){
						$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."blacklist WHERE meta_value='$user_phone' AND meta_key='2'");
						if($cc == 0){
							$wpdb->insert($wpdb->prefix.'blacklist', array('meta_value'=>$user_phone,'meta_key'=>2));
						}	
					}
									
					$user_skype = pn_strip_input($item->skype);
					if($user_skype){
						$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."blacklist WHERE meta_value='$user_skype' AND meta_key='3'");
						if($cc == 0){
							$wpdb->insert($wpdb->prefix.'blacklist', array('meta_value'=>$user_skype,'meta_key'=>3));
						}
					}
									
				}
			}
		}	
		/* end ЧС */	

		/* other */
		$sts = array('new','payed','realpay','delete','returned','verify','error','success');
		if(in_array($action, $sts)){
			foreach($_POST['id'] as $id){
				$id = intval($id);
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$id' AND status != '$action'");
				if(isset($item->id)){
									
					$wpdb->update($wpdb->prefix.'bids', array('status'=>$action,'editdate'=> $time), array('id'=>$id));
					do_action('change_bidstatus_all', $action, $item->id, $item, 'admin');
					do_action('change_bidstatus_'.$action, $item->id, $item, 'admin');							
									
				}
			}
		}	
		/* end other */				
					
	} 
			
	$url = is_param_post('_wp_http_referer') . $reply;
	$paged = intval(is_param_post('paged'));
	if($paged > 1){ $url .= '&paged='.$paged; }	
	wp_redirect($url);
	exit;				
} 

class trev_bids_List_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;
                
        parent::__construct( array(
            'singular'  => 'id',      
			'ajax' => false,  
        ) );
        
    }
	
    function column_default($item, $column_name){
        
		if($column_name == 'cid'){
			$new = get_bids_meta($item->id, 'new');
			if($new == 1){
				return '<span class="bred_dash" title="'. __('Pay attention. Exchange does novice.','pn') .'">'. $item->id .'</span>';
			} else {
				return $item->id;
			}
		} elseif($column_name == 'cdate'){
		    return '<span style="cursor: help;" title="'. pn_strip_input($item->userip) .'">'.get_mytime($item->tdate,'d.m.Y H:i').'</span>';
		} elseif($column_name == 'cip'){
		    return pn_strip_input($item->userip);		
		} elseif($column_name == 'cedit'){
			return get_mytime($item->editdate,'d.m.Y H:i');
		} elseif($column_name == 'cobmen'){
		    return '<b style="cursor: help;" title="'. __('Sum','pn') .'">'. is_sum($item->summ1) .'</b> / <b style="cursor: help;" title="'. __('Sum with commission','pn') .'">'. is_sum($item->summz1) .'</b> '. pn_strip_input($item->valut1) .' '. pn_strip_input($item->valut1type) .' &rarr; <span class="bgreen"><b style="cursor: help;" title="'. __('Sum','pn') .'">'. is_sum($item->summ2) .'</b> / <b style="cursor: help;" title="'. __('Sum with commission','pn') .'">'. is_sum($item->summz2) .'</b> '. pn_strip_input($item->valut2) .' '. pn_strip_input($item->valut2type).'</span>';
		} elseif($column_name == 'csoschet'){
			$my_dir = wp_upload_dir();
			$file = $my_dir['basedir'].'/bids/' . $item->id . '.txt';
			$data_f = '';
			if(is_file($file)){
				$data_f = @file_get_contents($file);
			}
			$schet_data = @unserialize($data_f);
			$schet1_file = pn_strip_input(is_isset($schet_data,'schet1'));
			if($schet1_file){ 
				$schet1_file = __('Account in file:','pn').' '.$schet1_file; 
			} else {
				$schet1_file = __('No account in file','pn'); 
			}
			$schet1 = pn_strip_input($item->schet1);
			$schet1_hash = pn_strip_input($item->schet1_hash);
			$cl = '';
			$wtxt = '';
			if($schet1_hash and is_array($schet_data) and !is_pn_crypt($schet1_hash, $schet1)){
				$cl = 'bred_dash';
				$wtxt = __('Account number does not match the initial!','pn');
			}
			$txt = '<span class="'. $cl .'" title="'. $wtxt .' '. $schet1_file .'">' . $schet1 . '</span>';
			$dmetas = @unserialize($item->dmetas);
			if(isset($dmetas[1]) and is_array($dmetas[1])){
				foreach($dmetas[1] as $value){				
					$title = pn_strip_input(is_isset($value,'title'));
					$data = pn_strip_input(is_isset($value,'data'));
					$hidden = intval(is_isset($value,'hidden'));
					if(trim($data)){
						$txt .= '<br /><span style="font-weight: bold;">'. $title .':</span> '. $data;
					}
				}
			}					
			return $txt;
		} elseif($column_name == 'cnaschet'){
			$my_dir = wp_upload_dir();
			$file = $my_dir['basedir'].'/bids/' . $item->id . '.txt';
			$data_f = '';
			if(is_file($file)){
				$data_f = @file_get_contents($file);
			}
			$schet_data = @unserialize($data_f);
			$schet2_file = pn_strip_input(is_isset($schet_data,'schet2'));
			if($schet2_file){ 
				$schet2_file = __('Account in file:','pn').' '.$schet2_file; 
			} else {
				$schet2_file = __('No account in file','pn'); 
			}
			$schet2 = pn_strip_input($item->schet2);
			$schet2_hash = pn_strip_input($item->schet2_hash);
			
			$cl = '';
			$wtxt = '';			
			if($schet2_hash and is_array($schet_data) and !is_pn_crypt($schet2_hash, $schet2)){
				$cl = 'bred_dash';
				$wtxt = __('Account number does not match the initial!','pn');
			}
			$txt = '<span class="'. $cl .'" title="'. $wtxt .' '. $schet2_file .'">' . $schet2 . '</span>';
			
			$dmetas = @unserialize($item->dmetas);
			if(isset($dmetas[2]) and is_array($dmetas[2])){
				foreach($dmetas[2] as $value){					
					$title = pn_strip_input(is_isset($value,'title'));
					$data = pn_strip_input(is_isset($value,'data'));
					$hidden = intval(is_isset($value,'hidden'));
					if(trim($data)){
						$txt .= '<br /><span style="font-weight: bold;">'. $title .':</span> '. $data;
					}
				}
			}	
			return $txt;
		} elseif($column_name == 'cdannie'){
		    $dann = get_exbfio($item->fname,$item->iname,$item->oname); 
		    if($item->tel){ $dann .= '<br />('. __('Phone','pn') .'. '. pn_strip_input($item->tel) .')'; } 
		    if($item->skype){ $dann .= '<br />('. __('Skype','pn') .': '. pn_strip_input($item->skype) .')'; } 
			if($item->pnomer){ $dann .= '<br />('. __('Passport','pn') .': '. pn_strip_input($item->pnomer) .')'; }
			$dann .= apply_filters('exb_admin_obmen_dann','',$item);
			
            return $dann;
		} elseif($column_name == 'cclient'){
		    $user_id = $item->user_id;
			if($user_id){
			    $ui = get_userdata($user_id);
				if(isset($ui->user_login)){
					$user_login = apply_filters('user_login_verify', pn_strip_input($ui->user_login) , $ui);
					return '<a href="user-edit.php?user_id='. $user_id .'">'. $user_login .'</a>';
				}
			} else {
			    return __('Guest','pn');
			}
		} elseif($column_name == 'cemail'){
		    return '<a href="mailto:'. pn_strip_input($item->email) .'">'. pn_strip_input($item->email) .'</a>';
		} elseif($column_name == 'cschet'){
		    return pn_strip_input($item->naschet);		
		} elseif($column_name == 'ccurs'){	
			return pn_strip_input($item->curs1 .' '. $item->valut1 .' '. $item->valut1type) .' <br /> '. pn_strip_input($item->curs2 .' '. $item->valut2 .' '. $item->valut2type);
		} elseif($column_name == 'copl'){
		    if($item->status == 'verify' or $item->status == 'payed' or $item->status == 'realpay'){
				$rxzt = valuts_rxzt($item->valut2i);
				if($rxzt == 2){
					return '<iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/small.xml?uid='. pn_strip_input($item->schet2) .'&amp;button-text=01&amp;button-size=s&amp;button-color=orange&amp;targets='. __('Payment application id','pn') .' '. $item->id .'&amp;default-sum='. round(summ_pers($item->summz2,'0.5'),2) .'" width="140px" height="31"></iframe>';
				}
			}
		} elseif($column_name == 'cstatus'){
	        $st = $item->status; 
			$cl = '';
			if($st == 'error' or $st =='delete'){
				$cl = 'bred';
			} elseif($st == 'success'){
				$cl = 'bgreen';
			}
			
            return '<a href="'. get_bids_url($item->hashed) .'" target="_blank" class="'. $cl .'">'. get_bid_status($st) .'</a>';	
		} elseif($column_name == 'metka'){
			$id = $item->id;
			$zametka = pn_strip_input($item->acomments);
			if($zametka){ $cl='active'; } else { $cl=''; }		
			return '<div class="question_div '. $cl .'" id="acomment-'. $id .'" data-vid="1" data-id="'. $id .'""></div>';
		} elseif($column_name == 'umetka'){	
			$id = $item->id;
			$zametka = pn_strip_input($item->ucomments);
			if($zametka){ $cl='active'; } else { $cl=''; }		
			return '<div class="question_div '. $cl .'" id="ucomment-'. $id .'" data-vid="0" data-id="'. $id .'""></div>';			
		} elseif($column_name == 'mclient'){	
			$mobile = get_bids_meta($item->id, 'mobile');
			if($mobile == 1){
				$cv = '<div class="mclientdiv mobile" title="'. __('Mobile version','pn') .'"></div>';
			} else {
				$cv = '<div class="mclientdiv desctop" title="'. __('Desctop version','pn') .'"></div>';
			}
			return $cv;
		} 
		
		return apply_filters('manage_restrict_bids_column_col', '', $column_name, $item);
		
    }	
	
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'], 
            $item->id                
        );
    }	

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'cid'    => __('ID','pn'),
			'cdate'    => __('Date','pn'),           
			'cobmen'     => __('Exchange','pn'),
			'ccurs'     => __('Course','pn'),
			'csoschet'    => __('From the account','pn'),
			'cnaschet'  => __('At the expense','pn'),
            'cdannie'  => __('Personal data','pn'),
            'cclient'  => __('Client','pn'),			
			'cemail'  => __('E-mail','pn'),
			'copl'  => __('Quick pay','pn'),
			'cstatus'  => __('Status','pn'),
			'cedit'  => __('Edit date','pn'),
			'metka'  => __('Comment to admin','pn'),
			'umetka'  => __('Comment to user','pn'),
			'cschet'  => __('Account','pn'),
			'cip'    => __('IP','pn'),			
        );
		$columns['mclient'] = __('Version of website','pn');
		
		$columns = apply_filters('manage_restrict_bids_column', $columns);
		
        return $columns;
    }	
	
    function get_sortable_columns() {
        $sortable_columns = array( 
			'id'     => array('id',false),
			'cdate'     => array('cdate',false),
        );
        return $sortable_columns;
    }
	
    function get_bulk_actions() {
		
		$actions = array(
			'new' => __('new application','pn'),
			'payed' => __('marked bid by user as paid','pn'),
			'realpay' => __('paid bid','pn'),
			'verify' => __('paid from another purse','pn'),
			'delete' => __('delete application','pn'),
			'returned' => __('money will be returned','pn'),
			'error' => __('error application','pn'),
			'success' => __('success application','pn'),
			'blacklist'    => __('Add to blacklist','pn'),
			'realdelete'    => __('Delete','pn'),
		);		
        return $actions;
    }
    
    function prepare_items() {
        global $wpdb; 
		
        $per_page = $this->get_items_per_page('trev_bids_per_page', 20);
        $current_page = $this->get_pagenum();
        
        $this->_column_headers = $this->get_column_info();

		$offset = ($current_page-1)*$per_page;
		$oby = is_param_get('orderby');
		if($oby == 'cdate'){
			$orderby = 'tdate';
		} else {
		    $orderby = 'id';
		}
		
		$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';
		if($order != 'asc'){ $order = 'desc'; }		
		
		$where = '';		
		
		$payed = intval(is_param_get('payed'));
		if($payed == 1){
			$where .= " AND status IN('payed','realpay','verify')";
		} elseif($payed == 2){
			$where .= " AND status IN('new','delete','returned','error','success')";
		}		
		
        $mod = intval(is_param_get('mod'));
        if($mod==1){ 
            $where .= " AND status = 'new'";
		} elseif($mod==2) {
			$where .= " AND status = 'payed'";	
		} elseif($mod==3) {
			$where .= " AND status = 'realpay'";			
		} elseif($mod==4) {
			$where .= " AND status = 'delete'";	
		} elseif($mod==5) {
			$where .= " AND status = 'returned'";				
		} elseif($mod==6) {
			$where .= " AND status = 'verify'";
		} elseif($mod==7) {
			$where .= " AND status = 'error'";
		} elseif($mod==8) {
			$where .= " AND status = 'success'";
		}			
		
        $val1 = intval(is_param_get('val1'));
        if($val1 > 0){ 
            $where .= " AND valut1i='$val1'"; 
		}
        $val2 = intval(is_param_get('val2'));
        if($val2 > 0){ 
            $where .= " AND valut2i='$val2'"; 
		}	

		$naps_id = intval(is_param_get('naps_id'));
        if($naps_id > 0){ 
            $where .= " AND naprid='$naps_id'"; 
		}		
		
		$suser = is_user(is_param_get('suser'));
		if($suser){
			$suser_id = username_exists($suser);
			if($suser_id){
				$where .= " AND user_id='$suser_id'";
			}
		}		
		
		$email = is_email(is_param_get('semail'));
		if($email){
		    $where .= " AND email LIKE '%$email%'";
		}			
		
		$schet1 = pn_sfilter(pn_strip_input(is_param_get('schet1')));
		if($schet1){
		    $where .= " AND schet1 LIKE '%$schet1%'";
		}		
		
		$schet2 = pn_sfilter(pn_strip_input(is_param_get('schet2')));
		if($schet2){
		    $where .= " AND schet2 LIKE '%$schet2%'";
		}
		
		$theip = pn_sfilter(pn_strip_input(is_param_get('theip')));
		if($theip){
		    $where .= " AND userip LIKE '%$theip%'";
		}
		
		$bidid = intval(is_param_get('bidid'));
		if($bidid){
			$where .= " AND id = '$bidid'";
		}		
		
		$skype = pn_sfilter(pn_strip_input(is_param_get('skype')));
		if($skype){
		    $where .= " AND skype LIKE '%$skype%'";
		}	
		
		$zdate = pn_sfilter(pn_strip_input(is_param_get('zdate')));
		if($zdate){
			$date1 = strtotime($zdate);
			$date2 = $date1 + (24 * 60 * 60);
			$date1 = date('Y-m-d 00:00:00',$date1);
			$date2 = date('Y-m-d 00:00:00',$date2);
			$where .= " AND tdate >= '$date1' AND tdate <= '$date2'";
		}		
		
		$where = pn_admin_search_where($where);
		$total_items = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE status != 'auto' $where");
		$data = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."bids WHERE status != 'auto' $where ORDER BY $orderby $order LIMIT $offset , $per_page");  		

        $current_page = $this->get_pagenum();
        $this->items = $data;
		
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  
            'per_page'    => $per_page,                     
            'total_pages' => ceil($total_items/$per_page)  
        ));
    }  
	
} 

add_action('premium_screen_pn_bids','my_myscreen_pn_bids');
function my_myscreen_pn_bids() {
    $args = array(
        'label' => __('Display','pn'),
        'default' => 20,
        'option' => 'trev_bids_per_page'
    );
    add_screen_option('per_page', $args );
	if(class_exists('trev_bids_List_Table')){
		new trev_bids_List_Table;
	}
}