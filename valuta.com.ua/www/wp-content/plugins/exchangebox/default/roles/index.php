<?php
if( !defined( 'ABSPATH')){ exit(); }

/* названия ролей */
function pn_role_title($text){

	if ( $text == 'users' ){
		return __('User','pn');
	}	

	return $text;
}

add_filter( 'gettext_with_context', 'standart_role_text', 10, 4 );
function standart_role_text( $translation, $text, $context, $domain ) {
	
	if ( $text == 'users' ){
		return __('User','pn');
	}

		return $translation;
}
/* end названия ролей */