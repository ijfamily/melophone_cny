<?php
if( !defined( 'ABSPATH')){ exit(); }

add_filter('pn_config_option', 'premiumbox_config_option');
function premiumbox_config_option($options){
global $exchangebox;
	
	$options['line1'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['pn_up_mode'] = array(
		'view' => 'select',
		'title' => __('Updating mode','pn'),
		'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('up_mode'),
		'name' => 'pn_up_mode',
		'work' => 'int',
	);	
	
	return $options;
}

add_action('pn_config_option_post', 'premiumbox_config_option_post');
function premiumbox_config_option_post($data){
global $exchangebox;
	
	$opts =  array('pn_up_mode'); 
	foreach($opts as $opt){
		$exchangebox->update_option('up_mode','',$data[$opt]);
	}	
	
}			