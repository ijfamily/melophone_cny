<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('siteplace_js','siteplace_js_zreserv');
function siteplace_js_zreserv(){	
?>	
/* request reserve */
jQuery(function($){
	$(document).on('click', '.js_reserv', function(){
		
		var title = '<?php _e('Request to reserve','pn'); ?> ' + $(this).attr('title');
		
		$(document).JsWindow('show', {
			id: 'window_to_zreserve',
			div_class: 'update_window',
			title: title,
			div_content: '.reserv_box_html',
			insert_div: '.reserv_box',
			shadow: 1
		});		
		
        var id = $(this).attr('data-id');		
		$('#reserv_box_id').val(id);	
			
	    return false;
	});			
});
/* end request reserve */	
<?php	
} 
/* end добавляем JS */

add_action('wp_footer','wp_footer_zreserv');
function wp_footer_zreserv(){
global $table_exchange;	

    if($table_exchange == 1){
		$ui = wp_get_current_user();
		$user_id = intval($ui->ID);
		
		$items = get_zreserv_form_filelds();
		$html = prepare_form_fileds($items, 'reserv_form_line', 'rb');		
		
		$array = array(
			'[result]' => '<div class="resultgo"></div>',
			'[html]' => $html,
			'[submit]' => '<input type="submit" formtarget="_top" name="submit" value="'. __('Submit request', 'pn') .'" />',
		);
		
		$temp = '
		<div class="reserv_box_html" style="display: none;">
			[result]
					
			[html]
					
			<p>[submit]</p>
		</div>	
		
		<form method="post" class="ajax_post_form" action="'. get_ajax_link('reservform') .'"><input type="hidden" name="id" id="reserv_box_id" value="0" />
			<div class="reserv_box"></div>
		</form>';
		
		$temp = apply_filters('zreserv_form_temp',$temp);
		echo get_replace_arrays($array, $temp);	
    } 
}

add_action('myaction_site_reservform', 'def_myaction_site_reservform');
function def_myaction_site_reservform(){
global $wpdb, $exchangebox;
	
	only_post();
	
	$log = array();
	$log['response'] = '';
	$log['status'] = '';
	$log['status_code'] = 0;
	$log['status_text'] = '';
	
	$exchangebox->up_mode();
	
	$log = apply_filters('before_ajax_form_field', $log, 'reservform');
	$log = apply_filters('before_ajax_reservform', $log);
	
	$id = intval(is_param_post('id'));
	$sum = is_sum(is_param_post('sum'),2);
	$email = is_email(is_param_post('email'));
	$comment = pn_maxf_mb(pn_strip_input(is_param_post('comment')),500);
		
	if($sum > 0){
		if($email){
			$valut = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."valuts WHERE id='$id'");
			if(isset($valut->id)){
					
				$last = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."zapros_rezerv WHERE email = '$email' AND valid='$id'");
					
				$array = array();
				$array['zdate'] = current_time('mysql');
				$array['valid'] = $id;
				$array['email'] = $email;
				$array['comment'] = $comment;
				$array['summk'] = $sum;
					
				if(isset($last->id)){
					$wpdb->update($wpdb->prefix ."zapros_rezerv", $array, array('id'=>$last->id));
				} else {
					$wpdb->insert($wpdb->prefix ."zapros_rezerv", $array);
				}
					
				$log['status'] = 'success_clear';
				$log['status_text'] = __('Request has been successfully created','pn');						
		
			} else {
				$log['status'] = 'error';
				$log['status_code'] = 1;
				$log['status_text'] = __('Error! currency does not exist','pn');			
			}
		} else {
			$log['status'] = 'error';
			$log['status_code'] = 1;
			$log['status_text'] = __('Error! You have not entered e-mail','pn');
		}
	} else {	
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! Requested amount is less than zero','pn');
	}	
	
	echo json_encode($log);
	exit;
}