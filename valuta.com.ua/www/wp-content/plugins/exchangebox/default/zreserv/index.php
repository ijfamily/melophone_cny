<?php 
if( !defined( 'ABSPATH')){ exit(); }

add_filter('list_user_notify','list_user_notify_zreserv');
function list_user_notify_zreserv($places_admin){
	$places_admin['zreserv'] = __('Reserve request','pn');
	return $places_admin;
}

add_filter('list_notify_tags_zreserv','def_list_notify_tags_zreserv');
function def_list_notify_tags_zreserv($tags){
	$tags['email'] = __('E-mail','pn');
	$tags['summrez'] = __('Sum resrve','pn');
	$tags['sum'] = __('Order price','pn');
	$tags['valut'] = __('Currency name','pn');
	return $tags;
}

add_action('pn_valuts_delete','zreserv_pn_valuts_delete',0,2);
function zreserv_pn_valuts_delete($id, $item){
global $wpdb;

	$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."zapros_rezerv WHERE valid = '$id'");
	foreach($items as $item){	
		$item_id = $item->id;
		do_action('pn_zreserv_delete_before', $item_id, $item);
		$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."zapros_rezerv WHERE id = '$item_id'");
		do_action('pn_zreserv_delete', $item_id, $item);
		if($result){
			do_action('pn_zreserv_delete_after', $item_id, $item);
		}
	}	
}

add_filter('list_icon_indicators', 'zreserv_icon_indicators');
function zreserv_icon_indicators($lists){
	$lists['zreserv'] = __('Reserve requests','pn');
	return $lists;
}

add_action('wp_before_admin_bar_render', 'wp_before_admin_bar_render_zreserv');
function wp_before_admin_bar_render_zreserv() {
global $wp_admin_bar, $wpdb, $exchangebox;
    if(current_user_can('administrator')){
		if(get_icon_indicators('zreserv')){
			$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."zapros_rezerv");
			if($count > 0){
				$wp_admin_bar->add_menu( array(
					'id'     => 'new_zrezerv',
					'href' => admin_url('admin.php?page=pn_zreserv'),
					'title'  => '<div style="height: 32px; width: 22px; background: url('. $exchangebox->plugin_url .'images/zreserv.png) no-repeat center center"></div>',
					'meta' => array( 
						'title' => sprintf(__('Reserve requests (%s)','pn'), $count) 
					)		
				));	
			}
		}
	}
}

add_action('after_update_valut_reserv','zreserv_update_valut_reserv', 10, 3);
function zreserv_update_valut_reserv($valut_id, $item, $valut_reserv){
global $wpdb;
	$valut_id = intval($valut_id);
	if(isset($item->id)){
		$reserv = is_sum($valut_reserv);
		$zapros = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."zapros_rezerv WHERE valid='$valut_id' AND summk <= $reserv");
		foreach($zapros as $za){	
			$zaid = $za->id;
			$wpdb->query("DELETE FROM ".$wpdb->prefix."zapros_rezerv WHERE id = '$zaid'");
								
			$user_email = is_email($za->email);			
			$valut = pn_strip_input($item->vname .' '. $item->vtype);
						
			if($user_email){	
					
				$notify_tags = array();
				$notify_tags['[sitename]'] = pn_strip_input(get_bloginfo('sitename'));
				$notify_tags['[summrez]'] = $reserv;
				$notify_tags['[sum]'] = pn_strip_input($za->summk);
				$notify_tags['[email]'] = $user_email;
				$notify_tags['[comment]'] = pn_strip_input($za->comment);
				$notify_tags['[valut]'] = $valut;
				$notify_tags = apply_filters('notify_tags_zreserv', $notify_tags, $za, $reserv);					
						
				$user_send_data = array(
					'user_email' => $user_email,
					'user_phone' => '',
				);	
				$result_mail = apply_filters('premium_send_message', 0, 'zreserv', $notify_tags, $user_send_data); 
						
			}				
		}
	}	
}

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'list');
$exchangebox->include_patch(__FILE__, 'window');