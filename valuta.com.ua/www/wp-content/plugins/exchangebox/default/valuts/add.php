<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_add_valuts', 'adminpage_title_pn_add_valuts');
function adminpage_title_pn_add_valuts(){
	$id = intval(is_param_get('item_id'));
	if($id){
		_e('Edit currency','pn');
	} else {
		_e('Add currency','pn');
	}
}

add_action('pn_adminpage_content_pn_add_valuts','def_adminpage_content_pn_add_valuts');
function def_adminpage_content_pn_add_valuts(){
global $wpdb, $exchangebox;

	$form = new PremiumForm();

	$id = intval(is_param_get('item_id'));
	$data_id = 0;
	$data = '';
	
	if($id){
		$data = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."valuts WHERE id='$id'");
		if(isset($data->id)){
			$data_id = $data->id;
		}	
	}

	if($data_id){
		$title = __('Edit currency','pn');
	} else {
		$title = __('Add currency','pn');
	}
	
	$vtypes = array();
	$vtypes[0] = __('No item','pn');
	$vtypes_datas = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."vtypes ORDER BY xname ASC");
	foreach($vtypes_datas as $vtype){
		$vtypes[pn_strip_input($vtype->xname)] = pn_strip_input($vtype->xname);
	}
	
	$quickpay = array();
	$quickpay[0] = '--'. __('No item','pn') .'--';
	$quickpay = apply_filters('quickpay_list', $quickpay);
	
	$merchants = array(); 
	$merchants[0] = '--'. __('No item','pn') .'--';
	$mers = apply_filters('list_merchants',array());
	foreach($mers as $mer){
		$merchants[is_isset($mer,'id')] = is_isset($mer,'title');
	}
	
	$back_menu = array();
	$back_menu['back'] = array(
		'link' => admin_url('admin.php?page=pn_valuts'),
		'title' => __('Back to list','pn')
	);
	if($data_id){
		$back_menu['add'] = array(
			'link' => admin_url('admin.php?page=pn_add_valuts'),
			'title' => __('Add new','pn')
		);	
	}
	$form->back_menu($back_menu, $data);

	$options = array();
	$options['hidden_block'] = array(
		'view' => 'hidden_input',
		'name' => 'data_id',
		'default' => $data_id,
	);	
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => $title,
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['vname'] = array(
		'view' => 'inputbig',
		'title' => __('PS title','pn'),
		'default' => is_isset($data, 'vname'),
		'name' => 'vname',
	);	
	$pn_icon_size = apply_filters('pn_icon_size','50 x 50');
	$options['vlogo'] = array(
		'view' => 'uploader',
		'title' => __('Logo','pn').' ('. $pn_icon_size .')',
		'default' => is_isset($data, 'vlogo'),
		'name' => 'vlogo',
	);	
	$options['vtype'] = array(
		'view' => 'select',
		'title' => __('Currency code','pn'),
		'options' => $vtypes,
		'default' => is_isset($data, 'vtype'),
		'name' => 'vtype',
	);	
	$options['line1'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['xname'] = array(
		'view' => 'input',
		'title' => __('Name for site','pn'),
		'default' => is_isset($data, 'xname'),
		'name' => 'xname',
	);	
	$options['xname_help'] = array(
		'view' => 'help',
		'title' => __('More info','pn'),
		'default' => sprintf(__('Allowed symbols: a-z, A-Z, 0-9_, min.: %1$s , max.: %2$s symbols','pn'), 3, 30),
	);
	$options['xml_value'] = array(
		'view' => 'input',
		'title' => __('Name for XML','pn'),
		'default' => is_isset($data, 'xml_value'),
		'name' => 'xml_value',
	);	
	$options['xml_value_help'] = array(
		'view' => 'help',
		'title' => __('More info','pn'),
		'default' => sprintf(__('Allowed symbols: a-z, A-Z, 0-9_, min.: %1$s , max.: %2$s symbols','pn'), 3, 30),
	);	
	$options['xml_value_warning'] = array(
		'view' => 'warning',
		'title' => __('More info','pn'),
		'default' => sprintf(__('Enter the name according to the standard: <a href="%s">Jsons.info</a>.','pn'), 'http://jsons.info/references/signatures/currencies'),
	);	
	$options['line2'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['xzt'] = array(
		'view' => 'select',
		'title' => __('Merchant','pn'),
		'options' => $merchants,
		'default' => is_isset($data, 'xzt'),
		'name' => 'xzt',
	);
	$options['rxzt'] = array(
		'view' => 'select',
		'title' => __('Quick payment','pn'),
		'options' => $quickpay,
		'default' => is_isset($data, 'rxzt'),
		'name' => 'rxzt',
	);		
	$options['line3'] = array(
		'view' => 'line',
		'colspan' => 2,
	);		
	$options['center_title'] = array(
		'view' => 'h3',
		'title' => '',
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['txt1'] = array(
		'view' => 'inputbig',
		'title' => __('Field title "From account"','pn'),
		'default' => is_isset($data, 'txt1'),
		'name' => 'txt1',
	);
	$options['show1'] = array(
		'view' => 'select',
		'title' => __('Show field "From account"?','pn'),
		'options' => array('1'=>__('Yes','pn'),'0'=>__('No','pn')),
		'default' => is_isset($data, 'show1'),
		'name' => 'show1',
	);	
	$options['txt2'] = array(
		'view' => 'inputbig',
		'title' => __('Field title "To account"','pn'),
		'default' => is_isset($data, 'txt2'),
		'name' => 'txt2',
	);
	$options['show2'] = array(
		'view' => 'select',
		'title' => __('Show filed "To account"?','pn'),
		'options' => array('1'=>__('Yes','pn'),'0'=>__('No','pn')),
		'default' => is_isset($data, 'show2'),
		'name' => 'show2',
	);	
	$options['line6'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['helps'] = array(
		'view' => 'textarea',
		'title' => __('Tip for field','pn'),
		'default' => is_isset($data, 'helps'),
		'name' => 'helps',
		'width' => '',
		'height' => '100px',
	);
	$options['line7'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['center_title2'] = array(
		'view' => 'h3',
		'title' => '',
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['numleng'] = array(
		'view' => 'input',
		'title' => __('Min. number of symbols','pn'),
		'default' => is_isset($data, 'numleng'),
		'name' => 'numleng',
	);
	$options['maxnumleng'] = array(
		'view' => 'input',
		'title' => __('Max. number of symbols','pn'),
		'default' => is_isset($data, 'maxnumleng'),
		'name' => 'maxnumleng',
	);
	$options['firstzn'] = array(
		'view' => 'input',
		'title' => __('First symbols','pn'),
		'default' => is_isset($data, 'firstzn'),
		'name' => 'firstzn',
	);
	$options['firstzn_help'] = array(
		'view' => 'help',
		'title' => __('More info','pn'),
		'default' => __('Checking the first symbols of the client to enter your account number. For example, for the purse WebMoney Z as the first symbol can be set Z.','pn'),
	);	
	if($exchangebox->get_option('partners','status') == 1){
		$options[] = array(
			'view' => 'line',
			'colspan' => 2,
		);		
		$options['pvivod'] = array(
			'view' => 'select',
			'title' => __('Allow paying affiliate remuneration','pn'),
			'options' => array('1'=>__('Yes','pn'),'0'=>__('No','pn')),
			'default' => is_isset($data, 'pvivod'),
			'name' => 'pvivod',
		);	
	}
	$options['vactive'] = array(
		'view' => 'select',
		'title' => __('Status','pn'),
		'options' => array('1'=>__('Active currency','pn'),'0'=>__('Not active currency','pn')),
		'default' => is_isset($data, 'vactive'),
		'name' => 'vactive',
	);	
	$options['valut_warning'] = array(
		'view' => 'warning',
		'title' => __('More info','pn'),
		'default' => sprintf(__('Caution! After addition of the currency it is necessary to set a <a href="%s">reserve</a>.','pn'), admin_url('admin.php?page=pn_reserv')),
	);	
	$params_form = array(
		'filter' => 'pn_currency_addform',
		'method' => 'post',
		'data' => $data,
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);					
} 

add_action('premium_action_pn_add_valuts','def_premium_action_pn_add_valuts');
function def_premium_action_pn_add_valuts(){
global $wpdb;	

	only_post();
	pn_only_caps(array('administrator'));
	
	$form = new PremiumForm();
	
	$data_id = intval(is_param_post('data_id'));
	$last_data = '';
	if($data_id > 0){
		$last_data = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "valuts WHERE id='$data_id'");
		if(!isset($last_data->id)){
			$data_id = 0;
		}
	}	
	
	$array = array();
	$array['rxzt'] = intval(is_param_post('rxzt'));
	$array['xzt'] = is_extension_name(is_param_post('xzt'));
	$array['xname'] = $site_value = is_site_value(is_param_post('xname'));	
	if(!$site_value){
		$form->error_form(__('Error! You have not entered the designation for the site!','pn'));
	}
			
	$cc = $wpdb->query("SELECT id FROM ". $wpdb->prefix ."valuts WHERE xname='$site_value' AND id != '$data_id'");
	if($cc > 0){
		$form->error_form(__('Error! currency with the designation for the site already exists!','pn'));
	}			

	$xml_value = is_xml_value(is_param_post('xml_value'));
	if(!$xml_value){
		$xml_value = pn_strip_symbols(replace_cyr($site_value), 0);
		$xml_value = unique_xml_value($xml_value, $data_id);
	}
			
	$array['xml_value'] = $xml_value;	
			
	$array['vactive'] = intval(is_param_post('vactive'));
			
	$array['vtype'] = '';
	$vtype = pn_strip_input(is_param_post('vtype'));
	if($vtype){
		$vtype_data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."vtypes WHERE xname='$vtype'");
		$array['vtype'] = $vtype_data->xname;
	} 
			
	$array['vname'] = pn_strip_input(is_param_post('vname'));
	$array['vlogo'] = esc_url(is_param_post('vlogo'));			
			
	$array['pvivod'] = intval(is_param_post('pvivod'));
			
	$array['helps'] = pn_strip_input(is_param_post('helps'));
	$array['show1'] = intval(is_param_post('show1'));
	$array['show2'] = intval(is_param_post('show2'));
	$array['txt1'] = pn_strip_input(is_param_post('txt1'));
	$array['txt2'] = pn_strip_input(is_param_post('txt2'));
			
	$array['numleng'] = intval(is_param_post('numleng'));
	$array['maxnumleng'] = intval(is_param_post('maxnumleng'));
	$array['firstzn'] = is_firstzn_value(is_param_post('firstzn'));

	$array = apply_filters('pn_valuts_addform_post',$array, $last_data);		
			
	if($data_id){		
		do_action('pn_valuts_edit_before', $data_id, $array, $last_data);
		if($array['vactive'] == 0){
			$wpdb->query("UPDATE ".$wpdb->prefix."napobmens SET status = '0' WHERE valsid1 = '$data_id' OR valsid2 = '$data_id'");
		}						
		$result = $wpdb->update($wpdb->prefix.'valuts', $array, array('id'=>$data_id));
		do_action('pn_valuts_edit', $data_id, $array, $last_data);
		if($result){
			do_action('pn_valuts_edit_after', $data_id, $array, $last_data);
		}				
	} else {		
		$wpdb->insert($wpdb->prefix.'valuts', $array);
		$data_id = $wpdb->insert_id;	
		do_action('pn_valuts_add', $data_id, $array);		
	}

	$url = admin_url('admin.php?page=pn_add_valuts&item_id='. $data_id .'&reply=true');
	$form->answer_form($url);
}	