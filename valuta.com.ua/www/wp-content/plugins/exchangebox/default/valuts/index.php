<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_valuts_delete','def_pn_valuts_delete',0,2);
function def_pn_valuts_delete($data_id, $item){
global $wpdb;
	
	$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts_meta WHERE item_id = '$data_id'");
	foreach($items as $item){
		$item_id = $item->id;
		do_action('pn_valutsmeta_delete_before', $id, $item);
		$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."valuts_meta WHERE id = '$item_id'");
		do_action('pn_valutsmeta_delete', $id, $item);
		if($result){
			do_action('pn_valutsmeta_delete_after', $id, $item);
		}
	}
}

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'add');
$exchangebox->include_patch(__FILE__, 'list');
$exchangebox->include_patch(__FILE__, 'sort');
$exchangebox->include_patch(__FILE__, 'sortres');