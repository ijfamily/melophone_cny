<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_vtypes', 'adminpage_title_pn_vtypes');
function adminpage_title_pn_vtypes(){
	_e('Currency codes','pn');
}

add_action('pn_adminpage_content_pn_vtypes','def_adminpage_content_pn_vtypes');
function def_adminpage_content_pn_vtypes(){

	if(class_exists('trev_vtypes_List_Table')){
		$Table = new trev_vtypes_List_Table();
		$Table->prepare_items();
		
		pn_admin_searchbox(array(), 'reply');
		
		pn_admin_submenu(array(), 'reply');
?>
	<style>
	.column-cid{ width: 80px!important; }
	</style>
	<form method="post" action="<?php pn_the_link_post(); ?>">
		<?php $Table->display() ?>
	</form>	
<?php 
	} else {
		echo 'Class not found';
	}
}

add_action('premium_action_pn_vtypes','def_premium_action_pn_vtypes');
function def_premium_action_pn_vtypes(){
global $wpdb;
	
	only_post();
	pn_only_caps(array('administrator'));
	
	$reply = '';
	$action = get_admin_action();
	
	if(isset($_POST['save'])){
				
		if(isset($_POST['vncurs']) and is_array($_POST['vncurs'])){
			foreach($_POST['vncurs'] as $id => $internal_rate){
				$id = intval($id);
				
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vtypes WHERE id='$id'");
				if(isset($item->id)){
					$internal_rate = is_sum($internal_rate);
					if($internal_rate <= 0){ $internal_rate = 1; }
							
					$arr = array();				
					if($internal_rate != $item->vncurs){
						$arr['vncurs'] = $internal_rate;
					}
					if(count($arr) > 0){
						$wpdb->update($wpdb->prefix . 'vtypes', $arr, array('id'=>$id));
					}					
				}
			}
		}			
				
		do_action('pn_vtypes_save');
		$reply = '&reply=true';
		
	} else {
		if(isset($_POST['id']) and is_array($_POST['id'])){				
			if($action == 'delete'){
				foreach($_POST['id'] as $id){
					$id = intval($id);		
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vtypes WHERE id='$id'");
					if(isset($item->id)){
						do_action('pn_vtypes_delete_before', $id, $item);
						$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."vtypes WHERE id = '$id'");
						do_action('pn_vtypes_delete', $id, $item);
						if($result){
							do_action('pn_vtypes_delete_after', $id, $item);
						}						
					}
				}		
			}
			
			do_action('pn_vtypes_action', $action, $_POST['id']);
			$reply = '&reply=true';
		} 
	}
					
	$url = is_param_post('_wp_http_referer') . $reply;
	$paged = intval(is_param_post('paged'));
	if($paged > 1){ $url .= '&paged='.$paged; }	
	wp_redirect($url);
	exit;			
} 

class trev_vtypes_List_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;
                
        parent::__construct( array(
            'singular'  => 'id',      
			'ajax' => false,  
        ) );
        
    }
	
    function column_default($item, $column_name){
        
		if($column_name == 'cid'){
			return $item->id;
		} elseif($column_name == 'od'){	
		    return '<input type="text" style="width: 100px;" name="vncurs['. $item->id .']" value="'. is_sum($item->vncurs) .'" />';			
		} 
		return apply_filters('vtypes_manage_ap_col', '', $column_name,$item);
    }	
	
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'], 
            $item->id                
        );
    }	

    function column_title($item){

        $actions = array(
            'edit'      => '<a href="'. admin_url('admin.php?page=pn_add_vtypes&item_id='. $item->id) .'">'. __('Edit','pn') .'</a>',
        );
   		$primary = apply_filters('vtypes_manage_ap_primary', is_site_value($item->xname), $item);
		$actions = apply_filters('vtypes_manage_ap_actions', $actions, $item);        
        return sprintf('%1$s %2$s',
            $primary,
            $this->row_actions($actions)
        );       
		
    }	
	
    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
			'cid' => __('ID','pn'),
			'title'     => __('Currency code','pn'),
			'od'    => __('Internal rate for','pn'). ' 1 '. cur_type() .'',
        );
		$columns = apply_filters('vtypes_manage_ap_columns', $columns);
        return $columns;
    }	
	

    function get_bulk_actions() {
        $actions = array(
            'delete'    => __('Delete','pn'),
        );
        return $actions;
    }
	
    function get_sortable_columns() {
        $sortable_columns = array( 
			'cid'     => array('cid',false),
            'title'     => array('title',false),
			'od'     => array('od',false),
        );
        return $sortable_columns;
    }	
    
    function prepare_items() {
        global $wpdb; 
		
        $per_page = $this->get_items_per_page('trev_vtypes_per_page', 20);
        $current_page = $this->get_pagenum();
        
        $this->_column_headers = $this->get_column_info();

		$offset = ($current_page-1)*$per_page;
		$oby = is_param_get('orderby');
		if($oby == 'title'){
		    $orderby = 'xname';
		} elseif($oby == 'od'){
			$orderby = '(vncurs -0.0)';			
		} else {
		    $orderby = 'id';
		}
		$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';		
		if($order != 'asc'){ $order = 'desc'; }			

		$where = '';
		$where = pn_admin_search_where($where);
		$total_items = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."vtypes WHERE id > 0 $where");
		$data = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."vtypes WHERE id > 0 $where ORDER BY $orderby $order LIMIT $offset , $per_page");  		

        $current_page = $this->get_pagenum();
        $this->items = $data;
		
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  
            'per_page'    => $per_page,                     
            'total_pages' => ceil($total_items/$per_page)  
        ));
    }
	
	function extra_tablenav( $which ) {
    ?>
		<div class="alignleft actions">
			<input type="submit" name="save" class="button" value="<?php _e('Save','pn'); ?>">
            <a href="<?php echo admin_url('admin.php?page=pn_add_vtypes');?>" class="button"><?php _e('Add new','pn'); ?></a>
		</div>
		<?php
	}	  
	
}

add_action('premium_screen_pn_vtypes','my_myscreen_pn_vtypes');
function my_myscreen_pn_vtypes() {
    $args = array(
        'label' => __('Display','pn'),
        'default' => 20,
        'option' => 'trev_vtypes_per_page'
    );
    add_screen_option('per_page', $args );
	if(class_exists('trev_vtypes_List_Table')){
		new trev_vtypes_List_Table;
	}
}