<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_add_vtypes', 'adminpage_title_pn_add_vtypes');
function adminpage_title_pn_add_vtypes(){
	$id = intval(is_param_get('item_id'));
	if($id){
		_e('Edit currency code','pn');
	} else {
		_e('Add currency code','pn');
	}
}

add_action('pn_adminpage_content_pn_add_vtypes','def_adminpage_content_pn_add_vtypes');
function def_adminpage_content_pn_add_vtypes(){
global $wpdb;

	$form = new PremiumForm();

	$id = intval(is_param_get('item_id'));
	$data_id = 0;
	$data = '';
	
	if($id){
		$data = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."vtypes WHERE id='$id'");
		if(isset($data->id)){
			$data_id = $data->id;
		}	
	}

	if($data_id){
		$title = __('Edit currency code','pn');
	} else {
		$title = __('Add currency code','pn');
	}	
	
	$back_menu = array();
	$back_menu['back'] = array(
		'link' => admin_url('admin.php?page=pn_vtypes'),
		'title' => __('Back to list','pn')
	);
	if($data_id){
		$back_menu['add'] = array(
			'link' => admin_url('admin.php?page=pn_add_vtypes'),
			'title' => __('Add new','pn')
		);	
	}
	$form->back_menu($back_menu, $data);

	$options = array();
	$options['hidden_block'] = array(
		'view' => 'hidden_input',
		'name' => 'data_id',
		'default' => $data_id,
	);	
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => $title,
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['xname'] = array(
		'view' => 'input',
		'title' => __('Currency code','pn'),
		'default' => is_isset($data, 'xname'),
		'name' => 'xname',
	);		
	$options['vncurs'] = array(
		'view' => 'input',
		'title' => __('Internal rate for','pn'). ' 1 '. cur_type() .'',
		'default' => is_isset($data, 'vncurs'),
		'name' => 'vncurs',
	);	
	$params_form = array(
		'filter' => 'pn_vtypes_addform',
		'method' => 'post',
		'data' => $data,
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);
}

add_action('premium_action_pn_add_vtypes','def_premium_action_pn_add_vtypes');
function def_premium_action_pn_add_vtypes(){
global $wpdb;

	only_post();
	pn_only_caps(array('administrator'));

	$form = new PremiumForm();
	
	$data_id = intval(is_param_post('data_id'));
	$last_data = '';
	if($data_id > 0){
		$last_data = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "vtypes WHERE id='$data_id'");
		if(!isset($last_data->id)){
			$data_id = 0;
		}
	}		
		
	$array = array();
	$array['xname'] = $vtype_title = is_site_value(is_param_post('xname'));
				
	if(!$vtype_title){ $form->error_form(__('Error! You did not enter the name','pn')); }

	$array['vncurs'] = is_sum(is_param_post('vncurs'));
	if($array['vncurs'] <= 0){ $array['vncurs'] = 1; }
		
	$array = apply_filters('pn_vtypes_addform_post',$array, $last_data);
				
	$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."vtypes WHERE xname='$vtype_title' AND id != '$data_id'");
	if($cc > 0){
		$form->error_form(__('Error! this type of exchange already exists!','pn'));
	}
				
	if($data_id){		
		do_action('pn_vtypes_edit_before', $data_id, $array, $last_data);
		$result = $wpdb->update($wpdb->prefix.'vtypes', $array, array('id'=>$data_id));
		do_action('pn_vtypes_edit', $data_id, $array, $last_data);
		if($result){
			do_action('pn_vtypes_edit_after', $data_id, $array, $last_data);
		}			
	} else {
		$wpdb->insert($wpdb->prefix.'vtypes', $array);
		$data_id = $wpdb->insert_id;	
		do_action('pn_vtypes_add', $data_id, $array);
	}

	$url = admin_url('admin.php?page=pn_add_vtypes&item_id='. $data_id .'&reply=true');
	$form->answer_form($url);
}	