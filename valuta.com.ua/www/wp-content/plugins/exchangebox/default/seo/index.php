<?php
if( !defined( 'ABSPATH')){ exit(); }
	
$path = get_extension_file(__FILE__);
$name = get_extension_name($path);
	
global $exchangebox;
$exchangebox->file_include($path.'/meta');
$exchangebox->file_include($path.'/seo');