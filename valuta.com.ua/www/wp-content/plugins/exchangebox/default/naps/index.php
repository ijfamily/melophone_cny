<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_naps_delete', 'def_pn_naps_delete');
function def_pn_naps_delete($id){
global $wpdb;

	$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."naps_meta WHERE item_id = '$id'");
	foreach($items as $item){
		$item_id = $item->id;
		do_action('pn_napsmeta_delete_before', $item_id, $item);
		$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."naps_meta WHERE id = '$item_id'");
		do_action('pn_napsmeta_delete', $item_id, $item);
		if($result){
			do_action('pn_napsmeta_delete', $item_id, $item);
		}
	}	
	
	delete_naps_txtmeta($id);
}

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'add');
$exchangebox->include_patch(__FILE__, 'list');
$exchangebox->include_patch(__FILE__, 'sort');
$exchangebox->include_patch(__FILE__, 'temps');