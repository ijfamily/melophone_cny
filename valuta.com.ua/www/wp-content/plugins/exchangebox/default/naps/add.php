<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_add_naps', 'adminpage_title_pn_add_naps');
function adminpage_title_pn_add_naps(){
	$id = intval(is_param_get('item_id'));
	if($id){
		_e('Edit direction exchange','pn');
	} else {
		_e('Add direction exchange','pn');
	}
}


add_action('pn_adminpage_content_pn_add_naps','def_adminpage_content_pn_add_naps');
function def_adminpage_content_pn_add_naps(){
global $wpdb, $exchangebox;

	$form = new PremiumForm();

	$id = intval(is_param_get('item_id'));
	$data_id = 0;
	$data = '';
	
	if($id){
		$data = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE id='$id'");
		if(isset($data->id)){
			$data_id = $data->id;
		}	
	}

	if($data_id){
		$title = __('Edit direction exchange','pn');
	} else {
		$title = __('Add direction exchange','pn');
	}
	
	$valuts = array();
	$valuts[0] = __('No item','pn');
	$vals = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts ORDER BY site_order ASC");
	foreach($vals as $item){
		$valuts[$item->id] = pn_strip_input($item->vname .' '. $item->vtype);
	}	
	
	$back_menu = array();
	$back_menu['back'] = array(
		'link' => admin_url('admin.php?page=pn_naps'),
		'title' => __('Back to list','pn')
	);
	if($data_id){
		$back_menu['add'] = array(
			'link' => admin_url('admin.php?page=pn_add_naps'),
			'title' => __('Add new','pn')
		);	
	}
	$form->back_menu($back_menu, $data);	
?>	

<div class="premium_body">	
	<form method="post" action="<?php pn_the_link_post(); ?>">
		<input type="hidden" name="data_id" value="<?php echo $data_id; ?>" />
		
			<table class="premium_standart_table">
				<tr>
					<td colspan="3">
						<div class="premium_h3"><?php echo $title; ?></div>
						<div class="premium_h3submit">
							<input type="submit" name="" class="button" value="<?php _e('Save'); ?>" />
						</div>
					</td>
				</tr>	
					
				<tr>
					<th width="150"></th>
					<td><?php _e('Giving','pn'); ?></td>
					<td><?php _e('Receive','pn'); ?></td>
				</tr>			
				<tr>
					<th><?php _e('Direction','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<select name="valsid1" id="valsid1" autocomplete="off">
								<?php foreach($valuts as $key => $val){ ?>
									<option value="<?php echo $key;?>" <?php selected(is_isset($data, 'valsid1'),$key,true); ?>><?php echo $val;?></option>
								<?php } ?>
							</select>
						</div>
					</td>
					<td>
						<div class="premium_wrap_standart">
							<select name="valsid2" id="valsid2" autocomplete="off">
								<?php foreach($valuts as $key => $val){ ?>
									<option value="<?php echo $key;?>" <?php selected(is_isset($data, 'valsid2'),$key,true); ?>><?php echo $val;?></option>
								<?php } ?>
							</select>
						</div>			
					</td>
				</tr>
				
				<tr>
					<th><?php _e('Status','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<select name="status" autocomplete="off">
								<?php 
								$status = pn_strip_input(is_isset($data, 'status')); 
								if(!is_numeric($status)){ $status = 1; }
								?>
											
								<option value="1" <?php selected($status,1); ?>><?php _e('Active direction','pn');?></option>
								<option value="0" <?php selected($status,0); ?>><?php _e('Not active direction','pn');?></option>
											
							</select>
						</div>
					</td>
					<td></td>
				</tr>				
				
				<tr>
					<th><?php _e('Exchange rate','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="curs1" style="width: 200px;" value="<?php echo is_sum(is_isset($data, 'curs1')); ?>" />
						</div>			
					</td>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="curs2" style="width: 200px;" value="<?php echo is_sum(is_isset($data, 'curs2')); ?>" />	
						</div>			
					</td>
				</tr>
				
				<?php do_action('exb_napobmens_add_rate', $data); ?>
				
				<tr>
					<th width="150"></th>
					<td><?php _e('User &rarr; Exchanger','pn'); ?></td>
					<td><?php _e('Exchanger &rarr; User','pn'); ?></td>
				</tr>						
				<tr>
					<th><?php _e('Commission','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<div><input type="text" name="prsum1" style="width: 100px;" value="<?php echo is_sum(is_isset($data, 'prsum1')); ?>" /> S</div>
							<div><input type="text" name="prproc1" style="width: 100px;" value="<?php echo is_sum(is_isset($data, 'prproc1')); ?>" /> %</div>
						</div>			
					</td>
					<td>
						<div class="premium_wrap_standart">
							<div><input type="text" name="prsum2" style="width: 100px;" value="<?php echo is_sum(is_isset($data, 'prsum2')); ?>" /> S</div>
							<div><input type="text" name="prproc2" style="width: 100px;" value="<?php echo is_sum(is_isset($data, 'prproc2')); ?>" /> %</div>	
						</div>	
					</td>
				</tr>

				<tr>  
					<th><?php _e('Min. amount of commission','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="minsumm1com" value="<?php echo is_sum(is_isset($data, 'minsumm1com')); ?>" />		
						</div>
					</td>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="minsumm2com" value="<?php echo is_sum(is_isset($data, 'minsumm2com')); ?>" />				
						</div>
					</td>
				</tr>		
				<tr>
					<th><?php _e('Max. amount of commission','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="maxsumm1com" value="<?php echo is_sum(is_isset($data, 'maxsumm1com')); ?>" />		
						</div>
					</td>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="maxsumm2com" value="<?php echo is_sum(is_isset($data, 'maxsumm2com')); ?>" />				
						</div>
					</td>
				</tr>

				<tr>
					<th><?php _e('Minimum amount','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="minsumm1" style="width: 200px;" value="<?php echo is_sum(is_isset($data, 'minsumm1')); ?>" />
						</div>			
					</td>
					<td><!--
						<div class="premium_wrap_standart">
							<input type="text" name="minsumm2" style="width: 200px;" value="<?php echo is_sum(is_isset($data, 'minsumm2')); ?>" />	
						</div>
						-->
					</td>
				</tr>
				<tr>
					<th><?php _e('Maximum amount','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="maxsumm1" style="width: 200px;" value="<?php echo is_sum(is_isset($data, 'maxsumm1')); ?>" />
						</div>			
					</td>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="maxsumm2" style="width: 200px;" value="<?php echo is_sum(is_isset($data, 'maxsumm2')); ?>" />	
						</div>			
					</td>
				</tr>

				<?php
					$com_box_summ1 = is_sum(is_isset($data, 'combox'));
					$com_box_pers1 = is_sum(is_isset($data, 'comboxpers'));
					$cdis1 = '';
					$cdis2 = '';
					if($com_box_summ1 > 0){
						$cdis2 = 'disabled="disabled"';
						$com_box_pers1 = 0;
					} elseif($com_box_pers1 > 0) {
						$cdis1 = 'disabled="disabled"';
						$com_box_summ1 = 0;
					}
				?>						
						
				<tr>
					<th><?php _e('Additional commission from the recipient','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="combox" id="com_box_summ1" <?php echo $cdis1; ?> value="<?php echo $com_box_summ1; ?>" /> S		
						</div>
					</td>
					<td></td>
				</tr>
				<tr>
					<th></th>
					<td>
						<div class="premium_wrap_standart">
							<input type="text" name="comboxpers" id="com_box_pers1" <?php echo $cdis2; ?> value="<?php echo $com_box_pers1; ?>" /> %		
						</div>
					</td>
					<td></td>
				</tr>

				<tr>
					<th><?php _e('Member discount','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<?php 
							$delsk = pn_strip_input(is_isset($data, 'delsk')); 
							?>									
									
							<select name="delsk" autocomplete="off">
								<option value="0" <?php selected($delsk,0); ?>><?php _e('Yes','pn'); ?></option>
								<option value="1" <?php selected($delsk,1); ?>><?php _e('No','pn'); ?></option>
							</select>
						</div>
					</td>
					<td>			
					</td>
				</tr>	

				<tr>
					<th><?php _e('Privacy areas for guests','pn'); ?></th>
					<td>
						<div class="premium_wrap_standart">
							<?php 
							$hidegost = pn_strip_input(is_isset($data, 'hidegost')); 
							?>									
									
							<select name="hidegost" autocomplete="off"> 
								<option value="0" <?php selected($hidegost,0); ?>><?php _e('not to hide','pn'); ?></option>
								<option value="2" <?php selected($hidegost,2); ?>><?php _e('not to hide, but to ban','pn'); ?></option>
								<option value="1" <?php selected($hidegost,1); ?>><?php _e('hide','pn'); ?></option>
							</select>
						</div>
					</td>
					<td>			
					</td>
				</tr>
				
				<?php
				$uf = @unserialize($data->uf);
				if(!is_array($uf)){ $uf = array(); }
				
				$fields = array(
					'last_name' => __('Last name','pn'),
					'first_name' => __('First name','pn'),
					'second_name' => __('Second name','pn'),
					'user_phone' => __('Phone','pn'),
					'user_passport' => __('Passport','pn'),
					'user_skype' => __('Skype','pn')
				);			
				?>
				<tr>
					<th><?php _e('Personal data','pn'); ?></th>
					<td colspan="2">
						<div class="premium_wrap_standart">	
							<div class="cf_div">
								<div style="font-weight: 500;"><label><input type="checkbox" class="check_all" name="" value="1" /> <?php _e('Check all/Uncheck all','pn'); ?></label></div>	
								<?php foreach($fields as $field_name => $field_title){ 
									$ch = '';
									if(is_isset($uf,$field_name) == 1){
										$ch = 'checked="checked"';
									}
								?>
									<div><label><input type="checkbox" name="uf[<?php echo $field_name; ?>]" <?php echo $ch; ?> value="1" /> <?php echo $field_title; ?></label></div>
								<?php } ?>
							</div>
						</div>
					</td>
					<td>			
					</td>
				</tr>	
				
				<?php do_action('exb_napobmens_add', $data); ?>				
				
				<?php do_action('exb_napobmens_add_other', $data); ?>
				
				<?php if($exchangebox->get_option('partners','status') == 1){ ?>
				<tr>
					<td colspan="3">
						<div class="premium_h3"><?php _e('Partners settings','pn'); ?></div>
						<div class="premium_h3submit">
							<input type="submit" name="" class="button" value="<?php _e('Save', 'pn'); ?>" />
						</div>
					</td>
				</tr>				
				
				<tr>
					<th><?php _e('The maximum percentage affiliate','pn'); ?></th>
					<td colspan="2">
						<div class="premium_wrap_standart">
							<input type="text" name="partmax" style="width: 100px;" value="<?php echo pn_strip_input(is_isset($data, 'partmax')); ?>" />%
						</div>
					</td>
					<td>			
					</td>
				</tr>

				<tr>
					<th><?php _e('Affiliate payments','pn'); ?></th>
					<td colspan="2">
						<div class="premium_wrap_standart">								
							<select name="parthide" autocomplete="off"> 
								<option value="0" <?php selected(is_isset($data, 'parthide'),0); ?>><?php _e('pay','pn'); ?></option>
								<option value="1" <?php selected(is_isset($data, 'parthide'),1); ?>><?php _e('not pay','pn'); ?></option>
							</select>
						</div>
					</td>
					<td>			
					</td>
				</tr>				
				<?php } ?>		

				<tr>
					<td colspan="3">
						<div class="premium_h3"></div>
						<div class="premium_h3submit">
							<input type="submit" name="" class="button" value="<?php _e('Save', 'pn'); ?>" />
						</div>
					</td>
				</tr>				
				
				<!--
				<tr>
					<th></th>
					<td colspan="2"><div class="premium_line"></div></td>
				</tr>
				-->				
				
				<tr>
					<td colspan="3">
						<div class="premium_h3"><?php _e('SEO settings','pn'); ?></div>
						<div class="premium_h3submit">
							<!--<input type="submit" name="" class="button" value="<?php _e('Save', 'pn'); ?>" />-->
						</div>
					</td>
				</tr>	
				
				<?php  
					$form = new PremiumForm();
				
					$atts_input = array();
					$atts_input['class'] = 'big_input';
				
					$data_id = is_isset($data, 'id');
					$seo_title = pn_strip_input(get_naps_meta($data_id, 'seo_title')); 
					$seo_key = pn_strip_input(get_naps_meta($data_id, 'seo_key'));
					$seo_descr = pn_strip_input(get_naps_meta($data_id, 'seo_descr'));
				?>		
				<tr>
					<th>SEO</th>
					<td colspan="2">
						<strong>Title:</strong><br />
						<?php $form->input('seo_title' , $seo_title, $atts_input, 0); ?><br />
							
						<strong>Description:</strong><br />
						<?php $form->textarea('seo_descr', $seo_descr, '', '100px', array(), 0); ?><br />
											
						<strong>Keywords:</strong><br />
						<?php $form->textarea('seo_key', $seo_key, '', '50px', array(), 0); ?>
					</td>
				</tr>
			
				<tr>
					<td colspan="3">
						<div class="premium_h3"><?php _e('Information for users','pn'); ?></div>
						<div class="premium_h3submit">
							<input type="submit" name="" class="button" value="<?php _e('Save', 'pn'); ?>" />
						</div>
					</td>
				</tr>

				<?php
				$data_id = is_isset($data, 'id');
				$list_naps_temp = apply_filters('list_naps_temp',array());
				if(is_array($list_naps_temp)){
					foreach($list_naps_temp as $key => $title){
									
						$text = get_naps_txtmeta($data_id, $key);								
						if(!$text){ 
							$text = $exchangebox->get_option('naps_temp',$key); 
						} 
				?>
						<tr>
							<th><?php echo $title; ?></th>
							<td colspan="2">
								<?php $form->editor($key, $text, 8, false, 0); ?>
							</td>
						</tr>							
				<?php 
					}
				} ?>
					
				<?php do_action('exb_napobmens_instruction_add', $data); ?>	
				
				<tr>
					<td colspan="3">
						<div class="premium_h3"></div>
						<div class="premium_h3submit">
							<input type="submit" name="" class="button" value="<?php _e('Save'); ?>" />
						</div>
					</td>
				</tr>			
			</table>
		
	</form>		
</div>	
<script type="text/javascript">
$(function(){
	
	function combox_func(value1,value2){
		var com_box_summ1 = parseFloat($.trim(value1.val()));
		var com_box_pers1 = parseFloat($.trim(value2.val()));
		value1.prop('disabled', false);
		value2.prop('disabled', false);
		
		if(com_box_summ1 > 0){
			value2.prop('disabled', true).val('0');
		} else {
			if(com_box_pers1 > 0){
				value1.prop('disabled', true).val('0');
			} 
		}	
	}	
	
	$('#com_box_summ1, #com_box_pers1').on('keyup', function(){
		combox_func($('#com_box_summ1'), $('#com_box_pers1'));
	});
	$('#com_box_summ1, #com_box_pers1').on('change', function(){
		combox_func($('#com_box_summ1'), $('#com_box_pers1'));
	});

	$(document).on('click', '.check_all', function(){
		var par = $(this).parents('.cf_div');
		if($(this).prop('checked')){
			par.find('input').prop('checked',true);
		} else {
			par.find('input').prop('checked',false);
		}
	});	
	
});
</script>	
<?php
}  

add_action('premium_action_pn_add_naps','def_premium_action_pn_add_naps');
function def_premium_action_pn_add_naps(){
global $wpdb, $exchangebox;	

	only_post();
	pn_only_caps(array('administrator'));	

	$data_id = intval(is_param_post('data_id'));
	$old_data = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE id='$data_id'");
	if(!isset($old_data->id)){
		$data_id = 0;
	}	
	
	$array = array();
			
	$array['valsid1'] = $valut_id1 = intval(is_param_post('valsid1'));
	$array['valsid2'] = $valut_id2 = intval(is_param_post('valsid2'));	 
					
	if($valut_id1 == $valut_id2){
		pn_display_mess(__('Error! give may not be getting the same!','pn'));
	}
			
	$valut_data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut_id1'");
	if(!isset($valut_data->id)){
		pn_display_mess(__('Error! You give currency does not exist!','pn'));
	}
			
	$valut_data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut_id2'");
	if(!isset($valut_data->id)){
		pn_display_mess(__('Error! You get currency does not exist!','pn'));
	}
	
	$array['curs1'] = $curs1 = is_sum(is_param_post('curs1'));
	$array['curs2'] = $curs2 = is_sum(is_param_post('curs2'));
			
	if($curs1 == 0 or $curs2 == 0){
		pn_display_mess(__('Error! The course can not be 0!','pn'));
	}			
					
	$array['prsum1'] = is_sum(is_param_post('prsum1'));	
	$array['prproc1'] = is_sum(is_param_post('prproc1'));
	$array['prsum2'] = is_sum(is_param_post('prsum2'));	
	$array['prproc2'] = is_sum(is_param_post('prproc2'));
	$array['maxsumm1com'] = is_sum(is_param_post('maxsumm1com'));
	$array['maxsumm2com'] = is_sum(is_param_post('maxsumm2com'));
	$array['minsumm1com'] = is_sum(is_param_post('minsumm1com'));
	$array['minsumm2com'] = is_sum(is_param_post('minsumm2com'));					

	$array['minsumm1'] = is_sum(is_param_post('minsumm1'));
	$array['maxsumm1'] = is_sum(is_param_post('maxsumm1'));
	$array['minsumm2'] = is_sum(is_param_post('minsumm2'));
	$array['maxsumm2'] = is_sum(is_param_post('maxsumm2'));						

	$array['status'] = intval(is_param_post('status'));			
			
	$array['combox'] = is_sum(is_param_post('combox'));	
	$array['comboxpers'] = is_sum(is_param_post('comboxpers'));	
	
	$array['delsk'] = intval(is_param_post('delsk'));
	$array['hidegost'] = intval(is_param_post('hidegost'));
			
	$array['partmax'] = is_sum(is_param_post('partmax'),2);
	$array['parthide'] = intval(is_param_post('parthide'));
	
	$uf = is_param_post('uf');
	$array['uf'] = @serialize($uf);
			
	$array = apply_filters('pn_naps_addform_post', $array, $old_data);
			
	if($data_id){
		do_action('pn_naps_edit_before', $data_id, $array, $old_data);		
		$result = $wpdb->update($wpdb->prefix.'napobmens', $array, array('id'=>$data_id));
		do_action('pn_naps_edit', $data_id, $array, $old_data);		
	} else {	
		$wpdb->insert($wpdb->prefix.'napobmens', $array);
		$data_id = $wpdb->insert_id;	
		do_action('pn_naps_add', $data_id, $array);	
	}
				
	if($data_id){
		$seo_title = pn_strip_input(is_param_post('seo_title'));
		$seo_key = pn_strip_input(is_param_post('seo_key'));
		$seo_descr = pn_strip_input(is_param_post('seo_descr'));
		update_naps_meta($data_id, 'seo_title', $seo_title);
		update_naps_meta($data_id, 'seo_key', $seo_key);
		update_naps_meta($data_id, 'seo_descr', $seo_descr);
						
		$list_naps_temp = apply_filters('list_naps_temp',array());
		if(is_array($list_naps_temp)){
			foreach($list_naps_temp as $key => $title){						
				$value = pn_strip_text(is_param_post($key));
				$otv = update_naps_txtmeta($data_id, $key, $value);
				if($otv != 1){
					pn_display_mess(sprintf(__('Error! folder <b>%s</b> is missing or can not be written!','pn'),'/wp-content/uploads/napsmeta/'));
				}									
			}
		}			
	}

	$url = admin_url('admin.php?page=pn_add_naps&item_id='. $data_id .'&reply=true');
	wp_redirect($url);
	exit;
}	