<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_naps', 'adminpage_title_pn_naps');
function adminpage_title_pn_naps(){
	_e('Directions exchanges','pn');
}

add_action('pn_adminpage_content_pn_naps','def_adminpage_content_pn_naps');
function def_adminpage_content_pn_naps(){

	if(class_exists('trev_naps_List_Table')){
		$Table = new trev_naps_List_Table();
		$Table->prepare_items();
		
		pn_admin_searchbox(array(), 'reply');

		$options = array();
		$options['mod'] = array(
			'name' => 'mod',
			'options' => array(
				'1' => __('active direction','pn'),
				'2' => __('inactive direction','pn'),
			),
			'title' => '',
		);	
		pn_admin_submenu($options, 'reply');		
?>
<style>
.column-cid{ width: 20px!important; }
</style>
	<form method="post" action="<?php pn_the_link_post(); ?>">
		<?php $Table->display() ?>
	</form>
<?php 
	} else {
		echo 'Class not found';
	}
}

add_action('premium_action_pn_naps','def_premium_action_pn_naps');
function def_premium_action_pn_naps(){
global $wpdb;

	only_post();
	pn_only_caps(array('administrator'));

	$reply = '';
	$action = get_admin_action();
	
	if(isset($_POST['filter'])){
			
		$ref = is_param_post('_wp_http_referer');
		$url = pn_admin_filter_data($ref, 'reply, val1, val2');			
			
		$val1 = intval(is_param_post('val1'));
		if($val1){
			$url .= '&val1='.$val1;
		}
				
		$val2 = intval(is_param_post('val2'));
		if($val2){
			$url .= '&val2='.$val2;
		}				
				
		wp_redirect($url);
		exit;
				
	} elseif(isset($_POST['back_filter'])){	
				
		$ref = is_param_post('_wp_http_referer');
		$url = pn_admin_filter_data($ref, 'reply, val1, val2');		
			
		$val1 = intval(is_param_post('val1'));
		if($val1){
			$url .= '&val2='.$val1;
		}
				
		$val2 = intval(is_param_post('val2'));
		if($val2){
			$url .= '&val1='.$val2;
		}				
				
		wp_redirect($url);
		exit;				
			
	} elseif(isset($_POST['save'])){
				
		if(isset($_POST['curs1']) and is_array($_POST['curs1']) and isset($_POST['curs2']) and is_array($_POST['curs2'])){
			foreach($_POST['curs1'] as $id => $curs1){
				$id = intval($id);
							
				$arr = array();
				$arr['minsumm1'] = is_sum($_POST['minsumm1'][$id]);
				$arr['curs1'] = is_sum($curs1);
				$arr['curs2'] = is_sum($_POST['curs2'][$id]);
				$wpdb->update($wpdb->prefix.'napobmens', $arr, array('id'=>$id));	
			}
		}	
		
		do_action('pn_naps_save');
		$reply = '&reply=true';

	} else {
				
		if(isset($_POST['id']) and is_array($_POST['id'])){				
				
			if($action == 'active'){		
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE id='$id' AND status != '1'");
					if(isset($item->id)){
						do_action('pn_naps_active_before', $id, $item);
						$wpdb->query("UPDATE ".$wpdb->prefix."napobmens SET status = '1' WHERE id = '$id'");
						do_action('pn_naps_active', $id, $item);
						if($result){
							do_action('pn_naps_active_after', $id, $item);
						}						
					}		
				}				
			}

			if($action == 'notactive'){	
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE id='$id' AND status != '0'");
					if(isset($item->id)){	
						do_action('pn_naps_notactive_before', $id, $item);
						$result = $wpdb->query("UPDATE ".$wpdb->prefix."napobmens SET status = '0' WHERE id = '$id'");
						do_action('pn_naps_notactive', $id, $item);
						if($result){
							do_action('pn_naps_notactive_after', $id, $item);
						}						
					}		
				}				
			}

			if($action == 'notcorr'){		
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE id='$id' AND parser != '0'");
					if(isset($item->id)){
						do_action('pn_naps_notcorr_before', $id, $item);
						$result = $wpdb->query("UPDATE ".$wpdb->prefix."napobmens SET parser = '0' WHERE id = '$id'");
						do_action('pn_naps_notcorr', $id, $item);
						if($result){
							do_action('pn_naps_notcorr_after', $id, $item);
						}						
					}		
				}				
			}					
				
			if($action == 'delete'){		
				foreach($_POST['id'] as $id){
					$id = intval($id);
					$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE id='$id'");
					if(isset($item->id)){							
						do_action('pn_naps_delete_before', $id, $item);
						$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."napobmens WHERE id = '$id'");
						do_action('pn_naps_delete', $id, $item);
						if($result){
							do_action('pn_naps_delete_after', $id, $item);
						}						
					}
				}			
			}
			
			do_action('pn_naps_action', $action, $_POST['id']);
			$reply = '&reply=true';
					
		} 	
	}
			
	$url = is_param_post('_wp_http_referer') . $reply;
	$paged = intval(is_param_post('paged'));
	if($paged > 1){ $url .= '&paged='.$paged; }	
	wp_redirect($url);
	exit;					
} 

class trev_naps_List_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;
                
        parent::__construct( array(
            'singular'  => 'id',      
			'ajax' => false,  
        ) );
        
    }
	
    function column_default($item, $column_name){
        
		if($column_name == 'course1'){		
		    return '<input type="text" style="width: 100%; max-width: 80px;" name="curs1['. $item->id .']" value="'. is_sum($item->curs1) .'" />';
		} elseif($column_name == 'course2'){		
		    return '<input type="text" style="width: 100%; max-width: 80px;" name="curs2['. $item->id .']" value="'. is_sum($item->curs2) .'" />';			
		} elseif($column_name == 'ccomis'){
			
			$com1 = $com2 = array();
			$com1[] = '<strong>'. is_sum($item->prsum1) . '</strong>S';
			$com1[] = '<strong>'. is_sum($item->prproc1) . '</strong>%';
			$com2[] = '<strong>'. is_sum($item->prsum2) . '</strong>S';
			$com2[] = '<strong>'. is_sum($item->prproc2) . '</strong>%';
			
		    return join('+',$com1) .' &rarr; '. join('+',$com2);
			
		} elseif($column_name == 'csumm'){
			$id = $item->id;
            return '<input type="text" name="minsumm1['. $id .']" style="width: 60px;" value="'. is_sum($item->minsumm1) .'" />';
			
		} elseif($column_name == 'status'){
		    if($item->status == 0){ 
			    return '<span class="bred">'. __('not active direction','pn') .'</span>'; 
			} else { 
			    return '<span class="bgreen">'. __('active direction','pn') .'</span>'; 
			}
		} elseif($column_name == 'cid'){
			return '<strong>'. $item->id .'</strong>';			
		} 
		
		return apply_filters('naps_manage_ap_col', '', $column_name,$item);
		
    }	
	
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'], 
            $item->id                
        );
    }	

    function column_title($item){

        $actions = array(
            'edit'      => '<a href="'. admin_url('admin.php?page=pn_add_naps&item_id='. $item->id) .'">'. __('Edit','pn') .'</a>',
        );
        
   		$primary = apply_filters('naps_manage_ap_primary', get_vtitle($item->valsid1) .' &rarr; '. get_vtitle($item->valsid2), $item);
		$actions = apply_filters('naps_manage_ap_actions', $actions, $item);       
        return sprintf('%1$s %2$s',
            $primary,
            $this->row_actions($actions)
        );		
		
    }	
	
	function single_row( $item ) {
		$class = '';
		if($item->status == 1){
			$class = 'active';
		}
		echo '<tr class="pn_tr '. $class .'">';
			$this->single_row_columns( $item );
		echo '</tr>';
	}	
	
    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
			'cid'     => __('ID','pn'),
			'title'     => __('Direction','pn'),
			'course1' => __('Exchange course 1','pn'),
			'course2' => __('Exchange course 2','pn'),
			'ccomis' => __('Commission','pn'),
			'csumm' => __('Min. amount','pn'),
			'status'    => __('Status','pn'),
        );
		$columns = apply_filters('naps_manage_ap_columns', $columns);
        return $columns;
    }	
	
    function get_sortable_columns() {
        $sortable_columns = array( 
			'course1'     => array('course1',false),
			'course2'     => array('course2',false),
        );
        return $sortable_columns;
    }
	
    function get_bulk_actions() {
        $actions = array(
			'active'    => __('Activate','pn'),
			'notactive'    => __('Deactivate','pn'),
			'notcorr' => __('Disable autocorrection','pn'),
            'delete'    => __('Delete','pn'),
        );
        return $actions;
    }
    
    function prepare_items() {
        global $wpdb; 
		
        $per_page = $this->get_items_per_page('trev_naps_per_page', 20);
        $current_page = $this->get_pagenum();
        
        $this->_column_headers = $this->get_column_info();

		$offset = ($current_page-1)*$per_page;
		$oby = is_param_get('orderby');
		if($oby == 'course1'){
			$orderby = '(curs1 -0.0)';
		} elseif($oby == 'course2'){
			$orderby = '(curs2 -0.0)';
		} else {
		    $orderby = 'id';
		}
		$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';
		if($order != 'asc'){ $order = 'desc'; }		
		
		$where = '';
		
        $mod = intval(is_param_get('mod'));
        if($mod == 1){ 
            $where .= " AND status='1'"; 
		} elseif($mod == 2){
			$where .= " AND status='0'";
		}		
		
        $val1 = intval(is_param_get('val1'));
        if($val1 > 0){ 
            $where .= " AND valsid1='$val1'"; 
		}
        $val2 = intval(is_param_get('val2'));
        if($val2 > 0){ 
            $where .= " AND valsid2='$val2'"; 
		}		
		$where = pn_admin_search_where($where);
		$total_items = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."napobmens WHERE id > 0 $where");
		$data = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."napobmens WHERE id > 0 $where ORDER BY $orderby $order LIMIT $offset , $per_page");  		

        $current_page = $this->get_pagenum();
        $this->items = $data;
		
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  
            'per_page'    => $per_page,                     
            'total_pages' => ceil($total_items/$per_page)  
        ));
    }
	
	function extra_tablenav( $which ) {
 		global $wpdb;
		
		$valuts = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."valuts ORDER BY site_order ASC");		  	
    ?>
		<div class="alignleft actions">
<?php   
		if ( 'top' == $which ) {
			$val1 = intval(is_param_get('val1'));
			$val2 = intval(is_param_get('val2'));
?>
			<select name="val1" autocomplete="off">
				<option value="0" <?php selected($val1,0); ?>>-- <?php _e('All currency','pn'); ?> --</option>
            <?php
				foreach($valuts as $item){
					echo "\t<option value='" . $item->id . "' " . selected($item->id, $val1, false ) . ">". pn_strip_input($item->vname ." ". $item->vtype) ."</option>\n";
			    }
			?>
			</select>
			
			<input type="submit" name="back_filter" class="back_filter" value="">
			
			<select name="val2" autocomplete="off">
				<option value="0" <?php selected($val2,0); ?>>-- <?php _e('All currency','pn'); ?> --</option>
            <?php
				foreach($valuts as $item){
					echo "\t<option value='" . $item->id . "' " . selected($item->id, $val2, false ) . ">". pn_strip_input($item->vname ." ". $item->vtype) ."</option>\n";
			    }
			?>
			</select>			
			<input type="submit" name="filter" class="button" value="<?php _e('Filter','pn'); ?>">
<?php
		}
    ?>
	    </div>	
	
		<div class="alignleft actions">
			<input type="submit" name="save" class="button" value="<?php _e('Save','pn'); ?>">
            <a href="<?php echo admin_url('admin.php?page=pn_add_naps');?>" class="button"><?php _e('Add new','pn'); ?></a>
		</div>		
	<?php  
	}	  
	
}

add_action('premium_screen_pn_naps','my_myscreen_pn_naps');
function my_myscreen_pn_naps(){
    $args = array(
        'label' => __('Display','pn'),
        'default' => 20,
        'option' => 'trev_naps_per_page'
    );
    add_screen_option('per_page', $args );
	if(class_exists('trev_naps_List_Table')){
		new trev_naps_List_Table;
	}
} 