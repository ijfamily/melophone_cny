<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_sort_naps', 'adminpage_title_pn_sort_naps');
function adminpage_title_pn_sort_naps(){
	_e('Sort directions exchanges','pn');
}

add_action('pn_adminpage_content_pn_sort_naps','def_adminpage_content_pn_sort_naps');
function def_adminpage_content_pn_sort_naps(){
global $wpdb;

	$form = new PremiumForm();

	$place = is_param_get('place');
	$places_t = array('0','1');
	$selects = array();
	$selects[0] = array(
		'link' => admin_url("admin.php?page=pn_sort_naps"),
		'title' => __('Left column','pn'),
		'background' => '',
		'default' => '',
	);		
	$selects[1] = array(
		'link' => admin_url("admin.php?page=pn_sort_naps&place=1"),
		'title' => __('Right column','pn'),
		'background' => '',
		'default' => '1',
	);		
	$form->select_box($place, $selects, __('Setting up','pn'));	
	
	if(in_array($place, $places_t)){
		$place = intval($place);
		if($place == 0){
			$items = $wpdb->get_results("SELECT DISTINCT(valsid1) FROM ".$wpdb->prefix."napobmens WHERE status='1' ORDER BY rorder ASC");
			$sort_list = array();
			foreach($items as $item){
				$sort_list[0][] = array(
					'title' => get_vtitle($item->valsid1),
					'id' => $item->valsid1,
					'number' => $item->valsid1,
				);		
			}
			$link = pn_link_post('pn_sort_table1_sort', 'post');		
		} else {
			$items = $wpdb->get_results("SELECT DISTINCT(valsid2) FROM ".$wpdb->prefix."napobmens WHERE status='1' ORDER BY rorder2 ASC");
			$sort_list = array();
			foreach($items as $item){
				$sort_list[0][] = array(
					'title' => get_vtitle($item->valsid2),
					'id' => $item->valsid2,
					'number' => $item->valsid2,
				);		
			}
			$link = pn_link_post('pn_sort_table2_sort', 'post');
		}
		$form->sort_one_screen($sort_list);
		?>
	<script type="text/javascript">
	$(document).ready(function(){ 									   
		$(".thesort ul").sortable({ 
			opacity: 0.6, 
			cursor: 'move',
			revert: true,
			update: function() {
				$('#premium_ajax').show();
				var order = $(this).sortable("serialize"); 
				$.post("<?php echo $link; ?>", order, function(theResponse){
					$('#premium_ajax').hide();
				}); 															 
			}	 				
		});
	});	
	</script>		
		<?php
	}
}

add_action('premium_action_pn_sort_table1_sort','def_premium_action_pn_sort_table1_sort');
function def_premium_action_pn_sort_table1_sort(){
global $wpdb;	
	if(current_user_can('administrator')){
		only_post();	
		$number = is_param_post('number');
		$y = 0;
		if(is_array($number)){				
			foreach($number as $theid) { $y++;				
				$theid = intval($theid);
				$wpdb->query("UPDATE ".$wpdb->prefix."napobmens SET rorder='$y' WHERE valsid1 = '$theid'");					
			}				
		}
	}
}

add_action('premium_action_pn_sort_table2_sort','def_premium_action_pn_sort_table2_sort');
function def_premium_action_pn_sort_table2_sort(){
global $wpdb;	
	if(current_user_can('administrator')){
		only_post();	
		$number = is_param_post('number');
		$y = 0;
		if(is_array($number)){				
			foreach($number as $theid) { $y++;				
				$theid = intval($theid);
				$wpdb->query("UPDATE ".$wpdb->prefix."napobmens SET rorder2='$y' WHERE valsid2 = '$theid'");					
			}				
		}
	}
}