<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_naps_temp', 'pn_admin_title_pn_naps_temp');
function pn_admin_title_pn_naps_temp($page){
	_e('Exchange direction templates','pn');
} 

add_action('pn_adminpage_content_pn_naps_temp','def_adminpage_content_pn_naps_temp');
function def_adminpage_content_pn_naps_temp(){
global $exchangebox;	
	
	$form = new PremiumForm();
	
	$place = is_status_name(is_param_get('place'));
	$places = apply_filters('list_naps_temp',array());
	$places = (array)$places;
	$places_t = array();
	foreach($places as $key => $v){
		$places_t[] = $key;
	}	
	
	$selects = array();
	$selects[] = array(
		'link' => admin_url("admin.php?page=pn_naps_temp"),
		'title' => '--' . __('Make a choice','pn') . '--',
		'background' => '',
		'default' => '',
	);		
	if(is_array($places)){ 
		foreach($places as $key => $val){ 
			$selects[] = array(
				'link' => admin_url("admin.php?page=pn_naps_temp&place=".$key),
				'title' => $val,
				'background' => '',
				'default' => $key,
			);		
		}
	}		
	$form->select_box($place, $selects, __('Setting up','pn'));	

	if(in_array($place,$places_t)){
		$options = array();
		$options['hidden_block'] = array(
			'view' => 'hidden_input',
			'name' => 'place',
			'default' => $place,
		);	
		$not = array('description_txt','timeline_txt');
		if(!in_array($place, $not)){
			$options[] = array(
				'view' => 'inputbig',
				'title' => __('Short status description','pn'),
				'default' => $exchangebox->get_option('naps_status',$place),
				'name' => 'status',
			);		
			$not2 = array('status_new', 'status_new_mobile');
			if(!in_array($place, $not2)){
				$options[] = array(
					'view' => 'select',
					'title' => __('Auto-update page','pn'),
					'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
					'default' => $exchangebox->get_option('naps_timer',$place),
					'name' => 'timer',
				);
			}	
		}
		$options['temp'] = array(
			'view' => 'editor',
			'title' => __('Text', 'pn'),
			'default' => $exchangebox->get_option('naps_temp',$place),
			'name' => 'temp',
			'rows' => 10,
			'media' => false,
		);
		$params_form = array(
			'filter' => 'pn_naps_temp_option',
			'method' => 'post',
			'button_title' => __('Save','pn'),
		);
		$form->init_form($params_form, $options);		
	} 	
}  

add_action('premium_action_pn_naps_temp','def_premium_action_pn_naps_temp');
function def_premium_action_pn_naps_temp(){
global $wpdb, $exchangebox;	

	only_post();
	pn_only_caps(array('administrator', 'pn_naps'));
	
	$form = new PremiumForm();
	
	$place = is_status_name(is_param_post('place'));
	if($place){
		$exchangebox->update_option('naps_status', $place, pn_strip_input(is_param_post('status')));
		$exchangebox->update_option('naps_timer', $place, intval(is_param_post('timer')));
		$exchangebox->update_option('naps_temp', $place, pn_strip_text(is_param_post('temp')));
	}
	
	$url = admin_url('admin.php?page=pn_naps_temp&place='. $place .'&reply=true');
	$form->answer_form($url);
}