<?php
if( !defined( 'ABSPATH')){ exit(); }

global $exchangebox;
$path = get_extension_file(__FILE__);
$exchangebox->file_include($path.'/add');
$exchangebox->file_include($path.'/add_many');
$exchangebox->file_include($path.'/list');
$exchangebox->file_include($path.'/config');
$exchangebox->file_include($path.'/api');