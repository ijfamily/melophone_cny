<?php
if( !defined( 'ABSPATH')){ exit(); }
 
add_filter('banner_pages', 'def_banner_pages');
function def_banner_pages($banner_pages){
global $exchangebox;
	
	$text_banners = intval($exchangebox->get_option('partners','text_banners'));
	if(!$text_banners){
		if(isset($banner_pages['text'])){
			unset($banner_pages['text']);
		}
	}
	
	return $banner_pages;
}

add_filter('pp_banners','def_pp_banners');
function def_pp_banners($banners){
	
	$banners = array(
		'text'=> __('Text materials','pn'),
		'banner1'=> __('Banners','pn').'(468 x 60)',
		'banner2'=> __('Banners','pn').'(200 x 200)',
		'banner3'=> __('Banners','pn').'(120 x 600)',
		'banner4'=> __('Banners','pn').'(100 x 100)',
		'banner5'=> __('Banners','pn').'(88 x 31)',
		'banner6'=> __('Banners','pn').'(336 x 280)',
		'banner7'=> __('Banners','pn').'(250 x 250)',
		'banner8'=> __('Banners','pn').'(240 x 400)',
		'banner9'=> __('Banners','pn').'(234 x 60)',
		'banner10'=> __('Banners','pn').'(120 x 90)',
		'banner11'=> __('Banners','pn').'(120 x 60)',
		'banner12'=> __('Banners','pn').'(120 x 240)',
		'banner13'=> __('Banners','pn').'(125 x 125)',
		'banner14'=> __('Banners','pn').'(300 x 600)',
		'banner15'=> __('Banners','pn').'(300 x 250)',
		'banner16'=> __('Banners','pn').'(80 x 150)',
		'banner17'=> __('Banners','pn').'(728 x 90)',
		'banner18'=> __('Banners','pn').'(160 x 600)',
		'banner19'=> __('Banners','pn').'(80 x 15)',
	);	
	
	return $banners;
}
 
add_filter('list_admin_notify','list_admin_notify_payout');
function list_admin_notify_payout($places_admin){
	$places_admin['payout'] = __('Affiliate reward payout request','pn');
	return $places_admin;
}

add_filter('list_user_notify','list_user_notify_partprofit');
function list_user_notify_partprofit($places_admin){
	$places_admin['partprofit'] = __('Affiliate reward charging','pn');
	return $places_admin;
}

add_filter('list_notify_tags_payout','def_mailtemp_tags_payout');
function def_mailtemp_tags_payout($tags){
	$tags['user'] = __('User','pn');
	$tags['sum'] = __('Amount','pn');
	return $tags;
}

add_filter('list_notify_tags_partprofit','def_mailtemp_tags_partprofit');
function def_mailtemp_tags_partprofit($tags){
	$tags['sum'] = __('Amount','pn');
	$tags['ctype'] = __('Currency code','pn');
	return $tags;
}

global $exchangebox;
if($exchangebox->get_option('partners','status') == 1){
	add_action('change_bidstatus_all', 'change_bidstatus_all_pp',1,3);
}
function change_bidstatus_all_pp($status, $item_id, $item){
global $wpdb, $exchangebox;
	$not = array('realdelete','autodelete','auto');
	if(!in_array($status, $not)){
		if($status == 'success'){
			$calc = intval($exchangebox->get_option('partners','calc'));
			if($calc == 0 or $calc == 1 and $item->user_id > 0){
				$ref_id = $item->ref_id;
				$psum = is_sum($item->ref_sum);
				if($ref_id and $psum > 0){
					$rd = get_userdata($ref_id);
					$ctype = cur_type();
					if(isset($rd->user_email)){
						$ref_email = is_email($rd->user_email);
						$wpdb->update($wpdb->prefix.'bids', array('pcalc'=> 1), array('id'=>$item_id));
				
						$notify_tags = array();
						$notify_tags['[sitename]'] = pn_strip_input(get_bloginfo('sitename'));
						$notify_tags['[sum]'] = $psum;
						$notify_tags['[ctype]'] = $ctype;
						$notify_tags = apply_filters('notify_tags_partprofit', $notify_tags);		

						$user_send_data = array(
							'user_email' => $ref_email,
							'user_phone' => '',
						);	
						$result_mail = apply_filters('premium_send_message', 0, 'partprofit', $notify_tags, $user_send_data);
					}
				}
			}
		} else {
			$wpdb->update($wpdb->prefix.'bids', array('pcalc'=> 0), array('id'=>$item_id));
		}
	}
}

add_action('wp_before_admin_bar_render', 'wp_before_admin_bar_render_payouts');
function wp_before_admin_bar_render_payouts() {
global $wp_admin_bar, $wpdb, $exchangebox;
	
    if(current_user_can('administrator')){
		if(get_icon_indicators('pp_user_payouts')){
			$count = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."payoutuser WHERE bstatus = '0'");
			if($count > 0){
				$wp_admin_bar->add_menu( array(
					'id'     => 'new_payoutuser',
					'href' => admin_url('admin.php?page=pn_payouts&mod=1'),
					'title'  => '<div style="height: 32px; width: 22px; background: url('. $exchangebox->plugin_url .'images/newpayout.png) no-repeat center center"></div>',
					'meta' => array( 'title' => sprintf(__('Requests for payment (%s)','pn'), $count) ) 		
				));	
			}
		}
	}
}

add_filter('list_icon_indicators', 'pp_user_payouts_icon_indicators');
function pp_user_payouts_icon_indicators($lists){
	$lists['pp_user_payouts'] = __('Requests for payouts','pn');
	return $lists;
}

global $exchangebox;
$path = get_extension_file(__FILE__);
$exchangebox->file_include($path.'/psettings');
$exchangebox->file_include($path.'/users');
$exchangebox->file_include($path.'/stats');
$exchangebox->file_include($path.'/preferals');
$exchangebox->file_include($path.'/plinks');
$exchangebox->file_include($path.'/cron');
$exchangebox->file_include($path.'/partnpers');
$exchangebox->file_include($path.'/add_partnpers');
$exchangebox->file_include($path.'/pbanners');
$exchangebox->file_include($path.'/pexch'); 
$exchangebox->file_include($path.'/payouts');