<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action( 'delete_user', 'delete_user_pp');
function delete_user_pp($user_id){
global $wpdb;

	$user_id = intval($user_id);
    $wpdb->update($wpdb->prefix."users" , array('ref_id'=>'0'), array('ref_id'=>$user_id));
	
	$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."plinks WHERE user_id = '$user_id'");
	foreach($items as $item){
		$item_id = $item->id;
		do_action('pn_plinks_delete_before', $item_id, $item);
		$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."plinks WHERE id = '$item_id'");
		do_action('pn_plinks_delete', $item_id, $item);
		if($result){
			do_action('pn_plinks_delete_after', $item_id, $item);
		}
	}	
	
 	$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."payoutuser WHERE userid = '$user_id'");
	foreach($items as $item){
		$item_id = $item->id;
		do_action('pn_payoutuser_delete_before', $item_id, $item);
		$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."payoutuser WHERE id = '$item_id'");
		do_action('pn_payoutuser_delete', $item_id, $item);
		if($result){
			do_action('pn_payoutuser_delete_after', $item_id, $item);
		}
	}   
	
	$wpdb->query("DELETE FROM ". $wpdb->prefix. "archive_data WHERE item_id = '$user_id' AND meta_key='plinks'");
	
}

add_action( 'pn_user_register', 'pn_register_user_pp');
function pn_register_user_pp($user_id) {
global $wpdb, $exchangebox;
	
	if($exchangebox->get_option('partners','status') == 1){
		$user_id = intval($user_id);
		$ref_id = intval(get_pn_cookie('ref_id')); if($ref_id < 0){ $ref_id = 0; }
		if($ref_id){
			$wpdb->update($wpdb->prefix."users" , array('ref_id'=>$ref_id), array('ID'=>$user_id));
		}
	}
	
}

add_action('init','init_pp',0);
function init_pp(){
global $wpdb, $exchangebox;
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	if($exchangebox->get_option('partners','status') == 1){
		$ref_id = intval(is_param_get(stand_refid())); 
		if(!$ref_id){ $ref_id = intval(is_param_get('exbid')); }
		if($ref_id > 0 and !$user_id){
			$user = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."users WHERE ID='$ref_id'");
			if(isset($user->ID)){
				
				$time = current_time('timestamp');
				$date = current_time('mysql');
				
				$clife = intval($exchangebox->get_option('partners','clife'));
				if($clife < 1){ $clife = 365; }
				$cookie_time = $time + ($clife*24*60*60);
				
				add_pn_cookie('ref_id', $ref_id, $cookie_time);
				$ip = pn_real_ip();
				$browser = pn_maxf(pn_strip_input(is_isset($_SERVER,'HTTP_USER_AGENT')),250);
				$referer = pn_maxf(pn_strip_input(is_isset($_SERVER,'HTTP_REFERER')),500);
				$wpdb->insert( $wpdb->prefix.'plinks' , array( 'user_id' => $ref_id, 'user_login'=> is_user($user->user_login),'pbrowser' => $browser, 'pdate' => $date, 'pip' => $ip, 'prefer'=> $referer));
			
			}
		}	
	}
}

add_action( 'profile_update', 'pn_profile_update_pp');
function pn_profile_update_pp($user_id){
global $wpdb;
	$user_id = intval($user_id);
	if(isset($_POST['pn_profile_update_pp'])){
		if(current_user_can('administrator')){ 
			$array = array();
			$array['ref_id'] = intval(is_param_post('ref_id'));
			$array['partner_pers'] = is_sum(is_param_post('partner_pers'));
			$wpdb->update($wpdb->prefix."users" , $array, array('ID'=>$user_id));
		}
	}	
}

global $exchangebox;
if($exchangebox->get_option('partners','status') == 1){
	add_action( 'show_user_profile', 'pn_edit_user_pp');
	add_action( 'edit_user_profile', 'pn_edit_user_pp');
	
	add_filter('manage_users_columns', 'pp_users_columns');
	add_filter('manage_users_custom_column', 'pp_manage_status_column', 10, 3);	
}	

function pn_edit_user_pp($user){
global $wpdb;	
	$user_id = $user->ID;
		
	if(current_user_can('administrator')){ 
			
		$ref_id = $user->ref_id;
		$users = $wpdb->get_results("SELECT ID, user_login FROM ". $wpdb->prefix ."users WHERE ID != '$user_id' ORDER BY user_login ASC");
		$partner_pers = is_sum($user->partner_pers);
		?>
		<input type="hidden" name="pn_profile_update_pp" value="1" />
		
		<h3><?php _e('Affiliate program','pn'); ?></h3>
	    <table class="form-table">
			<tr>
				<th>
					<label for="ref_id"><?php _e('Referrer','pn'); ?></label>
				</th>
				<td>
					<select name="ref_id" id="ref_id" autocomplete="off">
						<option value="0"><?php _e('No','pn'); ?></option>
						<?php foreach($users as $us){ ?>
							<option value="<?php echo $us->ID; ?>" <?php selected($ref_id,$us->ID); ?>><?php echo is_user($us->user_login); ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>

			<tr>
				<th>
					<label for="partner_pers"><?php _e('Personal partnership interest','pn'); ?></label>
				</th>
				<td>
					<input type="text" name="partner_pers" id="partner_pers" autocomplete="off" value="<?php echo $partner_pers;?>" />%
				</td>
			</tr>
			
			<tr>
				<th>
					<?php _e('Transitions','pn'); ?>
				</th>
				<td>
					<?php echo get_partner_plinks($user_id); ?>
				</td>
			</tr>
			
			<tr>
				<th>
					<?php _e('Partners exchange','pn'); ?>
				</th>
				<td>
					<?php echo get_user_count_refobmen($user_id);?> (<?php echo get_user_sum_refobmen($user_id); ?> <?php echo cur_type(); ?>)
				</td>
			</tr>	

			<tr>
				<th>
					<?php _e('Partnership interest','pn'); ?>
				</th>
				<td>
					<?php echo get_user_pers_refobmen($user_id);?>%
				</td>
			</tr>

			<tr>
				<th>
					<?php _e('The money in the account','pn'); ?>
				</th>
				<td>
					<?php echo get_partner_money($user_id);?> <?php echo cur_type(); ?>
				</td>
			</tr>

			<tr>
				<th>
					<?php _e('Earned for all time','pn'); ?>
				</th>
				<td>
					<?php echo get_partner_earn_all($user_id); ?> <?php echo cur_type(); ?>
				</td>
			</tr>

			<tr>
				<th>
					<?php _e('Payout','pn'); ?>
				</th>
				<td>
					<?php echo get_partner_payout($user_id); ?> <?php echo cur_type(); ?>
				</td>
			</tr>
        </table>		
		
		<?php
	}
}

function pp_users_columns($columns) { 
	$columns['partnermoney'] = __('The money in the account','pn');		
	return $columns;
}

function pp_manage_status_column($empty='', $column_name, $user_id) {
global $exchangebox;

	if($column_name == 'partnermoney'){
        $minpay = is_sum($exchangebox->get_option('partners','minpay'));
	    $balans = get_partner_money_now($user_id);
	    $dbalans = 0;
	    if($balans >= $minpay){
            $dbalans = $balans;
        } 	
		
	    return $balans.' <span class="bgreen" title="'. __('Awaits for payout','pn') .'">('. $dbalans .')</span> '. cur_type();
	}	
	
	return $empty;
} 