<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_psettings', 'adminpage_title_pn_psettings');
function adminpage_title_pn_psettings(){
	_e('Settings','pn');
}

add_action('pn_adminpage_content_pn_psettings','def_adminpage_content_pn_psettings');
function def_adminpage_content_pn_psettings(){
global $exchangebox;
	
	$form = new PremiumForm();
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Settings','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['status'] = array(
		'view' => 'select',
		'title' => __('Include affiliate program?','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('partners','status'),
		'name' => 'status',
	);	
	$options['line1'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['wref'] = array(
		'view' => 'select',
		'title' => __('Referral lifetime','pn'),
		'options' => array('0'=>__('Eternally','pn'), '1'=>__('By cookies','pn')),
		'default' => $exchangebox->get_option('partners','wref'),
		'name' => 'wref',
	);	
	$options['clife'] = array(
		'view' => 'input',
		'title' => __('Cookies lifetime (days)','pn'),
		'default' => $exchangebox->get_option('partners','clife'),
		'name' => 'clife',
	);	
	$options['line2'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['minpay'] = array(
		'view' => 'input',
		'title' => __('Minimum payout','pn').' ('. cur_type() .')',
		'default' => $exchangebox->get_option('partners','minpay'),
		'name' => 'minpay',
	);	
	$options['calc'] = array(
		'view' => 'select',
		'title' => __('Consider affiliate reward from ','pn'),
		'options' => array('0'=>__('All users','pn'),'1'=>__('Only registered users','pn')),
		'default' => $exchangebox->get_option('partners','calc'),
		'name' => 'calc',
	);
	$options['reserv'] = array(
		'view' => 'select',
		'title' => __('Consider affiliate payments in reserve','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('partners','reserv'),
		'name' => 'reserv',
	);	
	$options['line3'] = array(
		'view' => 'line',
		'colspan' => 2,
	);
	$options['text_banners'] = array(
		'view' => 'select',
		'title' => __('Show promo text materials','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('partners','text_banners'),
		'name' => 'text_banners',
	);
	$options['line4'] = array(
		'view' => 'line',
		'colspan' => 2,
	);
	$options['payouttext'] = array(
		'view' => 'textareatags',
		'title' => __('Text above form of withdrawal of partner funds','pn'),
		'default' => $exchangebox->get_option('partners','payouttext'),
		'tags' => array('minpay'=>__('Minimum payout','pn'), 'currency'=>__('Currency type','pn')),
		'width' => '',
		'height' => '150px',
		'prefix1' => '[',
		'prefix2' => ']',
		'name' => 'payouttext',
		'work' => 'text',
	);	
	
	$params_form = array(
		'filter' => 'pn_pp_adminform',
		'method' => 'post',
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);	
}

add_action('premium_action_pn_psettings','def_premium_action_pn_psettings');
function def_premium_action_pn_psettings(){
global $wpdb, $exchangebox;	

	only_post();
	pn_only_caps(array('administrator'));
	
	$exchangebox->update_option('partners','wref',intval(is_param_post('wref')));
	$exchangebox->update_option('partners','payouttext', pn_strip_text(is_param_post('payouttext')));
	$exchangebox->update_option('partners','clife',intval(is_param_post('clife')));
	$exchangebox->update_option('partners','status',intval(is_param_post('status')));
	$exchangebox->update_option('partners','text_banners',intval(is_param_post('text_banners')));
	$exchangebox->update_option('partners','calc',intval(is_param_post('calc')));
	$exchangebox->update_option('partners','reserv',intval(is_param_post('reserv')));
	$exchangebox->update_option('partners','minpay',is_sum(is_param_post('minpay')));		
			
	do_action('pn_pp_adminform_post');
			
	$url = admin_url('admin.php?page=pn_psettings&reply=true');
	wp_redirect($url);
	exit;
}	