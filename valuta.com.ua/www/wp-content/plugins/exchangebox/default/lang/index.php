<?php
if( !defined( 'ABSPATH')){ exit(); }

if(!function_exists('admin_menu_lang')){

	add_action('pn_adminpage_title_pn_lang', 'def_pn_adminpage_title_pn_lang');
	function def_pn_adminpage_title_pn_lang($page){
		_e('Language settings','pn');
	}

	add_filter( 'whitelist_options', 'lang_whitelist_options' );
	function lang_whitelist_options($whitelist_options){
		if(isset($whitelist_options['general'])){	
			$key = array_search('WPLANG',$whitelist_options['general']);
			if(isset($whitelist_options['general'][$key])){
				unset($whitelist_options['general'][$key]);
			}		
		}
		return $whitelist_options;
	}	
	
	add_action('admin_footer', 'lang_admin_lang_footer');
	function lang_admin_lang_footer(){
		$screen = get_current_screen();
		
		if($screen->id == 'options-general'){
			?>
			<script type="text/javascript">
			jQuery(function($){
				$('#WPLANG').parents('tr').hide();
			});
			</script>
			<?php
		}		
		if($screen->id == 'profile' or $screen->id == 'user-edit'){
			?>
			<script type="text/javascript">
			jQuery(function($){
				$('.user-language-wrap').hide();
			});
			</script>
			<?php		
		}	
	}	

	add_filter('pn_lang_option', 'def_pn_lang_option', 1);
	function def_pn_lang_option($options){
	global $wpdb, $exchangebox;	
		
		$langs = apply_filters('pn_site_langs', array());
		
		$lang = get_option('pn_lang');
		if(!is_array($lang)){ $lang = array(); }		
		
		$admin_lang = is_isset($lang,'admin_lang');
		if(!$admin_lang){
			$admin_lang = get_locale();
		}		
		
		$site_lang = is_isset($lang,'site_lang');
		if(!$site_lang){
			$site_lang = get_locale();
		}				
		
		$options['top_title'] = array(
			'view' => 'h3',
			'title' => __('Language settings','pn'),
			'submit' => __('Save','pn'),
			'colspan' => 2,
		);		
		$options['admin_lang'] = array(
			'view' => 'select',
			'title' => __('Admin-panel language','pn'),
			'options' => $langs,
			'default' => $admin_lang,
			'name' => 'admin_lang',
			'work' => 'input',
		);
		$options['site_lang'] = array(
			'view' => 'select',
			'title' => __('Website language','pn'),
			'options' => $langs,
			'default' => $site_lang,
			'name' => 'site_lang',
			'work' => 'input',
		);						
		
		return $options;
	}	
	
 	add_action('pn_adminpage_content_pn_lang','def_pn_adminpage_content_pn_lang');
	function def_pn_adminpage_content_pn_lang(){
	global $exchangebox;
		
		$form = new PremiumForm();
		$params_form = array(
			'filter' => 'pn_lang_option',
			'method' => 'post',
			'data' => '',
			'form_link' => '',
			'button_title' => __('Save','pn'),
		);
		$form->init_form($params_form);		
		
	} 

	add_action('premium_action_pn_lang','def_premium_action_pn_lang');
	function def_premium_action_pn_lang(){
	global $wpdb, $exchangebox;	

		only_post();
		pn_only_caps(array('administrator'));

		$form = new PremiumForm();
		$data = $form->strip_options('pn_lang_option', 'post');
		
		$lang = get_option('pn_lang');
		$lang['admin_lang'] = $admin_lang = $data['admin_lang'];
		$lang['site_lang'] = $site_lang = $data['site_lang'];
		update_option('pn_lang',$lang);
			
		do_action('pn_lang_option_post', $data);			
				
		$back_url = is_param_post('_wp_http_referer');
		$back_url .= '&reply=true';
				
		$form->answer_form($back_url);							
	} 	
}