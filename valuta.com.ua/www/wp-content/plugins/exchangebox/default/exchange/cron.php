<?php
if( !defined( 'ABSPATH')){ exit(); }

function delete_auto_bids(){
global $wpdb, $exchangebox;

	$count_minute = intval($exchangebox->get_option('logssettings', 'delete_auto_bids'));
	if(!$count_minute){ $count_minute = 15; }

	$second = $count_minute * 60;
	$second = apply_filters('del_autobids_second', $second);
	
	$time = current_time('timestamp') - $second;
	if($second != '-1'){
		$ldate = date('Y-m-d H:i:s', $time);
		$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids WHERE tdate < '$ldate' AND status='auto'");
		foreach($items as $item){
			$id = $item->id;	
			$result = $wpdb->query("DELETE FROM ".$wpdb->prefix."bids WHERE id = '$id'");
			if($result == 1){
				$wpdb->query("DELETE FROM ".$wpdb->prefix."bids_meta WHERE item_id = '$id'");
				do_action('change_bidstatus_all', 'autodelete', $item->id, $item, 'admin','system');
				do_action('change_bidstatus_autodelete', $item->id, $item, 'admin','system'); 
			}
		}
	}
} 

add_filter('list_cron_func', 'delete_auto_bids_list_cron_func');
function delete_auto_bids_list_cron_func($filters){
global $exchangebox;	

	if(!$exchangebox->is_up_mode()){
		$filters['delete_auto_bids'] = array(
			'title' => __('Removing orders with inappropriate rules','pn'),
			'site' => 'now',
		);
	}
	
	return $filters;
}

add_filter('list_logs_settings', 'delete_auto_bids_list_logs_settings');
function delete_auto_bids_list_logs_settings($filters){
			
	$filters['delete_auto_bids'] = array(
		'title' => __('Removing orders with inappropriate rules','pn') .' ('. __('minuts','pn') .')',
		'count' => 15,
		'minimum' => 1,
	);
		
	return $filters;
} 

function delete_notpay_bids(){
global $wpdb, $exchangebox;

	if($exchangebox->get_option('exchange','autodelete') == 1){
		$sec = 0;
	    $hour = intval($exchangebox->get_option('exchange','ad_h'));
		if($hour > 0){
			$sec = $hour * 60 * 60;
		}
		$minuts = intval($exchangebox->get_option('exchange','ad_m'));
		if($minuts > 0){
			$sec = $sec + ($minuts * 60);
		}		
		if($sec > 0){
			$time = current_time('timestamp') - $sec;
			$ldate = date('Y-m-d H:i:s', $time);
			$items = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."bids WHERE tdate < '$ldate' AND status='new'");
			foreach($items as $item){
			    $id = $item->id;
				$wpdb->update($wpdb->prefix.'bids', array('status'=>'delete','editdate'=>current_time('mysql')), array('id'=>$id));
				do_action('change_bidstatus_all', 'delete', $item->id, $item, 'admin','system');
				do_action('change_bidstatus_delete', $item->id, $item, 'admin','system');				
			}
		}	
	}
} 

add_filter('list_cron_func', 'delete_notpay_bids_list_cron_func');
function delete_notpay_bids_list_cron_func($filters){
global $exchangebox;	

	if(!$exchangebox->is_up_mode()){
		$filters['delete_notpay_bids'] = array(
			'title' => __('Removing unpaid orders','pn'),
			'site' => 'now',
		);
	}
	
	return $filters;
}