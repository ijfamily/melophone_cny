<?php
if( !defined( 'ABSPATH')){ exit(); }
		
function get_exchange_table(){
global $wpdb, $exchangebox;	
	
	$temp = '';
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	if($exchangebox->get_option('up_mode') == 1){
		$temp .= '<div class="resultfalse">'. __('Maintenance','pn') .'</div>';
	}	
	
	$techreg = get_technical_mode();  
	if($techreg){
		$techregtext = get_technical_mode_text();
		if($techregtext){
			$temp .= '<div class="resultfalse">'. $techregtext .'</div>';
		}
	}
	
	if($techreg == 0 or $techreg == 2 or $techreg == 3){ /* тех. режим */
		$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide')); 
		if($gostnaphide != 1 or $gostnaphide == 1 and $user_id){ /* видимость для гостей */
			$temp .= apply_filters('exchange_table_type1','');
		} else {
			$temp .= '<div class="resultfalse">'. __('directions are available only to authorized users','pn') .'</div>';
		}
	} 	
	
	return $temp;
}	
add_shortcode('changetable', 'get_exchange_table');

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'function');
$exchangebox->include_patch(__FILE__, 'peremen');
$exchangebox->include_patch(__FILE__, 'action');
$exchangebox->include_patch(__FILE__, 'cron');
$exchangebox->include_patch(__FILE__, 'table1');
$exchangebox->include_patch(__FILE__, 'widget');
$exchangebox->include_patch(__FILE__, 'mails');