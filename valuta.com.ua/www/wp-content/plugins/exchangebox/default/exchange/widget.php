<?php
if( !defined( 'ABSPATH')){ exit(); }

/* добавляем JS */
add_action('siteplace_js','siteplace_js_exchange_widget');
function siteplace_js_exchange_widget(){ 
?>	
jQuery(function($){
	var countwidget = $('#widgetobmentable').length;
	if(countwidget > 0){ 
		$(document).on('click', 'a.onehrine', function(){
	        var xname1 = $(this).attr('data-xname1');
			var xname2 = $(this).attr('data-xname2');
			$('a.onehrine').removeClass('active');
			$(this).addClass('active');
			
            var param = 'xname1='+xname1+'&xname2='+xname2;
            $.ajax({
				type: "POST",
				url: "<?php echo get_ajax_link('widtable'); ?>",
				data: param,
				dataType: 'json',
				error: function(res, res2, res3){
					<?php do_action('pn_js_error_response', 'ajax'); ?>
				},			
				success: function(res){
					if(res['status'] == 'success'){
						$('#widgetobmentable').html(res['table']);
					} else {
						alert('<?php _e('Error!','pn'); ?>');
					}
				}
            });	
			
	        return false;
	    });
    }	
});	
<?php	
}
/* end добавляем JS */

function the_exchange_widget(){
global $table_exchange;	
	if($table_exchange == 1){
?>
<form method="post" class="ajax_post_bids" action="<?php echo get_ajax_link('bidsform'); ?>">
    <div id="widgetobmentable">
	    <?php //echo exb_xchangetable_widget('CARDRUB', 'OKUSD'); ?>
	</div>
</form>
<?php	
	} 
} 

add_action('myaction_site_widtable', 'def_myaction_ajax_widtable');
function def_myaction_ajax_widtable(){
global $exchangebox;
	
	$log = array();
	$log['status'] = '';
	$log['response'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = __('Error','pn');	
	
	$exchangebox->up_mode();
	
	$sv1 = is_param_post('xname1');
	$sv2 = is_param_post('xname2');

	$log['status'] = 'success';
	$log['table'] = exb_xchangetable_widget($sv1, $sv2);
	
	echo json_encode($log);
	exit;
}

function exb_xchangetable_widget($sv1, $sv2){
global $wpdb, $exchangebox, $naps_id, $naps_data;	
	
	$temp = '';
		
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	$techreg = get_technical_mode(); 
	if($techreg == 0 or $techreg == 2 or $techreg == 3){ /* тех. режим */		
		$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide')); 
		if($gostnaphide != 1 or $gostnaphide == 1 and $user_id){ /* видимость для гостей */	
			$sv1 = is_site_value($sv1);
			$sv2 = is_site_value($sv2);
			if($sv1 and $sv2){
				$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE xname='$sv1'");
				$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE xname='$sv2'");
				if(isset($vd1->id) and isset($vd2->id)){
 					$valut1 = $vd1->id;
					$valut2 = $vd2->id;
					
					$where = get_naps_where('home');
					$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE valsid1='$valut1' AND valsid2='$valut2' AND $where");
					if(isset($naps->id)){		

						$naps_id = $naps->id;
						$naps_data = array();
						$naps_data['item1'] = pn_strip_input($vd1->vname .' '. $vd1->vtype);
						$naps_data['item2'] = pn_strip_input($vd2->vname .' '. $vd2->vtype);
						$naps_data['valut1'] = $vd1->id;
						$naps_data['valut2'] = $vd2->id;
						$naps_data['vd1'] = $vd1;
						$naps_data['vd2'] = $vd2;
						$naps_data['direction'] = $naps;
						$naps_data = (object)$naps_data;
						
						$temp .= '
						<input type="hidden" name="naps_id" class="js_naps_id" value="'. $naps->id .'" />
						<input type="hidden" name="place" value="2" />						
						';
							
						$temp .= '
						<div class="widgetobmen">
							<div class="widgetobmenvn">
								<div class="widgetobmentitle">
									<div class="widgetobmentitlevn">'. __('Data input','pn') .'</div>
								</div>
							';
										
								$temp .= get_the_naprtext1($naps->id, '<div class="wopredblock"><div class="wopredtext"><div class="wopred">'. __('Attention!','pn') .'</div>', '</div></div>');
											
								$temp .= '
								<div class="woblock giving">
									<div class="woblock_ins">
										<div class="wotitle">
											<div class="wotitlevn">'. __('Giving','pn') .' &rarr;</div>
										</div>
											<div class="clear"></div>
									';
									
										$temp .= get_the_napravlennost($vd1,$vd2,$naps,'1','widget');
											
								$temp .= '
									</div>
								</div>
								<div class="woblock getting">
									<div class="woblock_ins">										
										<div class="wotitle">
											<div class="wotitlevn">'. __('Receive','pn') .' &larr;</div>
										</div>
											<div class="clear"></div>
											';
											
										$temp .= get_the_napravlennost($vd1,$vd2,$naps,'2','widget');
											
								$temp .= '
									</div>
								</div>';
								
								$uf = @unserialize($naps->uf);
								if(!is_array($uf)){ $uf = array(); }

								$pers_title = 0;
								foreach($uf as $uf_name => $uf_enable){
									if($uf_enable == 1){
										$pers_title = 1;
									}
								}
								
								$temp .= '
								<div class="woblock persdata">
									<div class="woblock_ins">';
										if($pers_title == 1){
											$temp .= '
											<div class="wotitle">
												<div class="wotitlevn">'. __('Personal data','pn') .'</div>
											</div>
												<div class="clear"></div>
											';
										}
										
										$temp .='
										<div class="wocdblock">
											<div class="wocdblockvn">';	
					
												$temp .= obmenw_pole_function('user_email', $naps, 'widget');
					
												foreach($uf as $uf_name => $uf_enable){
													if($uf_enable == 1){
														$temp .= obmenw_pole_function($uf_name ,$naps, 'widget');
													}
												}	
				
												$temp .= apply_filters('exb_obmwidget_upole','',$naps,$ui);	
												$temp .= apply_filters('exchange_step1','',$naps,$ui);
											
										$temp .='
											</div>
										</div>';
								$temp .='				
									</div>
								</div>										
								';
										
								$temp .= '
								<div class="wosubmit">
									<input type="submit" formtarget="_top" name="" value="'. __('Continue','pn') .'" />
										<div class="clear"></div>
								</div>';
						$temp .= '			
							</div>
						</div>									
						';			

					} else {
						$temp = '<div class="resultfalse">'. __('Error! Direction is available only to registered users','pn') .'</div>';
					}	
				} else {
					$temp = '<div class="resultfalse">'. __('Error! The direction does not exist','pn') .'</div>';
				} 
			} else {
				$temp = '<div class="resultfalse">'. __('Error! system error','pn') .'</div>';
			}
		} else {
			$temp = '<div class="resultfalse">'. __('Error! directions are available only to authorized users','pn') .'</div>';
		}
	}  else {
		$techregtext = get_technical_mode_text();
		$temp = '<div class="resultfalse">'. $techregtext .'</div>';
	}		
	
	return $temp;
}