<?php
if( !defined( 'ABSPATH')){ exit(); }

/* добавляем JS */
add_action('siteplace_js','siteplace_js_exchange_table1');
function siteplace_js_exchange_table1(){ 	
?>	
jQuery(function($){
	$(document).on('click', '.leftgo', function(){
		
	    var id = $(this).attr('id').replace('napobmen-','');
		$('.leftgo, .tabhome').removeClass('act');
		$(this).addClass('act');
		$('#napobmento-'+id).addClass('act');
		
	    return false;
	});	
});			
<?php	
}	
/* end добавляем JS */

add_filter('exchange_table_type1','get_exchange_table1');
function get_exchange_table1($temp){
global $wpdb, $table_exchange;	

	$table_exchange = 1;

	$temp = '';
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	$temp .= apply_filters('exb_before_xchange_form','');
	
	$temp .= '
	<div class="lefthomecol">
		<div class="lefthomecolvn">';
			$temp .= apply_filters('exb_before_xchange_left','');
	
			$where = get_naps_where('home');
			
			$napobmens = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC");
			$naps = array();
			foreach($napobmens as $napob){
				$naps[$napob->valsid1] = $napob;
			}

			$napobmens2 = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder2 ASC");
			$napsto = array();
			foreach($napobmens2 as $napob){
				$napsto[$napob->valsid1][] = $napob;
			}	
		
			$valuts = array();
			$valutsn = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts");
			foreach($valutsn as $valut){
				$valuts[$valut->id] = $valut;
			}		
		
			$r=0;
			$cc = count($naps);
			foreach($naps as $napob){ $r++;
				$valid = $napob->valsid1;
				$cl = array('onehline','leftgo');
				if($r==1){ $cl[] = 'act'; $cl[] = 'first'; } 
				if($r==$cc){ $cl[] = 'last'; } 
				$temp .= '
				<div class="'. join(' ', $cl) .'" id="napobmen-'. $valid .'">
					<div class="onehlug"></div>
					<div class="onehldopug"></div>
					<div class="onehlineico" style="background: url('. is_ssl_url($valuts[$valid]->vlogo) .') no-repeat center center"></div>
					<div class="onehlinetext">
						'. pn_strip_input($valuts[$valid]->vname .' '. $valuts[$valid]->vtype) .'
					</div>
						<div class="clear"></div>
				</div>';
			} 		
	
	$temp .= '
		</div>
	</div>';
	$temp .= '
	<div class="righthomecol">
		<div class="righthomecolvn">';
			$temp .= apply_filters('exb_before_xchange_right','');
	
			$r=0;
			foreach($naps as $napob){ $r++;
				$valid = $napob->valsid1;	
				if($r==1){ $cl='act'; } else { $cl=''; }
				$temp .= '<div class="tabhome '. $cl .'" id="napobmento-'. $valid .'">';
				
					if(isset($napsto[$valid])){
						$fors = $napsto[$valid];
						$nc = count($fors);
						if(is_array($fors) and $nc > 0){
							$s=0;
							foreach($fors as $for){ $s++;
								$nvalid = $for->valsid2;
								$ncl = array('onehrine');
								if($s==1){ $ncl[] = 'first'; } 
								if($s==$nc){ $ncl[] = 'last'; } 								
								$temp .= '
								<a href="'. get_exchange_link($valuts[$valid]->xname, $valuts[$nvalid]->xname) .'" data-xname1="'. pn_strip_input($valuts[$valid]->xname) .'" data-xname2="'. pn_strip_input($valuts[$nvalid]->xname) .'" class="'. join(' ',$ncl) .'">
									<div class="onehrug"></div>
									<div class="onehrdopug"></div>
								';
									
									$temp .= '
									<div class="onehrineico" style="background: url('. is_ssl_url($valuts[$nvalid]->vlogo) .') no-repeat center center"></div>
									<div class="onehrinetext">
										'. pn_strip_input($valuts[$nvalid]->vname .' '. $valuts[$nvalid]->vtype) .'
									</div>
									<div class="onehrinerezerv">
										<div class="zaprosvalutrelvn js_reserv" title="'. esc_attr(pn_strip_input($valuts[$nvalid]->vname .' '. $valuts[$nvalid]->vtype)) .'" data-id="'. $nvalid .'">
											<div class="zaprosvalutrel">
												'. __('I am missing?','pn') .'
											</div>
										</div>
										'. is_out_sum(get_valut_reserv($valuts[$nvalid]), 12, 'reserv') .'
									</div>';
						
								$temp .= '
									<div class="clear"></div>
								</a>';
							}
						} 
					}
					
				$temp .= '</div>';			
			}		
	
	$temp .= '
		</div>
	</div>
		<div class="clear"></div>';
	
	$temp .= apply_filters('exb_after_xchange_form','');
	return $temp;
}

add_filter('get_naps_where', 'get_naps_where_home', 10, 2);
function get_naps_where_home($where, $place){
	if($place == 'home' or $place == 'exchange'){
		$ui = wp_get_current_user();
		$user_id = intval($ui->ID);
		if(!$user_id){ $where .= "AND hidegost IN('0','2') "; }
	}
	return $where;
}