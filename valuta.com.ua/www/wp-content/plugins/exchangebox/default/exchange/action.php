<?php
if( !defined( 'ABSPATH')){ exit(); }

/* добавляем JS */  
add_action('siteplace_js','siteplace_js_exchange_action');
function siteplace_js_exchange_action(){
?>	
/* exchange table */
jQuery(function($){
	
    $(document).on('click', 'input', function(){
        $(this).parents('.changewoerr').removeClass('err');
    });
    $(document).on('keyup', 'input', function(){
        $(this).parents('.changewoerr').removeClass('err');
    });		
	
function add_error_in(id, text){
	if(text.length > 0){
		id.parents('.changewoerr').addClass('err');
		id.parents('.changewoerr').find('.wowarnerr').html(text);
	}
}		

    $('.ajax_post_bids').ajaxForm({
        dataType: 'json',
		beforeSubmit: function(a,f,o) {
			f.addClass('thisactive');
			$('.thisactive input[type=submit]').attr('disabled',true);
			$('.ajax_post_bids_res').html('<div class="resulttrue"><?php echo esc_attr(__('Please wait while the exchange','pn')); ?></div>');
        },	
		error: function(res, res2, res3) {
			<?php do_action('pn_js_error_response', 'form'); ?>
		},		
        success: function(res) {
			    if(res['status'] == 'success'){
				    window.location.href = res['url']; 
			    } else if(res['status']== 'error'){
					if(res['errors']){
						$.each(res['errors'], function(index, value){
							add_error_in($('#' + index), value);
						});	
					}
					
					alert(res['status_text']); 
					
			    } else {
				    alert(res['status_text']);
				}
				
				<?php do_action('ajax_post_form_jsresult'); ?>
				
		    $('.thisactive input[type=submit]').attr('disabled',false);
			$('.thisactive').removeClass('thisactive');				
        }
    });					
	
});
/* end exchange table */	
<?php	
}	
/* end добавляем JS */

/* bids add */
add_action('myaction_site_bidsform', 'def_myaction_ajax_bidsform');
function def_myaction_ajax_bidsform(){
global $wpdb, $exchangebox;	
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	$log = array();
	$log['status'] = '';
	$log['response'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = '';	

	$naps_id = intval(is_param_post('naps_id'));
	
	$log['errors'] = array();
	
	$exchangebox->up_mode();
	
	$log = apply_filters('before_ajax_form_field', $log, 'exchangeform');
	$log = apply_filters('before_ajax_bidsform', $log, $naps_id);
	
	if(!$naps_id){
		$log['status'] = 'error';
		$log['status_code'] = 1; 
		$log['status_text'] = __('Error! The direction does not exist','pn');
		echo json_encode($log);
		exit;		
	}
	
	$techreg = get_technical_mode(); 
	if($techreg != 0){
		$techregtext = get_technical_mode_text();
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = $techregtext;
		echo json_encode($log);
		exit;		
	}
	
	$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide')); 
	if($gostnaphide == 1 and !$user_id){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! directions are available only to authorized users','pn');
		echo json_encode($log);
		exit;		
	}	
	
	$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE status='1' AND id='$naps_id'");
	if(!isset($naps->id)){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! The direction does not exist','pn');
		echo json_encode($log);
		exit;		
	}
	
	do_action('after_naps_exchange', $naps, $log); /* не убирать, для верификации */
	
	$valut1_id = intval($naps->valsid1);
	$valut2_id = intval($naps->valsid2);
	
	$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1_id'");
	$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut2_id'");

	if(!isset($vd1->id) or !isset($vd2->id)){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! The direction does not exist','pn');
		echo json_encode($log);
		exit;		
	}	
	
	$post_sum = is_sum(is_param_post('summ1'));
	
	$error = 0;
	$narr = array();
	$auto_data = array();
	
	$auto_data['account1'] = get_purse(is_param_post('wschet1'),$vd1);
	$showed = intval($vd1->show1);
	$showed = apply_filters('exb_obmen_showed',$showed,$naps,$vd1,1);
	if($showed == 1){	
		if(!$auto_data['account1']){
			$error = $error+1;
			$log['errors']['wschet1'] = __('invalid account number','pn');
		}
	}
	
	$auto_data['account2'] = get_purse(is_param_post('wschet2'),$vd2);
	$showed = intval($vd2->show2);
	$showed = apply_filters('exb_obmen_showed',$showed,$naps,$vd2,2);
	if($showed == 1){	
		if(!$auto_data['account2']){
			$error = $error+1;
			$log['errors']['wschet2'] = __('invalid account number','pn');
		}
	}
	
	$uf = @unserialize($naps->uf);
	if(!is_array($uf)){ $uf = array(); }
	
	$now_show = intval(is_isset($uf,'last_name')); 
	$now_show = apply_filters('exb_upole', $now_show, 'last_name',$naps);
	if($now_show){	
		$auto_data['last_name'] = pn_maxf_mb(pn_strip_input(get_caps_name(is_param_post('last_name'))),150); 
		if(mb_strlen($auto_data['last_name']) < 1){
			$error = $error+1;
			$log['errors']['exc_last_name'] = __('field filled not','pn');
		}
	}
	
	$now_show = intval(is_isset($uf,'first_name'));
	$now_show = apply_filters('exb_upole', $now_show, 'first_name',$naps);
	if($now_show){	
		$auto_data['first_name'] = pn_maxf_mb(pn_strip_input(get_caps_name(is_param_post('first_name'))),150);
		if(mb_strlen($auto_data['first_name']) < 1){
			$error = $error+1;
			$log['errors']['exc_first_name'] = __('field filled not','pn');
		}
	}

	$now_show = intval(is_isset($uf,'second_name'));  
	$now_show = apply_filters('exb_upole', $now_show, 'second_name',$naps);
	if($now_show){	
		$auto_data['second_name'] = pn_maxf_mb(pn_strip_input(get_caps_name(is_param_post('second_name'))),150);
		if(mb_strlen($auto_data['second_name']) < 1){
			$error = $error+1;
			$log['errors']['exc_second_name'] = __('field filled not','pn');
		} 
	}

	$auto_data['user_email'] = is_email(is_param_post('user_email'));
	if(!$auto_data['user_email']){
		$error = $error+1;
		$log['errors']['exc_user_email'] = __('field filled not','pn');
	}	
	
	$now_show = intval(is_isset($uf,'user_phone'));  
	$now_show = apply_filters('exb_upole', $now_show, 'user_phone',$naps);
	if($now_show){	
		$auto_data['user_phone'] = pn_maxf_mb(pn_strip_input(is_param_post('user_phone')),150);
		if(mb_strlen($auto_data['user_phone']) < 1){
			$error = $error+1;
			$log['errors']['exc_user_phone'] = __('field filled not','pn');
		}	
	}	
	
	$now_show = intval(is_isset($uf,'user_skype')); 
	$now_show = apply_filters('exb_upole', $now_show, 'user_skype',$naps);
	if($now_show){	
		$auto_data['user_skype'] = pn_maxf_mb(pn_strip_input(is_param_post('user_skype')),150);
		if(mb_strlen($auto_data['user_skype']) < 1){
			$error = $error+1;
			$log['errors']['exc_user_skype'] = __('field filled not','pn');
		}	
	}

	$now_show = intval(is_isset($uf,'user_passport'));  
	$now_show = apply_filters('exb_upole', $now_show, 'user_passport',$naps);
	if($now_show){	
		$auto_data['user_passport'] = pn_maxf_mb(pn_strip_input(is_param_post('user_passport')),150);
		if(mb_strlen($auto_data['user_passport']) < 1){
			$error = $error+1;
			$log['errors']['exc_user_passport'] = __('field filled not','pn');
		}	
	}	
	
	$checks = $exchangebox->get_option('blacklist','check');
	if(!is_array($checks)){ $checks = array(); }
	
	if($auto_data['account1'] and in_array(0, $checks)){
		$ccr = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."blacklist WHERE meta_value='{$auto_data['account1']}' AND meta_key='0'");
		if($ccr > 0){
			$error = $error+1;
			$log['errors']['wschet1'] = __('by blacklisted','pn');
		}
	}

	if($auto_data['account2'] and in_array(1, $checks)){
		$ccr = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."blacklist WHERE meta_value='{$auto_data['account2']}' AND meta_key='0'");
		if($ccr > 0){
			$error = $error+1;
			$log['errors']['wschet2'] = __('by blacklisted','pn');
		}
	}

	if($auto_data['user_email'] and in_array(4, $checks)){
		$value_arr = explode('@', $auto_data['user_email']);
		$domen = '@' . trim(is_isset($value_arr, 1));
		$ccr = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."blacklist WHERE meta_value='{$auto_data['user_email']}' AND meta_key='1' OR meta_value='$domen' AND meta_key='1'");
		if($ccr > 0){
			$error = $error+1;
			$log['errors']['exc_user_email'] = __('by blacklisted','pn');
		}
	}

	if(isset($auto_data['user_phone']) and $auto_data['user_phone'] and in_array(2, $checks)){
		$bltel = str_replace('+','',$auto_data['user_phone']);
		$ccr = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."blacklist WHERE meta_value='{$auto_data['user_phone']}' AND meta_key='2'");
		if($ccr > 0){
			$error = $error+1;
			$log['errors']['exc_user_phone'] = __('by blacklisted','pn');
		}	
	}

	if(isset($auto_data['user_skype']) and $auto_data['user_skype'] and in_array(3, $checks)){
		$ccr = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."blacklist WHERE meta_value='{$auto_data['user_skype']}' AND meta_key='3'");
		if($ccr > 0){
			$error = $error+1;
			$log['errors']['exc_user_skype'] = __('by blacklisted','pn');
		}	
	}
	
	$data = get_calc_data($vd1, $vd2, $naps, $user_id, $post_sum, 1);
	
	$curs1 = $data['curs1'];
	$curs2 = $data['curs2'];
	
	$summ1 = $data['summ1'];
	$summ1c = $data['summ1c'];
	$summ2 = $data['summ2'];
	$summ2c = $data['summ2c'];	
	$user_skidka = $data['user_discount'];
	$us_sk_v = $data['user_sksumm'];

	$exsum = is_sum(convert_sum($summ1, $vd1->vtype ,cur_type()));
	
	/* максимум и минимум */
	$min1 = get_min_sum_to_naps_give($naps, $vd1);
	$max1 = get_max_sum_to_naps_give($naps, $vd1);
	/* if($min1 > $max1 and is_numeric($max1)){ $min1 = $max1; } */

	$min2 = get_min_sum_to_naps_get($naps, $vd2);
	$max2 = get_max_sum_to_naps_get($naps, $vd2);
	/* if($min2 > $max2 and is_numeric($max2)){ $min2 = $max2; } */		
		
	if($summ1 < $min1){
		$error = $error+1;
		$log['errors']['wsumm1'] = __('min','pn').'.: '. $min1 .' '. pn_strip_input($vd1->vtype);													
	}						
	if($summ1 > $max1 and is_numeric($max1)){
		$error = $error+1;
		$log['errors']['wsumm1'] = __('max','pn').'.: '. $max1 .' '. pn_strip_input($vd1->vtype);													
	}						
	if($summ2 < $min2){
		$error = $error+1;
		$log['errors']['wsumm2'] = __('min','pn').'.: '. $min2 .' '. pn_strip_input($vd2->vtype);													
	}							
	if($summ2 > $max2 and is_numeric($max2)){
		$error = $error+1;
		$log['errors']['wsumm2'] = __('max','pn').'.: '. $max2 .' '. pn_strip_input($vd2->vtype);													
	}								
	if($summ1 <= 0){
		$error = $error+1;
		$log['errors']['wsumm1'] = __('min','pn').'.: '. $min1 .' '. pn_strip_input($vd1->vtype);
	}							
	if($summ2 <= 0){
		$error = $error+1;
		$log['errors']['wsumm2'] = __('min','pn').'.: '. $min2 .' '. pn_strip_input($vd2->vtype);
	}								
	/* end максимум и минимум */
	
	/* реферал */
	$ref_id = 0;
	$partpr = 0;
	$summp = 0;
	$p_sum = 0;
	
	if($exchangebox->get_option('partners','status') == 1){
		$p_sum = is_sum(convert_sum($summ2c, $vd2->vtype ,cur_type()));
		$p_enable = $naps->parthide;
		if($p_enable == 0){
			if($user_id){
				if(isset($ui->ref_id)){
					$ref_id = $ui->ref_id;
				}	
			} else {
				$ref_id = intval(get_pn_cookie('ref_id')); 
			}
			if($ref_id){
				$ref_cou = $wpdb->get_var("SELECT COUNT(ID) FROM ". $wpdb->prefix ."users WHERE ID='$ref_id'");
				if($ref_cou > 0){
					$partpr = get_user_pers_refobmen($ref_id);
					$p_max = is_sum($naps->partmax);
					if($p_max > 0 and $partpr > $p_max){ $partpr = $p_max; }
					if($partpr > 0 and $p_sum > 0){
						$summp = $p_sum / 100 * $partpr;
						$summp = is_sum($summp);
					}
				} else {
					$ref_id = 0;
				}
			}  
		}
	}
	/* end реферал */
	
	/* дополнительные поля */
	$dmetas = array(); 
	$dmetas[1] = $dmetas[2] = array();
	
	$doppoles = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."custom_fields_valut WHERE valut_id='$valut1_id' AND status='1' AND place_id IN('0','1') OR valut_id='$valut2_id' AND status='1' AND place_id IN('0','2') ORDER BY cf_order ASC");
	foreach($doppoles as $dp_item){
		$dp_id = $dp_item->id;
		$dp_vid = pn_strip_input($dp_item->vid);
		$dp_name = pn_strip_input($dp_item->cf_name);
		$dp_req = pn_strip_input($dp_item->cf_req);
		$dp_hidden = pn_strip_input($dp_item->cf_hidden);
		$dp_value = pn_maxf_mb(pn_strip_input(is_param_post('cfc'.$dp_id)),500);
		
		if($dp_vid == 0){
			$dp_value = get_cf_field($dp_value,$dp_item);
		} else {
			$dp_value = intval($dp_value);
		}
		
		$place_id = 1;
		if($dp_item->valut_id == $valut2_id){
			$place_id = 2;
		}
		
		if($dp_vid == 0){	
			$dmetas[$place_id][] = array(
				'title' => $dp_name,
				'data' => $dp_value,
				'hidden' => $dp_hidden,
			);	
			if(!$dp_value and $dp_req == 1){
				$error = $error+1;
				$log['errors']['doppole'.$dp_id] = __('Error!','pn');
			}	
		} else { /* select */
			$dp_datas = explode("\n",pn_strip_input($dp_item->datas));
			foreach($dp_datas as $key => $da){
				$da = trim($da);
				if($da){
					if($key == $dp_value){
						$dmetas[$place_id][] = array(
							'title' => $dp_name,
							'data' => pn_strip_input($da),
							'hidden' => $dp_hidden,
						);
					}		
				}
			}	
		}
	}	
	/* end доп.поля */

	/* проверки на обмен */	
	if($naps->hidegost == 1 and !$user_id or $naps->hidegost == 2 and !$user_id){
		$error = $error+1;
		$log['status_text'] = __('Error! directions are available only to authorized users','pn');		
	}
	
	$user_ip = pn_real_ip();
	
	if(in_array(5, $checks)){
		$blacklist = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."blacklist WHERE meta_value='$user_ip' AND meta_key='4'");
		if($blacklist > 0){
			$error = $error+1;
			$log['status_text'] = __('Error! For your exchange denied','pn');	
		}	
	}
	
	$newdataprov = array();
	$newdataprov['error'] = $error;
	$newdataprov['log'] = $log;
	$newdataprov = apply_filters('exb_obmen_newdataprov', $newdataprov, $naps, $vd1, $vd2, $summ1, $summ2, $summ1c, $summ2c, $auto_data);
	$error = $error + $newdataprov['error'];
	$log = $newdataprov['log'];
	/* end проверки */
	
	if(count($log['errors']) > 0 and strlen($log['status_text']) < 1){
		$log['status_text'] = __('Error!', 'pn');
	}
	
	$hashed = unique_bid_hashed();
	
	if($error > 0){
		
		$log['status'] = 'error';
		$log['status_code'] = 1;
		
	} else {
		
		$array = array();
		$array['user_id'] = intval($user_id);
		$array['user_skidka'] = $user_skidka;
		$array['user_sksumm'] = $us_sk_v;
		$array['fname'] = is_isset($auto_data,'last_name'); 
		$array['iname'] = is_isset($auto_data,'first_name');
		$array['oname'] = is_isset($auto_data,'second_name');
		$array['email'] = is_isset($auto_data,'user_email');
		$array['tel'] = is_isset($auto_data,'user_phone');
		$array['skype'] = is_isset($auto_data,'user_skype');
		$array['pnomer'] = is_isset($auto_data,'user_passport');
		$array['userip'] = $user_ip;
		$array['schet1'] = is_isset($auto_data,'account1');
		$array['schet2'] = is_isset($auto_data,'account2');
		$array['hashed'] = $hashed;
		$array['valut1'] = pn_strip_input($vd1->vname);
		$array['valut2'] = pn_strip_input($vd2->vname);
		$array['valut1i'] = $vd1->id;
		$array['valut2i'] = $vd2->id;			
		$array['valut1type'] = pn_strip_input($vd1->vtype);
		$array['valut2type'] = pn_strip_input($vd2->vtype);		
		$array['curs1'] = $curs1;
		$array['curs2'] = $curs2;
		$array['summ1'] = $summ1;
		$array['summ2'] = $summ2;
		$array['summz1'] = $summ1c;
		$array['summz2'] = $summ2c;
		$array['exsum'] = $exsum;
		$array['tpersent1'] = pn_strip_input($naps->prsum1);
		$array['tpersent2'] = pn_strip_input($naps->prsum2);
		$array['tcommis1'] = pn_strip_input($naps->prproc1);
		$array['tcommis2'] = pn_strip_input($naps->prproc2);
		$array['naprid'] = $naps->id;		
		$array['ref_id'] = $ref_id;
		$array['parthide'] = pn_strip_input($naps->parthide);		
		$array['partmax'] = pn_strip_input($naps->partmax);
		$array['partpr'] = $partpr;
		$array['ref_sum'] = $summp;
		$array['p_sum'] = $p_sum;
		$array['pcalc'] = 0;
		$array['tdate'] = current_time('mysql');
		$array['dmetas'] = serialize($dmetas);
		$array['status'] = 'auto';
		$array = apply_filters('exb_obmen_arraydata', $array);
				
		$wpdb->insert($wpdb->prefix.'bids', $array);
		$obmen_id = $wpdb->insert_id;
		if($obmen_id > 0){
			$obmen = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$obmen_id' AND status='auto'");
			if(isset($obmen->id)){
				do_action('change_bidstatus_all', 'auto',  $obmen->id, $obmen, 'site', 'user');
				do_action('change_bidstatus_auto', $obmen->id, $obmen, 'site', 'user');
				$log['url'] = get_bids_url($hashed);
				$log['status'] = 'success';
				$log['status_text'] = __('Bid successfully created','pn');
			}
		} else {
			$log['status'] = 'error';
			$log['status_code'] = 1;
			$log['status_text'] = __('Error! System error','pn');
		}
		
	}	
	
	echo json_encode($log);
	exit;
}

/* bids add */
add_action('myaction_site_createbids', 'def_myaction_ajax_createbids');
function def_myaction_ajax_createbids(){
global $wpdb, $exchangebox;	
	
	$log = array();
	$log['status'] = '';
	$log['response'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = __('Error','pn');	

	$hashed = is_bid_hash(is_param_post('hash'));
	
	if(!$hashed){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! System error','pn');
		echo json_encode($log);
		exit;			
	}
	
	$obmen = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed' AND status='auto'");
	if(!isset($obmen->id)){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! System error','pn');
		echo json_encode($log);
		exit;		
	}
	
	$valut1i = intval($obmen->valut1i);
	$valut2i = intval($obmen->valut2i);
	
	$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1i'");
	$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut2i'");

	if(!isset($vd1->id) or !isset($vd2->id)){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! System error','pn');
		echo json_encode($log);
		exit;		
	}

	$naps_id = intval($obmen->naprid);
	
	$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE status='1' AND id='$naps_id'");
	if(!isset($naps->id)){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! The direction does not exist','pn');
		echo json_encode($log);
		exit;			
	}	
	
	/* максимум и минимум */
	$max1 = get_max_sum_to_naps_give($naps, $vd1);
	$max2 = get_max_sum_to_naps_get($naps, $vd2);		
	$summ1 = pn_strip_input($obmen->summ1);
	$summ2 = pn_strip_input($obmen->summ2);
							
	if($summ1 > $max1 and is_numeric($max1) or $summ2 > $max2 and is_numeric($max2)){
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('Error! Not enough reserve for the transaction','pn');
		echo json_encode($log);
		exit;													
	}							
	/* end максимум и минимум */
	
	/* secure */
	$obmen_data = array();
	foreach($obmen as $key => $value){
		$obmen_data[$key] = $value;
	}
	
	$my_dir = wp_upload_dir();
	$dir = $my_dir['basedir'].'/bids/';
	if(!is_dir($dir)){
		@mkdir($dir, 0777);
	}
				
	$htacces = $dir.'.htaccess';
	if(!is_file($htacces)){
		$nhtaccess = "Order allow,deny \n Deny from all";
		$file_open = @fopen($htacces, 'w');
		@fwrite($file_open, $nhtaccess);
		@fclose($file_open);		
	}
				
	$file = $dir . $obmen->id .'.txt';
	$file_data = serialize($obmen_data);
	$file_open = @fopen($file, 'w');
	@fwrite($file_open, $file_data);
	@fclose($file_open);
	/* end secure */			
	
	$account1 = pn_strip_input($obmen->schet1);
	$account2 = pn_strip_input($obmen->schet2);
	$array = array();
	$array['tdate'] = current_time('mysql');
	$array['schet1_hash'] = pn_crypt_data($account1);
	$array['schet2_hash'] = pn_crypt_data($account2);
	$array['status'] = 'new';
	$array['user_hash'] = get_user_hash();
	
	$wpdb->update($wpdb->prefix.'bids', $array, array('id'=>$obmen->id));
	
	do_action('change_bidstatus_all', 'new',  $obmen->id, $obmen, 'site', 'user'); 
	do_action('change_bidstatus_new', $obmen->id, $obmen, 'site', 'user');

	$obmen_id = $obmen->id;
	
	/* проверка на новичка */
	$check_new_user = get_option('check_new_user');
	if(!is_array($check_new_user)){ $check_new_user = array(); }	
	
	$new = 0;
	$user_phone = pn_strip_input($obmen->tel);
	$user_skype = pn_strip_input($obmen->skype);
	$user_email = pn_strip_input($obmen->email);
	if($account1 and in_array(0,$check_new_user)){
		$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE schet1='$account1' AND id != '$obmen_id' AND status = 'success'");
		if($cc == 0){ $new = 1; }
	}
	if($account2 and $new == 0 and in_array(1,$check_new_user)){
		$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE schet2='$account2' AND id != '$obmen_id' AND status = 'success'");
		if($cc == 0){ $new = 1; }
	}
	if($user_phone and $new == 0 and in_array(2,$check_new_user)){
		$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE tel='$user_phone' AND id != '$obmen_id' AND status = 'success'");
		if($cc == 0){ $new = 1; }
	}
	if($user_skype and $new == 0 and in_array(3,$check_new_user)){
		$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE skype='$user_skype' AND id != '$obmen_id' AND status = 'success'");
		if($cc == 0){ $new = 1; }
	}
	if($user_email and $new == 0 and in_array(4,$check_new_user)){
		$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE email='$user_email' AND id != '$obmen_id' AND status = 'success'");
		if($cc == 0){ $new = 1; }
	}	
			
	update_bids_meta($obmen_id, 'new', $new);
	/* end проверка на новичка */
	
	$device = apply_filters('device_bids', 0);
	update_bids_meta($obmen_id, 'mobile', $device);

	$log['url'] = get_bids_url($obmen->hashed);
	$log['status'] = 'success';
	$log['status_text'] = __('Application successfully created','pn');		
	
	echo json_encode($log);
	exit;
}

/* bids cancel */
add_action('myaction_site_deletebids', 'def_myaction_ajax_deletebids');
function def_myaction_ajax_deletebids(){
global $wpdb;	
	
	$hashed = is_bid_hash(is_param_post('hash'));
	if($hashed){
		$obmen = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed'");
		if(isset($obmen->id)){
			if($obmen->status == 'new'){
				if(is_true_userhash($obmen)){
					$wpdb->update($wpdb->prefix.'bids', array('status'=>'delete','editdate'=>current_time('mysql')), array('id'=>$obmen->id));
					do_action('change_bidstatus_all', 'delete', $obmen->id, $obmen, 'site', 'user');
					do_action('change_bidstatus_delete', $obmen->id, $obmen, 'site', 'user');
				}
			}
		}
	} 
		$url = get_bids_url($hashed);
		wp_redirect($url);
		exit;		
}

/* bids payed */
add_action('myaction_site_payedbids', 'def_myaction_ajax_payedbids');
function def_myaction_ajax_payedbids(){
global $wpdb;	
	
	$hashed = is_bid_hash(is_param_post('hash'));
	if($hashed){
		$obmen = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed'");
		if(isset($obmen->id)){
			if($obmen->status == 'new'){
				if(is_true_userhash($obmen)){
					$wpdb->update($wpdb->prefix.'bids', array('status'=>'payed','editdate'=>current_time('mysql')), array('id'=>$obmen->id));
					do_action('change_bidstatus_all', 'payed', $obmen->id, $obmen, 'site', 'user');
					do_action('change_bidstatus_payed', $obmen->id, $obmen, 'site', 'user');
				}	
			}
		}
	} 
		$url = get_bids_url($hashed);
		wp_redirect($url);
		exit;		
}