<?php
if( !defined( 'ABSPATH')){ exit(); }
	
add_action('siteplace_js','siteplace_js_exchange_changes');
function siteplace_js_exchange_changes(){
?>	
/* exchange table */
jQuery(function($){
	
	function checknumbr(mixed_var) {
		return ( mixed_var == '' ) ? false : !isNaN( mixed_var );
	}		
	
	function goed_peremen(sum, dej){
		var id = $('.js_naps_id').val();
		
		var param = 'id='+id+'&sum='+sum+'&dej='+dej;
        $.ajax({
            type: "POST",
            url: "<?php echo get_ajax_link('exchange_peremen');?>",
            data: param,
	        dataType: 'json',
 			error: function(res, res2, res3){
				<?php do_action('pn_js_error_response', 'ajax'); ?>
			},			
            success: function(res){
                
				if(dej !== 1 && res['s1']){
					$('#wsumm1').val(res['s1']);
				}
				if(dej !== 2 && res['s2']){
					$('#wsumm2').val(res['s2']);
				}
				if(dej !== 3 && res['sc1']){
					$('#wsummcom1').val(res['sc1']);
				}
				if(dej !== 4 && res['sc2']){
					$('#wsummcom2').val(res['sc2']);
				}
				
				if(res['sctxt1']){
					$('#wcomtext-1').html(res['sctxt1']);
				}
				if(res['sctxt2']){
					$('#wcomtext-2').html(res['sctxt2']);
				}	
				
				if(res['summ1_error'] && res['summ1_error'] == 1){
					$('#wsumm1').parents('.changewoerr').addClass('err');
					$('#wsumm1').parents('.changewoerr').find('.wowarnerr').html(res['summ1_error_text']);
				} else {
					$('#wsumm1').parents('.changewoerr').removeClass('err');					
				}
				if(res['summ2_error'] && res['summ2_error'] == 1){
					$('#wsumm2').parents('.changewoerr').addClass('err');
					$('#wsumm2').parents('.changewoerr').find('.wowarnerr').html(res['summ2_error_text']);
				} else {
					$('#wsumm2').parents('.changewoerr').removeClass('err');
				}
				if(res['summ1c_error'] && res['summ1c_error'] == 1){
					$('#wsummcom1').parents('.changewoerr').addClass('err');
					$('#wsummcom1').parents('.changewoerr').find('.wowarnerr').html(res['summ1c_error_text']);
				} else {
					$('#wsummcom1').parents('.changewoerr').removeClass('err');
				}
				if(res['summ2c_error'] && res['summ2c_error'] == 1){
					$('#wsummcom2').parents('.changewoerr').addClass('err');
					$('#wsummcom2').parents('.changewoerr').find('.wowarnerr').html(res['summ2c_error_text']);
				} else {
					$('#wsummcom2').parents('.changewoerr').removeClass('err');
				}
				
            }
		});					    
		
	}
	
		$(document).on('keyup', '#wsumm1', function(){
			var vale = $(this).val().replace(/,/g,'.');
			if (checknumbr(vale)) {
				goed_peremen(vale, 1);
			} else {
				$(this).parents('.changewoerr').addClass('err');
			}

		    return false;
		});
		$(document).on('keyup', '#wsumm2', function(){
			var vale = $(this).val().replace(/,/g,'.');
			if (checknumbr(vale)) {
				goed_peremen(vale, 2);
			} else {
				$(this).parents('.changewoerr').addClass('err');
			}

		    return false;
		});	
		$(document).on('keyup', '#wsummcom1', function(){
			var vale = $(this).val().replace(/,/g,'.');
			if (checknumbr(vale)) {
				goed_peremen(vale, 3);
			} else {
				$(this).parents('.changewoerr').addClass('err');
			}

		    return false;
		});
		$(document).on('keyup', '#wsummcom2', function(){
			var vale = $(this).val().replace(/,/g,'.');
			if (checknumbr(vale)) {
				goed_peremen(vale, 4);
			} else {
				$(this).parents('.changewoerr').addClass('err');
			}

		    return false;
		});	
	
});
<?php
}	

add_action('myaction_site_exchange_peremen', 'def_myaction_ajax_exchange_peremen');
function def_myaction_ajax_exchange_peremen(){
global $wpdb, $exchangebox;	
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	$log = array();
	$log['status'] = '';
	$log['response'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = __('Error','pn');	
	
	$exchangebox->up_mode();

	$comis_text1 = '';
	$comis_text2 = '';
	$summ1_error = $summ2_error = $summ1c_error = $summ2c_error = 0;
	$summ1_error_text = $summ2_error_text = $summ1c_error_text = $summ2c_error_text = '';
	
	$summ1 = 0;
	$summ1c = 0;
	$summ2 = 0;
	$summ2c = 0;
	
	$naps_id = intval(is_param_post('id'));
	$sum = is_sum(is_param_post('sum'));
	$dej = intval(is_param_post('dej'));
	$techreg = get_technical_mode(); 
	if($techreg == 0 or $techreg == 2 or $techreg == 3){
		$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide'));
		if($gostnaphide != 1 or $gostnaphide == 1 and $user_id){ 
			if($dej > 0 or $dej < 5){ 
				$naps = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."napobmens WHERE status='1' AND id='$naps_id'");
				if(isset($naps->id)){
					$valut1 = pn_strip_input($naps->valsid1);
					$valut2 = pn_strip_input($naps->valsid2);
					$vd1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1'");
					$vd2 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut2'");
					if(isset($vd1->id) and isset($vd2->id)){
						if($sum > 0){
							
							$cdata = get_calc_data($vd1, $vd2, $naps, $user_id, $sum, $dej);
						
							$vtype1 = $cdata['vtype1'];
							$vtype2 = $cdata['vtype2'];
							$summ1 = $cdata['summ1'];
							$summ1c = $cdata['summ1c'];
							$summ2 = $cdata['summ2'];
							$summ2c = $cdata['summ2c'];
							$comis_text1 = $cdata['comis_text1'];
							$comis_text2 = $cdata['comis_text2'];					
													
							$min1 = get_min_sum_to_naps_give($naps, $vd1);
							$max1 = get_max_sum_to_naps_give($naps, $vd1);
							/* if($min1 > $max1 and is_numeric($max1)){ $min1 = $max1; } */

							$min2 = get_min_sum_to_naps_get($naps, $vd2);
							$max2 = get_max_sum_to_naps_get($naps, $vd2);
							/* if($min2 > $max2 and is_numeric($max2)){ $min2 = $max2; } */							
								
								if($summ1 < $min1){
									$summ1_error = 1;
									$summ1_error_text = __('min','pn').'.: '. $min1 .' '.$vtype1;													
								}
								
								if($summ1 > $max1 and is_numeric($max1)){
									$summ1_error = 1;
									$summ1_error_text = __('max','pn').'.: '. $max1 .' '.$vtype1;													
								}
								
								if($summ2 < $min2){
									$summ2_error = 1;
									$summ2_error_text = __('min','pn').'.: '. $min2 .' '.$vtype2;													
								}
								
								if($summ2 > $max2 and is_numeric($max2)){
									$summ2_error = 1;
									$summ2_error_text = __('max','pn').'.: '. $max2 .' '.$vtype2;													
								}								
						
						}
								if($summ1 <= 0){
									$summ1_error = 1;
								}							
								if($summ2 <= 0){
									$summ2_error = 1;
								}						
								if($summ1c <= 0){
									$summ1c_error = 1;
								}							
								if($summ2c <= 0){
									$summ2c_error = 1;
								}
					}
				}
			}
		}
	}
	
	$log['s1'] = $summ1;
	$log['s2'] = $summ2;
	$log['sc1'] = $summ1c;
	$log['sc2'] = $summ2c;
	$log['sctxt1'] = $comis_text1;
	$log['sctxt2'] = $comis_text2;
	$log['summ1_error'] = $summ1_error;
	$log['summ1_error_text'] = $summ1_error_text;
	$log['summ2_error'] = $summ2_error;
	$log['summ2_error_text'] = $summ2_error_text;
	$log['summ1c_error'] = $summ1c_error;
	$log['summ1c_error_text'] = $summ1c_error_text;
	$log['summ2c_error'] = $summ2c_error;
	$log['summ2c_error_text'] = $summ2c_error_text;
	echo json_encode($log);
	exit;
}