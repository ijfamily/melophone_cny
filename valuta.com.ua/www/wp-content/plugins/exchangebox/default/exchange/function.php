<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('siteplace_js','siteplace_js_exchange_toolinput');
function siteplace_js_exchange_toolinput(){
	if(function_exists('is_mobile') and !is_mobile() or !function_exists('is_mobile')){
?>	
jQuery(function($){
    $(document).on('mouseenter', '.toolinput', function(){
        $(this).parents('.parenttool').find('.exchboxtool').slideDown(200);
    });	
    $(document).on('mouseleave', '.toolinput', function(){
        $(this).parents('.parenttool').find('.exchboxtool').hide();
    });
});
<?php
	}
} 

function get_persdata_block($naps){
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	$uf = @unserialize($naps->uf);
	if(!is_array($uf)){ $uf = array(); }
	
	$temp = '
	<table class="obmentable">';
		$temp .= obmenw_pole_function('user_email',__('E-mail','pn'), $naps);
					
		foreach($uf as $uf_name => $uf_enable){
			if($uf_enable == 1){
				$temp .= obmenw_pole_function($uf_name,$naps);
			}
		}					
				
		$temp .= apply_filters('exb_obmsite_upole','',$naps,$ui);			
						
		$temp .= '<tr><td class="tdfirst"></td><td></td><td>'. apply_filters('exchange_step1','').'</td></tr>';	

	$temp .= '</table>';	
	
	return $temp;
}

function obmenw_pole_function($id, $naps, $place='shortcode'){
global $exchangebox;

	$temp = '';
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	$fields = array(
		'last_name' => __('Last name','pn'),
		'first_name' => __('First name','pn'),
		'second_name' => __('Second name','pn'),
		'user_phone' => __('Phone','pn'),
		'user_passport' => __('Passport','pn'),
		'user_skype' => __('Skype','pn'),
		'user_email' => __('E-mail','pn'),
	);	

	$show = apply_filters('exb_upole', 1, $id,$naps);
	if($show){
	
		$help = pn_strip_text($exchangebox->get_option('help',$id));
		$toolhelp = '';
		if($help){
			$toolhelp = '
				<div class="exchboxtool">
					<div class="exchboxtoolvnug"></div>
					<div class="exchboxtooltext">
						'. $help .'
					</div>
				</div>
			';
		}
		
		$title = is_isset($fields, $id);
		
		$default = pn_strip_input(is_isset($ui, $id));

		if($place == 'widget'){
			$temp .='
			<div class="wolineadres parenttool changewoerr">
				<div class="wolineinput">
					<input type="text" name="'. $id .'" id="exc_'. $id .'" class="toolinput" placeholder="'. pn_strip_input($title) .'" value="'. pn_strip_input($default) .'" />
						<div class="wowarnerr"></div>
				</div>
				'. $toolhelp .'
			</div>
			';
		} else {
			$temp .='
			<tr>
				<td class="tdfirst"></td>
				<td class="tdobmtitle">'. $title .':</td>
				<td>
				<div class="parenttool changewoerr">
					<div class="wolineinputtab">
						<input type="text" name="'. $id .'" id="exc_'. $id .'" class="toolinput" value="'. pn_strip_input($default) .'" />
							<div class="wowarnerr"></div>
					</div>
					'. $toolhelp .'
				</div>
				</td>
			</tr>	
			';		
		}
	
	}
	
    return $temp;
}

function get_the_naprtext1($napsid, $before='', $after=''){
global $wpdb;

	$napsid = intval($napsid);

	$temp = '';
	
	$text1 = trim(get_naps_txtmeta($napsid, 'timeline_txt'));
	if($text1){
		$temp .= $before;
		$temp .= apply_filters('comment_text',$text1);
		$temp .= $after;
	}
	return $temp;
}

function get_the_naprtext2($napsid, $title){
global $wpdb;

	$napsid = intval($napsid);

	$temp = '';

	$text2 = trim(get_naps_txtmeta($napsid, 'description_txt'));
	if($text2){
		$temp .= '
		<div class="fcwidget">
			<div class="fcwidgetvn">
				<div class="fcwtitle">
					<div class="fcwtitlevn">
						'. $title .'
					</div>
						<div class="clear"></div>
				</div>
				
				'. apply_filters('the_content',$text2) .'
					<div class="clear"></div>
			</div>
		</div>';
	}
	return $temp;
}

function get_the_napravlennost($vd1, $vd2, $naps, $id, $place=''){
global $wpdb;

	$temp = '';
	
	if(!is_object($vd1) or !is_object($vd2) or !is_object($naps)){
		return $temp;
	}
	
	if($id==1){
	    $vd = $vd1;
	} else {
	    $vd = $vd2;
	}	
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	
	$vtype = pn_strip_input($vd->vtype);
	$vname = pn_strip_input($vd->vname);
	$valid = $vd->id;
	
	if($id==1){
		$curs = pn_strip_input($naps->curs1);
		$minpl = pn_strip_input($naps->minsumm1);
		$txt = pn_strip_input($vd->txt1);
		if(!$txt){
		    $txt = __('From the account','pn');
		}
		$showed = pn_strip_input($vd->show1);
		$showed = apply_filters('exb_obmen_showed',$showed,$naps,$vd,1);
		$prproc = pn_strip_input($naps->prproc1);
		$prsum = pn_strip_input($naps->prsum1);		
		$maxcom = pn_strip_input($naps->maxsumm1com); 
		$mincom = pn_strip_input($naps->minsumm1com); 
		$combox = 0;
		$comboxpers = 0;
	} else {
		$curs = pn_strip_input($naps->curs2);
		$minpl = pn_strip_input($naps->minsumm2);
		$txt = pn_strip_input($vd->txt2);
		if(!$txt){
		    $txt = __('At the expense','pn');
		}
		$showed = pn_strip_input($vd->show2);
		$showed = apply_filters('exb_obmen_showed',$showed,$naps,$vd,2);
		$prproc = pn_strip_input($naps->prproc2);
		$prsum = pn_strip_input($naps->prsum2);
		$maxcom = pn_strip_input($naps->maxsumm2com);
		$mincom = pn_strip_input($naps->minsumm2com);
		$combox = pn_strip_input($naps->combox);
		$comboxpers = pn_strip_input($naps->comboxpers);
	}		

	$userschet = array();
	if($user_id){
	    $userschet = get_user_meta($user_id, 'userschet',true);
	}
	$skidka = get_us_sk($curs, $user_id,$naps->delsk);
	if($id==2){
		$curs = $curs + $skidka;
	}
	$curs_v = is_sum($curs);		
	
	$dcomm = 0;
	$dcomm_v = 0;
	$dis = '';
	$disclass= '';
	$ncurs = 0;
	$comis_text = '';
	if($id == 1){
		
	    $dis = 'disabled="disabled"';
		$disclass = 'disabled';
		$uotcom = exb_rash_com($curs, $prsum, $prproc, $mincom, $maxcom); /* комиссия оригинал */
		$uotcom_v = is_sum($uotcom); /* комиссия 12 зн */
		$ncurs = is_sum($curs + $uotcom); /* сумма с учетом комиссии */
	
	} else {
		
		$dcomm = exb_doprash_com($curs, $combox, $comboxpers);
		$dcomm_v = is_sum($dcomm);
		
		$ncurs = $curs - $dcomm;
		$uotcom = exb_rash_com($ncurs, $prsum, $prproc, $mincom, $maxcom); /* комиссия оригинал */
		$uotcom_v = is_sum($uotcom); /* комиссия 12 зн */
		
		$ncurs = is_sum($ncurs - $uotcom);
	}

	$comis_text = get_comis_text($uotcom_v, $dcomm_v, $vname, $vtype, $id, 1);

	$wotcl = 'style="display: none;"';
	if($uotcom_v > 0 or $dcomm_v > 0){
		$wotcl = '';
	}	
	
	if($place == 'widget'){
		
		$temp = '
		<div class="wosummblock">	
			<div class="wosummtitle">'. __('Sum','pn') .':</div>
			<div class="wosumline changewoerr">
				<div class="wosumminput"><input type="text" id="wsumm'. $id .'" name="summ'.$id.'" value="'. $curs_v .'" /></div>
				<div class="wosummnazv">'. $vname .' '. $vtype .'</div>
					<div class="clear"></div>
					<div class="wowarnerr"></div>
			</div>
				<div class="clear"></div>
		</div>		
		';
	
		$temp .= '
		<div class="wosummblock" '. $wotcl .'>
			<div class="wosummtitle">'. __('Sum','pn') .':</div>
			<div class="wosumline changewoerr">
				<div class="wosumminput"><input type="text" class="'. $disclass .'" '. $dis .' id="wsummcom'. $id .'" name="poluch'.$id.'" value="'. $ncurs .'" /></div>
				<div class="wosummopis" id="wcomtext-'. $id .'">'. $comis_text .'</div>
					<div class="clear"></div>
					<div class="wowarnerr"></div>
			</div>
				<div class="clear"></div>	
		</div>';			
			
		$helps = pn_strip_text($vd->helps);
		$helpinfo = '';
		if($helps){
			$helpinfo = '
				<div class="exchboxtool">
					<div class="exchboxtoolvnug"></div>
					<div class="exchboxtooltext">
					'. $helps .'
					</div>
				</div>	
			';
		}			
		
		$user_purse = pn_strip_input(is_isset($userschet,$valid));
		
		if($showed == 1){	
			$temp .='
			<div class="woschettitle">'. $txt .':</div>
			<div class="woschetblock">		
				<div class="woschetadres parenttool changewoerr">
					<div class="wolineinput">
						<input type="text" name="wschet'.$id.'" id="wschet'.$id.'" class="toolinput" placeholder="'. pn_strip_input($vd->firstzn) .'" value="'. $user_purse .'" />
						<div class="wowarnerr"></div>
					</div>
					'. $helpinfo .'
				</div>
			</div>';
		}

		$howe = "AND place_id IN('0','$id')"; 
		$vmetas = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."custom_fields_valut WHERE valut_id='$valid' AND status='1' $howe ORDER BY cf_order ASC"); 
		foreach($vmetas as $vmeta){ 
			$vmvid = pn_strip_input($vmeta->vid);
			
			if($vmvid == 0){ /* текстовое поле */
			
				$helps = pn_strip_text($vmeta->helps);
				$helpinfo = '';
				if($helps){
					$helpinfo = '
						<div class="exchboxtool">
							<div class="exchboxtoolvnug"></div>
							<div class="exchboxtooltext">
							'. $helps .'
							</div>
						</div>	
					';
				}	
				
				$temp .='
				<div class="woschettitle">'. pn_strip_input($vmeta->cf_name) .':</div>
				<div class="woschetblock">		
					<div class="woschetadres parenttool changewoerr">
						<div class="wolineinput">
							<input type="text" name="cfc'. $vmeta->id .'" placeholder="'. is_firstzn_value($vmeta->firstzn) .'" id="doppole'. $vmeta->id .'" class="toolinput" value="" />
							<div class="wowarnerr"></div>
						</div>
						'. $helpinfo .'
					</div>
				</div>';	

			} else { /* select */
			
				$selects = array();
				$datas = explode("\n",$vmeta->datas);
				$r=-1;
				foreach($datas as $dat){
					$dat = pn_strip_input($dat);
					if($dat){ $r++;
						$selects[$r] = $dat;
					}
				}
				
				if(count($selects) > 0){
				
					$temp .='
					<div class="woschettitle">'. pn_strip_input($vmeta->cf_name) .':</div>
					<div class="woschetblock">		
						<div class="woschetadres parenttool changewoerr">
							<div class="wolineinput">
								<select name="cfc'. $vmeta->id .'">';
								
									foreach($selects as $sk => $sel){
										$temp .= '<option value="'. $sk .'">'. $sel .'</option>';
									}							
								
								$temp .='
								</select>
							</div>
						</div>
					</div>';			
				
				}
			
			}
		}		
		
	} else {
		
		$where = get_naps_where('exchange');
		if($id==1){
			$napobmens = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC");
		} else {
			$valid1 = $vd1->id;
			$napobmens = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where AND valsid1='$valid1' ORDER BY rorder ASC");
		}
	
		$napobmens2 = array();
		foreach($napobmens as $nap){ 
			if($id==1){
				$napobmens2[$nap->valsid1] = $nap;
			} else {
				$napobmens2[$nap->valsid2] = $nap;
			}
		}	
	
		$valuts = array();
		$valutsn = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts");
		foreach($valutsn as $valut){
			$valuts[$valut->id] = $valut;
		}	
	
		$temp .= '
		<div class="obmenfirstblock">
			<div class="obmenfirstleft">
		';
	
			$temp .= '
			<div class="rselected" name="'.$id.'">
				<input type="hidden" name="idfirst'. $id .'" value="'. $valid .'" />
					<div class="rselecttitle">
						<div class="rselecttitlevn">
							<div class="rselectico" style="background: url('. is_ssl_url($vd->vlogo) .') no-repeat center center"></div>
							<div class="rselectname">'. $vname .' '. $vtype .'</div>
								<div class="clear"></div>
						</div>
					</div>
					
					<div class="rselectlinemenu">
			';
			
					if(is_array($napobmens2)){ 
						foreach($napobmens2 as $key=> $nap){
							if($key == $valid){ $cl='cur'; } else { $cl=''; }							
							
							$temp .= '
							<div class="rselectline '. $cl .'" name="'. $key .'">
								<div class="rselectico" style="background: url('. is_ssl_url($valuts[$key]->vlogo) .') no-repeat center center"></div>
								<div class="rselectname">'. pn_strip_input($valuts[$key]->vname .' '. $valuts[$key]->vtype) .'</div>
									<div class="clear"></div>
							</div>								
							';
						}
					}

			$temp .= '
					</div>
			</div>';
		
		$temp .= '
			</div>
			<div class="obmenfirstright">';
		
			$temp .= '
			<table class="obmminit">
				<tr>
					<td class="obmminitleft">'. __('Sum','pn') .':</td>
					<td class="obmminitright">
						<div class="changewoerr">
							<input type="text" name="summ'.$id.'" id="wsumm'.$id.'" value="'. $curs_v .'" />
							<div class="wowarnerr"></div>
						</div>
					</td>
				</tr>						
			</table>
			';	
		
		$temp .= '
			</div>
				<div class="clear"></div>
		</div>';
	
		$temp .= '
		<div class="obmenfirstblock" '. $wotcl .'>
			<div class="obmenfirstleft">';
			
				$temp .= '
				<div class="wcomtext" id="wcomtext-'. $id .'">
					'. $comis_text .'
				</div>
				';
				
			$temp .= '	
			</div>
			<div class="obmenfirstright">';
		
			$temp .= '
			<table class="obmminit">
				<tr>
					<td class="obmminitleft">'. __('Sum','pn') .':</td>
					<td class="obmminitright">
						<div class="changewoerr">
							<input type="text" name="" id="wsummcom'. $id .'" '. $dis .' class="'. $disclass .'" value="'. $ncurs .'" />
							<div class="wowarnerr"></div>
						</div>
					</td>
				</tr>						
			</table>';
			
			$temp .= '	
			</div>
				<div class="clear"></div>
		</div>';
				
		$helps = pn_strip_text($vd->helps);
		$helpinfo = '';
		if($helps){
			$helpinfo = '
				<div class="exchboxtool">
					<div class="exchboxtoolvnug"></div>
					<div class="exchboxtooltext">
					'. $helps .'
					</div>
				</div>	
			';
		}				
			
		$user_purse = pn_strip_input(is_isset($userschet,$valid));				
		if($showed == 1){ 		
			$temp .= '
			<div class="stepshetline">
				<div class="stepshettitle">'. $txt .':</div>				
					<div class="changewoerr">
						<div class="stepshetleft parenttool">
							<div class="stepshetinput">
								<input type="text" name="wschet'.$id.'" id="wschet'.$id.'" class="toolinput"  placeholder="'. is_firstzn_value($vd->firstzn) .'" value="'. $user_purse .'" />
							</div>
							'. $helpinfo .'							
						</div>
						<div class="stepshetright">
							<div class="wowarnerr"></div>					    
						</div>					
							<div class="clear"></div>
					</div>
			</div>				
			';
		}
	
		$howe = "AND place_id IN('0','$id')"; 
		$vmetas = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."custom_fields_valut WHERE valut_id='$valid' AND status='1' $howe ORDER BY cf_order ASC"); 
		foreach($vmetas as $vmeta){
			$vmvid = $vmeta->vid;
		
			if($vmvid == 0){ /* текстовое поле */
			
				$helps = pn_strip_text($vmeta->helps);
				$helpinfo = '';
				if($helps){
					$helpinfo = '
						<div class="exchboxtool">
							<div class="exchboxtoolvnug"></div>
							<div class="exchboxtooltext">
							'. $helps .'
							</div>
						</div>	
					';
				}	  
				
				$temp .= '
				<div class="stepshetline">
					<div class="stepshettitle">'. pn_strip_input($vmeta->cf_name) .':</div>				
						<div class="changewoerr">
							<div class="stepshetleft parenttool">
								<div class="stepshetinput">
									<input type="text" name="cfc'. $vmeta->id .'" placeholder="'. is_firstzn_value($vmeta->firstzn) .'" id="doppole'. $vmeta->id .'" class="toolinput" value="" />
								</div>
								'. $helpinfo .'							
							</div>
							<div class="stepshetright">
								<div class="wowarnerr"></div>					    
							</div>					
								<div class="clear"></div>
						</div>
				</div>				
				';	
				
			} else { /* select */
			
				$selects = array();
				$datas = explode("\n",$vmeta->datas);
				$r=-1;
				foreach($datas as $dat){
					$dat = pn_strip_input($dat);
					if($dat){ $r++;
						$selects[$r] = $dat;
					}
				}
				
				if(count($selects) > 0){
				
					$temp .= '
					<div class="stepshetline">
						<div class="stepshettitle">'. pn_strip_input($vmeta->cf_name) .':</div>				
							<div class="changewoerr">
								<div class="stepshetleft">
									<div class="stepshetinput">
										<select name="cfc'. $vmeta->id .'">
									';
										
										foreach($selects as $sk => $sel){
											$temp .= '<option value="'. $sk .'">'. $sel .'</option>';
										}
										
									$temp .= '
										</select>		
									</div>						
								</div>					
									<div class="clear"></div>
							</div>
					</div>				
					';		
			
				}
				
			}
		}		
	
	}
	
	return $temp;
}	