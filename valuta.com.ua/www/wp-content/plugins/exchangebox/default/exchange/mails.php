<?php
if( !defined( 'ABSPATH')){ exit(); }
	
add_filter('list_admin_notify','list_admin_notify_bids');
function list_admin_notify_bids($places_admin){
	
	$places_admin['new_bids1'] = __('When bids has status "new application"','pn');
	$places_admin['payed_bids1'] = __('When bids has status "marked bid by user as paid"','pn');
	$places_admin['realpay_bids1'] = __('When bids has status "paid bid"','pn');
	$places_admin['delete_bids1'] = __('When bids has status "delete application"','pn');
	$places_admin['returned_bids1'] = __('When bids has status "money will be returned"','pn');	
	$places_admin['verify_bids1'] = __('When bids has status "paid from another purse"','pn');
	$places_admin['error_bids1'] = __('When bids has status "error application"','pn');
	$places_admin['success_bids1'] = __('When bids has status "success application"','pn');	
	$places_admin['realdelete_bids1'] = __('Complete removal of the application site','pn');
	
	return $places_admin;
}
	
add_filter('list_user_notify','list_user_notify_bids');
function list_user_notify_bids($places_admin){
	
	$places_admin['new_bids2'] = __('When bids has status "new application"','pn');
	$places_admin['payed_bids2'] = __('When bids has status "marked bid by user as paid"','pn');
	$places_admin['realpay_bids2'] = __('When bids has status "paid bid"','pn');
	$places_admin['delete_bids2'] = __('When bids has status "delete application"','pn');
	$places_admin['returned_bids2'] = __('When bids has status "money will be returned"','pn');	
	$places_admin['verify_bids2'] = __('When bids has status "paid from another purse"','pn');
	$places_admin['error_bids2'] = __('When bids has status "error application"','pn');
	$places_admin['success_bids2'] = __('When bids has status "success application"','pn');
	$places_admin['realdelete_bids2'] = __('Complete removal of the application site','pn');
	
	return $places_admin;
}

add_filter('list_notify_tags_new_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_new_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_payed_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_payed_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_realpay_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_realpay_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_returned_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_returned_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_delete_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_delete_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_verify_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_verify_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_error_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_error_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_success_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_success_bids2','def_mailtemp_tags_bids');
add_filter('list_notify_tags_realdelete_bids1','def_mailtemp_tags_bids');
add_filter('list_notify_tags_realdelete_bids2','def_mailtemp_tags_bids');
function def_mailtemp_tags_bids($tags){
	
	$tags['id'] = __('Bid id','pn');
	$tags['createdate'] = __('Date','pn');
	$tags['curs1'] = __('Course give','pn');
	$tags['curs2'] = __('Course get','pn');
	$tags['valut1'] = __('Name ps give','pn');
	$tags['valut2'] = __('Name ps get','pn');
	$tags['vtype1'] = __('Currency type give','pn');
	$tags['vtype2'] = __('Currency type get','pn');
	$tags['account1'] = __('Account give','pn');
	$tags['account2'] = __('Account get','pn');
	$tags['first_name'] = __('First name','pn');
	$tags['last_name'] = __('Last name','pn');
	$tags['second_name'] = __('Second name','pn');
	$tags['user_phone'] = __('Phone','pn');
	$tags['user_skype'] = __('Skype','pn');
	$tags['user_email'] = __('E-mail','pn');
	$tags['user_passport'] = __('Passport','pn');
	$tags['summ1'] = __('Sum give','pn');
	$tags['summ1c'] = __('Sum commis give','pn');
	$tags['summ2'] = __('Sum get','pn');
	$tags['summ2c'] = __('Sum commis get','pn');
	$tags['bidurl'] = __('URL','pn');
	$tags = apply_filters('mailtemp_tags_bids', $tags);
	
	return $tags;
}	

function goed_mail_to_changestatus_bids($obmen_id, $obmen, $name1='', $name2=''){
global $wpdb;	
	
	if(isset($obmen->id)){
		
		$notify_tags = array();
		$notify_tags['[sitename]'] = pn_strip_input(get_bloginfo('sitename'));
		$notify_tags['[id]'] = $obmen->id;
		$notify_tags['[createdate]'] = $notify_tags['[create_date]'] = pn_strip_input($obmen->tdate);		
		$notify_tags['[curs1]'] = pn_strip_input($obmen->curs1);
		$notify_tags['[curs2]'] = pn_strip_input($obmen->curs2);
		$notify_tags['[valut1]'] = pn_strip_input($obmen->valut1);
		$notify_tags['[valut2]'] = pn_strip_input($obmen->valut2);
		$notify_tags['[vtype1]'] = pn_strip_input($obmen->valut1type);
		$notify_tags['[vtype2]'] = pn_strip_input($obmen->valut2type);
		$notify_tags['[account1]'] = pn_strip_input($obmen->schet1);
		$notify_tags['[account2]'] = pn_strip_input($obmen->schet2);
		$notify_tags['[first_name]'] = pn_strip_input($obmen->iname);
		$notify_tags['[last_name]'] = pn_strip_input($obmen->fname);
		$notify_tags['[second_name]'] = pn_strip_input($obmen->oname);
		$notify_tags['[user_phone]'] = pn_strip_input($obmen->tel);
		$notify_tags['[user_skype]'] = pn_strip_input($obmen->skype);
		$notify_tags['[user_email]'] = pn_strip_input($obmen->email);
		$notify_tags['[user_passport]'] = pn_strip_input($obmen->pnomer);
		$notify_tags['[summ1]'] = pn_strip_input($obmen->summ1);
		$notify_tags['[summ1c]'] = pn_strip_input($obmen->summz1);
		$notify_tags['[summ2]'] = pn_strip_input($obmen->summ2);
		$notify_tags['[summ2c]'] = pn_strip_input($obmen->summz2);
		$notify_tags['[bidurl]'] = get_bids_url($obmen->hashed);		
		$notify_tags = apply_filters('notify_tags_bids', $notify_tags, $obmen);		

		$user_send_data = array();
		$result_mail = apply_filters('premium_send_message', 0, $name1, $notify_tags, $user_send_data); 
		
		$user_send_data = array(
			'user_email' => $obmen->email,
			'user_phone' => $obmen->tel,
		);	
		$result_mail = apply_filters('premium_send_message', 0, $name2, $notify_tags, $user_send_data);		
		
	}		
}

add_action('change_bidstatus_all','def_change_bidstatus_all',1,4);
function def_change_bidstatus_all($status, $obmen_id, $obmen, $place='site'){ 
global $wpdb, $exchangebox;
	
	$action1 = '';
	if($place == 'site' or $exchangebox->get_option('exchange','admin_mail') == 1){
		$action1 = $status.'_bids1';
	}
	$action2 = $status.'_bids2';
	goed_mail_to_changestatus_bids($obmen_id, $obmen, $action1, $action2);	
	
}