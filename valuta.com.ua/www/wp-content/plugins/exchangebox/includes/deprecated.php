<?php
/*
Устаревшие функции фреймворка premium 
*/
if( !defined( 'ABSPATH')){ exit(); }

/* 5.3 */
function exb_template($page){
global $exchangebox;

	$exchangebox->_deprecated_function('exb_template', '5.3', '$exchangebox->file_include()');
	$exchangebox->file_include($page);
}
function pn_admin_themp($page){
global $exchangebox;

	$exchangebox->_deprecated_function('pn_admin_themp', '5.3', '$exchangebox->admin_temp()');
	$exchangebox->admin_temp();
}
function get_icon_link($icon){
global $exchangebox;

	$exchangebox->_deprecated_function('get_icon_link', '5.3', '$exchangebox->get_icon_link()');
	$exchangebox->get_icon_link($icon);
}
function exb_replace_arrays($array, $content){
global $exchangebox;

	$exchangebox->_deprecated_function('exb_replace_arrays', '5.3', 'get_replace_arrays');
	return get_replace_arrays($array, $content);
}
function get_exbtime($date, $format='d.m.Y'){
global $exchangebox;

	$exchangebox->_deprecated_function('get_exbtime', '5.3', 'get_mytime');
	return get_mytime($date, $format);	
}
function exb_cptgn($text){
global $exchangebox;	
	
	$exchangebox->_deprecated_function('exb_cptgn', '5.3', 'get_cptgn');
	return get_cptgn($text);
}
function get_exb_page($attr){
global $exchangebox;
	
	$exchangebox->_deprecated_function('get_exb_page', '5.3', '$exchangebox->get_page()');
	return $exchangebox->get_page($attr);
}
function exb_curl_parser($url){
global $exchangebox;	
	
	$exchangebox->_deprecated_function('exb_curl_parser', '5.3', 'get_curl_parser');
	return get_curl_parser($url);
}
function exb_sklonenie($num, $text1='', $text2='', $text3=''){
global $exchangebox;	
	
	$exchangebox->_deprecated_function('exb_sklonenie', '5.3', 'get_sklon');
    return get_sklon($num, $text1, $text2, $text3);	
}
function merch_mess($text, $species='error'){
global $exchangebox;	
	
	$exchangebox->_deprecated_function('merch_mess', '5.3', 'pn_display_mess($title, $text, $species)');
	pn_display_mess($text, $text, $species);
}
function exb_is_my_id(){
global $exchangebox;	
	
	$exchangebox->_deprecated_function('exb_is_my_id', '5.3', 'get_session_id');	
	return get_session_id();
}
function exb_mysession($name){
global $exchangebox;	
	
	$exchangebox->_deprecated_function('exb_mysession', '5.3', 'get_pn_cookie');	
    return get_pn_cookie($name);
}
function exb_addmysession($name,$arg){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_addmysession', '5.3', 'add_pn_cookie');	
	add_pn_cookie($name, $arg);
	return $arg;
}
function exb_is_my_site($arg){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_is_my_site', '5.3', 'esc_url');	
	return esc_url($arg);
}
function exb_is_user($username){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_is_user', '5.3', 'is_user');	
	return is_user($username);
}
function exb_is_password($password){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_is_password', '5.3', 'is_password');	
	return is_password($password);
}
function exb_is_my_money($arg, $cz=6){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_is_my_money', '5.3', 'is_sum');	
	return is_sum($arg, $cz);
}
function exb_is_weblic($url){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_is_weblic', '5.3', '');
}
function exb_strip_input($txt){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_strip_input', '5.3', 'pn_strip_input');	
	return pn_strip_input($txt);
}
function exb_strip_text($txt){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exb_strip_text', '5.3', 'pn_strip_text');	
	return pn_strip_text($txt);
}
function exbchange($option='', $option2=''){
global $exchangebox;
	
	$exchangebox->_deprecated_function('exbchange', '5.3', '$exchangebox->get_option($option, $option2)');
	return $exchangebox->get_option($option, $option2);
}
function exb_pagenavi_rasch($limit,$get,$count){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_pagenavi_rasch', '5.3', 'get_pagenavi_calc');	
	return get_pagenavi_calc($limit,$get,$count);
}
function exb_get_pagenavi($pagenavi=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_get_pagenavi', '5.3', 'get_pagenavi');	
	return get_pagenavi($pagenavi, 'standart', '');
}
function exb_pagenavi($pagenavi=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_pagenavi', '5.3', 'the_pagenavi');	
	the_pagenavi($pagenavi, 'standart', '');
}
function exb_year($year){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_year', '5.3', 'get_copy_date');	
	return get_copy_date($year);
}
function exbtableh3($title=false, $submit=false){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtableh3', '5.3', '');	
}
function exbtableline(){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtableline', '5.3', 'pn_line');	
}
function exbuploader($title, $name, $default=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbuploader', '5.3', '');	
}
function exbtablehelp($title, $content=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtablehelp', '5.3', '');	
}
function exbtableselect($title, $name='', $array=array(), $default='', $classed=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtableselect', '5.3', '');	
}
function exbtableinput($title, $name='', $default='', $classed=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtableinput', '5.3', '');
}
function exbtableinputbig($title, $name='', $default='', $classed=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtableinputbig', '5.3', '');	
}
function exbtableinputbigpass($title, $name='', $default='', $classed=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtableinputbig', '5.3', '');
}
function exbtabletextarea($title, $name='', $default='', $width='', $height='100px', $classed=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtabletextarea', '5.3', '');	
}
function exbtabledatetime($title, $name='', $default='', $classed=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtabledatetime', '5.3', '');	
}
function exbtabledate($title, $name='', $default='', $classed=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtabledate', '5.3', '');	
}
function exbtabletextareaico($title, $name='', $default='', $tags='', $prefix1='[', $prefix2=']',$width='', $height='100px'){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtabletextareaico', '5.3', '');	
}
function exbtabletextareaeditor($title, $name, $default=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtabletextareaeditor', '5.3', '');		
}
function exb_not_caps_name($name){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_not_caps_name', '5.3', 'get_caps_name');	
	return get_caps_name($name);
}
function get_otziv_link($otziv_id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_otziv_link', '5.3', 'get_review_link');	
	return get_review_link($otziv_id, '');
}
function get_vtype($vtype){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_vtype', '5.3', '');
}
function get_user_skidka_obmen($user_id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_user_skidka_obmen', '5.3', 'get_user_discount');	
	return get_user_discount($user_id);
}
function get_user_skidka_refobmen($user_id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_user_skidka_refobmen', '5.3', 'get_user_pers_refobmen');	
	return get_user_pers_refobmen($user_id);
}
function summtowmz($summ, $type){
global $exchangebox;	

	$exchangebox->_deprecated_function('summtowmz', '5.3', 'convert_sum');	
	return convert_sum($summ, $type, 'USD');
}
function get_user_count_obmen_summ($user_id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_user_count_obmen_summ', '5.3', 'get_user_sum_exchanges');	
	return get_user_sum_exchanges($user_id);
}
function get_user_count_obmen($user_id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_user_count_obmen', '5.3', 'get_user_count_exchanges');	
	return get_user_count_exchanges($user_id);
}
function get_out_valuts($id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_out_valuts', '5.3', 'get_valut_out');
	return get_valut_out($id, 12);
}
function get_in_valuts($id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_in_valuts', '5.3', 'get_valut_in');	
	return get_valut_in($id, 12);
}
function valuts_title($id){
global $exchangebox;	

	$exchangebox->_deprecated_function('valuts_title', '5.3', 'get_vtitle');	
	return get_vtitle($id);
}
function get_merchant_instruction($instruction, $xzt){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_merchant_instruction', '5.3', '');
}
function exb_enable_merchant($name){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_enable_merchant', '5.3', '');
}
function exbchangeadd($exb_change, $option='', $option2='', $add=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbchangeadd', '5.3', '');	
}
function is_exb_link($url){
global $exchangebox;	

	$exchangebox->_deprecated_function('is_exb_link', '5.3', 'esc_url');	
	return esc_url($url);
}
function exb_alter_summ($summ, $pers){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_alter_summ', '5.3', 'pers_alter_summ');	
	return pers_alter_summ($summ, $pers);
}
function get_partner_money_tek($user_id,$cn=5){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_partner_money_tek', '5.3', 'get_partner_money_now');	
	return get_partner_money_now($user_id);
}
function get_user_count_refobmen_summ($user_id){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_user_count_refobmen_summ', '5.3', 'get_user_sum_refobmen');	
	return get_user_sum_refobmen($user_id);
}
function get_partner_zar_all($user_id,$cn=5){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_partner_zar_all', '5.3', 'get_partner_earn_all');	
	return get_partner_earn_all($user_id);	
}
function is_enable_merchat($id){
global $exchangebox;	

	$exchangebox->_deprecated_function('is_enable_merchat', '5.3', 'is_enable_merchant');	
	return is_enable_merchant($id);
}

/* 6.0 */
function pn_template($page){
global $exchangebox;

	$exchangebox->_deprecated_function('pn_template', '6.0', '$exchangebox->file_include()');
	$exchangebox->file_include($page);
}
function get_rezerv_valuts($valut_data, $cn=5){
global $exchangebox;

	$exchangebox->_deprecated_function('get_rezerv_valuts', '6.0', 'get_valut_reserv');	
	return get_valut_reserv($valut_data);
}
function pn_auto_include($folder){
global $exchangebox;

	$exchangebox->_deprecated_function('pn_auto_include', '6.0', '$exchangebox->auto_include()');
	$exchangebox->auto_include($folder);
}
function get_pn_page($attr){
global $exchangebox;

	$exchangebox->_deprecated_function('get_pn_page', '6.0', '$exchangebox->get_page()');
	return $exchangebox->get_page($attr);
}
function the_warning($text, $species='error'){
global $exchangebox;
	
	$exchangebox->_deprecated_function('the_warning', '6.0', 'pn_display_mess($title, $text, $species)');
	pn_display_mess($text, $text, $species);
}
function get_change($option='', $option2=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_change', '6.0', '$exchangebox->get_option($option, $option2)');
	return $exchangebox->get_option($option, $option2);
}
function update_change($key1='', $key2='', $value){
global $exchangebox;	
	
	$exchangebox->_deprecated_function('update_change', '6.0', '$exchangebox->update_option($key1, $key2, $value)');
	return $exchangebox->update_option($key1, $key2, $value);
}
function link_tosaved($action=''){
global $exchangebox;
	
	$exchangebox->_deprecated_function('link_tosaved', '6.0', 'pn_link_post');
	return pn_link_post($action);
}
function the_link_tosaved($action=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('the_link_tosaved', '6.0', 'pn_the_link_post');
	echo pn_the_link_post($action);
}
function link_toajax($action=''){
global $exchangebox;	

	$exchangebox->_deprecated_function('link_toajax', '6.0', 'pn_link_post');
	return pn_link_post($action, 'post');
}
function the_link_toajax($action=''){
global $exchangebox;
	
	$exchangebox->_deprecated_function('the_link_toajax', '6.0', 'pn_the_link_post');
	echo pn_link_post($action, 'post');
}
function get_exb_partners(){
global $exchangebox;
	
	$exchangebox->_deprecated_function('get_exb_partners', '6.0', 'get_partners');
	return get_partners();
}
function the_pn_help($title, $content=''){
global $exchangebox;	
	$exchangebox->_deprecated_function('the_pn_help', '6.0', '');
}
function the_pn_editor($title, $content=''){
global $exchangebox;	
	$exchangebox->_deprecated_function('the_pn_editor', '6.0', '');
}
function ph_the_uploader($name, $content=''){
global $exchangebox;	
	$exchangebox->_deprecated_function('ph_the_uploader', '6.0', '');
}
function is_merchant_id($name){
global $exchangebox;	
	$exchangebox->_deprecated_function('is_merchant_id', '6.0', 'is_extension_name');
	return is_extension_name($name);
}
function get_userpage_exb($page_id){
global $exchangebox;	
	$exchangebox->_deprecated_function('get_userpage_exb', '6.0', 'update_term_meta');	
	return get_userpage_pn($page_id);
}
function update_pn_term_meta($id, $key, $value){
global $exchangebox;	
	$exchangebox->_deprecated_function('update_pn_term_meta', '6.0', 'update_term_meta');
	
	return update_term_meta($id, $key, $value);
}
function get_pn_term_meta($id, $key){
global $exchangebox;	
	$exchangebox->_deprecated_function('get_pn_term_meta', '6.0', 'get_term_meta');	
	
	return get_term_meta($id, $key);
}
function delete_pn_term_meta($id, $key){
global $exchangebox;	
	$exchangebox->_deprecated_function('delete_pn_term_meta', '6.0', 'delete_term_meta');	
	
	return delete_term_meta($id, $key);
}
function get_merchant_admin_options($m_id, $data){
global $exchangebox;
	$exchangebox->_deprecated_function('get_merchant_admin_options', '6.0', '');
}
function exb_delsimbol($title){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_delsimbol', '6.0', 'pn_strip_symbols');	
	return pn_strip_symbols($title);
}
function exbtablewarningmess($text){
global $exchangebox;	

	$exchangebox->_deprecated_function('exbtablewarningmess', '6.0', 'pn_display_mess');	
	pn_display_mess($text);
}
function get_exb_logo(){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_not_caps_name', '6.0', 'get_caps_name');	
	return get_logotype();
}
function is_exb_lang($arg){ 
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_not_caps_name', '6.0', '');
}
function get_array_rezerv_valuts($cn=6){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_array_rezerv_valuts', '6.0', '');
}
function get_rezerv_valuts_todate($id, $date){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_rezerv_valuts_todate', '6.0', '');
}
function exb_is_my_rez($sum){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_is_my_rez', '6.0', 'output_sum');
	return output_sum($sum);
}
function exb_is_my_moneyno($arg, $cz=6){
global $exchangebox;	

	$exchangebox->_deprecated_function('exb_is_my_moneyno', '6.0', 'output_sum($sum, $cz)');
	return output_sum($sum);
}
function get_id_by_xml($xml){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_id_by_xml', '6.0', '');
}
function valuts_title_by_xml($xml){
global $exchangebox;	

	$exchangebox->_deprecated_function('valuts_title_by_xml', '6.0', '');
}
function get_naps_by_id($valut1, $valut2){
global $exchangebox;	

	$exchangebox->_deprecated_function('get_naps_by_id', '6.0', '');
}
function prepare_old_merch($id){
	if($id == '3'){
		$id = 'webmoney';
	} elseif($id == '1'){
		$id = 'perfectmoney';
	} elseif($id == '7'){
		$id = 'deltakey';
	} elseif($id == '8'){
		$id = 'okpay';
	} elseif($id == '9'){
		$id = 'egopay';
	} elseif($id == '10'){
		$id = 'paypal';
	} elseif($id == '11'){
		$id = 'paymer';
	} elseif($id == '12'){
		$id = 'nixmoney';
	} elseif($id == '15'){
		$id = 'zpayment';
	} elseif($id == '18'){
		$id = 'airpay';
	} elseif($id == '19'){
		$id = 'onlymoney';
	} elseif($id == '20'){
		$id = 'cash4pay';
	} elseif($id == '21'){
		$id = 'ooopay';
	} elseif($id == '22'){
		$id = 'paxum';
	} elseif($id == '30'){
		$id = 'advcash';
	} elseif($id == '16'){
		$id = 'payeer';	
	} elseif($id == '2'){
		$id = 'liqpay';
	} elseif($id == '5'){
		$id = 'privat24';
	} elseif($id == '23'){
		$id = 'btce';
	} elseif($id == '24'){
		$id = 'webfin';
	} elseif($id == '4'){
		$id = 'yamoney';
	} elseif($id == '17'){
		$id = 'qiwi';
	} elseif($id == '13'){
		$id = 'bitcoin';
	} elseif($id == '25'){
		$id = 'helixmoney';
	} elseif($id == '26'){
		$id = 'edinar';	
	} elseif($id == '27'){
		$id = 'capitalist';
	} elseif($id == '28'){
		$id = 'livecoin';
	} elseif($id == '34'){
		$id = 'solid';		
	}
	
	return $id;
}

/* 6.1 */
function is_modul_name($name){
global $exchangebox;	
	$exchangebox->_deprecated_function('is_modul_name', '6.1', 'is_extension_name');
	return is_extension_name($name);
}
function get_merchant_file($file){
global $exchangebox;

	$exchangebox->_deprecated_function('get_merchant_file', '6.1', 'get_extension_file');
	return get_extension_file($file);	
}
function get_merchant_name($path){
global $exchangebox;

	$exchangebox->_deprecated_function('get_merchant_name', '6.1', 'get_extension_name');
	return get_extension_name($path);		
}
function get_merchant_num($name){
global $exchangebox;

	$exchangebox->_deprecated_function('get_merchant_num', '6.1', 'get_extension_num');
	return get_extension_num($file);	
}
function pn_allow_second_name(){
global $exchangebox;

	$exchangebox->_deprecated_function('pn_allow_second_name', '6.1', "pn_allow_uv('second_name')");
	return pn_allow_uv('second_name');	
}
function set_merchant_data($path, $map){
global $exchangebox;

	$exchangebox->_deprecated_function('set_merchant_data', '6.1', 'set_extension_data');
	return set_extension_data($path, $map);	
}
function output_sum($sum, $cz=0){
global $exchangebox;

	$exchangebox->_deprecated_function('output_sum', '6.1', 'is_out_sum($sum, $decimal=12, $place="all")');
	return is_out_sum($sum, $cz, 'all');	
}

/* 7.0 */
if(!function_exists('get_mycookie')){
	function get_mycookie($key){
		global $exchangebox;
			$exchangebox->_deprecated_function('get_mycookie', '7.0', "get_pn_cookie");
			return get_pn_cookie($key);
	}
}
if(!function_exists('add_mycookie')){
	function add_mycookie($key, $arg, $time=0){
		global $exchangebox;
			$exchangebox->_deprecated_function('add_mycookie', '7.0', "add_pn_cookie");
			return add_pn_cookie($key, $arg, $time);
	}
}
if(!function_exists('pn_editor_ml')){
	function pn_editor_ml(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_editor_ml', '7.0', "");
	}
}
if(!function_exists('pn_editor')){
	function pn_editor(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_editor', '7.0', "");
	}
}
if(!function_exists('pn_select')){
	function pn_select(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_select', '7.0', "");
	}
}
if(!function_exists('pn_select_disabled')){
	function pn_select_disabled(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_select_disabled', '7.0', "");
	}
} 
if(!function_exists('pn_admin_one_screen')){
	function pn_admin_one_screen(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_admin_one_screen', '7.0', "");
	}
}
if(!function_exists('pn_the_link_ajax')){
	function pn_the_link_ajax($action=''){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_the_link_ajax', '7.0', "pn_the_link_post");
			pn_the_link_post($action, 'post');
	}
}
if(!function_exists('pn_link_ajax')){
	function pn_link_ajax($action=''){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_link_ajax', '7.0', "pn_link_post");
			return pn_link_post($action, 'post');
	}
}
if(!function_exists('pn_admin_work_options')){
	function pn_admin_work_options(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_admin_work_options', '7.0', "");
	}
}
if(!function_exists('pn_set_option_template')){
	function pn_set_option_template(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_set_option_template', '7.0', "");
	}
}
if(!function_exists('pn_h3')){
	function pn_h3(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_h3', '7.0', "");
	}
}
if(!function_exists('pn_inputbig_ml')){
	function pn_inputbig_ml(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_inputbig_ml', '7.0', "");
	}
}
if(!function_exists('pn_inputbig')){
	function pn_inputbig(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_inputbig', '7.0', "");
	}
}
if(!function_exists('pn_input')){
	function pn_input(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_input', '7.0', "");
	}
}
if(!function_exists('pn_strip_options')){
	function pn_strip_options(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_strip_options', '7.0', "");
	}
}
if(!function_exists('pn_admin_substrate')){
	function pn_admin_substrate(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_admin_substrate', '7.0', "");
	}
}
if(!function_exists('pn_help')){
	function pn_help(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_help', '7.0', "");
	}
}
if(!function_exists('pn_uploader_ml')){
	function pn_uploader_ml(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_uploader_ml', '7.0', "");
	}
}
if(!function_exists('pn_uploader')){
	function pn_uploader(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_uploader', '7.0', "");
	}
}
if(!function_exists('pn_admin_select_box')){
	function pn_admin_select_box(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_admin_select_box', '7.0', "");
	}
}
if(!function_exists('pn_hidden_input')){
	function pn_hidden_input(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_hidden_input', '7.0', "");
	}
}
if(!function_exists('pn_admin_back_menu')){
	function pn_admin_back_menu(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_admin_back_menu', '7.0', "");
	}
}
if(!function_exists('pn_textareaico_ml')){
	function pn_textareaico_ml(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_textareaico_ml', '7.0', "");
	}
}
if(!function_exists('pn_textareaico')){
	function pn_textareaico(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_textareaico', '7.0', "");
	}
}
if(!function_exists('pn_sort_one_screen')){
	function pn_sort_one_screen(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_sort_one_screen', '7.0', "");
	}
}
if(!function_exists('get_sort_ul')){
	function get_sort_ul(){
		global $exchangebox;
			$exchangebox->_deprecated_function('get_sort_ul', '7.0', "");
	}
}
if(!function_exists('pn_textarea_ml')){
	function pn_textarea_ml(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_textarea_ml', '7.0', "");
	}
}
if(!function_exists('pn_textarea')){
	function pn_textarea(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_textarea', '7.0', "");
	}
}
if(!function_exists('pn_select_search')){
	function pn_select_search(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_select_search', '7.0', "");
	}
}
if(!function_exists('pn_textfield')){
	function pn_textfield(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_textfield', '7.0', "");
	}
}
if(!function_exists('pn_date')){
	function pn_date(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_date', '7.0', "");
	}
}
if(!function_exists('pn_checkbox')){
	function pn_checkbox(){
		global $exchangebox;
			$exchangebox->_deprecated_function('pn_checkbox', '7.0', "");
	}
}
if(!function_exists('delsimbol')){
	function delsimbol($arg, $symbols=''){
		global $exchangebox;
			$exchangebox->_deprecated_function('delsimbol', '7.0', "pn_strip_symbols");
			return pn_strip_symbols($arg);
	}
}
if(!function_exists('is_my_money')){
	function is_my_money($sum, $cz=12, $mode='half_up'){
		global $exchangebox;
			$exchangebox->_deprecated_function('is_my_money', '7.0', "is_sum");
			return is_sum($sum, $cz, $mode);
	}
}
if(!function_exists('get_summ_color')){
	function get_summ_color($sum, $max='bgreen',$min='bred'){
		global $exchangebox;
			$exchangebox->_deprecated_function('get_summ_color', '7.0', "get_sum_color");
			return get_sum_color($sum, $max, $min);
	}
}