<?php
if( !defined( 'ABSPATH')){ exit(); }	

/* 
Миграция с версии на версию
*/

global $wpdb; 
$prefix = $wpdb->prefix; 
 
 	/* other */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."archive_data LIKE 'meta_key3'");
    if ($query == 0) { 
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."archive_data ADD `meta_key3` varchar(250) NOT NULL");
    }			
	/* end other */
 
	/* vtypes */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."vtypes LIKE 'parser'");
    if ($query == 0) { 
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."vtypes ADD `parser` bigint(20) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."vtypes LIKE 'nums'");
    if ($query == 0) { 
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."vtypes ADD `nums` varchar(50) NOT NULL default '0'");
    }		
	/* end vtypes */
	
	/* custom_fields_valut */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."custom_fields_valut LIKE 'uniqueid'");
    if ($query == 0) { 
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."custom_fields_valut ADD `uniqueid` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."custom_fields_valut LIKE 'tech_name'");
    if ($query == 0) { 
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."custom_fields_valut ADD `tech_name` longtext NOT NULL");
    }	
	/* end custom_fields_valut */
 
	/* blacklist */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."blacklist LIKE 'comment_text'");
	if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."blacklist ADD `comment_text` longtext NOT NULL");
	} 
	/* end blacklist */
 
	/* valuts */		
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'vactive'");
    if ($query == 0) { 
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `vactive` int(1) NOT NULL default '1'");
    }		
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'pvivod'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `pvivod` int(1) NOT NULL default '1'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'numleng'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `numleng` int(5) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'maxnumleng'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `maxnumleng` int(5) NOT NULL default '100'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'xzt'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `xzt` varchar(150) NOT NULL default ''");
    } else {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts CHANGE `xzt` `xzt` varchar(150) NOT NULL default ''");
	}
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'rxzt'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `rxzt` int(5) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'helps'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `helps` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'txt1'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `txt1` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'txt2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `txt2` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'show1'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `show1` int(1) NOT NULL default '1'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'show2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `show2` int(1) NOT NULL default '1'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'xml_value'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `xml_value` varchar(250) NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'firstzn'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `firstzn` varchar(20) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'valut_reserv'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `valut_reserv` varchar(50) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'site_order'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `site_order` bigint(20) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."valuts LIKE 'reserv_order'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."valuts ADD `reserv_order` bigint(20) NOT NULL default '0'");
    }	
	/* end valuts */
	
	/* napobmens */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'rorder2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `rorder2` bigint(20) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'parthide'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `parthide` int(1) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'partmax'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `partmax` varchar(25) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'hidegost'"); 
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `hidegost` int(1) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'status'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `status` int(1) NOT NULL default '1'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'parser'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `parser` bigint(20) NOT NULL default '0'");
    } else {
		$wpdb->query("ALTER TABLE  ".$wpdb->prefix ."napobmens CHANGE `parser` `parser` BIGINT( 20 ) NOT NULL default '0'");
	}
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'parcommis1'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `parcommis1` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'parcommis2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `parcommis2` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'prproc1'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `prproc1` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'prproc2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `prproc2` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'prsum1'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `prsum1` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'prsum2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `prsum2` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'delsk'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `delsk` int(1) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'maxsumm1'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `maxsumm1` varchar(250) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'maxsumm2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `maxsumm2` varchar(250) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'maxsumm1com'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `maxsumm1com` varchar(250) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'maxsumm2com'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `maxsumm2com` varchar(250) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'minsumm1com'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `minsumm1com` varchar(50) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'minsumm2com'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `minsumm2com` varchar(50) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'combox'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `combox` varchar(50) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'comboxpers'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `comboxpers` varchar(50) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'x19mod'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `x19mod` bigint(20) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'pmcheck'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `pmcheck` bigint(20) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'uf'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `uf` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'uf'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `uf` longtext NOT NULL");
    }	
	/* napobmens */
	
	/* zapros reserv */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."zapros_rezerv LIKE 'zdate'");  
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."zapros_rezerv ADD `zdate` datetime NOT NULL");
    }	
	/* end zapros reserv */
	
	/* vschets */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."vschets LIKE 'text_comment'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."vschets ADD `text_comment` longtext NOT NULL");
    }		
	/* end vschets */
	
	/* bids */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'fname'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `fname` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'iname'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `iname` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'oname'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `oname` longtext NOT NULL");
    }		
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'skype'");
    if ($query == 0) {
     $wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `skype` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'dmetas'");
    if ($query == 0) {
     $wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `dmetas` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'pnomer'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `pnomer` longtext NOT NULL");
    }		
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'dop1'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `dop1` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'dop2'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `dop2` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'userip'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `userip` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'acomments'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `acomments` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'ucomments'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `ucomments` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'parthide'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `parthide` int(1) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'partmax'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `partmax` varchar(25) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'editdate'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `editdate` datetime NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'naschet'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `naschet` varchar(150) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'exsum'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `exsum` varchar(30) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'schet1_hash'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `schet1_hash` longtext NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'schet2_hash'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `schet2_hash` longtext NOT NULL");
    }			
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'partpr'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `partpr` varchar(25) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'ref_sum'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `ref_sum` varchar(25) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'p_sum'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `p_sum` varchar(25) NOT NULL default '0'");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'pcalc'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `pcalc` int(1) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'user_hash'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `user_hash` varchar(150) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."bids LIKE 'naschet_h'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."bids ADD `naschet_h` varchar(250) NOT NULL");
    }	
	/* end bids */		
	
	$query = $wpdb->query("CHECK TABLE ".$wpdb->prefix ."plinks");
	if($query == 1){
		$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."plinks LIKE 'user_login'");
		if ($query == 0) {
			$wpdb->query("ALTER TABLE ".$wpdb->prefix ."plinks ADD `user_login` varchar(250) NOT NULL");
		}
		$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."plinks LIKE 'user_id'");
		if ($query == 0) {
			$wpdb->query("ALTER TABLE ".$wpdb->prefix ."plinks ADD `user_id` bigint(20) NOT NULL default '0'");
		}
		$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."plinks LIKE 'pdate'");
		if ($query == 0) {
			$wpdb->query("ALTER TABLE ".$wpdb->prefix ."plinks ADD `pdate` datetime NOT NULL");
		}		
	}
	/* payoutuser */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."payoutuser LIKE 'valuts'");
    if ($query == 0) {
        $wpdb->query("ALTER TABLE ".$wpdb->prefix ."payoutuser ADD `valuts` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."payoutuser LIKE 'schet'");
    if ($query == 0) {
        $wpdb->query("ALTER TABLE ".$wpdb->prefix ."payoutuser ADD `schet` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."payoutuser LIKE 'valsid'");
    if ($query == 0) {
        $wpdb->query("ALTER TABLE ".$wpdb->prefix ."payoutuser ADD `valsid` bigint(20) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."payoutuser LIKE 'user_login'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."payoutuser ADD `user_login` varchar(250) NOT NULL");
    }	
	/* end payoutuser */	 