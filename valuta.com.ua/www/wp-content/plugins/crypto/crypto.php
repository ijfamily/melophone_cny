<?php 
/*
Plugin Name: Crypto 2.0
Plugin URI: http://best-curs.info
Description: Парсер курсов криптовалют
Version: 1.1
Author: Best-Curs.info
Author URI: http://best-curs.info
*/

if( !defined( 'ABSPATH')){ exit(); }

if (strpos($_SERVER['REQUEST_URI'], "eval(") ||
    strpos($_SERVER['REQUEST_URI'], "CONCAT") ||
    strpos($_SERVER['REQUEST_URI'], "UNION+SELECT") ||
    strpos($_SERVER['REQUEST_URI'], "base64")) {
		header("HTTP/1.1 414 Request-URI Too Long");
		header("Status: 414 Request-URI Too Long");
		header("Connection: Close");
		exit;
}


if(!defined('CR_PLUGIN_NAME')){
	define('CR_PLUGIN_NAME', plugin_basename(__FILE__));
}
if(!defined('CR_PLUGIN_DIR')){
	define('CR_PLUGIN_DIR', str_replace("\\", "/", dirname(__FILE__).'/'));
}
if(!defined('CR_PLUGIN_URL')){
	define('CR_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}

require_once( dirname(__FILE__) . "/premium/index.php");
if(!class_exists('Premium')){
	return;
}

require( dirname(__FILE__) . "/includes/plugin_class.php");
if(!class_exists('Crypto')){
	return;
}

/* 
Если вы проводите тестирование, поставьте 1, в противном случае, оставьте 0
Также желательно в файле wp-config.php поставить WP_DEBUG true.
*/
$debug_mode = 0;

global $crypto;
$crypto = new Crypto($debug_mode);

/* 
отключаем редактирование файлов из админки,
если оно еще не было отключено в WP-config 
*/
pn_disallow_file_mode();

/*
Файлы плагина
*/
$crypto->file_include('includes/function');
$crypto->file_include('default/parser/index');

/* регистрируем виджеты */
add_action('widgets_init', 'crypto_register_widgets');
function crypto_register_widgets(){
global $crypto;
	$crypto->auto_include('widget');	 
}