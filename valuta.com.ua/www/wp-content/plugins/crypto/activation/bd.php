<?php
if( !defined( 'ABSPATH')){ exit(); }		
			
global $wpdb;
$prefix = $wpdb->prefix;

	$table_name= $wpdb->prefix ."crypto_options";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`meta_key` varchar(250) NOT NULL,
		`meta_key2` varchar(250) NOT NULL,
		`meta_value` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);
	
	$table_name = $wpdb->prefix ."parser_pairs";
	$sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`title_pair_give` varchar(150) NOT NULL,
		`title_pair_get` varchar(150) NOT NULL,
		`title_birg` longtext NOT NULL,
		`pair_give` longtext NOT NULL,
		`pair_get` longtext NOT NULL,
		`menu_order` bigint(20) NOT NULL default '0', 
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);

	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'new_parser'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `new_parser` bigint(20) NOT NULL default '0'");
    }		
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'new_parser_actions_give'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `new_parser_actions_give` varchar(150) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'new_parser_actions_get'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `new_parser_actions_get` varchar(150) NOT NULL default '0'");
    }

	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."vtypes LIKE 'new_parser'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."vtypes ADD `new_parser` bigint(20) NOT NULL default '0'");
    }		
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."vtypes LIKE 'new_parser_actions'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."vtypes ADD `new_parser_actions` varchar(150) NOT NULL default '0'");
    }	