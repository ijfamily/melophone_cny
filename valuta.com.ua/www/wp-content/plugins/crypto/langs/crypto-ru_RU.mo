��    @        Y         �     �     �     �     �     �     �     �     �     �  	   �                9     J     c     p     u     �  6   �     �  
   �     �               .     A     H     P     U  	   Z     d     z  v  �     	     !	     1	     4	     <	     H	     X	     d	     q	     w	     |	     �	     �	     �	     �	  	   �	     �	     �	     
     
     
     
     !
     /
     6
     B
     O
     U
     a
     m
  J  q
  /   �     �     �  1     -   I  ,   w  *   �  "   �     �     �  B   	  5   L  -   �  -   �     �     �  '   �     &  Q   -  1     
   �  &   �  "   �  ,     (   3     \     k     �     �  #   �  4   �  -       5     <  '   Z     �     �     �  !   �     �     �            -   '     U     t  /   �  +   �     �  
             ,     L     P     c  !   v     �  #   �     �     �                 >                 *                           +       '         )         7       >      %             2   ?   (       ,         #   5              9       6           !   /   $       @         <   
               &      1          8   =      ;       3         .      0             4   :   "                           	   -        Add for currency Send Add new Add rate Add to Receive rate Add to Send rate Add to max rate Add to min rate Add to rate All All codes Auto adjust for max rate Auto adjust for min rate Auto adjust rate Automatic change of rate Back to list CURL Check all/Uncheck all Code Cron URL for updating rates of CB and cryptocurrencies Cron function does not exist Crypto 2.0 Currency code Receive Currency code Send Currency name Receive Currency name Send Delete Display Done Edit Edit rate Enable/disable source Example of formulas for parser For creating an exchange rate you can use the following mathematical operations:<br><br> 
		* multiplication<br> 
		/ division<br> 
		- subtraction<br> 
		+ addition<br><br> 
		An example of a formula where two exchange rates are multiplied: [bitfinex_btcusd_last_price] * [cbr_usdrub]<br> 
		For more detailed instructions, follow the <a href="%s" target="_blank">link</a>. Last update time Logging parsing No No item Only errors Parser settings Parser type Parsing date Place Rate Rate automatic adjustment Rate for Receive Rate for Send Rate formula for Receive Rate formula for Send Rate name Rates Rates parser Rates sources SSH Save Settings Sorting rates Source Source name Source rates Title Update rate Update time Yes Project-Id-Version: Crypto
POT-Creation-Date: 2019-08-10 21:03+0300
PO-Revision-Date: 2019-08-10 21:03+0300
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js
X-Poedit-SearchPathExcluded-1: premium
X-Poedit-SearchPathExcluded-2: moduls/geoip/data
 Добавить для валюты Отдаю Добавить Добавить курс Прибавляем к курсу Получаю Прибавляем к курсу Отдаю Прибавляем к макс. курсу Прибавляем к мин. курсу Прибавляем к курсу Все Все коды Автокорректировка курса макс. курса Автокорректировка мин. курса Автокорректировка курса Автокорректировка курса Назад к списку CURL Выбрать все/Снять все Код Cron URL для обновления курсов ЦБ и крипто валют Крон функция не существует Crypto 2.0 Код валюты Получаете Код валюты Отдаете Название валюты Получаю Название валюты Отдаю Удалить Отображать Выполнено Редактировать Редактировать курс Включить/выключить источник Пример формул для парсер Вы можете использовать следующие математические действия при создания курса: <br><br>
		* умножение <br> 
		/ деление<br> 
		- вычитание<br>  
		+ сложение<br><br> 
		Пример формулы, где перемножаются два курса: [bitfinex_btcusd_last_price] * [cbr_usdrub]<br>
		Более подробную инструкцию смотрите по <a href="%s" target="_blank">ссылке</a>. Дата обновления Логирование парсинга Нет Нет значения Только ошибки Настройки парсера Тип парсера Дата парсинга Значение Курс Автокорректировка курса Курс для Получаю Курс для Отдаю Формула курса для Получаю Формула курса для Отдаю Название курса Курсы Парсер курсов Источники курсов SSH Сохранить Настройки Сортировка курсов Источник Название источника Курсы источников Заголовок Обновление курса Дата обновления Да 