<?php
if( !defined( 'ABSPATH')){ exit(); }

if(is_admin()){
	add_action('pn_adminpage_title_pn_settings_new_parser', 'def_adminpage_title_pn_settings_new_parser');
	function def_adminpage_title_pn_settings_new_parser($page){
		_e('Parser settings','crypto');
	} 

	add_action('pn_adminpage_content_pn_settings_new_parser','def_adminpage_content_pn_settings_new_parser');
	function def_adminpage_content_pn_settings_new_parser(){
	global $wpdb, $crypto;
		
		$form = new PremiumForm();
		
		$options = array();
		$options['top_title'] = array(
			'view' => 'h3',
			'title' => __('Parser settings','crypto'),
			'submit' => __('Save','crypto'),
			'colspan' => 2,
		);
		$options['parser'] = array(
			'view' => 'select',
			'title' => __('Parser type','crypto'),
			'options' => array('0'=> __('CURL','crypto'), '1'=> __('SSH','crypto')),
			'default' => $crypto->get_option('newparser','parser'),
			'name' => 'parser',
		);	
		$options['parser_log'] = array(
			'view' => 'select',
			'title' => __('Logging parsing','crypto'),
			'options' => array('0'=> __('No','crypto'), '1'=> __('Yes','crypto'), '2'=> __('Only errors','crypto')),
			'default' => $crypto->get_option('newparser','parser_log'),
			'name' => 'parser_log',
		);
		$date = __('No','crypto');
		$time_parser = get_option('time_new_parser');
		if($time_parser){
			$date = date('d.m.Y H:i', $time_parser);
		}
		$options['time_last'] = array(
			'view' => 'textfield',
			'title' => __('Last update time','crypto'),
			'default' => $date,
		);
		$options['sources'] = array(
			'view' => 'user_func',
			'name' => 'sources',
			'func_data' => '',
			'func' => 'pn_settings_new_parser_sources_options',
			'work' => 'input_array',
		);		
		$params_form = array(
			'filter' => 'pn_settings_new_parser_options',
			'method' => 'ajax',
			'button_title' => __('Save','crypto'),
		);
		$form->init_form($params_form, $options);	
		
	}  
	
	function pn_settings_new_parser_sources_options($bd_data){ 
	global $wpdb;

		$birgs = apply_filters('new_parser_links', array());
		
		$work_birgs = get_option('work_birgs');
		if(!is_array($work_birgs)){ $work_birgs = array(); }
	?>
		<tr class="">
			<th>
				<label><?php _e('Add for currency Send','crypto'); ?></label>
			</th>
			<td>
				<div class="premium_wrap_standart">
					<?php
					$scroll_lists = array();
					if(is_array($birgs)){
						foreach($birgs as $birg_key => $birg_data){
							$checked = 0;
							if(in_array($birg_key, $work_birgs)){
								$checked = 1;
							}
							$scroll_lists[] = array(
								'title' => is_isset($birg_data, 'title'),
								'checked' => $checked,
								'value' => $birg_key,
							);
						}	
					}	
					foreach($scroll_lists as $sc){
					?>
						<div><label><input type="checkbox" name="sources[]" <?php checked(is_isset($sc, 'checked'), 1); ?> value="<?php echo is_isset($sc, 'value'); ?>" /> <?php echo is_isset($sc, 'title'); ?></label></div>
					<?php
					}
					?>						
				</div>
			</td>
		</tr>			
	<?php
	}	

	add_action('premium_action_pn_settings_new_parser','def_premium_action_pn_settings_new_parser');
	function def_premium_action_pn_settings_new_parser(){
	global $wpdb, $crypto;	

		only_post();
		pn_only_caps(array('administrator'));

		$form = new PremiumForm();
		
		$sources = is_param_post('sources');
		$work_birgs = array();
		if(is_array($sources)){
			foreach($sources as $id){
				$id = pn_string($id);
				if($id){
					$work_birgs[] = $id;
				}
			}	
		}
		update_option('work_birgs', $work_birgs);		
		
		$crypto->update_option('newparser', 'parser', intval(is_param_post('parser')));	
		$crypto->update_option('newparser', 'parser_log', intval(is_param_post('parser_log')));

		$url = admin_url('admin.php?page=pn_settings_new_parser&reply=true');
		$form->answer_form($url);
	}
}	