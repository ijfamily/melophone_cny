<?php
if( !defined( 'ABSPATH')){ exit(); }
		
$path = get_extension_file(__FILE__);
$name = get_extension_name($path);

global $crypto;
$crypto->include_patch(__FILE__, 'birg_filters');
$crypto->include_patch(__FILE__, 'list_courses');
$crypto->include_patch(__FILE__, 'list');
$crypto->include_patch(__FILE__, 'add');
$crypto->include_patch(__FILE__, 'sort');
$crypto->include_patch(__FILE__, 'filters');
$crypto->include_patch(__FILE__, 'settings');
$crypto->include_patch(__FILE__, 'cron');