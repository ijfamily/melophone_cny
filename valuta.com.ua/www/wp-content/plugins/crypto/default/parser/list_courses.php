<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_new_parser', 'pn_admin_title_pn_new_parser');
function pn_admin_title_pn_new_parser(){
	_e('Source rates','crypto');
} 

add_action('pn_adminpage_content_pn_new_parser','def_pn_admin_content_pn_new_parser');
function def_pn_admin_content_pn_new_parser(){

	if(class_exists('trev_sourcerates_List_Table')){  
		$Table = new trev_sourcerates_List_Table();
		$Table->prepare_items();
		
		global $birgs_list, $wpdb;
		$search = array();
			
		$lists = array();
		$lists[''] = '--' . __('All','crypto') . '--';
		$birgs = apply_filters('work_parser_links', array());
		foreach($birgs as $birg){
			$lists[is_isset($birg,'birg_key')] = is_isset($birg,'title');
		}
		$birgs_list = $lists;
			
		$search[] = array(
			'view' => 'select',
			'title' => __('Source','crypto'),
			'default' => pn_strip_input(is_param_get('birg')),
			'options' => $lists,
			'name' => 'birg',
		);			
			
		$currency_codes = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."vtypes ORDER BY xname ASC");
		$codes_lists = array();
		$codes_lists[''] = __('All codes','crypto');
		foreach($currency_codes as $vtype){
			$codes_lists[pn_strip_input($vtype->xname)] = pn_strip_input($vtype->xname);
		}
			
		$search[] = array(
			'view' => 'select',
			'title' => __('Currency code Send','crypto'),
			'default' => pn_strip_input(is_param_get('currency_give')),
			'options' => $codes_lists,
			'name' => 'currency_give',
		);	
		$search[] = array(
			'view' => 'select',
			'title' => __('Currency code Receive','crypto'),
			'default' => pn_strip_input(is_param_get('currency_get')),
			'options' => $codes_lists,
			'name' => 'currency_get',
		);			
		
		pn_admin_searchbox($search, 'reply');
		
		pn_admin_submenu(array(), 'reply');
?>
	<form method="post" action="<?php pn_the_link_post(); ?>">
		<?php $Table->display() ?>
	</form>
<?php 
	} else {
		echo 'Class not found';
	}
} 


add_action('premium_action_pn_new_parser','def_premium_action_pn_new_parser');
function def_premium_action_pn_new_parser(){
global $wpdb;	

	only_post();
	pn_only_caps(array('administrator'));

	$reply = '';
	$action = get_admin_action();
			
	if(isset($_POST['save'])){
				
		do_action('pn_parsercourses_save');
		$reply = '&reply=true';

	} else {
				
		if(isset($_POST['id']) and is_array($_POST['id'])){				
				
			do_action('pn_parsercourses_action', $action, $_POST['id']);
			$reply = '&reply=true';					
		} 
				
	}
			
	$url = is_param_post('_wp_http_referer') . $reply;
	$paged = intval(is_param_post('paged'));
	if($paged > 1){ $url .= '&paged='.$paged; }		
	wp_redirect($url);
	exit;			
}  

class trev_sourcerates_List_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;
                
        parent::__construct( array(
            'singular'  => 'id',      
			'ajax' => false,  
        ) );
        
    }
	
    function column_default($item, $column_name){
        
		if($column_name == 'title'){
			global $birgs_list;
			return is_isset($birgs_list, is_isset($item, 'birg')).' ('.  is_isset($item, 'give') .' => '. is_isset($item, 'get') .')';		
		} elseif($column_name == 'place'){
			return is_isset($item, 'title');
		} elseif($column_name == 'rate'){
			return '1 '.  is_isset($item, 'give') .' => '. is_isset($item, 'course') .' '. is_isset($item, 'get');
		} elseif($column_name == 'date'){
			$time = intval(is_isset($item, 'up'));
			if($time){
				return date('d.m.Y H:i:s', $time);
			}
		} elseif($column_name == 'code'){
			return '<input type="text" class="premium_input clpb_item" style="width: 200px; max-width: 100%;" name="" data-clipboard-text="['. is_isset($item, 'code') .']" value="['. is_isset($item, 'code') .']" />';
		}	
		
		return '';
    }	
	
    function get_columns(){
		$columns = array(
			'title'     => __('Title','crypto'),
			'place' => __('Place','crypto'),
			'code' => __('Code','crypto'),
			'rate' => __('Rate','crypto'),
			'date' => __('Parsing date','crypto'),
		);		
        return $columns;
    }	
    
    function prepare_items() {
        global $wpdb; 
		
        $per_page = $this->get_items_per_page('trev_sourcerates_per_page', 50);
        $current_page = $this->get_pagenum();
        
        $this->_column_headers = $this->get_column_info();

		$offset = ($current_page-1)*$per_page;
		
		$parser_pairs = get_parser_pairs(1);
			
		$s_birg = pn_strip_input(is_param_get('birg'));
		$s_give = strtoupper(pn_strip_input(is_param_get('currency_give')));
		$s_get = strtoupper(pn_strip_input(is_param_get('currency_get')));
			
		$items = array();
		foreach($parser_pairs as $pi_key => $pi_value){
			$birg = trim(is_isset($pi_value, 'birg'));
			$give = strtoupper(trim(is_isset($pi_value, 'give')));
			$get = strtoupper(trim(is_isset($pi_value, 'get')));
				
			$return = 1;
				
			if($s_birg and $s_birg != $birg){
				$return = 0;
			}
			if($s_give and $s_give != $give){
				$return = 0;
			}
			if($s_get and $s_get != $get){
				$return = 0;
			}		
				
			if($return == 1){
				$items[$pi_key] = $pi_value;
				$items[$pi_key]['code'] = $pi_key;
			}
		}
		
		$total_items = count($items);
		$data = array_slice($items, $offset, $per_page); 		

        $current_page = $this->get_pagenum();
        $this->items = $data;
		
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  
            'per_page'    => $per_page,                     
            'total_pages' => ceil($total_items/$per_page)  
        ));
    }
}

add_action('premium_screen_pn_new_parser','my_myscreen_pn_new_parser');
function my_myscreen_pn_new_parser() {
    $args = array(
        'label' => __('Display','crypto'),
        'default' => 50,
        'option' => 'trev_sourcerates_per_page'
    );
    add_screen_option('per_page', $args );
	if(class_exists('trev_sourcerates_List_Table')){
		new trev_sourcerates_List_Table;
	}
}