<?php
if( !defined( 'ABSPATH')){ exit(); }

if(!class_exists('Crypto')){
	class Crypto extends Premium {

		function __construct($debug_mode=0)
		{
		
			$this->debug_constant_name = 'CR_RICH_TEST';
			$this->plugin_version = '1.1';
			$this->plugin_name = 'Crypto 2.0';
			$this->plugin_path = CR_PLUGIN_NAME;
			$this->plugin_dir = CR_PLUGIN_DIR;
			$this->plugin_url = CR_PLUGIN_URL;
			$this->plugin_prefix = 'crypto';
			$this->plugin_option_name = 'crypto_options';
			$this->plugin_page_name = 'crypto_pages';
			
			parent::__construct($debug_mode);
			
		}			
		
		public function admin_menu(){ 
			
			add_menu_page(__('Crypto 2.0','crypto'), __('Crypto 2.0','crypto'), 'administrator', 'pn_new_parser', array($this, 'admin_temp'), $this->get_icon_link('parser'));  
			$hook = add_submenu_page("pn_new_parser", __('Source rates','crypto'), __('Source rates','crypto'), 'administrator', "pn_new_parser", array($this, 'admin_temp'));
			add_action( "load-$hook", 'pn_trev_hook' );
			$hook = add_submenu_page("pn_new_parser", __('Rates','crypto'), __('Rates','crypto'), 'administrator', "pn_parser_pairs", array($this, 'admin_temp'));	
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_new_parser", __('Add rate','crypto'), __('Add rate','crypto'), 'administrator', "pn_add_parser_pairs", array($this, 'admin_temp'));
			add_submenu_page("pn_new_parser", __('Sorting rates','crypto'), __('Sorting rates','crypto'), 'administrator', "pn_sort_parser_pairs", array($this, 'admin_temp'));
			add_submenu_page("pn_new_parser", __('Settings','crypto'), __('Settings','crypto'), 'administrator', "pn_settings_new_parser", array($this, 'admin_temp'));							
				
		}
		
	}    
}